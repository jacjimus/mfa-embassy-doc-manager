-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for bremak_manager
CREATE DATABASE IF NOT EXISTS `bremak_manager` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bremak_manager`;


-- Dumping structure for table bremak_manager.delivery
DROP TABLE IF EXISTS `delivery`;
CREATE TABLE IF NOT EXISTS `delivery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_id` int(11) unsigned DEFAULT NULL,
  `notification_id` int(11) unsigned NOT NULL,
  `priority` int(11) unsigned NOT NULL,
  `from_credentials` text,
  `from_credentials_hash` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.delivery: ~0 rows (approximately)
DELETE FROM `delivery`;
/*!40000 ALTER TABLE `delivery` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.delivery_attachment
DROP TABLE IF EXISTS `delivery_attachment`;
CREATE TABLE IF NOT EXISTS `delivery_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `name` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.delivery_attachment: ~0 rows (approximately)
DELETE FROM `delivery_attachment`;
/*!40000 ALTER TABLE `delivery_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_attachment` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.delivery_recipient
DROP TABLE IF EXISTS `delivery_recipient`;
CREATE TABLE IF NOT EXISTS `delivery_recipient` (
  `delivery_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `channel` int(11) unsigned NOT NULL DEFAULT '1',
  `send_time` timestamp NULL DEFAULT NULL,
  `credentials` text,
  `credentials_hash` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`delivery_id`,`credentials_hash`,`channel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.delivery_recipient: ~0 rows (approximately)
DELETE FROM `delivery_recipient`;
/*!40000 ALTER TABLE `delivery_recipient` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_recipient` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.delivery_reject
DROP TABLE IF EXISTS `delivery_reject`;
CREATE TABLE IF NOT EXISTS `delivery_reject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient_address` varchar(255) DEFAULT NULL,
  `channel_type` smallint(5) unsigned NOT NULL,
  `notification_type` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_address_index` (`recipient_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.delivery_reject: ~0 rows (approximately)
DELETE FROM `delivery_reject`;
/*!40000 ALTER TABLE `delivery_reject` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_reject` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.doc
DROP TABLE IF EXISTS `doc`;
CREATE TABLE IF NOT EXISTS `doc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `doc_type_id` int(11) unsigned NOT NULL,
  `name` varchar(128) NOT NULL DEFAULT 'N/A',
  `doc_alias` varchar(128) NOT NULL,
  `doc_file` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `category_id` tinyint(1) DEFAULT NULL,
  `approval` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `doc_type_id` (`doc_type_id`),
  KEY `location_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.doc: ~7 rows (approximately)
DELETE FROM `doc`;
/*!40000 ALTER TABLE `doc` DISABLE KEYS */;
INSERT INTO `doc` (`id`, `doc_type_id`, `name`, `doc_alias`, `doc_file`, `description`, `location_id`, `date_created`, `created_by`, `category_id`, `approval`) VALUES
	(1, 1, 'Child Labour', 'child_labour', '18dcea40a01764f17a44f78ef3f95536.pdf', NULL, NULL, '2014-11-13 14:25:07', 1, 1, NULL),
	(2, 2, 'Visa', '', '61fc4494c7045f5d4a6877ba38caea8a.pdf', NULL, NULL, '2016-04-28 22:40:03', 28, 3, NULL),
	(3, 2, 'Go home template', 'go_home_template', '2d0ae16d7053b626d8e13338f4342e41.pdf', NULL, NULL, '2016-04-28 22:41:48', 28, 3, NULL),
	(4, 1, 'Entry Visa Request', 'entry_visa_request', '901f9751cb4c211a6f1a78bcadf9f0eb.pdf', 'Visa application request by Somalia residents', NULL, '2016-09-18 08:35:57', 28, 4, 0),
	(5, 1, 'Immigration', 'immigration', '0b61cd0d0dc533c2e200655fe1201bb5.pdf', 'Request for clearance of travellers back to Somalia ', NULL, '2016-09-18 08:49:02', 28, 4, 0),
	(6, 1, 'Forwarding Note', 'forwarding_note', 'c216f60e1f7142358ee1dd5529fffa0b.pdf', 'Forwarding note from Somalia Ministry to GOK ', NULL, '2016-09-18 08:50:46', 28, 4, 1),
	(7, 1, 'No Objection Note', 'no_objection_note', '4f8f6836eb39c38299f073543057d6ca.pdf', 'No objection for provision of Entry Visa', NULL, '2016-09-18 08:52:00', 28, 4, 0);
/*!40000 ALTER TABLE `doc` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.doc_categories
DROP TABLE IF EXISTS `doc_categories`;
CREATE TABLE IF NOT EXISTS `doc_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.doc_categories: ~3 rows (approximately)
DELETE FROM `doc_categories`;
/*!40000 ALTER TABLE `doc_categories` DISABLE KEYS */;
INSERT INTO `doc_categories` (`id`, `name`, `description`, `date_created`, `created_by`) VALUES
	(1, 'Requisition forms', 'Requisition forms such as stock requisition, travel requisitions etc', '2014-03-20 23:49:00', 1),
	(3, 'Go Home', 'Applications for go home', '2016-04-28 22:41:03', 28),
	(4, 'Visa Application Docs', NULL, '2016-09-19 00:50:55', 28);
/*!40000 ALTER TABLE `doc_categories` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.doc_fields
DROP TABLE IF EXISTS `doc_fields`;
CREATE TABLE IF NOT EXISTS `doc_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_type_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.doc_fields: ~0 rows (approximately)
DELETE FROM `doc_fields`;
/*!40000 ALTER TABLE `doc_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_fields` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.empbanksview
DROP VIEW IF EXISTS `empbanksview`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `empbanksview` (
	`id` INT(11) NOT NULL,
	`emp_id` INT(11) NOT NULL,
	`account_no` VARCHAR(20) NULL COLLATE 'latin1_swedish_ci',
	`bank_id` INT(11) NULL,
	`bank_branch_id` INT(11) NULL,
	`paying_acc` TINYINT(1) NOT NULL,
	`bank_code` CHAR(15) NULL COLLATE 'latin1_swedish_ci',
	`bank_name` VARCHAR(64) NULL COLLATE 'latin1_swedish_ci',
	`branch_code` VARCHAR(15) NULL COLLATE 'latin1_swedish_ci',
	`branch_name` VARCHAR(64) NULL COLLATE 'latin1_swedish_ci',
	`emp_bank_account` VARCHAR(160) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.employeeslist
DROP VIEW IF EXISTS `employeeslist`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `employeeslist` (
	`paygroup_id` INT(11) NULL,
	`employment_status` ENUM('Active','Terminated','Suspended') NULL COLLATE 'latin1_swedish_ci',
	`id` INT(11) NOT NULL,
	`emp_code` VARCHAR(30) NULL COLLATE 'latin1_swedish_ci',
	`marital_status_id` INT(11) UNSIGNED NULL,
	`empname` VARCHAR(92) NULL COLLATE 'latin1_swedish_ci',
	`first_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`middle_name` VARCHAR(30) NULL COLLATE 'latin1_swedish_ci',
	`last_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`gender` ENUM('Male','Female') NULL COLLATE 'latin1_swedish_ci',
	`birthdate` DATE NULL,
	`name` VARCHAR(128) NULL COMMENT 'Country name' COLLATE 'latin1_swedish_ci',
	`hire_date` DATE NULL,
	`job_title` VARCHAR(145) NOT NULL COMMENT 'These are organization titles' COLLATE 'latin1_swedish_ci',
	`dept_name` VARCHAR(128) NOT NULL COLLATE 'latin1_swedish_ci',
	`work_hours_perday` DOUBLE NULL,
	`employment_class_id` INT(11) NULL COMMENT 'i.e Contractor/Consultant etc etc',
	`empclass` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`categoryname` VARCHAR(255) NULL COMMENT 'Full time or Part time' COLLATE 'latin1_swedish_ci',
	`pay_type_name` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`salary` DECIMAL(18,2) NULL,
	`status` ENUM('Active','Terminated','Suspended') NULL COLLATE 'latin1_swedish_ci',
	`email` VARCHAR(128) NULL COLLATE 'latin1_swedish_ci',
	`mobile` VARCHAR(25) NULL COLLATE 'latin1_swedish_ci',
	`company_phone` VARCHAR(26) NULL COLLATE 'latin1_swedish_ci',
	`company_phone_ext` VARCHAR(10) NULL COLLATE 'latin1_swedish_ci',
	`branch_id` INT(11) NULL,
	`currency_id` INT(11) UNSIGNED NULL
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.employee_housingtype
DROP TABLE IF EXISTS `employee_housingtype`;
CREATE TABLE IF NOT EXISTS `employee_housingtype` (
  `id` int(11) NOT NULL,
  `housing_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.employee_housingtype: ~3 rows (approximately)
DELETE FROM `employee_housingtype`;
/*!40000 ALTER TABLE `employee_housingtype` DISABLE KEYS */;
INSERT INTO `employee_housingtype` (`id`, `housing_type`) VALUES
	(1, 'Housed by Employer'),
	(2, 'Living in Own House'),
	(3, 'Other');
/*!40000 ALTER TABLE `employee_housingtype` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.empminorlist
DROP VIEW IF EXISTS `empminorlist`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `empminorlist` (
	`id` INT(11) NOT NULL,
	`employment_class_id` INT(11) NULL COMMENT 'i.e Contractor/Consultant etc etc',
	`employment_status` ENUM('Active','Terminated','Suspended') NULL COLLATE 'latin1_swedish_ci',
	`email` VARCHAR(128) NULL COLLATE 'latin1_swedish_ci',
	`mobile` VARCHAR(25) NULL COLLATE 'latin1_swedish_ci',
	`first_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`middle_name` VARCHAR(30) NULL COLLATE 'latin1_swedish_ci',
	`last_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`name` VARCHAR(61) NOT NULL COLLATE 'latin1_swedish_ci',
	`gender` ENUM('Male','Female') NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.event
DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_type_id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(128) DEFAULT NULL,
  `initial_start_date` date NOT NULL,
  `initial_end_date` date DEFAULT NULL,
  `repeated` enum('None','Daily','Weekly','Monthly','Yearly') NOT NULL DEFAULT 'Monthly',
  `user_id` int(11) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `location_id` int(11) unsigned DEFAULT NULL,
  `color_class` varchar(60) DEFAULT 'bg-color-darken txt-color-white',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `location_id` (`location_id`),
  KEY `event_type_id_2` (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.event: ~2 rows (approximately)
DELETE FROM `event`;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` (`id`, `event_type_id`, `name`, `description`, `initial_start_date`, `initial_end_date`, `repeated`, `user_id`, `is_active`, `location_id`, `color_class`, `date_created`, `created_by`) VALUES
	(4, 'utility_payments_reminder', 'Electricity bill', 'Electricity bill reminder', '2014-04-30', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-red txt-color-white', '2014-04-29 00:55:39', 1),
	(5, 'utility_payments_reminder', 'Water Bill', 'Payment of water bill', '2014-04-29', NULL, 'Monthly', NULL, 1, NULL, 'bg-color-blue txt-color-white', '2014-04-29 14:47:57', 1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.event_occurrence
DROP TABLE IF EXISTS `event_occurrence`;
CREATE TABLE IF NOT EXISTS `event_occurrence` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) unsigned NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date DEFAULT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.event_occurrence: ~14 rows (approximately)
DELETE FROM `event_occurrence`;
/*!40000 ALTER TABLE `event_occurrence` DISABLE KEYS */;
INSERT INTO `event_occurrence` (`id`, `event_id`, `date_from`, `date_to`, `notified`, `date_created`) VALUES
	(1, 4, '2014-05-06', NULL, 1, '2014-05-06 17:26:22'),
	(2, 5, '2014-05-06', NULL, 1, '2014-05-06 17:26:24'),
	(3, 6, '2014-05-06', NULL, 1, '2014-05-06 17:26:27'),
	(7, 4, '2014-05-07', NULL, 1, '2014-05-06 17:28:07'),
	(8, 4, '2014-08-05', NULL, 0, '2014-08-21 15:39:25'),
	(9, 5, '2014-10-08', NULL, 0, '2014-10-08 19:12:22'),
	(10, 4, '2014-10-08', NULL, 0, '2014-10-08 19:12:32'),
	(11, 7, '2015-01-23', NULL, 0, '2015-01-21 13:41:45'),
	(12, 8, '2015-01-05', NULL, 0, '2015-01-21 13:46:46'),
	(13, 9, '2015-02-05', NULL, 0, '2015-01-23 12:10:46'),
	(14, 10, '2015-01-23', NULL, 0, '2015-01-23 12:12:29'),
	(15, 11, '2015-01-29', NULL, 0, '2015-01-27 08:48:34'),
	(16, 5, '2016-04-29', NULL, 0, '2016-04-28 23:48:12'),
	(17, 4, '2016-04-29', NULL, 0, '2016-04-28 23:48:17');
/*!40000 ALTER TABLE `event_occurrence` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.event_type
DROP TABLE IF EXISTS `event_type`;
CREATE TABLE IF NOT EXISTS `event_type` (
  `id` varchar(30) NOT NULL,
  `name` varchar(128) NOT NULL,
  `notif_type_id` varchar(60) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_one_day_event` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.event_type: ~2 rows (approximately)
DELETE FROM `event_type`;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` (`id`, `name`, `notif_type_id`, `date_created`, `is_one_day_event`, `created_by`) VALUES
	('Activity', 'Planned Activity', 'events_reminder', '2014-09-16 15:15:40', 0, 1),
	('utility_payments_reminder', 'Utility Payments Reminder', 'events_reminder', '2014-04-27 23:55:48', 1, 1);
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.help_category
DROP TABLE IF EXISTS `help_category`;
CREATE TABLE IF NOT EXISTS `help_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.help_category: ~0 rows (approximately)
DELETE FROM `help_category`;
/*!40000 ALTER TABLE `help_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `help_category` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.help_content
DROP TABLE IF EXISTS `help_content`;
CREATE TABLE IF NOT EXISTS `help_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.help_content: ~0 rows (approximately)
DELETE FROM `help_content`;
/*!40000 ALTER TABLE `help_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `help_content` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_banks
DROP TABLE IF EXISTS `hr_banks`;
CREATE TABLE IF NOT EXISTS `hr_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_code` char(15) DEFAULT NULL,
  `bank_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_banks: ~3 rows (approximately)
DELETE FROM `hr_banks`;
/*!40000 ALTER TABLE `hr_banks` DISABLE KEYS */;
INSERT INTO `hr_banks` (`id`, `bank_code`, `bank_name`) VALUES
	(1, '03', 'Barclays Bank of Kenya'),
	(2, '08', 'ABC Bank'),
	(3, '01', 'K.C.B');
/*!40000 ALTER TABLE `hr_banks` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_bank_branches
DROP TABLE IF EXISTS `hr_bank_branches`;
CREATE TABLE IF NOT EXISTS `hr_bank_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `branch_code` varchar(15) DEFAULT NULL,
  `branch_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bankslink` (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_bank_branches: ~2 rows (approximately)
DELETE FROM `hr_bank_branches`;
/*!40000 ALTER TABLE `hr_bank_branches` DISABLE KEYS */;
INSERT INTO `hr_bank_branches` (`id`, `bank_id`, `branch_code`, `branch_name`) VALUES
	(1, 1, '016', 'Moi Av. Mombasa Premier Life Branch'),
	(3, 1, '057', 'Githunguri Branch');
/*!40000 ALTER TABLE `hr_bank_branches` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_corporations
DROP TABLE IF EXISTS `hr_corporations`;
CREATE TABLE IF NOT EXISTS `hr_corporations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `desc` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_corporations: ~0 rows (approximately)
DELETE FROM `hr_corporations`;
/*!40000 ALTER TABLE `hr_corporations` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_corporations` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_dependants
DROP TABLE IF EXISTS `hr_dependants`;
CREATE TABLE IF NOT EXISTS `hr_dependants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `name` varchar(145) NOT NULL,
  `dob` date NOT NULL,
  `relationship_id` int(11) NOT NULL COMMENT 'Relations e.g Wife,Husband,Son',
  `email` varchar(85) DEFAULT NULL,
  `mobile` varchar(65) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_hr_empnextofkin_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_empnextofkin_hr_relations1_idx` (`relationship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_dependants: ~0 rows (approximately)
DELETE FROM `hr_dependants`;
/*!40000 ALTER TABLE `hr_dependants` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_dependants` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_empcontacts
DROP TABLE IF EXISTS `hr_empcontacts`;
CREATE TABLE IF NOT EXISTS `hr_empcontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_empcontacts_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_empcontacts_hr_phone_categories1_idx` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_empcontacts: ~0 rows (approximately)
DELETE FROM `hr_empcontacts`;
/*!40000 ALTER TABLE `hr_empcontacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_empcontacts` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_emphousing
DROP TABLE IF EXISTS `hr_emphousing`;
CREATE TABLE IF NOT EXISTS `hr_emphousing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `housing_type` int(11) DEFAULT NULL,
  `housing_value` decimal(18,2) DEFAULT NULL,
  `employer_rent` decimal(18,2) DEFAULT NULL,
  `allowable_occupier` decimal(18,2) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `emphousingtype` (`housing_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_emphousing: ~0 rows (approximately)
DELETE FROM `hr_emphousing`;
/*!40000 ALTER TABLE `hr_emphousing` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_emphousing` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_employees
DROP TABLE IF EXISTS `hr_employees`;
CREATE TABLE IF NOT EXISTS `hr_employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(30) DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `job_title_id` int(11) DEFAULT NULL,
  `corporation_id` int(11) NOT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `work_hours_perday` double DEFAULT NULL,
  `employment_class_id` int(11) DEFAULT NULL COMMENT 'i.e Contractor/Consultant etc etc',
  `employment_cat_id` int(1) DEFAULT NULL COMMENT 'Fulltime,Parttime',
  `pay_type_id` int(11) DEFAULT NULL,
  `currency_id` int(11) unsigned DEFAULT NULL,
  `salary` decimal(18,2) DEFAULT NULL,
  `employment_status` enum('Active','Terminated','Suspended') DEFAULT 'Active',
  `email` varchar(128) DEFAULT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `company_phone` varchar(26) DEFAULT NULL,
  `company_phone_ext` varchar(10) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `paygroup_id` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `company_name` varchar(100) DEFAULT NULL,
  `email_2` varchar(128) DEFAULT NULL,
  `postal_address` varchar(128) DEFAULT NULL,
  `phy_address` varchar(128) DEFAULT NULL,
  `phy_address2` varchar(128) DEFAULT NULL,
  `division` varchar(128) DEFAULT NULL,
  `supervises` varchar(128) DEFAULT NULL,
  `accountabilities` text,
  `qualification` varchar(128) DEFAULT NULL,
  `qualification_desc` text,
  `title` varchar(128) DEFAULT NULL,
  `institution` varchar(128) DEFAULT NULL,
  `fullname` varchar(228) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_currency` (`currency_id`),
  KEY `job_departments` (`department_id`),
  KEY `job_paytypes` (`pay_type_id`),
  KEY `job_empclass` (`employment_class_id`),
  KEY `job_jobtitle` (`job_title_id`),
  KEY `job_manager` (`manager_id`),
  KEY `employmentcategory` (`employment_cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.hr_employees: ~0 rows (approximately)
DELETE FROM `hr_employees`;
/*!40000 ALTER TABLE `hr_employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_employees` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_employee_identification
DROP TABLE IF EXISTS `hr_employee_identification`;
CREATE TABLE IF NOT EXISTS `hr_employee_identification` (
  `id` int(11) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `national_id` varchar(25) DEFAULT NULL,
  `pin` varchar(64) DEFAULT NULL,
  `nhif` varchar(64) DEFAULT NULL,
  `nssf` varchar(63) DEFAULT NULL,
  `driving_license` varchar(63) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_employee_identification: ~0 rows (approximately)
DELETE FROM `hr_employee_identification`;
/*!40000 ALTER TABLE `hr_employee_identification` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_employee_identification` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_employement_categories
DROP TABLE IF EXISTS `hr_employement_categories`;
CREATE TABLE IF NOT EXISTS `hr_employement_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(255) NOT NULL COMMENT 'Full time or Part time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_employement_categories: ~2 rows (approximately)
DELETE FROM `hr_employement_categories`;
/*!40000 ALTER TABLE `hr_employement_categories` DISABLE KEYS */;
INSERT INTO `hr_employement_categories` (`id`, `categoryname`) VALUES
	(1, 'Full Time'),
	(2, 'Part Time');
/*!40000 ALTER TABLE `hr_employement_categories` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_employmentclass
DROP TABLE IF EXISTS `hr_employmentclass`;
CREATE TABLE IF NOT EXISTS `hr_employmentclass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empclass` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  `modified_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_employmentclass: ~3 rows (approximately)
DELETE FROM `hr_employmentclass`;
/*!40000 ALTER TABLE `hr_employmentclass` DISABLE KEYS */;
INSERT INTO `hr_employmentclass` (`id`, `empclass`, `date_created`, `created_by`, `modified_by`, `last_modified`) VALUES
	(1, 'Consultant', '2014-07-28 14:03:54', 0, 0, '0000-00-00 00:00:00'),
	(2, 'Contractor', '2014-07-28 14:03:54', 0, 0, '0000-00-00 00:00:00'),
	(3, 'Permanent', '2014-07-28 14:03:54', 0, 0, '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `hr_employmentclass` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_empskills
DROP TABLE IF EXISTS `hr_empskills`;
CREATE TABLE IF NOT EXISTS `hr_empskills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `proficiency_id` int(11) NOT NULL,
  `notes` varchar(100) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `certification` varchar(50) DEFAULT NULL,
  `certifying_body` varchar(50) DEFAULT NULL,
  `country_of_inst` int(11) DEFAULT NULL,
  `competencies` text,
  `comp_used` text,
  `comp_not_used` text,
  PRIMARY KEY (`id`),
  KEY `fk_hr_empskills_hr_proficiency1_idx` (`proficiency_id`),
  KEY `fk_hr_empskills_hr_skills1_idx` (`skill_id`),
  KEY `fk_hr_empskills_hr_employees1_idx` (`emp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_empskills: ~1 rows (approximately)
DELETE FROM `hr_empskills`;
/*!40000 ALTER TABLE `hr_empskills` DISABLE KEYS */;
INSERT INTO `hr_empskills` (`id`, `skill_id`, `emp_id`, `start_date`, `end_date`, `proficiency_id`, `notes`, `date_created`, `created_by`, `certification`, `certifying_body`, `country_of_inst`, `competencies`, `comp_used`, `comp_not_used`) VALUES
	(1, 1, 1, '2016-02-01', '2016-03-11', 1, NULL, '2016-03-11 08:35:20', 1, 'CCNA2', 'CISCO', 1, '-managememnt\r\n-time keeping', 'management', 'time keeping');
/*!40000 ALTER TABLE `hr_empskills` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_emp_banks
DROP TABLE IF EXISTS `hr_emp_banks`;
CREATE TABLE IF NOT EXISTS `hr_emp_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `account_no` varchar(20) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_branch_id` int(11) DEFAULT NULL,
  `paying_acc` tinyint(1) NOT NULL DEFAULT '0',
  `account_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `banks` (`bank_id`),
  KEY `bankbranches` (`bank_branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_emp_banks: ~3 rows (approximately)
DELETE FROM `hr_emp_banks`;
/*!40000 ALTER TABLE `hr_emp_banks` DISABLE KEYS */;
INSERT INTO `hr_emp_banks` (`id`, `emp_id`, `account_no`, `bank_id`, `bank_branch_id`, `paying_acc`, `account_name`) VALUES
	(1, 26, '012457896235', 1, 1, 1, ''),
	(2, 27, '23423423423', 1, 3, 1, ''),
	(3, 1, '020-128596251555', 1, 1, 1, 'JOHN MEEKS');
/*!40000 ALTER TABLE `hr_emp_banks` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_emp_courses
DROP TABLE IF EXISTS `hr_emp_courses`;
CREATE TABLE IF NOT EXISTS `hr_emp_courses` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `date_attended` date DEFAULT NULL,
  `course_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_emp_courses_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_emp_courses_hr_courses1_idx` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_emp_courses: ~0 rows (approximately)
DELETE FROM `hr_emp_courses`;
/*!40000 ALTER TABLE `hr_emp_courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_emp_courses` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_emp_qualifications
DROP TABLE IF EXISTS `hr_emp_qualifications`;
CREATE TABLE IF NOT EXISTS `hr_emp_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `specialization` varchar(64) NOT NULL COMMENT 'Institutions  i.e Universities and tertiary colleges',
  `education_id` int(11) NOT NULL COMMENT 'Qualification level  which depends on qualification type. Qualification are like 1st class,A,B\\n',
  `grade` varchar(245) DEFAULT NULL COMMENT 'Notes',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `certificate` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_emp_qualifications_hr_qualificationlevels1_idx` (`education_id`),
  KEY `fk_hr_emp_qualifications_hr_employees1_idx` (`emp_id`),
  KEY `fk_hr_emp_qualifications_hr_institutions1_idx` (`specialization`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.hr_emp_qualifications: ~1 rows (approximately)
DELETE FROM `hr_emp_qualifications`;
/*!40000 ALTER TABLE `hr_emp_qualifications` DISABLE KEYS */;
INSERT INTO `hr_emp_qualifications` (`id`, `emp_id`, `specialization`, `education_id`, `grade`, `start_date`, `end_date`, `institution`, `certificate`, `date_created`, `created_by`, `country_id`) VALUES
	(1, 1, 'Bachelor of Science in Compute Science', 1, 'Upper 2nd Class honors', '2009-10-11', '2013-05-17', 'University of Nairobi', NULL, '2016-03-10 23:58:18', 1, 1);
/*!40000 ALTER TABLE `hr_emp_qualifications` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_emp_trainings
DROP TABLE IF EXISTS `hr_emp_trainings`;
CREATE TABLE IF NOT EXISTS `hr_emp_trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `institution` varchar(128) NOT NULL,
  `training` varchar(128) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `notes` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_emp_trainings: ~0 rows (approximately)
DELETE FROM `hr_emp_trainings`;
/*!40000 ALTER TABLE `hr_emp_trainings` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_emp_trainings` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_holidays
DROP TABLE IF EXISTS `hr_holidays`;
CREATE TABLE IF NOT EXISTS `hr_holidays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `holiday_name` varchar(150) NOT NULL,
  `hol_day` int(11) NOT NULL,
  `hol_month` int(11) NOT NULL,
  `hol_year` int(11) DEFAULT NULL,
  `recurring` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_holidays: ~7 rows (approximately)
DELETE FROM `hr_holidays`;
/*!40000 ALTER TABLE `hr_holidays` DISABLE KEYS */;
INSERT INTO `hr_holidays` (`id`, `holiday_name`, `hol_day`, `hol_month`, `hol_year`, `recurring`, `approved`, `date_created`, `created_by`) VALUES
	(1, 'New Year\'s Day', 1, 1, NULL, 1, 0, '2013-02-01 09:20:11', 1),
	(2, 'Labor Day', 1, 5, NULL, 1, 0, '2013-02-01 09:19:59', 1),
	(3, 'Madaraka Day', 1, 6, NULL, 1, 0, '2013-02-01 09:20:27', 1),
	(4, 'Mashujaa (Heroes) Day', 20, 10, NULL, 1, 0, '2013-02-01 09:21:32', 1),
	(5, 'Jamhuri (Republic/Independence) Day', 12, 12, NULL, 1, 0, '2013-02-01 09:21:31', 1),
	(6, 'Christmas Day', 25, 12, NULL, 1, 0, '2013-02-01 09:21:54', 1),
	(7, 'Boxing Day', 26, 12, NULL, 1, 0, '2013-02-01 09:22:16', 1);
/*!40000 ALTER TABLE `hr_holidays` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_institutions
DROP TABLE IF EXISTS `hr_institutions`;
CREATE TABLE IF NOT EXISTS `hr_institutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_name` varchar(145) DEFAULT NULL,
  `p_contact_person` varchar(145) DEFAULT NULL,
  `p_email` varchar(120) DEFAULT NULL,
  `p_address` text,
  `p_phone` varchar(45) DEFAULT NULL,
  `p_mobile` varchar(45) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_institutions: ~0 rows (approximately)
DELETE FROM `hr_institutions`;
/*!40000 ALTER TABLE `hr_institutions` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_institutions` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_institution_types
DROP TABLE IF EXISTS `hr_institution_types`;
CREATE TABLE IF NOT EXISTS `hr_institution_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_institution_types: ~0 rows (approximately)
DELETE FROM `hr_institution_types`;
/*!40000 ALTER TABLE `hr_institution_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_institution_types` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_jobhistory
DROP TABLE IF EXISTS `hr_jobhistory`;
CREATE TABLE IF NOT EXISTS `hr_jobhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  `start_date` date NOT NULL COMMENT 'This is used for managing staff changes history',
  `end_date` date NOT NULL,
  `notes` tinytext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `institution` varchar(50) DEFAULT NULL,
  `location_of_inst` varchar(50) DEFAULT NULL,
  `accountabilities` varchar(50) DEFAULT NULL,
  `reports_to` varchar(50) DEFAULT NULL,
  `supervises` varchar(52) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hr_jobhistory_hr_deparments1_idx` (`department_id`),
  KEY `fk_hr_jobhistory_hr_jobs1_idx` (`job_id`),
  KEY `FK_hr_jobhistory` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_jobhistory: ~0 rows (approximately)
DELETE FROM `hr_jobhistory`;
/*!40000 ALTER TABLE `hr_jobhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_jobhistory` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_jobs_titles
DROP TABLE IF EXISTS `hr_jobs_titles`;
CREATE TABLE IF NOT EXISTS `hr_jobs_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(145) NOT NULL COMMENT 'These are organization titles',
  `min_salary` decimal(10,0) NOT NULL,
  `max_salary` decimal(10,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_jobs_titles: ~16 rows (approximately)
DELETE FROM `hr_jobs_titles`;
/*!40000 ALTER TABLE `hr_jobs_titles` DISABLE KEYS */;
INSERT INTO `hr_jobs_titles` (`id`, `job_title`, `min_salary`, `max_salary`, `date_created`, `created_by`) VALUES
	(1, 'Executive Director', 45000, 50000, '2014-09-03 16:24:36', 1),
	(2, 'Program Manager', 103000, 180000, '2014-09-03 16:24:44', 1),
	(3, 'Finance Manager', 103000, 180000, '2014-09-03 16:25:26', 1),
	(4, 'HR/Procurement Manager', 103000, 180000, '2014-09-03 16:30:20', 1),
	(5, 'Senior program Officer', 103000, 180000, '2014-09-03 16:30:21', 1),
	(6, 'Program Officer', 103000, 180000, '2014-09-03 16:30:21', 1),
	(7, 'Program Assistant', 103000, 180000, '2014-09-03 16:30:22', 1),
	(8, 'Program Accountant', 103000, 180000, '2014-09-03 16:30:23', 1),
	(9, 'Admin Assistant', 103000, 180000, '2014-09-03 16:30:24', 1),
	(10, 'Accounts Assistant', 103000, 180000, '2014-09-03 16:30:25', 1),
	(11, 'Admin Assistant-Wezesha', 103000, 180000, '2014-09-03 16:30:26', 1),
	(12, 'Program Accountant-Wezesha', 103000, 180000, '2014-09-03 16:30:27', 1),
	(13, 'Data Officer', 103000, 180000, '2014-09-03 16:30:27', 1),
	(14, 'Psychosocial lifeskills Officer', 103000, 180000, '2014-09-03 16:30:28', 1),
	(15, 'M & E manager', 103000, 180000, '2014-09-03 16:30:29', 1),
	(16, 'Data Assistant', 103000, 180000, '2014-09-03 16:30:31', 1);
/*!40000 ALTER TABLE `hr_jobs_titles` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_paytypes
DROP TABLE IF EXISTS `hr_paytypes`;
CREATE TABLE IF NOT EXISTS `hr_paytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_paytypes: ~0 rows (approximately)
DELETE FROM `hr_paytypes`;
/*!40000 ALTER TABLE `hr_paytypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_paytypes` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_phone_categories
DROP TABLE IF EXISTS `hr_phone_categories`;
CREATE TABLE IF NOT EXISTS `hr_phone_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone_cat_desc` varchar(45) NOT NULL COMMENT 'This table is for contact phone cateoty. It can either be home or Personal Mobile, Work etc',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_phone_categories: ~3 rows (approximately)
DELETE FROM `hr_phone_categories`;
/*!40000 ALTER TABLE `hr_phone_categories` DISABLE KEYS */;
INSERT INTO `hr_phone_categories` (`id`, `phone_cat_desc`, `date_created`, `created_by`) VALUES
	(1, 'Home', '2013-01-18 07:35:38', 1),
	(2, 'Mobile', '2013-01-18 07:35:44', 1),
	(3, 'Work', '2013-01-18 07:35:52', 1);
/*!40000 ALTER TABLE `hr_phone_categories` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_proficiency
DROP TABLE IF EXISTS `hr_proficiency`;
CREATE TABLE IF NOT EXISTS `hr_proficiency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prof_name` varchar(145) DEFAULT NULL COMMENT 'These are the grading for the skill usage',
  `yearsfrom` double DEFAULT NULL,
  `yearsto` double DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_proficiency: ~1 rows (approximately)
DELETE FROM `hr_proficiency`;
/*!40000 ALTER TABLE `hr_proficiency` DISABLE KEYS */;
INSERT INTO `hr_proficiency` (`id`, `prof_name`, `yearsfrom`, `yearsto`, `notes`, `date_created`, `created_by`) VALUES
	(1, 'Good', 1, 2, NULL, '2013-01-27 14:24:21', 1);
/*!40000 ALTER TABLE `hr_proficiency` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_projects
DROP TABLE IF EXISTS `hr_projects`;
CREATE TABLE IF NOT EXISTS `hr_projects` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `client_name` varchar(200) NOT NULL,
  `manager_id` int(20) NOT NULL,
  `created_by` int(20) NOT NULL,
  `date_created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_projects: ~0 rows (approximately)
DELETE FROM `hr_projects`;
/*!40000 ALTER TABLE `hr_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_projects` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_public_holidays
DROP TABLE IF EXISTS `hr_public_holidays`;
CREATE TABLE IF NOT EXISTS `hr_public_holidays` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `holiday_name` varchar(100) NOT NULL,
  `approved` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `created_by` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_public_holidays: ~2 rows (approximately)
DELETE FROM `hr_public_holidays`;
/*!40000 ALTER TABLE `hr_public_holidays` DISABLE KEYS */;
INSERT INTO `hr_public_holidays` (`id`, `date`, `holiday_name`, `approved`, `date_created`, `created_by`) VALUES
	(4, '2014-05-01', 'Labor day', 1, '2014-03-08', 1),
	(5, '2013-12-25', 'Christmas Day', 0, '2014-03-08', 1);
/*!40000 ALTER TABLE `hr_public_holidays` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_qualificationlevels
DROP TABLE IF EXISTS `hr_qualificationlevels`;
CREATE TABLE IF NOT EXISTS `hr_qualificationlevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_qualificationlevels: ~6 rows (approximately)
DELETE FROM `hr_qualificationlevels`;
/*!40000 ALTER TABLE `hr_qualificationlevels` DISABLE KEYS */;
INSERT INTO `hr_qualificationlevels` (`id`, `level_name`, `enabled`, `date_created`, `created_by`) VALUES
	(1, 'Bachelors Degree', 0, '2013-01-18 11:56:22', 1),
	(2, 'Masters Degree', 0, '2013-01-18 11:56:29', 1),
	(3, 'Diploma', 0, '2016-03-10 23:45:07', 1),
	(4, 'High School', 0, '2016-03-10 23:45:37', 1),
	(5, 'Primary School', 0, '2016-03-10 23:45:50', 1),
	(6, 'Postgradute ', 0, '2016-03-10 23:46:43', 1);
/*!40000 ALTER TABLE `hr_qualificationlevels` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_agency
DROP TABLE IF EXISTS `hr_rec_agency`;
CREATE TABLE IF NOT EXISTS `hr_rec_agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `website` varchar(155) DEFAULT NULL,
  `cost` decimal(19,0) NOT NULL,
  `contact_person` varchar(100) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `address` tinytext NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_agency: ~0 rows (approximately)
DELETE FROM `hr_rec_agency`;
/*!40000 ALTER TABLE `hr_rec_agency` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_agency` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicants
DROP TABLE IF EXISTS `hr_rec_applicants`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `title_id` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicants: ~0 rows (approximately)
DELETE FROM `hr_rec_applicants`;
/*!40000 ALTER TABLE `hr_rec_applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicants` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicants_contacts
DROP TABLE IF EXISTS `hr_rec_applicants_contacts`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicants_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `alt_contactno` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `address` tinytext,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicants_contacts: ~0 rows (approximately)
DELETE FROM `hr_rec_applicants_contacts`;
/*!40000 ALTER TABLE `hr_rec_applicants_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicants_contacts` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicants_selected
DROP TABLE IF EXISTS `hr_rec_applicants_selected`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicants_selected` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `comments` text,
  `invited` tinyint(4) DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicants_selected: ~0 rows (approximately)
DELETE FROM `hr_rec_applicants_selected`;
/*!40000 ALTER TABLE `hr_rec_applicants_selected` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicants_selected` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicant_academic_qualif
DROP TABLE IF EXISTS `hr_rec_applicant_academic_qualif`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicant_academic_qualif` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `qualification_id` int(11) NOT NULL,
  `institution` varchar(100) NOT NULL,
  `specialization` varchar(155) NOT NULL,
  `year` year(4) NOT NULL,
  `document` varchar(155) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicant_academic_qualif: ~0 rows (approximately)
DELETE FROM `hr_rec_applicant_academic_qualif`;
/*!40000 ALTER TABLE `hr_rec_applicant_academic_qualif` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicant_academic_qualif` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicant_shortlis_offer
DROP TABLE IF EXISTS `hr_rec_applicant_shortlis_offer`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicant_shortlis_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interview_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `offered` tinyint(4) DEFAULT NULL,
  `offer_date` date DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicant_shortlis_offer: ~0 rows (approximately)
DELETE FROM `hr_rec_applicant_shortlis_offer`;
/*!40000 ALTER TABLE `hr_rec_applicant_shortlis_offer` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicant_shortlis_offer` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_applicant_work_exp
DROP TABLE IF EXISTS `hr_rec_applicant_work_exp`;
CREATE TABLE IF NOT EXISTS `hr_rec_applicant_work_exp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_id` int(11) NOT NULL,
  `current_org` varchar(100) NOT NULL,
  `designation` varchar(128) DEFAULT NULL,
  `industry_type` varchar(128) DEFAULT NULL,
  `start_date` date NOT NULL COMMENT 'This is used for managing staff changes history',
  `end_date` date NOT NULL,
  `functional_expertise` text,
  `notes` tinytext,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_applicant_work_exp: ~0 rows (approximately)
DELETE FROM `hr_rec_applicant_work_exp`;
/*!40000 ALTER TABLE `hr_rec_applicant_work_exp` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_applicant_work_exp` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_interview_header
DROP TABLE IF EXISTS `hr_rec_interview_header`;
CREATE TABLE IF NOT EXISTS `hr_rec_interview_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `interview_date` date NOT NULL,
  `venue` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_interview_header: ~0 rows (approximately)
DELETE FROM `hr_rec_interview_header`;
/*!40000 ALTER TABLE `hr_rec_interview_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_interview_header` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_interview_interviewers
DROP TABLE IF EXISTS `hr_rec_interview_interviewers`;
CREATE TABLE IF NOT EXISTS `hr_rec_interview_interviewers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interview_id` int(11) NOT NULL,
  `interviewer_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_interview_interviewers: ~0 rows (approximately)
DELETE FROM `hr_rec_interview_interviewers`;
/*!40000 ALTER TABLE `hr_rec_interview_interviewers` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_interview_interviewers` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_jobpositions
DROP TABLE IF EXISTS `hr_rec_jobpositions`;
CREATE TABLE IF NOT EXISTS `hr_rec_jobpositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title_id` int(11) NOT NULL,
  `position_details` tinytext,
  `vacancies_number` decimal(19,0) NOT NULL,
  `department_id` int(11) NOT NULL,
  `jobtype_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `approved` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(11) NOT NULL DEFAULT '0',
  `submit` tinyint(4) NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `shift_id` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `travel_required` tinyint(4) NOT NULL DEFAULT '0',
  `relocation_provided` tinyint(4) NOT NULL DEFAULT '0',
  `experience_years` int(11) DEFAULT NULL,
  `experience_months` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_jobpositions: ~0 rows (approximately)
DELETE FROM `hr_rec_jobpositions`;
/*!40000 ALTER TABLE `hr_rec_jobpositions` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_jobpositions` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_jobqualifications
DROP TABLE IF EXISTS `hr_rec_jobqualifications`;
CREATE TABLE IF NOT EXISTS `hr_rec_jobqualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `qualification_name` varchar(213) DEFAULT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_jobqualifications: ~0 rows (approximately)
DELETE FROM `hr_rec_jobqualifications`;
/*!40000 ALTER TABLE `hr_rec_jobqualifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_jobqualifications` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_jobresponsibilities
DROP TABLE IF EXISTS `hr_rec_jobresponsibilities`;
CREATE TABLE IF NOT EXISTS `hr_rec_jobresponsibilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `responsibility_name` varchar(244) DEFAULT NULL,
  `description` text,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_jobresponsibilities: ~0 rows (approximately)
DELETE FROM `hr_rec_jobresponsibilities`;
/*!40000 ALTER TABLE `hr_rec_jobresponsibilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_jobresponsibilities` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_jobskills
DROP TABLE IF EXISTS `hr_rec_jobskills`;
CREATE TABLE IF NOT EXISTS `hr_rec_jobskills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `skill_id` int(128) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_jobskills: ~0 rows (approximately)
DELETE FROM `hr_rec_jobskills`;
/*!40000 ALTER TABLE `hr_rec_jobskills` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_rec_jobskills` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_rec_jobtype
DROP TABLE IF EXISTS `hr_rec_jobtype`;
CREATE TABLE IF NOT EXISTS `hr_rec_jobtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_rec_jobtype: ~3 rows (approximately)
DELETE FROM `hr_rec_jobtype`;
/*!40000 ALTER TABLE `hr_rec_jobtype` DISABLE KEYS */;
INSERT INTO `hr_rec_jobtype` (`id`, `name`, `date_created`, `last_modified`, `created_by`) VALUES
	(1, 'Internal', '2014-06-11 21:01:20', '0000-00-00 00:00:00', 1),
	(2, 'External', '2014-06-11 21:01:31', '0000-00-00 00:00:00', 1),
	(3, 'Both', '2014-06-11 21:01:39', '0000-00-00 00:00:00', 1);
/*!40000 ALTER TABLE `hr_rec_jobtype` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_relations
DROP TABLE IF EXISTS `hr_relations`;
CREATE TABLE IF NOT EXISTS `hr_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rel_name` varchar(145) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_relations: ~5 rows (approximately)
DELETE FROM `hr_relations`;
/*!40000 ALTER TABLE `hr_relations` DISABLE KEYS */;
INSERT INTO `hr_relations` (`id`, `rel_name`) VALUES
	(1, 'Son'),
	(2, 'Daughter'),
	(3, 'Spouse'),
	(4, 'Mother'),
	(5, 'Father');
/*!40000 ALTER TABLE `hr_relations` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_skills
DROP TABLE IF EXISTS `hr_skills`;
CREATE TABLE IF NOT EXISTS `hr_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(120) NOT NULL,
  `description` varchar(120) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_skills: ~1 rows (approximately)
DELETE FROM `hr_skills`;
/*!40000 ALTER TABLE `hr_skills` DISABLE KEYS */;
INSERT INTO `hr_skills` (`id`, `skill`, `description`, `enabled`, `created_by`, `date_created`) VALUES
	(1, 'Human Resource skills', 'HR Skills', 1, 1, '2016-02-26 21:20:56');
/*!40000 ALTER TABLE `hr_skills` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_title
DROP TABLE IF EXISTS `hr_title`;
CREATE TABLE IF NOT EXISTS `hr_title` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(45) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`t_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_title: ~3 rows (approximately)
DELETE FROM `hr_title`;
/*!40000 ALTER TABLE `hr_title` DISABLE KEYS */;
INSERT INTO `hr_title` (`t_id`, `t_name`, `created_by`, `date_created`) VALUES
	(1, 'Mr', 1, '2013-01-02 22:46:02'),
	(2, 'Mrs', 1, '2013-01-02 22:46:09'),
	(3, 'Miss', 1, '2013-01-02 22:46:15');
/*!40000 ALTER TABLE `hr_title` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_training_institutions
DROP TABLE IF EXISTS `hr_training_institutions`;
CREATE TABLE IF NOT EXISTS `hr_training_institutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `training_type_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_training_institutions: ~0 rows (approximately)
DELETE FROM `hr_training_institutions`;
/*!40000 ALTER TABLE `hr_training_institutions` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_training_institutions` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.hr_workexperience
DROP TABLE IF EXISTS `hr_workexperience`;
CREATE TABLE IF NOT EXISTS `hr_workexperience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `company_name` varchar(145) NOT NULL,
  `position` varchar(45) DEFAULT NULL,
  `notes` text,
  `institution` varchar(128) DEFAULT NULL,
  `location_of_inst` varchar(200) DEFAULT NULL,
  `accountabilities` varchar(200) DEFAULT NULL,
  `reports_to` varchar(200) DEFAULT NULL,
  `supervises` varchar(200) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workexperience` (`emp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.hr_workexperience: ~0 rows (approximately)
DELETE FROM `hr_workexperience`;
/*!40000 ALTER TABLE `hr_workexperience` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_workexperience` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.monthsgroup
DROP TABLE IF EXISTS `monthsgroup`;
CREATE TABLE IF NOT EXISTS `monthsgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monthname` varchar(4) DEFAULT NULL,
  `longname` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.monthsgroup: ~12 rows (approximately)
DELETE FROM `monthsgroup`;
/*!40000 ALTER TABLE `monthsgroup` DISABLE KEYS */;
INSERT INTO `monthsgroup` (`id`, `monthname`, `longname`) VALUES
	(1, 'JAN', 'JANUARY'),
	(2, 'FEB', 'FEBRUARY'),
	(3, 'MAR', 'MARCH'),
	(4, 'APR', 'APRIL'),
	(5, 'MAY', 'MAY'),
	(6, 'JUN', 'JUNE'),
	(7, 'JUL', 'JULY'),
	(8, 'AUG', 'AUGUST'),
	(9, 'SEP', 'SEPTEMBER'),
	(10, 'OCT', 'OCTOBER'),
	(11, 'NOV', 'NOVEMBER'),
	(12, 'DEC', 'DECEMBER');
/*!40000 ALTER TABLE `monthsgroup` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.msg_email
DROP TABLE IF EXISTS `msg_email`;
CREATE TABLE IF NOT EXISTS `msg_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sent_by` int(11) DEFAULT NULL,
  `from_name` varchar(64) DEFAULT NULL,
  `from_email` varchar(128) NOT NULL,
  `to_email` varchar(128) NOT NULL,
  `to_name` varchar(60) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date_queued` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `to_email` (`to_email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.msg_email: 0 rows
DELETE FROM `msg_email`;
/*!40000 ALTER TABLE `msg_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `msg_email` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notif
DROP TABLE IF EXISTS `notif`;
CREATE TABLE IF NOT EXISTS `notif` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `notif_type_id` (`notif_type_id`),
  KEY `user_id` (`user_id`),
  KEY `item_id` (`item_id`),
  KEY `is_read` (`is_read`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.notif: ~185 rows (approximately)
DELETE FROM `notif`;
/*!40000 ALTER TABLE `notif` DISABLE KEYS */;
INSERT INTO `notif` (`id`, `notif_type_id`, `user_id`, `item_id`, `is_read`, `is_seen`, `date_created`) VALUES
	(1, 'pending_leave_approval', 28, 10, 0, 1, '2015-01-27 18:08:22'),
	(2, 'pending_requisition_approval', 7, 41, 1, 1, '2015-02-09 10:55:31'),
	(3, 'pending_requisition_approval', 5, 41, 0, 1, '2015-02-09 10:55:31'),
	(4, 'pending_requisition_approval', 8, 41, 1, 1, '2015-02-09 10:55:31'),
	(5, 'pending_requisition_approval', 7, 37, 1, 1, '2015-02-09 12:16:20'),
	(6, 'pending_requisition_approval', 5, 37, 0, 1, '2015-02-09 12:16:20'),
	(7, 'pending_requisition_approval', 8, 37, 1, 1, '2015-02-09 12:16:20'),
	(8, 'pending_requisition_approval', 7, 37, 1, 1, '2015-02-09 12:18:07'),
	(9, 'pending_requisition_approval', 5, 37, 0, 1, '2015-02-09 12:18:07'),
	(10, 'pending_requisition_approval', 28, 37, 1, 1, '2015-02-09 12:18:07'),
	(11, 'pending_requisition_approval', 28, 37, 1, 1, '2015-02-09 12:18:17'),
	(12, 'pending_requisition_approval', 5, 37, 0, 1, '2015-02-09 12:18:17'),
	(13, 'pending_requisition_approval', 8, 37, 1, 1, '2015-02-09 12:18:17'),
	(14, 'pending_requisition_approval', 7, 37, 1, 1, '2015-02-09 12:25:15'),
	(15, 'pending_requisition_approval', 5, 37, 0, 1, '2015-02-09 12:25:15'),
	(16, 'pending_requisition_approval', 28, 37, 1, 1, '2015-02-09 12:25:15'),
	(17, 'pending_requisition_approval', 7, 1, 1, 1, '2015-02-10 08:47:54'),
	(18, 'pending_requisition_approval', 5, 1, 0, 1, '2015-02-10 08:47:54'),
	(19, 'pending_requisition_approval', 8, 1, 1, 1, '2015-02-10 08:47:54'),
	(20, 'pending_requisition_approval', 7, 5, 1, 1, '2015-02-10 08:48:44'),
	(21, 'pending_requisition_approval', 5, 5, 0, 1, '2015-02-10 08:48:44'),
	(22, 'pending_requisition_approval', 8, 5, 1, 1, '2015-02-10 08:48:44'),
	(23, 'pending_requisition_approval', 7, 8, 1, 1, '2015-02-10 08:49:03'),
	(24, 'pending_requisition_approval', 28, 8, 0, 1, '2015-02-10 08:49:03'),
	(25, 'pending_requisition_approval', 8, 8, 1, 1, '2015-02-10 08:49:03'),
	(26, 'pending_requisition_approval', 7, 9, 1, 1, '2015-02-10 08:49:20'),
	(27, 'pending_requisition_approval', 5, 9, 0, 1, '2015-02-10 08:49:20'),
	(28, 'pending_requisition_approval', 8, 9, 1, 1, '2015-02-10 08:49:20'),
	(29, 'pending_requisition_approval', 7, 15, 1, 1, '2015-02-10 08:49:35'),
	(30, 'pending_requisition_approval', 5, 15, 0, 1, '2015-02-10 08:49:35'),
	(31, 'pending_requisition_approval', 8, 15, 1, 1, '2015-02-10 08:49:35'),
	(32, 'pending_requisition_approval', 7, 18, 1, 1, '2015-02-10 08:49:52'),
	(33, 'pending_requisition_approval', 5, 18, 0, 1, '2015-02-10 08:49:52'),
	(34, 'pending_requisition_approval', 8, 18, 1, 1, '2015-02-10 08:49:52'),
	(35, 'pending_requisition_approval', 7, 20, 1, 1, '2015-02-10 08:50:06'),
	(36, 'pending_requisition_approval', 5, 20, 0, 1, '2015-02-10 08:50:06'),
	(37, 'pending_requisition_approval', 8, 20, 1, 1, '2015-02-10 08:50:06'),
	(38, 'pending_requisition_approval', 7, 21, 1, 1, '2015-02-10 08:50:19'),
	(39, 'pending_requisition_approval', 5, 21, 0, 1, '2015-02-10 08:50:19'),
	(40, 'pending_requisition_approval', 8, 21, 1, 1, '2015-02-10 08:50:19'),
	(41, 'pending_requisition_approval', 7, 22, 1, 1, '2015-02-10 08:50:38'),
	(42, 'pending_requisition_approval', 5, 22, 0, 1, '2015-02-10 08:50:38'),
	(43, 'pending_requisition_approval', 8, 22, 1, 1, '2015-02-10 08:50:38'),
	(44, 'pending_requisition_approval', 7, 23, 1, 1, '2015-02-10 08:51:00'),
	(45, 'pending_requisition_approval', 5, 23, 0, 1, '2015-02-10 08:51:00'),
	(46, 'pending_requisition_approval', 8, 23, 1, 1, '2015-02-10 08:51:00'),
	(47, 'pending_requisition_approval', 7, 24, 1, 1, '2015-02-10 08:51:15'),
	(48, 'pending_requisition_approval', 5, 24, 0, 1, '2015-02-10 08:51:15'),
	(49, 'pending_requisition_approval', 8, 24, 1, 1, '2015-02-10 08:51:15'),
	(50, 'pending_requisition_approval', 7, 25, 1, 1, '2015-02-10 08:51:27'),
	(51, 'pending_requisition_approval', 5, 25, 0, 1, '2015-02-10 08:51:27'),
	(52, 'pending_requisition_approval', 8, 25, 1, 1, '2015-02-10 08:51:27'),
	(53, 'pending_requisition_approval', 7, 26, 1, 1, '2015-02-10 08:51:37'),
	(54, 'pending_requisition_approval', 5, 26, 0, 1, '2015-02-10 08:51:37'),
	(55, 'pending_requisition_approval', 8, 26, 1, 1, '2015-02-10 08:51:37'),
	(56, 'pending_requisition_approval', 7, 28, 1, 1, '2015-02-10 08:51:48'),
	(57, 'pending_requisition_approval', 5, 28, 0, 1, '2015-02-10 08:51:48'),
	(58, 'pending_requisition_approval', 8, 28, 1, 1, '2015-02-10 08:51:48'),
	(59, 'pending_requisition_approval', 7, 29, 1, 1, '2015-02-10 08:51:58'),
	(60, 'pending_requisition_approval', 5, 29, 0, 1, '2015-02-10 08:51:58'),
	(61, 'pending_requisition_approval', 8, 29, 1, 1, '2015-02-10 08:51:58'),
	(62, 'pending_requisition_approval', 7, 27, 1, 1, '2015-02-10 08:52:08'),
	(63, 'pending_requisition_approval', 5, 27, 0, 1, '2015-02-10 08:52:08'),
	(64, 'pending_requisition_approval', 8, 27, 1, 1, '2015-02-10 08:52:08'),
	(65, 'pending_requisition_approval', 7, 30, 1, 1, '2015-02-10 08:52:21'),
	(66, 'pending_requisition_approval', 5, 30, 0, 1, '2015-02-10 08:52:21'),
	(67, 'pending_requisition_approval', 8, 30, 1, 1, '2015-02-10 08:52:21'),
	(68, 'pending_requisition_approval', 7, 31, 1, 1, '2015-02-10 08:52:32'),
	(69, 'pending_requisition_approval', 5, 31, 0, 1, '2015-02-10 08:52:32'),
	(70, 'pending_requisition_approval', 8, 31, 1, 1, '2015-02-10 08:52:32'),
	(71, 'pending_requisition_approval', 7, 32, 1, 1, '2015-02-10 08:52:43'),
	(72, 'pending_requisition_approval', 5, 32, 0, 1, '2015-02-10 08:52:43'),
	(73, 'pending_requisition_approval', 8, 32, 1, 1, '2015-02-10 08:52:43'),
	(74, 'pending_requisition_approval', 7, 33, 1, 1, '2015-02-10 08:52:58'),
	(75, 'pending_requisition_approval', 5, 33, 0, 1, '2015-02-10 08:52:58'),
	(76, 'pending_requisition_approval', 8, 33, 1, 1, '2015-02-10 08:52:58'),
	(77, 'pending_requisition_approval', 7, 34, 1, 1, '2015-02-10 08:53:11'),
	(78, 'pending_requisition_approval', 5, 34, 0, 1, '2015-02-10 08:53:11'),
	(79, 'pending_requisition_approval', 8, 34, 1, 1, '2015-02-10 08:53:11'),
	(80, 'pending_requisition_approval', 7, 35, 1, 1, '2015-02-10 08:53:26'),
	(81, 'pending_requisition_approval', 5, 35, 0, 1, '2015-02-10 08:53:26'),
	(82, 'pending_requisition_approval', 8, 35, 1, 1, '2015-02-10 08:53:26'),
	(83, 'pending_requisition_approval', 7, 36, 1, 1, '2015-02-10 08:53:39'),
	(84, 'pending_requisition_approval', 5, 36, 0, 1, '2015-02-10 08:53:39'),
	(85, 'pending_requisition_approval', 8, 36, 1, 1, '2015-02-10 08:53:39'),
	(86, 'pending_requisition_approval', 7, 38, 1, 1, '2015-02-10 08:53:51'),
	(87, 'pending_requisition_approval', 5, 38, 0, 1, '2015-02-10 08:53:51'),
	(88, 'pending_requisition_approval', 8, 38, 1, 1, '2015-02-10 08:53:51'),
	(89, 'pending_requisition_approval', 7, 39, 1, 1, '2015-02-10 08:54:02'),
	(90, 'pending_requisition_approval', 5, 39, 0, 1, '2015-02-10 08:54:02'),
	(91, 'pending_requisition_approval', 8, 39, 1, 1, '2015-02-10 08:54:02'),
	(92, 'pending_requisition_approval', 7, 40, 1, 1, '2015-02-10 08:54:13'),
	(93, 'pending_requisition_approval', 5, 40, 0, 1, '2015-02-10 08:54:13'),
	(94, 'pending_requisition_approval', 8, 40, 1, 1, '2015-02-10 08:54:13'),
	(95, 'pending_requisition_approval', 7, 41, 1, 1, '2015-02-10 08:54:29'),
	(96, 'pending_requisition_approval', 5, 41, 0, 1, '2015-02-10 08:54:29'),
	(97, 'pending_requisition_approval', 8, 41, 1, 1, '2015-02-10 08:54:29'),
	(98, 'pending_requisition_approval', 8, 37, 1, 1, '2015-02-10 11:18:48'),
	(99, 'pending_requisition_approval', 8, 37, 1, 1, '2015-02-10 15:24:10'),
	(100, 'pending_requisition_approval', 8, 52, 1, 1, '2015-02-10 15:44:07'),
	(101, 'pending_requisition_approval', 8, 55, 1, 1, '2015-02-10 18:01:40'),
	(102, 'pending_requisition_approval', 8, 56, 1, 1, '2015-02-10 18:25:56'),
	(103, 'pending_requisition_approval', 8, 52, 1, 1, '2015-02-10 18:26:48'),
	(104, 'pending_requisition_approval', 7, 42, 0, 1, '2015-02-11 10:09:46'),
	(105, 'pending_requisition_approval', 5, 42, 0, 1, '2015-02-11 10:09:46'),
	(106, 'pending_requisition_approval', 8, 42, 1, 1, '2015-02-11 10:09:46'),
	(107, 'pending_requisition_approval', 7, 43, 0, 1, '2015-02-11 10:12:43'),
	(108, 'pending_requisition_approval', 5, 43, 0, 1, '2015-02-11 10:12:43'),
	(109, 'pending_requisition_approval', 8, 43, 1, 1, '2015-02-11 10:12:43'),
	(110, 'pending_requisition_approval', 7, 44, 0, 1, '2015-02-11 11:01:04'),
	(111, 'pending_requisition_approval', 5, 44, 0, 1, '2015-02-11 11:01:04'),
	(112, 'pending_requisition_approval', 8, 44, 1, 1, '2015-02-11 11:01:04'),
	(113, 'pending_requisition_approval', 7, 47, 0, 1, '2015-02-11 11:04:20'),
	(114, 'pending_requisition_approval', 5, 47, 0, 1, '2015-02-11 11:04:20'),
	(115, 'pending_requisition_approval', 8, 47, 1, 1, '2015-02-11 11:04:20'),
	(116, 'pending_requisition_approval', 7, 48, 0, 1, '2015-02-11 11:07:12'),
	(117, 'pending_requisition_approval', 5, 48, 0, 1, '2015-02-11 11:07:12'),
	(118, 'pending_requisition_approval', 8, 48, 1, 1, '2015-02-11 11:07:12'),
	(119, 'pending_requisition_approval', 7, 49, 0, 1, '2015-02-11 11:21:24'),
	(120, 'pending_requisition_approval', 5, 49, 0, 1, '2015-02-11 11:21:24'),
	(121, 'pending_requisition_approval', 8, 49, 1, 1, '2015-02-11 11:21:24'),
	(122, 'pending_requisition_approval', 7, 50, 0, 1, '2015-02-11 11:22:37'),
	(123, 'pending_requisition_approval', 5, 50, 0, 1, '2015-02-11 11:22:37'),
	(124, 'pending_requisition_approval', 8, 50, 1, 1, '2015-02-11 11:22:37'),
	(125, 'pending_requisition_approval', 7, 52, 0, 1, '2015-02-11 11:48:02'),
	(126, 'pending_requisition_approval', 5, 52, 0, 1, '2015-02-11 11:48:02'),
	(127, 'pending_requisition_approval', 8, 52, 1, 1, '2015-02-11 11:48:02'),
	(128, 'pending_requisition_approval', 8, 57, 1, 1, '2015-02-11 11:54:23'),
	(129, 'pending_requisition_approval', 8, 57, 1, 1, '2015-02-11 12:32:05'),
	(130, 'pending_requisition_approval', 7, 55, 0, 1, '2015-02-11 12:39:47'),
	(131, 'pending_requisition_approval', 5, 55, 0, 1, '2015-02-11 12:39:47'),
	(132, 'pending_requisition_approval', 8, 55, 1, 1, '2015-02-11 12:39:47'),
	(133, 'pending_requisition_approval', 7, 56, 0, 1, '2015-02-11 13:17:06'),
	(134, 'pending_requisition_approval', 5, 56, 0, 1, '2015-02-11 13:17:06'),
	(135, 'pending_requisition_approval', 8, 56, 1, 1, '2015-02-11 13:17:06'),
	(136, 'pending_requisition_approval', 5, 58, 0, 1, '2015-02-11 14:02:40'),
	(137, 'pending_requisition_approval', 8, 59, 1, 1, '2015-02-11 14:14:05'),
	(138, 'pending_requisition_approval', 8, 57, 1, 1, '2015-02-11 14:20:28'),
	(139, 'pending_requisition_approval', 5, 61, 0, 1, '2015-02-11 14:52:23'),
	(140, 'pending_requisition_approval', 7, 61, 0, 1, '2015-02-11 14:59:49'),
	(141, 'pending_requisition_approval', 5, 61, 0, 1, '2015-02-11 14:59:49'),
	(142, 'pending_requisition_approval', 8, 61, 1, 1, '2015-02-11 14:59:49'),
	(143, 'pending_requisition_approval', 7, 58, 0, 1, '2015-02-11 15:00:57'),
	(144, 'pending_requisition_approval', 5, 58, 0, 1, '2015-02-11 15:00:57'),
	(145, 'pending_requisition_approval', 8, 58, 1, 1, '2015-02-11 15:00:57'),
	(146, 'pending_requisition_approval', 7, 46, 0, 1, '2015-02-12 12:13:18'),
	(147, 'pending_requisition_approval', 5, 46, 0, 0, '2015-02-12 12:13:18'),
	(148, 'pending_requisition_approval', 8, 46, 1, 1, '2015-02-12 12:13:18'),
	(149, 'pending_requisition_approval', 5, 61, 0, 0, '2015-02-12 12:57:14'),
	(150, 'pending_requisition_approval', 8, 60, 1, 1, '2015-02-12 14:14:59'),
	(151, 'pending_requisition_approval', 8, 62, 1, 1, '2015-02-12 14:34:31'),
	(152, 'pending_requisition_approval', 7, 43, 0, 1, '2015-02-12 14:36:14'),
	(153, 'pending_requisition_approval', 5, 43, 0, 0, '2015-02-12 14:36:14'),
	(154, 'pending_requisition_approval', 8, 43, 1, 1, '2015-02-12 14:36:14'),
	(155, 'pending_requisition_approval', 8, 59, 1, 1, '2015-02-12 15:04:29'),
	(156, 'pending_requisition_approval', 8, 63, 1, 1, '2015-02-12 15:24:19'),
	(157, 'pending_requisition_approval', 5, 58, 0, 0, '2015-02-12 16:18:45'),
	(158, 'pending_requisition_approval', 8, 63, 1, 1, '2015-02-12 16:35:10'),
	(159, 'pending_requisition_approval', 5, 58, 0, 0, '2015-02-12 17:30:25'),
	(160, 'pending_requisition_approval', 5, 64, 0, 0, '2015-02-16 12:51:33'),
	(161, 'pending_requisition_approval', 5, 66, 0, 0, '2015-02-16 15:41:46'),
	(162, 'pending_requisition_approval', 8, 49, 1, 1, '2015-02-17 12:11:02'),
	(163, 'pending_requisition_approval', 8, 67, 1, 1, '2015-02-17 13:36:09'),
	(164, 'pending_requisition_approval', 8, 68, 1, 1, '2015-02-17 14:47:14'),
	(165, 'pending_requisition_approval', 8, 69, 1, 1, '2015-02-17 14:56:24'),
	(166, 'pending_requisition_approval', 8, 69, 1, 1, '2015-02-17 15:28:29'),
	(167, 'pending_requisition_approval', 8, 69, 1, 1, '2015-02-17 17:55:42'),
	(168, 'pending_requisition_approval', 8, 68, 1, 1, '2015-02-18 09:10:27'),
	(169, 'pending_requisition_approval', 8, 71, 1, 1, '2015-02-18 09:29:33'),
	(170, 'pending_requisition_approval', 8, 67, 1, 1, '2015-02-18 10:38:45'),
	(171, 'pending_requisition_approval', 8, 67, 1, 1, '2015-02-18 10:43:19'),
	(172, 'pending_requisition_approval', 8, 71, 1, 1, '2015-02-18 10:44:00'),
	(173, 'pending_requisition_approval', 8, 70, 1, 1, '2015-02-18 10:47:59'),
	(174, 'pending_requisition_approval', 8, 72, 1, 1, '2015-02-18 11:29:25'),
	(175, 'pending_requisition_approval', 8, 49, 1, 1, '2015-02-18 11:29:47'),
	(176, 'pending_requisition_approval', 8, 73, 1, 1, '2015-02-18 12:09:35'),
	(177, 'pending_requisition_approval', 8, 73, 1, 1, '2015-02-19 09:26:27'),
	(178, 'pending_requisition_approval', 8, 74, 1, 1, '2015-02-19 09:48:10'),
	(179, 'pending_requisition_approval', 8, 75, 1, 1, '2015-02-19 12:58:24'),
	(180, 'pending_requisition_approval', 8, 76, 1, 1, '2015-02-19 13:10:22'),
	(181, 'pending_requisition_approval', 8, 77, 1, 1, '2015-02-19 15:55:50'),
	(182, 'pending_requisition_approval', 8, 79, 1, 1, '2015-02-19 17:16:56'),
	(183, 'pending_requisition_approval', 8, 80, 1, 1, '2015-02-20 11:59:33'),
	(184, 'pending_requisition_approval', 8, 81, 0, 0, '2015-02-23 11:44:32'),
	(185, 'pending_requisition_approval', 8, 82, 0, 0, '2015-02-23 12:18:40');
/*!40000 ALTER TABLE `notif` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notification
DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_notification_type_idx` (`type`),
  KEY `fk_notification_create_time_idx` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.notification: ~0 rows (approximately)
DELETE FROM `notification`;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notification_static
DROP TABLE IF EXISTS `notification_static`;
CREATE TABLE IF NOT EXISTS `notification_static` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) unsigned NOT NULL,
  `subject` varchar(256) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.notification_static: ~0 rows (approximately)
DELETE FROM `notification_static`;
/*!40000 ALTER TABLE `notification_static` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification_static` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notif_settings
DROP TABLE IF EXISTS `notif_settings`;
CREATE TABLE IF NOT EXISTS `notif_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `setting_key` varchar(60) NOT NULL,
  `setting_value` text NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.notif_settings: ~0 rows (approximately)
DELETE FROM `notif_settings`;
/*!40000 ALTER TABLE `notif_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `notif_settings` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notif_types
DROP TABLE IF EXISTS `notif_types`;
CREATE TABLE IF NOT EXISTS `notif_types` (
  `id` varchar(60) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `notif_template` varchar(500) NOT NULL,
  `email_template` text,
  `send_email` tinyint(1) NOT NULL DEFAULT '1',
  `notify` enum('all_users','specified_users') NOT NULL DEFAULT 'all_users',
  `notify_days_before` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `model_class_name` varchar(60) NOT NULL,
  `fa_icon_class` varchar(30) NOT NULL DEFAULT 'fa-bell',
  `notification_trigger` enum('system','manual') NOT NULL DEFAULT 'system',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.notif_types: ~6 rows (approximately)
DELETE FROM `notif_types`;
/*!40000 ALTER TABLE `notif_types` DISABLE KEYS */;
INSERT INTO `notif_types` (`id`, `name`, `description`, `notif_template`, `email_template`, `send_email`, `notify`, `notify_days_before`, `model_class_name`, `fa_icon_class`, `notification_trigger`, `is_active`, `date_created`, `created_by`) VALUES
	('events_reminder', 'Events Reminder', 'Events reminders e.g electricity bill, water bill etc', ' {{IAPF Report}} is due on {{23/1/2015}}. Please make arrangements.', '<p>\r\n	                       Hi {{George}},\r\n</p>\r\n<p>\r\n	                  {{<span style="background-color: initial;">IAPF Report</span><span style="background-color: initial;">}} is due on {{23/1/2015}}.</span>\r\n</p>\r\n<p>\r\n	                       Please make arrangements.\r\n</p>\r\n<p>\r\n	                       Chimesgreen.\r\n</p>', 1, 'specified_users', 1, 'Event', 'fa-calendar', 'system', 1, '2014-04-27 00:43:44', 1),
	('fleet_insurance_renewal', 'Vehicle insurance renewal notification', '', 'The vehicle {{vehicle}} is due for insurance renewal with {{insurer}} on {{renewal_date}}. Premium amount is {{premium_amount}} .\r\nPlease make arrangements.', '<p>\r\n	                    Hi {{user}}\r\n</p>\r\n<p>\r\n	                    The vehicle {{vehicle}} is due for insurance renewal with {{insurer}} on {{renewal_date}}.\r\n</p>\r\n<p>\r\n	          Premium amount is {{premium_amount}} .\r\n</p>\r\n<p>\r\n	                    Please make arrangements.\r\n</p>', 1, 'specified_users', 9, 'FleetVehicleInsurance', 'fa-truck', 'system', 1, '2014-04-01 21:07:30', 1),
	('fleet_vehicle_servicing', 'Vehicle servicing notification', '', 'The vehicle {{vehicle}} is due for servicing on {{date}}. Please make arrangments.', '<p>\r\n	                        Hi {{user}}\r\n</p>\r\n<p>\r\n	                        The vehicle {{vehicle}} is due for servicing on {{date}}.\r\n</p>\r\n<p>\r\n	                        Please make arrangments.\r\n</p>', 1, 'specified_users', 12, 'FleetVehicles', 'fa-truck', 'system', 1, '2014-04-01 21:09:43', 1),
	('pending_leave_approval', 'Pending Leave Approval', 'Pending Leave Approval notification', 'We will determine this later', '<p>\r\n	 Hi! {{user}},<br>\r\n	            {{sender}} has an {{type}}  leave application starting {{startdate}} to {{enddate}} for {{days}} that needs your approval.<br>\r\n	            Thanks<br>\r\n	            Chimesgreen\r\n</p>', 1, 'all_users', 2, 'Leaves', 'fa-bell', 'manual', 1, '2014-03-24 14:23:35', 1),
	('pending_requisition_approval', 'Requisition', 'Requisition Notification', 'Requisition Number {{reqid}} by {{sender}} needs your attention on. Please make arrangments.', '<p>\r\n	          Hi! {{user}},<br>\r\n	          {{sender}} made a requisition on {{reqdate}} that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>', 1, 'all_users', 2, 'ReqRequisitionHeaders', 'fa-bell', 'system', 1, '2014-04-24 18:04:36', 1),
	('rfq_suppliers_invite', 'Request for Quotation Invitation', 'Invite all the suppliers added to quote', ' Dear Supplier,\r\nYou are hereby invited to submit your quotation for the supply of the following goods and /or services.\r\nRequisition Details\r\nOur Ref: {{quote_no}}.\r\nClosing Date : {{closing_date}}\r\nClosing Time: {{closing_time}}\r\n{{items}}\r\nKind Regards\r\nE-procure', '<p>\r\n	    Dear Supplier,<br>\r\n	    You are hereby invited to submit your quotation for the supply of the following goods and /or services.\r\n</p>\r\n<p>\r\n	 <strong>Requisition Details</strong>\r\n</p>\r\n<p>\r\n	 <strong>Our Ref: {{quote_no}}.</strong>\r\n</p>\r\n<p>\r\n	<strong>Closing Date : {{closing_date}}</strong>\r\n</p>\r\n<p>\r\n	<strong>Closing Time: {{closing_time}}</strong>\r\n</p>\r\n<p>\r\n	<strong></strong>\r\n</p>\r\n<p>\r\n	    {{items}}\r\n</p>\r\n<p>\r\n	    Kind Regards\r\n</p>\r\n<p>\r\n	    E-procure<br>\r\n	 <strong></strong>\r\n</p>', 1, 'all_users', 2, 'RfqInvitedSuppliers', 'fa-bell', 'system', 1, '2014-12-15 16:19:05', 1);
/*!40000 ALTER TABLE `notif_types` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notif_type_roles
DROP TABLE IF EXISTS `notif_type_roles`;
CREATE TABLE IF NOT EXISTS `notif_type_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.notif_type_roles: ~0 rows (approximately)
DELETE FROM `notif_type_roles`;
/*!40000 ALTER TABLE `notif_type_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `notif_type_roles` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.notif_type_users
DROP TABLE IF EXISTS `notif_type_users`;
CREATE TABLE IF NOT EXISTS `notif_type_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `notif_type_id` varchar(60) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `notif_type_id` (`notif_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.notif_type_users: ~2 rows (approximately)
DELETE FROM `notif_type_users`;
/*!40000 ALTER TABLE `notif_type_users` DISABLE KEYS */;
INSERT INTO `notif_type_users` (`id`, `notif_type_id`, `user_id`, `date_created`, `created_by`) VALUES
	(61, 'fleet_insurance_renewal', 1, '2014-04-26 18:28:02', 1),
	(62, 'fleet_vehicle_servicing', 1, '2014-04-26 18:28:20', 1);
/*!40000 ALTER TABLE `notif_type_users` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person
DROP TABLE IF EXISTS `person`;
CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) NOT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `marital_status_id` int(11) unsigned DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `birthdate_estimated` tinyint(1) NOT NULL DEFAULT '0',
  `country_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `id_no` varchar(30) DEFAULT NULL,
  `pin_no` varchar(30) DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(60) DEFAULT NULL,
  `next_of_kin_name` varchar(50) DEFAULT NULL,
  `next_of_kin_phone` varchar(25) DEFAULT NULL,
  `next_of_kin_email` varchar(128) DEFAULT NULL,
  `spouse` varchar(50) DEFAULT NULL,
  `full_names` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marital_status_id` (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.person: ~1 rows (approximately)
DELETE FROM `person`;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`id`, `first_name`, `middle_name`, `last_name`, `gender`, `marital_status_id`, `birthdate`, `birthdate_estimated`, `country_id`, `date_created`, `created_by`, `last_modified`, `id_no`, `pin_no`, `nationality`, `place_of_birth`, `next_of_kin_name`, `next_of_kin_phone`, `next_of_kin_email`, `spouse`, `full_names`) VALUES
	(28, 'James', 'M', 'Makau', 'Male', 1, '1986-01-01', 0, 1, '2016-09-18 22:18:45', NULL, '2016-09-30 07:30:09', '23478504', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'James M Makau');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_address
DROP TABLE IF EXISTS `person_address`;
CREATE TABLE IF NOT EXISTS `person_address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `phone1` varchar(15) DEFAULT NULL,
  `phone2` varchar(15) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `residence` varchar(255) DEFAULT NULL,
  `current` tinyint(1) NOT NULL DEFAULT '1',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_address: ~1 rows (approximately)
DELETE FROM `person_address`;
/*!40000 ALTER TABLE `person_address` DISABLE KEYS */;
INSERT INTO `person_address` (`id`, `person_id`, `phone1`, `phone2`, `email`, `address`, `residence`, `current`, `date_created`, `created_by`, `last_modified`) VALUES
	(1, 28, NULL, NULL, NULL, NULL, NULL, 1, '2016-09-18 22:21:09', 28, '2016-09-30 07:30:10');
/*!40000 ALTER TABLE `person_address` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_department
DROP TABLE IF EXISTS `person_department`;
CREATE TABLE IF NOT EXISTS `person_department` (
  `person_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `has_left` tinyint(4) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(256) DEFAULT NULL,
  `date_left` date DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_department: ~21 rows (approximately)
DELETE FROM `person_department`;
/*!40000 ALTER TABLE `person_department` DISABLE KEYS */;
INSERT INTO `person_department` (`person_id`, `dept_id`, `has_left`, `reason_for_leaving`, `date_left`, `created_by`, `last_modified_by`, `last_modified`, `date_created`) VALUES
	(5, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 09:25:50'),
	(7, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 09:30:46'),
	(8, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 09:34:51'),
	(9, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 09:38:44'),
	(10, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:09:46'),
	(11, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:12:44'),
	(12, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:16:54'),
	(13, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:19:43'),
	(14, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:23:58'),
	(15, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:40:15'),
	(16, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:47:34'),
	(17, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 10:56:10'),
	(18, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:04:06'),
	(19, 3, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:20:17'),
	(20, 1, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:25:18'),
	(21, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:29:22'),
	(22, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:33:30'),
	(23, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:36:57'),
	(24, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:41:31'),
	(25, 2, 0, NULL, NULL, 3, NULL, NULL, '2014-09-04 11:45:51'),
	(28, 1, 0, NULL, NULL, 1, NULL, NULL, '2016-01-30 09:15:44');
/*!40000 ALTER TABLE `person_department` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_images
DROP TABLE IF EXISTS `person_images`;
CREATE TABLE IF NOT EXISTS `person_images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `image` varchar(128) NOT NULL,
  `image_size_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_profile_image` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `image_size_id` (`image_size_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_images: ~4 rows (approximately)
DELETE FROM `person_images`;
/*!40000 ALTER TABLE `person_images` DISABLE KEYS */;
INSERT INTO `person_images` (`id`, `person_id`, `image`, `image_size_id`, `date_created`, `is_profile_image`) VALUES
	(1, 18, '5bca7b648bb3b35229b9271a4583715a.jpg', NULL, '2015-01-21 11:59:06', 1),
	(3, 5, '7d29ecf2f156ae6251af1f464518ee4c.jpg', NULL, '2015-01-21 13:38:26', 1),
	(4, 26, '8c008ee894681726a9aabc647cb8de94.JPG', NULL, '2015-02-02 16:55:24', 1),
	(6, 28, '8320789d321429ca97b5f892608384ac.jpg', NULL, '2016-09-30 07:30:09', 1);
/*!40000 ALTER TABLE `person_images` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_image_sizes
DROP TABLE IF EXISTS `person_image_sizes`;
CREATE TABLE IF NOT EXISTS `person_image_sizes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `width` smallint(5) unsigned NOT NULL,
  `height` smallint(5) unsigned NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_image_sizes: ~0 rows (approximately)
DELETE FROM `person_image_sizes`;
/*!40000 ALTER TABLE `person_image_sizes` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_image_sizes` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_jobtitles
DROP TABLE IF EXISTS `person_jobtitles`;
CREATE TABLE IF NOT EXISTS `person_jobtitles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `title_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `branch_id` (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_jobtitles: ~0 rows (approximately)
DELETE FROM `person_jobtitles`;
/*!40000 ALTER TABLE `person_jobtitles` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_jobtitles` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_location
DROP TABLE IF EXISTS `person_location`;
CREATE TABLE IF NOT EXISTS `person_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(11) unsigned NOT NULL,
  `location_id` int(11) unsigned NOT NULL,
  `has_left` tinyint(1) NOT NULL DEFAULT '0',
  `reason_for_leaving` varchar(255) DEFAULT NULL,
  `date_left` timestamp NULL DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `branch_id` (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_location: ~22 rows (approximately)
DELETE FROM `person_location`;
/*!40000 ALTER TABLE `person_location` DISABLE KEYS */;
INSERT INTO `person_location` (`id`, `person_id`, `location_id`, `has_left`, `reason_for_leaving`, `date_left`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
	(1, 4, 2, 0, NULL, NULL, '2014-09-03 16:46:31', 3, NULL, NULL),
	(2, 5, 2, 0, NULL, NULL, '2014-09-04 09:25:50', 3, NULL, NULL),
	(3, 7, 2, 0, NULL, NULL, '2014-09-04 09:30:46', 3, NULL, NULL),
	(4, 8, 2, 0, NULL, NULL, '2014-09-04 09:34:51', 3, NULL, NULL),
	(5, 9, 2, 0, NULL, NULL, '2014-09-04 09:38:44', 3, NULL, NULL),
	(6, 10, 2, 0, NULL, NULL, '2014-09-04 10:09:46', 3, NULL, NULL),
	(7, 11, 2, 0, NULL, NULL, '2014-09-04 10:12:44', 3, NULL, NULL),
	(8, 12, 2, 0, NULL, NULL, '2014-09-04 10:16:54', 3, NULL, NULL),
	(9, 13, 2, 0, NULL, NULL, '2014-09-04 10:19:43', 3, NULL, NULL),
	(10, 14, 2, 0, NULL, NULL, '2014-09-04 10:23:58', 3, NULL, NULL),
	(12, 16, 2, 0, NULL, NULL, '2014-09-04 10:47:34', 3, NULL, NULL),
	(13, 17, 2, 0, NULL, NULL, '2014-09-04 10:56:10', 3, NULL, NULL),
	(14, 18, 2, 0, NULL, NULL, '2014-09-04 11:04:06', 3, NULL, NULL),
	(15, 19, 2, 0, NULL, NULL, '2014-09-04 11:20:17', 3, NULL, NULL),
	(16, 20, 2, 0, NULL, NULL, '2014-09-04 11:25:18', 3, NULL, NULL),
	(17, 21, 2, 0, NULL, NULL, '2014-09-04 11:29:22', 3, NULL, NULL),
	(18, 22, 2, 0, NULL, NULL, '2014-09-04 11:33:30', 3, NULL, NULL),
	(19, 23, 2, 0, NULL, NULL, '2014-09-04 11:36:57', 3, NULL, NULL),
	(20, 24, 2, 0, NULL, NULL, '2014-09-04 11:41:31', 3, NULL, NULL),
	(21, 25, 2, 0, NULL, NULL, '2014-09-04 11:45:51', 3, NULL, NULL),
	(22, 26, 2, 0, NULL, NULL, '2015-01-28 11:54:57', 26, NULL, NULL),
	(23, 28, 2, 0, NULL, NULL, '2016-01-30 09:15:44', 1, NULL, NULL);
/*!40000 ALTER TABLE `person_location` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_marital_status
DROP TABLE IF EXISTS `person_marital_status`;
CREATE TABLE IF NOT EXISTS `person_marital_status` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_marital_status: ~2 rows (approximately)
DELETE FROM `person_marital_status`;
/*!40000 ALTER TABLE `person_marital_status` DISABLE KEYS */;
INSERT INTO `person_marital_status` (`id`, `name`, `date_created`, `created_by`) VALUES
	(1, 'Single', '0000-00-00 00:00:00', NULL),
	(2, 'Married', '0000-00-00 00:00:00', NULL);
/*!40000 ALTER TABLE `person_marital_status` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.person_signature
DROP TABLE IF EXISTS `person_signature`;
CREATE TABLE IF NOT EXISTS `person_signature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `file_name` varchar(149) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `file_size` varchar(128) DEFAULT NULL,
  `file_content` blob,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.person_signature: ~4 rows (approximately)
DELETE FROM `person_signature`;
/*!40000 ALTER TABLE `person_signature` DISABLE KEYS */;
INSERT INTO `person_signature` (`id`, `person_id`, `file_name`, `file_type`, `file_size`, `file_content`, `date_created`, `created_by`) VALUES
	(1, 8, 'p1.png', 'image/png', '10962', _binary 0x6956424F5277304B47676F414141414E53556845556741414162734141414373434149414141427472355865414141584D476C445131424A51304D6755484A765A6D6C735A5141415749573157515655567375336E2F503152336433647A665344644B4E434878304E7967684B43496F4945684B4B64324341684A6945494B41696F67434B714567556F4B4B496F684B76494E36372F322F576D2B39746436627465624D622B335A7332666D37443137396A3448414D5A31516E437750344943674944413846414C5055304F4F3373484475774D5141465751416F2F78516C75596345615A6D5A487758396274696342644E672B467A325539642F7A2F5A65463074306A7A41304179417A47727535686267457776676B4151744D744F445163414F514F54483936496A775978716768474E4F4577677545386377683976714E4E77367836792B4D5276336973624C51676A454441446753416948554377425348706A4F45656E6D42637368315159415178586F37684D49414C55646A46586476416E754144446D777A7769415146426837676678674B752F794C4836392F4A645031624A6F48673954662B765A6466426166744578627354346A365837364F2F376B452B4566384E51635658456B432F55304F64554D483178563367725952334C4C4164542F592F35664F59423649795350513276495046676C304E5448396731553951335574666F2B467A494C444E51387876442F494D7A6A637A4F6F50505337615738766B63423459353369453666776C703953585948696F4D7A49597434524757466A444748344855453959704B554F6A47474C677435466531765A2F754835367536682F59654F51486A36364272387756512B34516148633948416D4D7376794D6A69393177494F5741452F4945486941436838444D5169494B6A5141746F2F336D4B416B3941674873693462347734416557594277416A776943787754426D4F4D506E395A2F6F756A2B477563466A2F7633456A6D4147387758386665636631482F6B654144334F48324C7A726854392F6836734B6366524C2B6D654666356630614B644567735371782B31632F6967386C685A4A4661614A55554B6F6F526343426F6B4D784156475544456F427059465351796E44665972774B742F39577557664E52374B44326A786A4D7750696C4B793866367A4239652F6432447A6939766E76397A526E37552F576539592F337546494E7A6A5A50696841576B46425565462B6E683568334E6F7743665851345444494E424E54495244536B4A5336762F636276382F793648502B6F30324C583735496F687539422B61343055415A4E5867632F376F483170774A51424E4D67437743663144453679436A77374D32365467466845612B5A7432364534414768414263746843475145623441594338487557416E4A414761674448574149544945567341644F384E7632686D3077464A77417365414D53414B7034424C49425957674246534157744149576B414875414E367751507743447746453241617A49464673415932774462344355455146694B467143464769423369685951684B556742556F56306F4B4F51425751507555426555434155416356435A3646554B417371684D71674F71675A75675831517350514750514B6D6F64576F532F514477515351594B67516241692B4244694341574542734949595955346A7642436843436945596D496445512B6F6878784464474F36455538516B7767356842726943306B5142496A365A43635346476B416C494C6159703051486F69513546787942526B4872496332595473516734696E79506E6B4F76493779674D6968724667524B46375651665A59317951345767346C427071454A554C616F64315939366A707048626144323061526F4672517757676C74674C5A44653646506F4A5051656568716442743641443242586B5276597A41594F67772F5268366A6A3748482B474A694D476D594B356A726D42374D474759427334584659686D78776C675672436D57674133484A6D454C734E65773364686E32455873446F34597834365477756E6948484342754152634871346564772F33444C654D2B346D6E775050696C66436D654864384644344458346E76776F2F69462F452F6953694A2B496C55694B794966496E4F454F55544E52454E454D30516252495445334D524B784B62452F73516E79624F4A37354250455138542F7964684970456945534C784A456B676953647049616B682B5156795359704B536B667154717041326B346154707048656C393074656B4F3254555A474A6B426D54755A50466B525754745A4D2F4950704C6A79586E4A4E636964794B504A383868627955664A31796E774648775557685145696A694B496F70624643386F746969704B53557054536B444B4E4D6F36796D484B56656F7346523856447055376C534A5642565539366B57714A4855334E5261314737555A366B727151656F46326B774E507730426A532B4E4B6B306A54525061445A6F7157686C6147316F543949573064366C6E614E443076485247644435303258517464424E307632675A3658586F5065677630446652502B4D2F6873444D344D366777644443734E3168676D47483477636A44714D666F795A6A42324D7330776F4A69456D633659545446655A42706A576D576D596C5A6E646D464F595735696E57424173516977574C4445734653795057625A5932566A3157494E5A43316A767336367A30624770732F6D79356244645931746C70325A585A6664687A3248765A6E2F5051637568776548506B632F527A37484279634B707A786E425763623568504D6E467A2B584E566343313357755757346962675675542B3463376A377544523532486D4F65574A34476E696C65504B3843727A66765A643542336D39382F487932664D6C38485877722F417A3842767A522F4133384D774B6B416D6F4349514C6C41754F434745454651542F424B344A5068524243736B4C65516B56436F38494959546C68482B4572776D4D69614246466B554352637045586F69536947714B526F67326938324A30596B6646457351367844364B383467376947654B443472765338684B2B457455536B784C556B6B6153695A49646B6C2B6B524B5363704D716B6871584A7058576C5936583770542B4C434D733479467A5665616C4C4C577373577979624A2F736E707938584B68636B39797150492B38693379782F4173464767557A68545346495557306F715A69764F49647865394B636B72685369314B6E3552466C663255363556586A7641663854685365575242685575466F464B6D4D71664B6F65716957716F3670386170526C4172563375727A713375726C367476717768714F47726355336A6F3661455A71686D6D2B59334C53577455316F39326B68745065305537536336564472574F6F553672335735644C3130473351333947543159765236394E48365276715A2B69384D57413363444F6F4D4E677A6C4455385A3968755247466B6146527139505370304E50526F6C7A4843324E413432336A47684E636B304B5444464A67616D4761627A7072786D345759335462486D4A755A46356B765755686178466F4D576C4A624F6C7657573235626156706C57453162433168485750665A6B4E73343274545A664C5056747332796E624D54747A746C393869657964374876744D423632446A554F327764557A6E574F36785255645A78795448796550387830386548335A6963764A337575744D376B7877626E564275396936314C7673456B774A35595174567750585974634E4E79323379323572377572754F65367248696F655752374C6E697165575A34725869706532563672336D726565643772506C6F2B68543666666656395333792F2B5A6E36316667642B4E763658772F41426267453341716B4376514C37413969437A6F5A4E425973484A775550426569464A49627368467146466F64426F5564442B734D70344744773863524168486E4975596A56534F4C496E644F324A786F50556C354D76446B34796968714174527939473630565578714269336D4C35597A74677A73664F6E4E45365678554678726E463938647A786966474C702F564F313534684F754E335A695242496945723465745A32374E646961794A70784D587A756D64613067695377704E657047736E467879486E5865352F795443394958436937737037696E50457956534D314C3355317A5333743455664A692F735744644D2F304A786C7947566376595334465870724D564D75737A614C4D697335617944624F62732F6879456E4A2B5A72726E4475634A354E5863706E6F6373546C7566796A2B5A304650415758436E594C7651736E696A534C7268657A4646386F2F6E62462F63717A712B70586D30705953314A4C667054366C4C3473307974724C2B63727A367641564552574C465861564135574B56545656544E5670316276315154577A4E56613150625879646656316250555A7A51674769496156713835586E7661714E33593253546156486164376E72714458416A347362375A70666D7952616A6C7235576864616D6D3777336939756F32314C616F66616F396F304F373436355476764F73567547742F71366C4C7661626F766472726E446561666F4C7533646A48744539784C764858524864322F3142506573393372314C765135393033667437732F336D2F652F3254416147446F676536442B344D61673931444B6B4E3368705747627A3155654E6A78534F35522B32505A7832306A73694E74542B536574492F4B6A33592B56587A614E585A6B374E347A745765397A3757665078673347483830595449784E6D6B392B664B46343475356C2B347656313735762F6F3846546E31632F723044486F6D5A5A5A694E753831792B76794E344A7672732F4A7A6432643135352F2F4E62793766534332384C61753742337534754A5336524C656376737933557255697433566E56586E37342F396E35784C586A743533725342386F507852384650743738705037703859626478754C6E304D38485839493247546472767370383764737932337139486244393831764B44754E4F3758654637344D2F624838732F7A79786939334E3378506336396F33327038354344673443436145456E3646416B6934496A77394166685341386637396E44753842514149724C664F635766676F534444775463326B4463304132454C314952685563746F3373772B64674533416D384235453973514F4A50616B546D5165354C30554535546D71517570576D6F6530712F526F4268354758535A76356C535746745A5A646879484E4B637A567970334638384B4878752F6B554363594A505176416964714C35596A486954784B77556862536D544C42736D64796F2F4C36696B4A4B31637679526570586E71742F56325451304E4432317A6D765836447A515864414842697947636B5A6D5237324E343078795465764E757333484C5659736436334A626268735A653330374F30642F492F464F5759634C33653636547A6B4D6B33343641613530336749654237784D76503238496E79766568583774386538444477646443584548516F51356877754571455361544C69614354385646703051557856624533546E5847396359506E5234354D355977666E596963654C63654E4A5938736A356F5175394B6264536D394E714C68616E58386F3465796B38307A504C4E6C7333527A61584F343879372B44795776364C677675463134734B69784F76424636314B3945734653366A4B6473725836775971577972756C4A397273612F31724A4F715A366A4164767734647034593164543666576B4733374E46693379726177336B54645832703630332B776F374478397936504C364C624548626F3775336666336876716275724A36593371633736763353383051444777395742367347656F61766A4377384248466F2F6C52356848447034736A443534576A2B5739697A6F75635734374154397850664A36526433586C3539465439466D4E6163345A6C467A79362F66766A6D3274796C2B5969334467736137775158715266336C3961585A316565726A3534333731325A2F3375683936507735386D59477636766B6E3156587A4C64447673572B484F7750664E6E7A7937316E7358396E7350446D443943344E2B4B4264686A365246766B4F566F734D77646C684648424D656A56386A6D69496549786B6B37534F37543935484D55413551765763656F466D6B3361666E7071426A31475A795A6F356D4F5543617A5662482F7337546A5158463763756A7A64764B6C386A2F356A416C684354384245525A3945457353727841596B6C4B59793067497932724B74636E487942516F7669734E4A7235613871574656474E5546315251316454517574593972754F67473659586F6E39474D4D34673350474A30356573623474456D38615A785A6A486D555261526C754657496459434E723632586E596539717750686D4C506A386550486E5279644856324F453578636E643049377134656E70362B5876376577543452766C462B702F7754417049433034497542656547464952654353734C7234796F6A71773755582B7949616F68756A366D5072623256465663576679563077566E73684D756E6A325847486375504D6B766D584465356F4A68696D71715A42725052627030585072336A506558706A4B487332356C312B546B3569626D68567832796A63716B432F6B4C6949762B6C6D38664F5835316536532B744B38736F547967417237537030717957725747714B6162375876367362717578736172785533706A52465866653559643973304B4C594B6E79547059304D396D4362486375643037656564673363766E326E2B57376476624C7577703773336F792B3150764A2F556B443578346B445359507051356E504D7837645056787A556A4C6B2B37526B61657A59782B66493862704A38516D645638515873612B4B7079364E543031732F75612F5933576E5064382B74756243394F4C30424C2F7376464B794772752B343631462B746248796B2F4357326F663762383472595A394456714B323737394C66346E5A6A7634543938667837664E643154337863375950796C66315A7746754B4147684761694F64494C78514F56513548776A755963717735446F6E727741664145656B4363546D4A4F78785A727045316B556453614647535555355431564248304F6A537374437530585854357A44344D4B6F7A4D544A744D412B786C4C484773466D7869334A674F4F5935753769797550313539486A5A6562663552766772424B494554595734686261466830554B525833466C4D527834684D535A5A494255677253514870514A6C3357536F3552626C612B564D46646B56647853616C613251754F55525A564B6C5864344A6A6B6A666F564455644E4A733258577065317258566F644A37725A75745A366C50726A78766B47646F5A4D52764E4869307A396A51524D766C6B326D3532796C7A626773526977724C4579736461326E7266357146746E703237765A514463486879374B706A344845314A77716E743835744C736D4559363769626969334B66646D6A2F4F654C6C344B33705465617A34506645763959763374416D51434B514D2F425930454E34536B685071453659667A5232416946695037543153655042766C477130657778367A467A734E653558632B4E44544A6D6545456A414A383266764A4F616443303479534F5A4F336A302F636145784A536E564B55337549746E46356654756A50784C495A6D475754785A2B396B766331707A4D2F4C384C75766E387857674368594B2B34757169704F762B4630314B3545725A5331446C3330716E366F59724779767171374F72306D725461694C726F396F434C305732686A52464873392B555A756332314C622B75624E6C5337524964625A386D74786475796479376433656B4F374E6E73532B6F58475667597644616338756A55534E4A6F35646A554F4E2F6B6856664936647A584B6E4D2F466834744E612F577254642F47767A79615A7675752F4875355550392F2F363264466777636742637A6F517A3145344172445541534B4D465141446D594377417749775541437446674E696F4149693250674456627631396630432F636B354B4F4F506B42754A77706D6B415A35672B4941616B67334C51435562424D6751674A6B67577A6732446F48536F4352714650694E6F45596F495A38513552434E694573376F524A42327943526B472B782F364F424D4C526256444E394462476862644362364D5159503531334A6D4345734D5A78683557426E63447934494E78745042357668362F4437784B5A4539555351385448694474493645676953563652717042576B7047516E534262494C636776303868523946417955565A524556486C55314E525A314E51306454524D74463230416E533964486230342F7A784447694755735A704A68476D48325A6B477A564C4A71737936787062424C734C2F6B4F4D4D70796A6E466C634B747A50324A703472334F423839337A682F746F4356494B506747364536345167526256453630585778667645536954684A46796C646158455A466C6B534F53433349372B7038455678532B6E6E4559774B6A5371666D724B367455616F5A705A57682F5962585349395258312F67334C446D614F4D7876596D563077587A635574546C6D4F57765059784E712B744A64334B446832634E7A6261634A466D3944704A755A6534386E6A5665556A3448766458794667494D673665445530507077686F75324531636C7630566469645539396A693839593345576C396954464874652B634A6561742F4631417A6254503673487A6C6A6564667955777239697332764B7058796C394E586B6C536A616B453975495A714972354233384A3355364864704E4F374B2F464F7862332B6E75583778414D53677A62444D59394B526E70483538663278756B6E52562B7154686E4F6D4C2B326D724E34612F684F64556C306858353166323375512F656E6F7338526D3065334F4C612F37417A394B4E723132316638355438517350364A415256674158784143716744552B414351734535554168756745457742333541744A41555A416F4651686E51446567353941334241767361583051323469356946623531744A42687941726B424F7835564642687148725541706F5662512F6E34424D59576F77747067677A682B5848426D4737634269634661345374343033784A666876784E5A457430674A69634F4A6E354F6F6B6853526B7043476B573651755A414E6B71755339354E6F557A5252616C4D3251336E71795055646E4275476B474C6F53326B6B365162706E646E4141776C6A47714D62356D536D55575A4A316C4F733471775473453650384B2B7756484636636846787A58476E63356A7A45764F2B3477766E39395A51464267533742664B466659573052566C463730693968543857614A584D6C594B51397043786C4E57566B3559586B6542513546446956755A63456A30697271716D5A7137756F7847726D617256726A326A7536484870472B74454731777A6E6A7A495A323842787A55747A4E677376793576574B42746232305A376A4950727364376A3345374A7A70384939713450334755396172795976624E386966325341314342696348596B4E517771764369534C3454725647613063396A50552F74786D6566455535346B4F6961424A4A4C4C36696E7645314C5368664B474D324D7947624A755A2F6E6C303954634C66493877723531633553516A6C52525775565977323674724865706D472F73657136385932764C5555334E6474574F7A4A757958584E336B6D384A394939336E7671506E2F2F7377667851324C447278396C6A7567394F5868362B316E4D754E6F6B397358457139727068466E584E34627A69677353692B4C4C3871754761783466556A37642B767A35712F5232334D37495439363938372F306A7752345141303467415451424C596743467741315741414C4546456B41526B443532466466344751596E515155516A576845666B454A494832513938694E4B426A376E673268617441653641304F4B3863423059316D78636469334F414E634B3534546E30574549596F6E2B6B6B63513378416B6B784B445A397152624A6E354D4555564253646C4F3555314653443150453052326A326150766F55756E744741515A3968676E6D5A715A4D316E43574F335A4E4F4862683457546A417642395950374B38384737776266462F3574675430687244434E434C656F6A4A692B754C504553636B6371526270357A4A6263737A7957676F426967564B67386F374B734B714C6D7158315A39716B6D675A6171666F6A4F6852366473625642682B4F71706D6E476D795A4B5A71586D4378592B566733573072594A66726744345737666A464B516A5758626A726E6E754B4A374E5873342B523734702F53714245304B7551354443463850584979704E4F305377784D36644B346A334F694352734A2F596D705A39335442464A336273346D6C454F6130302F6C7A5876632F3541595746783446574E557071797059714F71755161367A724F2B7256727255306E627367336632317462434E306B486432646832372F6533757857376D6E76492B7A7676354137674877594E6A773049506F782F316A61436571492B4750793064473379324F6F36596F4A2F6B6679487855766156374A5445744D414D30797A52374E66584D322B363538726D343937614C3069387737783773566937464C477375594A6665626161393935686A5756745A72333467393148326F2B6A6E38357661477A73664C372B78583254596650783137677479613235375576664E4C3574376C5239742F71422B74483630325758624C64727A333266624C2F74774F46512F3247653072382F7745496B6D6743675878386362504942674D304359432F7A344F426E2B634842586756734A444D4139506A2F2F6C2F7836363668414B44592B524431734871632F6F2F665350384E61784A36507264446F4345414141414A6345685A6377414143784D4141417354415143616E42674141424E49535552425648696337643150694E546D777766775A312F655779386975447944682B364146466B68533646463346566F38526B4B56664451496D5257684349396144584C347547484674534555756842696F3774526277734A6C4C526737414A6C4E3269594C4B795743674B536739436368755A67486A7837487434586B4E2B535361545A4A4C4A6E2F6C2B5472505A33646C6E4D7050765076387A382F37396577494141416E3854396B464141436F4453516D4145425353457741674B53516D4141415353457841514353516D494341435346784151415341714A4351435146424954414341704A43594151464A4954414341704A43594141424A49544542414A4A43596749414A49584542414249436F6B4A414A415545684D4149436B6B4A674241556B684D4149436B6B4A674141456B684D5145416B6B4A694167416B686351454145674B69516B416B425153457741674B53516D4145425353457741674B53516D4E42776971496F696C4A324B6141682F7266734167415552644F30382B6650763337396D6E393536644B6C637373444454447A2F763337737373416B445058645664575675376375654D646F5A54322B2F30536977544E67446F6D4E493372756F49676546564C626E5A3274717A79514A4F676A676D4E45686D586C4E4C6E7A352F763272577272464A42597941786F546B63787A6C77344541674C676B68746D33507A63325655534A6F476F795651304E596C74567574384E78615A6F6D34684C79676A6F6D4E494872757045396C615A704C69307454623438304651592B594861343332586759506F753451696F46554F39525935314D4D59362F66376945764948524954617377776A4E6E5A3255426379724B387362465256704767326441716837706157566D35667632362F77696C394E3639652B6934684F49674D61462B496D63524D63593054554E4C48417146566A6E556A4B5A7034566C456A4C474E6A5133454A52514E6455796F6B30366E73376D3547546A4934374B553873433051523054367345776A46617268626945636D45474F395241654A4348773435454D47466F6C554F6C44567371546A374D555A39386B5743616F56554F3162577973684B35564A786753512B554248564D71434C444D4536644F68575A6C51527843655642596B4C6C644C74642F2F62704159684C4B424661355641686645416363516D5668635345536E42647439507048446C795A46684C6E47422F446167414A43615554394F3032646E5A3846784C503879376843704150796155795847635938654F6A5A776B684C6945696B4164453072444A7738684C7146476B4A6851416B33545771315765426D504B4971424934684C7142516B4A6B7955347A674C4377764C793875424552354245485264662F6A776F66386734684B71426F6B4A6B78505A444B65557171713675626B5A6D4C4B4F7549514B516D4C434A4D5130772F763966716654436479724233454A31595378636969575A566C6E7A70774A442B38496776446777514E2B4A334847474F4953616746315443694B347A6A646276666777594F527A66426E7A353778754F78304F7634666F4A52716D6A62686F67496B684D53455169694B306D3633772B73644A556E71392F76646270642F47646854485973676F654C514B6F65633362687834397935632B486A6A4C47624E322F7965695748754954615152305463734F4864384A7853536E566458316A59774E7843585748784951634B496F794D7A4D546E6D564A4346465674642F76662F3331312F36446945756F4B62544B382B45344469486B3563755862392B2B4A59537372363937333372343847484D666A78705555712F2F504A4C37387646786357644F336353516E62733244452F503838502B71747952564D5535664C6C793548666B6D58353071564C34654F495336677633426B744E64643133373137743757313965725671332F2F2F546666514D79526C363137392B37647332635049575278635A486B6C4B654F3431793865484859527061694B50373838382B52667768784362574778427A4E635A797472613374376532584C312F473730685749344967374E7533623965755866763337302B56704D506D5633497857556C437434524558454C397649662F4E68674D6446337639587268585345616A31497169714B7171725A7468382B4D7171714349417A3758566D5734302B734C4D7542767A555944504A393432525A6C695170333664746B7346677742677A54625073677451593670694545474A5A316A2F2F2F4C4F317452567A7634516B654D68366659766B51304D34494657376D48634342413575625731356A37302B30786376587552374E31704B3666486A782F6676332F2F71316174686E5A575530744F6E54306632562F7146757A747432383672663242746263332F3546375631624B73547A37354248565954744F303565566C2F7467307A61576C70584C4C553150546D35694759667A3939392F3337392F506B444B3869354433442B62594F5A676A2F306A556D7A6476654D4C6D47366D55307174587233707A30574F45347A4B584B7A5A6D4E324C4732507A382F50587231374D312F4233482B65696A6A394C2B6C6D455974322F66646C32336D6B733857363257312B457569694957566D5655646956336F6D7A62377656364D55334C5955525237505636757135484E6C647278375A7432375A565665576444355453444A2B6358712B5835477A6F7568373478567861685A496B4A53786E7175612F7171725A6673742F44686C6A575639574958686A3348394F644630767531416C34356441686F2F695643536D626475794C43665068666A75764B6179625474623179316A624E69354D6B307A384D506A782B56674D426A32566734376E7552394E45307A384B2B55556A727974337139587551667263346E787A544E51416B4651536937554A506756517455565A566C57525446794D7042326E39765455354D487054785637746E436950537738644D737455302F53696C6B695235395A64775849356674516B2F4A2F6D77746364674D4269572B5046766138794956737776786E793041765854775742676D6D61713138346251386C2F66706A4945364B7136766A50584471766B73677A6B5164696F437164554B702F4951314D7A49513153752F796E75616856563358347A396B676944775769472F68704E2F49734D5A4E484977666154425942442B5139373148314F32794F437A6258746B307A35634146335852396245765373775547315065415A73322B5A6E62357A576661436A594F545A71425437412F5544385950344D35394E7167526F5647496D36614D5542434668423179446D6159354D69786B575235326C744B6D4A794645464D5578797878756A4450472B4763397070334F4253364A6848335A505068344456475735655464333779786B71312B5A3975322F2B636C535570376F6D7A626A6E6C724A742F4861763833316364724C4975696D47463059557938387931744E314554566B6C616C6E586C7970575264377465585633392F50505070336D756957565A642B2F6544572B45377364505647415A654D4463334E7A5A733266506E6A314C434845635A3331392F65624E6D2F476A3848667533484664642B517A782B6832752F3631565A496B586274326A526667774945442F6D3952536E2F383855662F686943376475336935587A773445484D353454503676646D6D413047412F2F34386A43794C44392B2F4E6A2F744E7662323934386E6F443139665834325156726132767866793647347A692F2F7670722F507337507A397647415A66797A752B376531743133583952334B66346A594F62396B626E2F446E7253516561325A4C516639594A69424A3778746A624D71484252505742796D6C765635767A41364B6B5731384C6B507A764E667242643557667477623276612F6B4D4667454B69706A53524A457139514A2B2F3439722B51354633414D56316D746D337275683534716F53396D5A6C483765714F3130386C53654B5656744D30655530323751637375566F6D706D6D614933766678722F2B3634753366524A655170496B356255495A4754724F504233453336794139325858756845337172584F774E4A796941495171435A505049584B6157794C5076505747547671762F6E7731324B336B5565337944317568306965654D6532595937716B38514248352B5A466D655743434F564C50456A4F6E504A68382B7A64505A52326D615A71712B78647858793058324D2F4961627378624A6F726979506372304F557153564C6B792F5433304D566E642F7A6E4A467A52383261624463757659582F4C7179466D437A566579504141534A4A2F5378577363764C543650467930422B46316239346137506D4A325A584D554B494B496F2F2F50444439437A3834707544384D3254557133734645587878496B546D547354597753362F414A334E347676516F3166676A497A4D7A5079722F64365064367636676C7642533849776A666666485079354D6D5233566A65757033467863556B6933384D777A687935496A2F5347436E4F396431662F7270702F6765786C7A34463249464C686E47574336642B487A336C736876426459455A31673356514E6C522F5A6F3852506663706D32566B47384D3835667863673833597778567654306745444259675A6B5939374E5966326238612B4F55687054576662714C2B4F2F786E68382B6F462F4F6D725979486F66722F786D6D786A4C6D2F39467630796F644749472B7676394D6B774C7144492B687A547A677357774361536B4A784358535A624B444F74646959792F6D496C5164637949774177625068584A2F30366C6D76544B312B38323656716F75497132796733444F485871564F5445446C6D576B7A53736173523158554551787479576D452B6B4F487230364F4C69346952507A6A673742477561647637382B63414C703554322B33332F456464312F2F6A6A44362B4A7A526737644F6851777A34444162775648356934342B324A5663334E58365A46325A45644644502F647678464939575572563770395A325832466B65726C316D6D4A3851627164482F746A557A6E794153716C5748585059384D36774F3859306732565A4277386548505A64336D485075397635464E794B56433779766639457439766C51316A596D4232717243714A4756363577596D69654F336174635A6650355A6C2F6658585835586462544F73694E76314F4935542F52634F5536345369656E564C2F776F70666675335A756543554D3167727562776451712B58376C686D47305771317758504B6258434D754B77687843644F737A4A3034497175576A44464E303341465668506945715A634F596B5A3257744A4B6231313631595279314567463931754E3744787A354D6E547843584D46564B614A557269744A7574774E784B5970697639394858465A5770394D4A4E41684D3038524144557962696459785864634E31314E517461792B51474F63345061744D4B306D6C3569575A5833373762666871695875416C7078694573417A34526135597169484478344D4243587571346A4C69734F63516E674E346B365A6E676C6A7941496D3575624744536F4F4D516C514544686463787758457153394F7A5A4D38526C7853457541634B4B72574F4734314A563166673751304870584E646C6A506E76623456356C77426367596B5A6A6B745555716F767650556334684C41553153724848465A5234684C67486946374D51523372344D63566C3934656C66474B4144434D692F56593634724B50777578613474526B416B4E78623561377242693438565655526C78576E615272694569434A6E424D7A634B3936575A59784D6C3578697149734C792F376A346969694C6745694A526E596E5936485838766D43524A4462375652444F45422B686B57635A434C4942686375764856425446502B475A4D586274327257386E68794B454A366A33757A374B51474D4C352B78387343344161616B564638344C6E5664787735534150487953637857712B56766A3975326A5A30544B797679397569597A774351524137396D49487553313358455A65565A566C5765493636626475495334416B787533483144544E3337675452524574753872534E4330774C49372B45344255786D32562B39766A6C4E4A2B763539487153422F4B7973723136396639782F4270457541744D61715979714B346D2F6650586E795A4F7A7951434843347A7A592F523467672B7831544E64315A32646E765338784D615761496D2F62326576317A70343957316152414F6F7265324947376A5A65784934654D4B5A777879584273446A4147444B32796833483863656C4C4D73356C516479452F6958526A444F417A43326A4C4F4C31746257764D6555557254484B38563133595746685542634D7362362F543769456D41634756766C4D7A4D7A336D4E30696C564B654E3832677663494943645A367069475966692F784B565948667775782F346A6C464C544E50456541655169537A2F6D68517358764D65694B4F5A58474D6A4F646431757478755951735159307A514E4C584741764B52756C54754F3032363376532B78684C774B776A6563494952496B6F54746F7744796C62705637682F7A4551514263566B3633684950784B57753634684C674E796C6270586676332F66652F7A3939392F6E576868494C62795942314F4941497154726C5565574F637A474178775A5A596C636A4550316A344346437064712F7A703036666559306F703472497369714B30322B3141584B7171697267454B46533656766E7432376539783664506E3836374D444261354A67347066544A6B79666F5577596F57726F36706E385A79654844682F4D754449786747495967434F4664695072395075495359414B793738472B652F6675484D7342493632737242773563675174635941537052763538532B4F78475A4645784D3579434D49776F4D4844314331424A696B3748564D3133567A4C41634D632B50476A6641676A79524A7A3534395131774354466A3250646A667658754873664A434452766B75585872466D366D4246434B484F346C43555749484F52686A44312F2F687878435641574A475956646276643843425072396662324E684176523667524F50656652667946546E49672B6D5741425752726F3770333974746132737237384A4D753557566C636842486B7933424B694964496D35642B3965372F4832396E6265685A6C656A754F30577133412F63517070646943434B42533069586D5A353939356A312B394F68527A6D575A5670487A682F687465544449413141703657617742375954786954324D55583257684A43564658746472756C46416B41597153725938374E7A56464B76532B78506D386333573433736D6F35474177516C7744566C4870323066486A7837334835382B667A37557730304C54744A6D5A6D63446463516B68717170692F6842416C61564F7A4E585656652F78363965764655584A7454774E357A6A4F77734C4338764A79344469716C6743316B446F78352B626D2F484F4D4C6C2B2B3744684F6E6956714B4C376B7364317550332F2B33482B634434696A61676C5143366E764A666E2F762B626278496778747247786B562B5247756A476A52766E7A70304C48386674486748714A654D7153565656766365626D35746F6D772B6A615671723151724870534149746D306A4C6748714A574D646B784379734C446762324471756F374A673336575A5A30356379625142696659664169677A72496E5A6D42754A694845746D307335694F454F49357A38654C463846413449555357355575584C6B322B53414351692B78374638334E7A5A6D6D36543979344D43424B52384663687948442B2B453431495578634667674C6745714C5778646E7462576C71535A646E3738765872312B3132327A434D7355745650355A6C4463744B336D577061527047777748714C6E7572334E5070644D4A334E357965355543617076337979792F682F6B7043434B5830337231375330744C6B793856414251686838516B5561485A2B50454E7833485731745975583734632B56314B36645772567A456A4861427033756645337A7A33694B4B59312F4E58683671716769414D4F352B55556C56567979346A414251696E7A6F6D4E32776E6E6D594D45467557396474767630574F67484F4349507A2B2B2B396F67774D305765345A3746394436616C767A63753262556D5334732B684B497132625A64645567416F584A353154493968474B644F6E5170584E676B687369796650486D792B744D324C637536652F667533627433493138464A776A43662F377A48335257416B795051684B545578516C5A6D446B394F6E5468773866726C5162316E4763396658316D7A6476526735382B306D53744C71365776336342344238465A6959584C66626A656E374934517778723737377276467863584A42354472756B2B665076337A7A7A386650586F304D69554A495979784B3165755643726C41574353436B394D457274714D49417864757A597355382F2F58543337743146424B6A6A4F46746257363965765872382B4846674F6C523871565A585678733855776F4145707045596E6F4D7737687734554B5332707848454952392B2F62787834754C697A74333776532B745750486A766E3565663434664376674E322F6565416554684857594B496F6E5470784155414B415A364B4A79664732384F336274374D46576146455554783639476770585151415548306C4A4B5966587A6C7A2F2F37395642585048416D43384D555858337A313156667A382F4E495351434956334A692B76464F7876583139526376586851586F4979785134634F37646D7A4278564A41456972516F6B5A7876654F652F6E7935647533622F6D52396658314A4C2B34642B2F6550587632384D6366662F7A7837743237435348495277415955365554457743675573626148784D41594B6F674D5145416B6B4A694167416B686351454145674B69516B416B425153457741674B53516D4145425353457741674B53516D4141415353457841514353516D49434143543166363558636668387A4479504141414141456C46546B5375516D4343, '2015-02-16 09:56:22', 1),
	(2, 5, 'tesr.jpg', 'image/jpeg', '5548', _binary 0x2F396A2F34414151536B5A4A5267414241514141415141424141442F3277434541416B4742776748456855534278515746524D58465277594742595547426757487873594779415946786B5A49427765485367684768307048426767495445694A536B724C6934754878387A52445173517967744C79304243676F4B42515546446755464469735A45786B724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B7973724B2F2F4141424549414B41424F774D4249674143455145444551482F7841416341414541416749444151414141414141414141414141414142676345425149444341482F78414249454141424177494542414D4443515147435155414141414241414944424245464269457842784A4255524D6959544A786752515649304A53596E4B526F554F53736447436F734843307645584A564E5567355068347641574A444E45302F2F454142514241514141414141414141414141414141414141414141442F784141554551454141414141414141414141414141414141414141412F396F4144414D42414149524178454150774338555245424552415245514552454245524152455145574E69466653595A4736584548746A6A594C7565383241482F6E54716F6E6B7269546857636169576E77794F556547336E443367414F6143473330506C31494942334875515456455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542616E4D7559734C7978435A385965474D476747376E75364E6150724F2F77413946695A7A7A6868575434544C6954764D5166446942484E4965774862756467716977444C654F3857716E35646D636D4F696162527362635862663249373744547A53626B2F7742554D666C7A4E78707162757642683854744E7931762F7743737048776144307672646555387134546C4F455134517932334F393272336B66576337723774414F6743326442525575485274696F474E5A47775761316F7341466B4943496941694967496949434C6A4A497949463070416141535354594144556B6E6F46566561754D39465353664A38715247736C76626D462B532F5A6F414C7050685964695546724C58592F6A6D485A656864506938676A6A62314F3550527252753578374256444678757866436E47504E4F486C6B6E4C7A4E44656545323174354A415459323376304F6858504C47566357346D546A4573375862533730394D4C674F5A2F467244596137763330467268326A4E47652B497A694D6E732B5230594A486A79614F6430506D736466534D61486479324E5077677248446D78444661743033326D4F6341443858456E38777252706165436B59324F6C61316A4767427257674E4141324141304158616771334C6D4B5A6A79625877345A6D6D6235564255412F4A716B364F446D6A566A726B6B3632466953664D33585567576B717A34732F53563243736939763561484333325775694C7A37724B7A45424552415245514552454245524152455145524542455241524551455245424552415556346735336F4D6C7763395235356E67694B49485678376E7377645438466C5A347A58525A50705856465A713732593437324C3339472B67366B39423846562F44764B4F495A3471546A4764664D776D384D5467624F736444796B36524E364E2B73626B2F65446A6B66496D4A5A356E2B6463396B6C6A6A654F4533484F30657A703953496447377533324E335868464779494273514161414141425941445141446F4679524152455145516D32362B416737495071497545307363414C70334272527558454144346C427A57506946645334624736617665316B6242647A6E4777415557786E69666B37435038413561706B6A767377586D2F5674326A346B4B6A4F49576538537A394D4936426B67705747374957676C7A6A3974346263463359616765757049533272784C4D50474B706442684C6E552B4752753837374563772B39592B647833444C3247685064542B6B77624B7643756B6B714774735774383072374F6C6B6430594362574A49396B574858755643634D346A34746C366D45654734464E4654784E76636D5777413163397A76422B4A4A4B31744257597A786D7259666C73506834665475764B31726A79334E7A71545975633677626F504B4364726B6B49726E576D7A466D4F46324E593041794B535273554D5A4A767965636A6C48324259366E32695356365779713537364B6C4D6F7334303052494851386A6271732B507046517A44384F6F67413657636372514C41574169594C44596653322B4374796E696254746179505A725130653443775164694973664561326E77324A38315965574F4E6865346E6F316F75554665596F4859356D576D6A6A4E3255564D36562F577A354C744454324E6E4D643843724C56623847594B6E454756574B34694C535673354C5276614A6C327441394153572B6F61315751674969494349694169496749694943496941694967496949434969416949674C47784B7570734D69664E584F445934326C376E486F42716666376C6B7147635863487848484D4D6C69776F674F426139776351336D597A7A46747A6F4E67646579436E59735267346934692B737A584B324444344E655237726554366B54527535377258647936372B69334F614F4E4E5A554F62545A4669384E7477786A334D446E753261317249745130644144636E54514B70634A777576786D5673474678756B6B636447742F5539674F354F69394A634D754746466C494366454F5757734939726473563943475836324E6937667462573454544C7A73526653776E47774255474E706C41745950734F5961616239744673455241524643754A326559386F51427447412B736D38734D646964394F636764416468314F6E6577614C69356D4B71726E4D77584C6E6E71716D776C494F6A497A71515430754153657A6239777243792F686A6346706F615A6A693452524E6A356A31355142663946454F466D534A734261367378342B4A5831486D6B63377A46674F764A667564334564624470637A3942442B4B47636E5A4B70424E547344355A482B4847486579484546334D377151414E687663624B75734E3465356A7A7330567566367438634A48694E6A754C745A6139374879516932757850634257706E6E4B64466E4B6C4E4E576B74504D487365335573654C67473355574A424859394E4376503162555A37784A383244304D386C584843537877693553437868734C76747A6375777335317236494F65415A446F633459692B4C4C4C70426838524850504A7135773638766C41356E473967526F4E5432587048426348772F416F5777595647324F4E6F73413372366B37754A366B366C556E6C4B75346C35517032307444686A584D6153376D63787863533433314C5A4144323977437A38547A3578546F6F337A5647485252784D424C6E474F513872527554394C73423173673376477648366C6B635746344E7255316A6730744734694A3566687A4F30763244314D736C3562706371556B644E53324A614C795041747A7947334F2F346E6273414230586E33414975494F61367334746738666953746357695233684E59337938764B3173684173477536583337725A5A357A42785377426A506E2B63524E6D446D675265426653334E717763774E6A7544315153504270502F586D5A483145666D707146746D4863457475316C766649585042374E43756C6566736D634C7339556B596C6F4B7474474A6D744C6D68372B653270627A42726258383231394C6C62352F43584E64556231754D796B2F385633385A516775506264564A78797A46464D324443364F566A58314D72504765534C4D6A3567427A472B674C74666330393171636134514D77366E6C714D657853567A49347935784D5A36624457513375644C6453516F397770345777357668665534772B534F486D354978487967764939703133412B55626144652F5A426474426A325573426869703471796D61794E6A574E426E6A765A6F412B31757673756638414B4D51753675702F364D6A58666F4C714E526345636E4D396F54753958532F794157557A67336B687538447A373570663748494D352F464C4A4C4E36786E77624966344D5749376A426B5A75315554376F5A2F3843375965457552347471514838556B782F76724962777879553361696A2B4A656634755159482B6D50492F2B384F2F354D332B4259353431354D477A35542F776E4C65733463354F5A7452512F46742F346C6376394875542F77446359503341676A6834335A4F377A2F38414B2F376B5047374A7746377A2B3777762B36796B4A34645A4F503841394B4439316358384E736D50336F6F666743503446426F344F4E65554A6A6138342F464750385332444F4B75556E2F746E4433734B376E384C636B76336F32664230672F6739594E56776B79497870644A427941416B75386155414471645832515A6A654B47557A2B332F716C646A4F4A57566E37546A386A2F4A55786D7948687468334E48676A5A716957397278794F35422F534F2F77756F78686556735678312F2B7259584E6232753533366C4236656F4D34344469424461615A704A36624C664167374B6C4D706349712B6C633253756B35534E6244557135615742314F787241623246726C42336F694943496941694C575A697837447375514F714D57654752742F4E7A74534774485678747367376361786567774B463952697278484577584C6A2B6741334A50514455716A7356782F4E484632633075585775686F5162506337515737797547353752742F573178786870386634315666695646344D4F68665957366477336F2B5569317A7330483341336C676D4430474251746777714D52787447674855395354753578366B366C42714D6A3549776E4A6B584A683435705841654A4D34655A352F7574767330667275704D6949434974426E584E6D483550707A50694275646F3477664E492F6F30656E63394238415178732F5A31772F4A63486956666D6C64635251673676642F597764586450556B41777268626C577678796334336D2F77413030683571646A68594E47776B355473414E47447435756F4B3032523871596A7848717A6932634239427A66525245477A773332576748614966316A663156366742756A646B48314557486A474A5532447753564661625278734C3348304851657032487167673346724F732B444D625134426431665565566F5A71574E63626333346A73337471656D753334615A4E677964534E6A4942714832644D2F65372F414C495032573744346E716F78776B77796F782B6566484D616150456E6357774E4F764A47504B534C2B6735416577643356716F43726A6A746A45744468347036532F6956557259674276792B303633653967332B6B7248564F38554D567735754F595A4869377779476E615A33756465774A4A633061647A433338776773504C394251354C772B4F4F64775A484246655235327637556A7669346B2F6B465657586F4B6E69316970727135686268394B51316A48665749387A5748707A456B506676705A76597270787A476359347731516F3876683057487875426B6B634C582B2B37386A795239547166753354674743304F5834475532474E3559324377376B395845395845366B6F4E69694C684F31373275455A355845454232396A30507751557078677875737A585678594C6C36377250426E4C5263632B6C6754396C67504D377065335671754C42734D707347676A703645576A6959474E486F4F70376B376B3979564375466E4479584B426D6D785A375A71715678484F336D4E6D5875645841457563645865344B545A707A4A545A64597A6E6136576156346A68676A747A79504A41734C364143397934364434684275305245424552415245514558776B4E314B706A4F33456E454D78532F4E7644734F6539784C587A7330754E6E63682B7130645A4E50546F534571346763557349796C654B433039562F736D75305A2B4E33316677372B36393157464E685766754B6A684A694A6448536B676A6D764646626F574D336B2F467237315063673848384F77506C6E7A426170716661736459324F39416662507137344162717A77414E6B4543797A776F792F676742714234372B3778595839472F7A757078543030464D4F576E61316F48526F4158636941694967496949434974466E484E4F485A52703354346B66526A4237556A2B6A522F616467672B35767A5468755571637A346F3730597757356E752B7930667850514B6E734579396A2F414263715257356C4C6F714670496A594461376673526A34446D6B4F2F54617A666D564D763476785A712F6E444E563230624351786775304F742B7A5A32614437543979644E2F5A767143474B6D61316C4F304E5930414E6130574141304141477751646548304E4C686B62496142675A47776372577430414379455776782F4771484C3844366E453363736242633979656A514F7269644145474E6D764D32473555703356474B7573426F316F397037756A576A7166304735564F554654784134707A654C5179756F714E7272427A487559305736417473365A2F35432F325678775043635534785668724D6576485151754C574D61534C6A6677322B704675642F754174707979484E76456755376D34587734694573397644612B4A6F4C49376157594E6E45446478387266585777537A5058454443636C523874533778616B74386B4C534F59396E4F4F7A47333639646241325651354D777A452B4C65494F716379754C7165477865317432747354354957612B5548556B37324231755156427332596257345A565069786155533146775A584278665A377453307650744F414F7662626F724B79396E575443366550444F47644D366565313561687A62423068747A50446533514F655141414E43677536737844434D416A614B3253476E6A41445742376D524E734E41317479426F4F6757727A526E6A41637455346E713557764468654A6B626D75644A2B477831476F7537594B6873373850732B387A4B6A46772B736B6C3050676C387A6F7A75476B42766C627270792B5561376158345648434C4D4E4851535674614F57526C6E436D614F64355A6341754A42733067486D734C6D773662494C72345935366B7A7A464E4A4C4234506879426F73376E424246787259616A72703143686E47724D6B474B5646506738456F59783872506C4D674E77323767474E50753973672F645663344A6D4C4F474834632B48416D50697051357A3561694B4E774A35724E494D757A64674E4C48625653504B6E4269747A485352316456552B432B587A7459364D767577374F4C75636175486D3232495157726D6E4F4F582B48564D79474F7A70475268734E4F776A6D49417330752B7733753437363242575477797A672F4F74496169614E73623279756A633170754E4131774F75757A782B5257747970776C793367506E72472F4B3575723667427A522B4750566F32363878486452657634525A677071695A75574B305539444F376D6377506B613575353565526F733843396864773030507145747A76785177504B3134346A386F7164684445516555394F6433316664716652564C6C7642612F69746973306D5A4847494D6148534D594330686F73316B6251362F4C33756239653674374A5044484C2B553753527438616F48376155416B48376A646D652F552B71317554347A535A67785A6B6D3734345A472B726243354878645A424F63457766443843686242684D62593432374E6233366B6B3675507164566E6F6941694B485A793468595A6C772B425367314E6137526C4E44356E637832357241386E753350627167323262633059666C57487863514A4C6E486C6A6962712B522F52725231392F5261624A75586136535A324A357031725A57325A4675326D694F306266766E367A76654F704A78636E5A5278476566357A7A75524A576B66525244566C4F33733058493576573574334A31552B51455245424552415245506F67702F694C6A754B5A79712F6D544B5A7330482F414E334D4E6742626D61543059322F6D3675506C37336E2B544D6E59546B2B4552595932376A376372674F6435395430485A75772F566176685A6C4B664B3950496355414E584E4B39387A7751376D4163517A55644C656133647839796D714169496749694943496941694C58592F6A56446C36423954696A75574E6775653550526F4856784F674344467A666D664473703037716A456A6F4E474D487450663061332B6651584B70504C7543347A78677254575A674A5A53526D334B323446742F436A2F764F2F36573663506F635A347834675A712B38644C487059625278333059336F5A48626B2F4859414C3046685747306545524D677735675A4777576130662B616E31516474465355394447324B6A614752736147746130574141304143376B5241586D7A69426D3648507465796E664D49634F686562794533424130664E5958356952634D46756F376C656A366D49547363776D334D3074755055575645595077457276482F317A55522F4A7737396A7A4637322B357A624D4A48713633727567362F6E5846382B6375465A426A4E4E6873514448796D344A5A314C336236362B51486D64715364546132636B5A4A776A4A73584A687A6561516A36535A7738377A2F414857396D6A54336E566266413847772F4159577759544749343237416454314A4F376E487564566E6F4B4B6677517854456136615446616867703379766B356D584D6A6735786461786279744F75704A4E757856775A637937684F576F6844673054593239534E584F50647A6A71342B39625645424552425858464A787879616A77656E4E766C456F6B6E74307034764D6664636A543161724368696A6761316B4973316F445142304130412F4A517A4B4545574C313964695273527A436B68503349726549523647532F37716D79416949674B754F496A4A737331744E6A564D48474E672B543162572F374635387237656A6A3853474B7831776E686971476C6B3751357267513572674343446F5151647767367147747063516A624C5176612B4E34356D7661626768527A4D6E45584B32584C69757147756B48374F483652312B784130616678454B50346A7758774B6F4C685254314D45546A6377787941732B4163442B704B334F5775474F564D76454F7034424C4950326B397048584778414935576E3144516769337A396E6A6948354D75784F772B6A647655795835334E2B357364667566766854504A75524D46796B4C3054532B6433747A79655A376964396671692F5166472B366C4349434969416949674969494B797A4E6E544E2B57362B54787146302B48446C355877736358415742632F6D467863486D484B344459616A63325454544E71474E657745427A513442774C53415266554855483058596941694967496949434969416949673479794D6842644B514767456B6E514144556B6E73764E2B6338775633464C456F365442622F4A32764C5977646A612F504F37734C626468366B7177654F754D3164505474704B4D48366632334471304565572F7633585067646B357542557A717172623950507466647351324137584F70397A5545357978674E486C716D5A5455413872527165726E66576566552F77416830573152454245524152455145524542455241576C7A70693773436F61696F6A39746B5A355056353872422B385174306F706E696A2B64354B4B6C4A3872716B53794E37787767764950707A636F5162504B47466E42614B4343543232786A6E506552336D656633695675455241524551455245424552415245514552454245524152455145524542455241524551455245424552426A314E46533164766C54477674747A41473335727661413057626F4639524152455145524542455241524551455245426173554D4D31595A7A666D6A68384D64764D6564337830483572614C344767626466386B483145524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245424552415245514552454245524152455145524542455241524551455245482F2F32513D3D, '2015-02-16 09:59:18', 1),
	(3, 7, 'lucyw.png', 'image/png', '4944', _binary 0x6956424F5277304B47676F414141414E5355684555674141415259414141433143414D41414143746243434A41414142506C424D5645582F2F2F384141473041414763414146384141464C372B2F73414147454141453841414667414146774141465541414648323976674149483841474876313966594148486B414A346B414D5938644C6E6F4141484C73376641414147554148336B414B49594149494D4143573841463373414C597741496E3441474849414548634144323041496E67504C495141414563414432586E364F7341474848683475765932754F596F4C7930754D51594D6F5141484673414448576C717345414658664E304E7579743877785249322B77395941445667414847734149347741476D49414C5A41414E59313567715541473234584B58494145574F656F72572F77637835674A71506C4C4A54594A427664614D75526F6F6550704A44576143677163714A6C62303652327864596F4A6C625A6B4149467376506D7033684C4E56595A645461616B41443143496A5A3878544A5938574A5243546F536B714C564B58364A4C57494D624C326472647170485433754A6B713531664A4971525A596A51494D514C5734574C474131526E3961596E78736435736C4E6E344D4B48644E574959414B4767765058556752556677414141527A556C45515652346E4F326443562F617A4E62414251575847674B6B6B5951316D52704943495445694A6751515462724E56536570324C64754E62724339462B2F792F77547253324B71424151414835642F6C56494852794F4F764D6D5746756273614D47544E6D7A4A6778597753674B49726A44507265777867666D506A3335746D366657324E637137505A38354F47336E6D76596630336A44785A6E6F746D6F32474B57725A616263376C3366574E374D37792F2F4C34653839737665443354317A5A714E52796D376F6E43534B7356694D4651564F33566E6279615258646A2B6D797543354D796F61446474726E4C6A36374B6B59647A47667954687A377A4B75643057386357617A564C6F675078664A62316A646D566B362F566957684F59304B6875314E385358587352716D637A53427A496B706B6E52744F326E35486A6C6451366F4D4764764D714978674333616F46424B62432B767A533374374935344F4F4F42654F4F6E66616C43723762527A4B783841444F4B3658343654484E647647774848417562333063346E724741675A70437459532B72766D2B634471693059774C4B6B583757792F476E6736775339575244475A4D634F514D4F6F7A4A66562B484C71794D5944546A5170794B7574336341426669557977573857654B74685547756A532B636A766B7759774C364B6D6654716B447076476E6D3833686A6D5A6369464E307042596238474A325A546B2F314E474D4361684F30573570344D73504D742B474F4A697851597A36625072675A664350356158344545637A4A6A6961535272704E314E3578492F6C7A42516D63394372704F6F577071783335394E6E7231585A6B34656170464F4465355535356E52394371635652434F554F72597775665A394B624F7354353275364248366333394634525079326B34364F335865567452416F446134717551766E4A717A4F48554C6159326B4656574A6E31475A64573371736A6A38324F30754435725734726C734E453170466C7A316D424933517038484B77766E56735869576A62724C453664707343774845484B4130322F34704A75703961706E5A376E655363495667736C53774E6378334133746E435557742B64516B57424276535A6A7651394134644B366871567A61367636663150336B30456A5969723170384A72497271745332617A564A2B505439317564733944743056366365416D487A6A326B6252644854747543424F71557A675866346B79463674414D304C5259794B2B7569775031795370336E39585979417A373149685245466E5936346656424C714768546D734B7738786A684D776938747177636B775456754A4D496E624A6471304A507939415454534E41744C6F766F614A736E6973562F6136517A2B304C702F7A6C456A6639456F476752635264362F784D544334552B596A4C37664F3533616D5555533949624F3872304A4D4E5978414239636B6A447079564261355570714641364A44506C30723664465671367757626171437A64543255646967725359587A6369546751704251434546636B57537156684445615134326E636B6C655242624657564F4C35634441566549684E4967335A4649692F395138766962654B476D716567756E6765456934544B51554235424A49623952496E69387A55356D636451574F694B48483768335865465442317732634132754F4B424349495879384A6B687A37515034445A30525A4B687A574E7A61675670416B516B4151442B4969655A36503147414345767334306E4241785A416C54693174494147534A48345449694F7555486D6A706E4B434A4C4A4962356E74564944485A45453935774E6270436D4E45454A34344438694C6C41374C4843432B47674C42386F444D4F6A6B35455342796D714E4A7A3065416742416B4B514C43714D4F6A555351593269374332567070474A424B737A756759575276683335722B5549516F41676442384270467A616C3251576638466C7342476B63326262342F2B326C4A6B662F34556774706C307777416249452F324A6261585A435075497178495A646575335834612B2F4B3547514768414C4976395A78392F5343446453762F3334366D375978397836325970434F466671706131514F73364D704E56744F4D437774763844616772564A665335744E6768686B66762B42437967567A586A7574385478647A5576637733492F63477652712F5447715A707A2B33317634754C5079794E36703178744542676B426262332B415A5138506F58323078376B7431595A49586952674D4243777345624F476F576D5533763745667A386C4A726A63786D6B43574A68797A4E6B4E7A624233326F534954724B75734D426A4A5151314B61677178314E584D624175346E44777139466657533172544872416155634D57456C58386A614439672F59347A484F734943776B4E7275326A432F506F587A4D6E455344473542374C5850734531666B784D4D4971526E384E5357737873704330324734777548444F3574325A39527732366848335638345569502B7671724F744F7747646D665578655654527239574244627548685565655A7047764D2F33697946647139394848454C4F7966656E6761353162554D63736863346247424D4471563966394E5756552F5A7467655434546E6C3561367A62546B6C6859574A3067757865654C7A482B52576C51345465332B2B646B682B41304D2B794D577355566A30654E483033444D77664A746C2B3077717866727439575669616B4230427152374B67724B4A644B75714551614E73667835476A6141777A497239747874483061356A2F6B555478356E4C6D4E744F35564D597A6D6476623238564A4356644D53456C3254446A6976684350595267662B5A4F35696A55336649422B3241534F6137526D32503461474E35593072544D6371506A6E54764F45726533695A564A735345323467313269694C4D73597548594344796343663563674241716151653343757247526A3131344451676A326A5861313332614F48486B4264796679616C4834674D5244633648516A655238777063496E483078454E4E77416739727A527A7361546932642F754E726D61592F713131316D3150493663755A52474B354F5247375175416742584B72314B6E356F4A47384538704449377444634C75686D444465384E312F33714A4B3056704B66376A4E574E4E6D594A7042505645564A73374578447A58624B3274707A4F4A323356394D6C546C68373151494D6D4F365970365A30436838723079344149494161532B77575075736E6E667247716B6A4564365935356B6F3046566F5A36345773666D7A767A38656A5272614E71744267336F515938596E42556C4B632F6D476D704431322B4B2B73474E66763372576A2F514433596238626A59307A4C57364744734741303670375A4E733165422B4F31716D66306B4164773171576C674557484F6B56643546773156412F5066793877685869664E69556F746666507366705A76623639756F6269754E417944596C4631766479793257312B5078574F6874666833396D646148596E6D39314A5A394E7049353247376A717A7337342B37387A634E446E787664626566744159543359497A4137786D4F5A35344F4A4D3432494B4942446B69624C453051625063384A31306B306274417644744F7964304E68434E6D58414B684871797350384C59367A724D684B41746543597447754E4367554447724C56546F616A667243564A69692F50616F317271364B645A30746469456E44614C70373875546A504C356D4639363944656F4E4E616436376366492B2F673278794E4161656C33654F6D46427A7554486552397A746278467245645078366A4C586F71466A7757696677644D525542446E316A55445A2B51436C7149784F6C574B2F55686661595A686C44472F33356279553151344850576C4D5367522B436A74793461707A475936717A63626E43434B6247797557793856773472352B4F357059736D356B306E66517431786E6A586A6232745465417644576B386545626D6169377950503259657778544B304A62677A36316A41316F4E5A675A724F764B544D364D35533846416E584C546D4F476A56436A4137396B7271424F59596634786F58302B38376847662F68593178754336556A365779504334376E64713657647A47306973376E3036773050656D523459474438687354696A7457594C4F3066496951435252416B5957726964737A46754661454149446355493337753457614573464B777538424D6D756D6152683073696263666644786559704B32667851446C525A4C786162685677754C7A4B3474515937504C2B6257566C4B5A3234335679376962394F726C3363706F4F36447763626C43675151496752416851634557573649654269446A67554A385141426864696458686A5A63416F72504F6E662F775639684F32612B357348786D4C6F3671716A5135754D4E527834376D49464B73336D776C736367616B47464A3664613043464D45304741494167726870336C383769667568465942784B71666470786855567668485935376537436E33453649643544393434573870554D3473484978614D5179644132637A45474C554735584A644B334279374D2B4E6E37703870487444654D6A553048476F376353446C5551317362413779686E30574175514C3878504F695268444E765538594F465248557A4D6270464B494830676B6D7059782F446E6935557177762F4E364A3362776133447965304254762F376439717074725434474F694A4169534A50665966592F57765673545045562F384B6D612B50614B58427A792F6B6B516F69694B46784938327064653279714A56785450524463677837394175627A30417246476D4C4C77626C2B6531452B3853744372654C666872332B2B76684447684B42436A43697572754B786D437877584B46514B4E566773614E79484364314F31725A41766D4636755A2F757A347248554578584A357A386B4E3567624B797842325A34746B2B2B74706C7A5562663970344D665A794D4B42584F57352F76735356624736324E6A58494C73396E357A7A61627A523475466A6C356D4238462B366D36324757616E4B6C42757A6E73454535772B6641455768535554507449324C4C697364436C3051374B536D6F35596B716A56564D354B59597A6A69633237466A4652564657693141364E6C30596D754C6B467171647A5367506F46433675316878582F454774355839707A70544343724234626B56526C4B784A42534975397A44446C5A57356B71475061734F4B5333346C766A554B58324A4B386F2F727978747976735636486A4F2F7735593350414754346155504C4E5361534D5343555261717444484369764C4665332B767071447534457656762F542F6D6A63362B32685064516848336F39573374663733565833613573665233436947414B58414975583868466C776259356F774B502B654C5131435A673853584E6C7468774861503761454F526C4938573465774374355467712F7056792F67776E457946454B6754415A755A4D47506E54584C47734E32574C4C566C54356169523343526E4437306C757057472F495962687942416B686B54706E7258595537477536785855526648487A2B617857336C7670377A3061536B57706E466A556C56576846554243726C524A746C3433724462736159734C3059762F506C2F38723376374379656356396C544363567A5973476D70566F4549556836574A466B4C7536304E7979397762666E3537336A536C2F4B777577466732594A4A4E584256755872514A3830717762493042426C59694C4F5531626B346D6A546C76683250357355476D547736486361355A444C675147436B567750414F445368393139495472584C4D7963724835356E75666D504C312F61674B68624439756B785872573872587676796C58436342795676307352334A525A32442B3933383470646E4B56686A7131666E4B56555535664A5A4C49773167354539716463306E4F4D39414B6D4E7145336E4E6A753447583337392F6C6B56473672703669506671306F587343314F785038304F50783750667738614D434D4B316E5A505059756654796F4F72792F644E7A5A5A6D4C6237326567546A7935313559583365356566455138586A2B655357636F515745494C5A362B3971597757446D647762304C766C5037643837676F504C56363653447858493551744F42433841722F656C754953714152447353616373634C412B6D42584650695536744F6170337650756C2B42437A5A534A306D6E4F34516B6336513136546A717244467277674B424C48585748546D35356F42502B48596C716D776C423049723373754E4E78365444765731544A75645344336545536A5843473978726A39674F4C6749556F6A443674715734633542643965685A3955744834784E683244312F4F68766F454C6C7A575068346F653063397034444D397952782F74384D6F734468414A656D4D735A486E486E414E6F533230783836624C494C39624E71636E4C66773733762B37766E352F76566261336F594E5667706637665239384A355539425045772B57442B7A437538752F513254535735355265326A38647A755677486D386774644A554B4A482B7551446D596547456443502F654F2B514750423552306F4558624A336662616558547A78386F5068576E5459584F39317638474C6C33383332566E447859715836356355304370724E34586D7466673431526D4B74485569456D316C6230484D75317A7A4155332B7A35555A3065626C722B456358713956715A6A662F53506D5A2B4F375A536E587A66322F5A6C5A67766C53754134506D6A4E31794662366176756A2F3562534652725359326C395976547075376A567A6A57325A704D3550592F504C575839764A67416F416647577632314C4B304247647A70657975666A70776B496D6356744E4A444B5A444A51496C4E464B39667462642F5557747051414A78644A425869364C6A494E46647A35327265486F506E64693858465479744C4F357566466861723337362F655465426F36776F6536597759444B4442425879386E4455466F786561596C65506E6F5546635634505034757039766C4B387266535278634C704D45384244376F35524D724A5765482F654F6273355465586F384D46736F68377842776E4D3468486E626A6F6A7A36526364797A69674235576A746B38757874555245435332396B5A787775757558584F4F65647352586C65436E55745052696843612B4939734F776371744B4936577A32624D77506A5A494254335A76416B4C6C6F6F73416745434F396F66566B634557375A703934423230627752304B36386353754B514778766B335346312F4746374632712F694E662B4E485539376A75446D30476C33454F5964496771545A686134776D63514E6B4D716A65697564754775686E33485A7834576648327650575A6B6451794E4367465770514C3150626C57482F65675A5761305442742B4B2F48334E58434434395850483265697956794A536762355037675277525737584C736457566A38357A75542F6E5336616A2F654E787A46624D35742B495A5A4C6B6546595853547864554842364145454A476B4131597868634553597A68547851495A637A54757656794F4F576D61647048615531703348304B68434D716C6F37787A426630466831776B6162506F55456F684C6A4E4852552B6D7566354D735A6A795751716C626F37384A2F32705778365152702F5054484A425A553936396B497A6F70436F31537275794F42674E7674432F6C385542446D62783864445964544B5439317244636B64694A3273357245696372654D4E38505A5267784C356E647036704A5178436B76426962744F4F365255396C59384B472F415977514648476277664965344F574B386F34624245614D36347233736D4943322B4B71684154765264694E484442587070365078726939755651322F366E412F6179637654655978672F594244796A766E45324875674B374D67314535634355376A6F5A4E57345373764E464A39574E524B6E3773465067524D304476424F33314852736C72355A447461555830656D5A6C637A732F6C536B384A6473796F6C64353779474D493864424339392F4D625749696E666D576472526C566E683341344B5A695669422B4C656F5336415441764832374D617352304745444F483234366757506B6D73366D6C46687A3776704C3367412F4F316C626259562F61782F64786951646E56574948684D6B2B55323155634A344A364D393665395374576462534164337A33694D595330717A4766394F4E4C7A7650594B784A446562722B79454F4E4F576A69437A334C38542B374F3870525034724C4E79786F775A4D32624D4741582F4478577930664B4579354C744141414141456C46546B5375516D4343, '2015-02-16 09:59:48', 1),
	(4, 28, 'signature.png', 'image/png', '3230', _binary 0x6956424F5277304B47676F414141414E5355684555674141414863414141417A43414941414141696B67436E4141414141584E535230494172733463365141414141526E51553142414143786A777638595155414141414A6345685A6377414144734D414141374441636476714751414141777A5355524256486865375A74316346524C4538586842517675376B354267424153334C5741554C69374279336367684D494871427768384974454B6745647773514167514C5672693742747676782F617762396D4E6B727673506A374F48366E706E726C336D584E6E756B2F50766354512F59586C385A666C4D42455546445275334C684A6B795A35656E703665586E6476486C546455516466316B4F45323362746F3168424163486833333739716D2B4B4F4A50594C6C3438654C62746D3154686B5A34392B34647A4F624D6D664F79487375574C634D6350333638366F34692F764D73653368344D502F703036637257794D45424152773236316274796F62706D4C45344C65554555583835316C326358464A6C436A52793563766C6130527A703037423630484478345573336E7A35706862746D77524D367177585A6276337233372F7631375A595342745776584D6E6B334E7A646C6134654C46793979353075584C6C323764713175336271307332624E476849536F72716A4342746C6D595475354F5130637552495A5965475435382B79524B37632B654F636D6D48713165766375644B6C53716C547032615275584B6C566E6471692F717342575776337A356F6C7036504872307148446877766232396D516835544C4435382B6657375A734351585172567861344D6D544A3774333732626C636D63514F33627350587632714C356668553277334B4E4844384C726D7A64766C4B30484570564A44687734554E6C6D65507A3463625A73325A4142304B316330514337352F72313677554C466852794D3254494D47444141434A2B7A4A677854353438715162394B6D79435A61515945787338654C4379663642323764722F2F504F504D7377776576526F726F712B686E7637396D33506E6A307A5A737A4933574C466974577256362F753362732F666671554C683866487A7A3538755737642B2B65445034313241544C6844396D7947524D49757A476A527678392B376457396C47655033364E56304554524B55637630536A68343957714A454357376C364F6A5974476E545938654F715934666D4474334C7232464368566970484A46485461306C67464A786954496C696C54686A31725546514755506779667436386563714F4F716965697859744B722F723775354F4A6C41645A6C69306142466A45695A4D754866765875574B496D794335566D7A5A6A474E2F506E7A32396E5A3054444F35714B6F367465762F2F486A522B585367306A43326D64464B7A73532B504468773430624E79356375434250434B524C6C36357332624B715777386951364E476A597A3378376476332F6A72352B664865444C6867514D487842386C3241544C67446D347572724F6E7A2B66527537637559303537644368413035445259416147544A6B434A374A6B79654C4A304B67637A6476336C797A5A6B3265445265434B6C57713446486452764433397A65354D354B38573764754E4E61765830395833727835663045343267724C32624E6E5A323253694B5A4E6D385A6B6A4750786C5374583473614E32364A46437A46506E44684252754B526650333656547A68592F587131575252376F6C67364E4F6E6A37653339366C547031536647633665506374495A32646E77324D65503334386E693564757443654D6D574B454C312F2F3337706A53533059666E5A7332667977456E5134637768484A517558547058726C7A3337392B6E7A614A6A4A675A68782B4B745636386530784E7A7A707735744E6E34596F594444772B506C436C544D686967434131564F4D714D3353427445776A4C41456D6E584470646E547031384569625A5534624E523271366A682F2F6E797A5A73334D343567324C42382B664A6A66726C617447724756787336644F31564870454569496C413865504341397371564B376B4A516B3236414C776B5470773465664C6B4C313638594D6B6A4C56534845616866676F4F4469546B3849533458384F2B5A4F6E577147764544524636365542544B4E6F496345685572566F792F42516F5577454F46516A6A476C4147416859774A7A41394371576749524F61467544597367794A466972415957574B5A4D32664F6C436B5471317431524136494F55676B4C4E4A2B2F2F343930674A61447830364A4C31677A4A67785449776E77642F6C793564544151634742703435633462436A4E586472312B2F6375584B3665662B58585652356978597349436E70533757332F503036644E5359586274327056684B564B6B4D46384E524363794D4C6474333734395938694E6345316A38654C46616F5165636842596F5549466B3549314C476A4738727031362F6A687357504879714D755762496B43563331525149624E6D7A674B734D4235766274327A476851307A41664A675654755174757A567432725455337743506747564C324F5842584C74326A624B51454D2F435A365338342B6A5973534E31782F486A78326D3775626E4A4A5168482F62332F4266494F50346F4E51556E746C795A4E47714B352B5736673270514E51575A57726E43684763734152516B463541304A494D6776784A507169776969324567765974362B665A76536D5A3072706F4448787067474452725152696B6A794762506E723170307962694363784B745961794A68617A4C5A496B53634A75415079724A414B415571564B6B544E5A2B456D544A6D5642344B6C6174536F4C2F2F6E7A35317A4C383844442B6D446A662F38397662446A58794A744533414A6D3462784C482F6C43687461736A78697841675746337A525A7366466A782F663139645875694B456E4F63694D4A5374307730644F6851503055445A4F68314C4477396F3071534A63706D42614D73413869634E6C68736A7956304A4569544169544B52613975326263746A59444336356676746A4944554934504A72534B456E4C7353797051644E72526B4F53676F6946396C68725233376472465444424A455A4752584D794E7756356558737257366467544A447163314C695972436E616848377554364E54703034797A4267736468597632552F5A503842674C6D455453425548434D725378556F6B7A545A7533426A713639617475334468517646484574794B564778656C3574415335624A725130624E755348665878384D4D6B744C41337159384A7268466B4371632B46734B427350556947304A456A527737614D32624D594141684259586E364F684947335A4D6E702F6F575A4E79412B4C6B4748504A6B695759516A5452566E716A435949506477745672686844533559426F59326C5242456863594E774B62797A414D32506457435767705663564C68775952455032624A6C6333467863584277494E4652546278363959707941482B376475306B313875462B4E4F6E54342F5A76486C7A3439412F624E6777777868426C69785A3841674D6A354348683936516476517865504467434B74516A566B474F3362735945706F41454D6C537032474A3036634F4E5146523434634956696A736551316A774156534D6E4C4A625352437354665A4D6D53535A6378544D36474A41533361644E4732547264384F48443864446768795A4F6E4D677A773052346F41747068484D655A476C6F7A7A4967566A437257624E6D4B56756E51364C684D565269494657715643787A314969686970733563795A2B4F5935426842487353496B7345374B6F584D49544F6E72304B414E6B444267306142442B43524D6D69436C335149476863326B51784E6E5237416B4346337063786C6746466D455A4D456D6D2B764468513149696D6F6D314B596D65674D694B382F54304E4B39514C312B2B484339655044382F5032582F6748774C59514A69426156423538366435526976646576576D4167534369495A344F7A735445595646514864366C355767715659466A574B396B533330716859735349726B5242473279434B5458447131436C5370636E626547713236745772633557675A637557444F6A62747938564A6A6B4E545531707A724F52587446714168356E756E5470614E536F55534F614A2F335268365659706B61414343624A2F695551473436344D6D5449674C4E37392B356947674E5252624B434F3258724961475748436A52774643746F57636F7A2B54496E32776D2F4C5A71316372623235753144386831382B665058374E6D446346484C7245694C4D5579347052704578424E33706D2B66763161644A6937753774794751483236564B4758714A6755686E54774B52797736536B4E482B784C5771614F3173787859554469374173427757536771536D4D416131673869796E6A31374B7463507949473974472F6475695656435457306549415162583468656C787152524B7363746B5374476435373936397A4A5941536A536B4C6A4B76784143795773345730467679796B646759426D6E434456327658514A37742B2F54317244623378634A7942366946376D4763674A7175314159355A5A552F4B396A3553714D6D3254696B3741534C714163544A733171775A486872795653766157667A47494E54534A5157684F555161392B2F6658396D324159315A70684B72566175574D43575157694D674945445A50794E7834735430556A694975587A3563734F724F654B764F4D30686C5451466F624A2F426B55357652524879725942614D7779576B4B4B4F6D5872644D51457A506274323466364C6461715661766B55476E70307157597763484242426E4D3875584C47343466517755536D47486D4A3739673337353964424770754A7479575273617377795638676D4C67534E4542556D4D4E5273594743676545357734635549304E632B44395573447A4A3439573357484159492B69746A42775346556F53627677744746797259324E47595A6A426F3169686C536A79686270317578596755652F696F374E48796E5667393556342F795652316851373767516C6F6F2B32664932527468576A5331646145397930432B6C334231645A575855745276386A4970314266736C4F414C46697777764D3641485459374659316F3548414166524B64776A7048373969784937304E477A61304F74455759526C51317A4A44616C77786F597A39477A743262446C6F667662733266486A78773866506B79387071706D7049754C693465486879693550486E79494B6766503334733134614471316576706B6D544A6B36634F4D5A7930426A636A564C4636755766705667473876304B50487036656D4C323674554C383869524977687177386F4672456630674E424538757A577252764F70456D5452764B6A7450432F4D317139656A584B54786E576777565A44676B4A6B5265394144306E5A7A715A4D6D5643713547346576546F6759446275584F6E2B57735569657A4733324F4567382B66503874705833532B544C51304C4D6779594957795733313966534755436B574F69717056717862686D384378593864472F7333736E547433434474772F63762F48382F5373437A4C4A72683337353639766232546B354F386D646351506A342B457438312F34392F6D7543337367776D544A67512B5767514A636833582B5966394E7343666A664C387430463159636D2F786E454747544F4E6D33617246323756746D32684E2F4E4D6D4561535A633965335A4C71437565584669537A727234335377442B61376432646C5A3266384873414C4C514537634562504B2F744E68485A62392F663374374F7853703035744C70622F534669485A644371565375306C33794339496444702F7366656C7144676538676D503841414141415355564F524B35435949493D, '2016-04-28 20:02:46', 28);
/*!40000 ALTER TABLE `person_signature` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.queue
DROP TABLE IF EXISTS `queue`;
CREATE TABLE IF NOT EXISTS `queue` (
  `id` varchar(128) NOT NULL,
  `params` longtext NOT NULL,
  `task` varchar(30) NOT NULL,
  `progress_message` text,
  `progress_status` varchar(20) NOT NULL DEFAULT 'Progress',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `max_attempts` tinyint(2) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `pop_key` varchar(128) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `task` (`task`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.queue: ~160 rows (approximately)
DELETE FROM `queue`;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
INSERT INTO `queue` (`id`, `params`, `task`, `progress_message`, `progress_status`, `date_created`, `max_attempts`, `attempts`, `pop_key`, `owner`) VALUES
	('01287714b40e694ce5c3c9f819e863c4a24e6665', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:13:15</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 12:13:18', 3, 0, NULL, 8),
	('01424ef9c5c84f9990e1e27006d86cb8432acf6a', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 10:43:19', 3, 0, NULL, 17),
	('01d54f92e0dd65be83d676ee0ee3caab4d7220a6', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-19 13:46:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 17:16:56', 3, 0, NULL, 17),
	('034fe4f3a14c1498890d7910b8b4a3fe58d76248', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:06', 3, 0, NULL, 7),
	('04781f837245e284744019b155cab03d4492c830', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 13:17:06', 3, 0, NULL, 8),
	('04ebba9db4d0c33132ce31ca13ec744865f3b8a3', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:24:37</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:22:37', 3, 0, NULL, 8),
	('056c9c280f3207cae8a11ff52f41fe5a02c9df3d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-12 11:28:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 14:34:31', 3, 0, NULL, 17),
	('07109f3855067049c537dac2a3cfc691ff195317', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:35', 3, 0, NULL, 7),
	('07e4e1683f9cd6ef8f20bfb7bdc131c272fb7f31', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:48:44', 3, 0, NULL, 7),
	('0ac4c954ddb3ef1f5369b8d4c54fddeb2d7eeb63', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:51', 3, 0, NULL, 7),
	('0d5b634f01b0273e179eca0fc65120a1e812a86f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:01:04', 3, 0, NULL, 8),
	('0e68525d2ab9703d449e80ab80f6f8d97b538161', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-12 12:21:36</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 16:35:10', 3, 0, NULL, 17),
	('0ee09550fb331cfa159382a314caf789bae8e557', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:11', 3, 0, NULL, 7),
	('0eedc9c2f3896d72faa16ad6d532641883c04732', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:20', 3, 0, NULL, 7),
	('18214d8687facf7d603a3bc39de78ca5f9c6c2d0', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:48', 3, 0, NULL, 7),
	('18977bd938e7d1377dac2d0c9d1b73489a1e0836', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 18:25:56', 3, 0, NULL, 17),
	('1d4842583ec2488eb4d183530fc554812e4fc030', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:48:44', 3, 0, NULL, 7),
	('1dea6aedb445b5c0712e5a624eaf5edf97c9f7cb', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:00:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 10:12:43', 3, 0, NULL, 8),
	('1eea693aa3ab17db1399ae309d018cee26adb421', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 18:26:48', 3, 0, NULL, 17),
	('1f6841fe4a20b4b67b5b0b5da7d20b2d09afabce', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:22:"Pending Leave Approval";s:7:"message";s:456:"<p>\r\n	 Hi! Kabucho Kiruri,<br>\r\n	            {{sender}} has an <a href="http://192.168.1.155/chimesgreen/leavetype/index">Annual</a>  leave application starting <a href="http://192.168.1.155/chimesgreen/leave/index">2015-01-30</a> to <a href="http://192.168.1.155/chimesgreen/leave/index">2015-02-14</a> for <a href="http://192.168.1.155/chimesgreen/leave/index">15</a> that needs your approval.<br>\r\n	            Thanks<br>\r\n	            Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-01-27 18:08:22', 3, 0, NULL, 10),
	('1fa3256641fedf887eef8b46b9324fe0752f560e', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-19 12:54:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 15:55:50', 3, 0, NULL, 17),
	('2006876751718ce17eadcae08855c5c24495a455', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:08', 3, 0, NULL, 7),
	('20b1844a1faf747923811ad0d0c5f93112463666', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-23 09:07:00</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-23 12:18:40', 3, 0, NULL, 17),
	('20c83128cfdcb02854fe1c691d88f876b6a10124', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:373:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Wilberforce Majanga Odanga</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-16 09:28:53</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-16 12:51:34', 3, 0, NULL, 14),
	('21152519058684535f87325e251258a648bf9786', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:363:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 17:30:25', 3, 0, NULL, 12),
	('212ea6b6b7c5f1cb9a803bdf849929d761e18335', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:13', 3, 0, NULL, 7),
	('21390c2459da1087b8ff2ad5f8de31b42b2f3e1f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 15:28:29', 3, 0, NULL, 17),
	('21915170bc5defe72093c58b0a99de023fc7b5de', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 10:38:45', 3, 0, NULL, 17),
	('2241f4f76e7a4355490903172ee525319f36584a', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 14:57:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 12:39:47', 3, 0, NULL, 8),
	('22d09dd462a588f239ca0a9da253be63aa3591f7', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:14:05', 3, 0, NULL, 17),
	('259a4228a0073e79241869259678dd475295d642', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:06', 3, 0, NULL, 7),
	('25df12279567d180f98b4681252bcce604a3cdef', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:07:12', 3, 0, NULL, 8),
	('270a23ea3fb49d5f0ad8d31b5c8a0831e89f1b34', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:15', 3, 0, NULL, 7),
	('27deee6ae8cddc20ac85be7dfc5fddf5582e9b99', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:363:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:02:40', 3, 0, NULL, 12),
	('2a02c592dade86446a7695f64a9b3e87839cb4fc', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:27', 3, 0, NULL, 7),
	('2a98fc74527f2863b0e24e15932c4e4facbd337a', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:58', 3, 0, NULL, 7),
	('2b3beab5532b51add4de926414425f9a0a112005', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 15:24:10', 3, 0, NULL, 17),
	('2b71bad5a37e5f5aa46cfece750266644b08b125', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 11:00:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:48:44', 3, 0, NULL, 7),
	('2eb472fa8a372eaad622ee88666c37f05fead03c', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-23 08:39:52</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-23 11:44:32', 3, 0, NULL, 17),
	('2f6ab39ff838b872a3179e98d9b1ff49feda9453', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:20', 3, 0, NULL, 7),
	('3072ef2419ae15b287de0838d4fc39906089997b', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 15:04:29', 3, 0, NULL, 17),
	('30b5197ec752104cf65dc57a98ec2f95ad63ecc5', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:32:08</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:06', 3, 0, NULL, 7),
	('314d703aaa776831b4d8ce53ab83b05ca9a1a722', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:19', 3, 0, NULL, 7),
	('3173cddd6bce197b1afac63f96eec6d805612e68', 'a:5:{s:9:"from_name";s:11:"Doc Manager";s:10:"from_email";s:26:"noreply@felsoftsystems.com";s:8:"to_email";s:18:"jacjimus@gmail.com";s:7:"subject";s:17:"Password Recovery";s:7:"message";s:513:"<p>\r\n	 Hello James Makau,\r\n</p>\r\n<p>\r\n	     Please <a target="_blank" rel="nofollow" href="http://http://localhost:5310/bremak/auth/default/reset-password?id=28&token=2e1da57dddf7f2394dec0829eda4450c">click here</a>   or copy and paste this link: http://localhost:5310/bremak/auth/default/reset-password?id=28&token=2e1da57dddf7f2394dec0829eda4450c to your browser to change your password. If you never initiated this password recovery process, please just ignore this email.\r\n</p>\r\n<p>\r\n	  Chimesgreen Team\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2016-07-03 03:13:30', 3, 0, NULL, NULL),
	('321a3d48abac8d268647246dbdaa2686115ff319', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:02', 3, 0, NULL, 7),
	('33e6ee8d8458f869ebd032e48867f479dcb3be5a', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:351:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:47:54', 3, 0, NULL, 7),
	('3637b4bb375435c8332922dc6c6fbd29bd7adc84', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:58', 3, 0, NULL, 7),
	('3741533decb12b2426930603a31eeeb58954d149', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:51', 3, 0, NULL, 7),
	('37c015ff4a20c88bf1aedbe13ad1fe3f1425c3e9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:361:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Paul Wafula Sikuku</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:59:49', 3, 0, NULL, 5),
	('38655656daa8a455478ec390531a4d9569dec410', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:361:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Paul Wafula Sikuku</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:59:49', 3, 0, NULL, 5),
	('38becedcb48568184988892193a6d9b02ad1c890', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:03', 3, 0, NULL, 7),
	('3964df77cd2b1e2e3db6ebe6e6b72fab730665ea', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:32', 3, 0, NULL, 7),
	('39726dfd66b9861b47d99244705aa81fb5e65ce2', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 10:55:31', 3, 0, NULL, 7),
	('3a165b5cc57e90cf5a9abde9350ff2104b2f0d2d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:58', 3, 0, NULL, 7),
	('3d2f0a2bdfa244183ebf9e5851c9156169725ade', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:58', 3, 0, NULL, 7),
	('3daa33b0c3621f1def88504e20a1654643ee5e91', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:373:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Wilberforce Majanga Odanga</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-16 12:31:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-16 15:41:47', 3, 0, NULL, 14),
	('3e7dc9d7e0bc85a0a73af5e7c0658249ce385778', 'a:5:{s:9:"from_name";s:11:"Doc Manager";s:10:"from_email";s:26:"noreply@felsoftsystems.com";s:8:"to_email";s:18:"jacjimus@gmail.com";s:7:"subject";s:17:"Password Recovery";s:7:"message";s:513:"<p>\r\n	 Hello James Makau,\r\n</p>\r\n<p>\r\n	     Please <a target="_blank" rel="nofollow" href="http://http://localhost:5310/bremak/auth/default/reset-password?id=28&token=6e8dc770ff0265c25772a11559e96b02">click here</a>   or copy and paste this link: http://localhost:5310/bremak/auth/default/reset-password?id=28&token=6e8dc770ff0265c25772a11559e96b02 to your browser to change your password. If you never initiated this password recovery process, please just ignore this email.\r\n</p>\r\n<p>\r\n	  Chimesgreen Team\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2016-07-03 03:18:09', 3, 0, NULL, NULL),
	('41ba2da62d339965498536ed055d3a97cbf68117', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-19 06:45:24</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 09:48:11', 3, 0, NULL, 17),
	('4244715d0589594be2c8df8b5aaefbdd948e4a66', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:20:28', 3, 0, NULL, 17),
	('42a8cfe8904eb4c59e08bbb65448ad7f4a87a63b', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 12:16:20', 3, 0, NULL, 5),
	('43d0498fc32398167784bc94003a1f41560cbc6f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 12:18:07', 3, 0, NULL, 5),
	('444f7561ad1b38a193bf90b576f6aef90983788f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:38', 3, 0, NULL, 7),
	('45227f36d07598745691f0ba56d71b72b8e16bda', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 14:59:22</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 10:47:59', 3, 0, NULL, 17),
	('4b84dd143babc98f5675f1c5fc8eb7a9f48327aa', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:07:12', 3, 0, NULL, 8),
	('4edb233850319d4829fe0ead1805d85a9440fb5f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-26 13:33:32</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:20', 3, 0, NULL, 7),
	('4edc93a755c3b69930bf8e7a25dbbc203f2c5458', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:20:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:07:12', 3, 0, NULL, 8),
	('4f91a48a671a76bde52276401aff80a74ccd5a8f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:38', 3, 0, NULL, 7),
	('50b832fd4dec4c9c01dc6a5b9b77e84b4fc22eee', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-18 06:19:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 10:44:00', 3, 0, NULL, 17),
	('50d15be1da7631bd34bcb55244699f79034a1b89', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:365:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Paul Wafula Sikuku</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:52:23', 3, 0, NULL, 11),
	('51e699799af83cb3b29958f306f96f4b3f443a9d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:39', 3, 0, NULL, 7),
	('5802b709ca39f44a1d7d4629381b09d2157ce541', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:52:23</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:38', 3, 0, NULL, 7),
	('58aa90371d1153d81db3d9072dc87ac32ce22699', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:26', 3, 0, NULL, 7),
	('5bd0198ba35ce3238d9dd88952de740ab6ff118b', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 11:36:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 09:10:27', 3, 0, NULL, 17),
	('5d1bcbce06781eb37352e340bd4e5e6ae195e414', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 10:24:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 13:36:09', 3, 0, NULL, 17),
	('5d6311c39979728d0b5f115224d02d8cee0a9d8f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:365:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Paul Wafula Sikuku</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 12:57:14', 3, 0, NULL, 11),
	('5f3ac0e657ab0c71ae802f456889cfd1e8006907', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 10:55:31', 3, 0, NULL, 7),
	('60b6c285787a1766d3a1c88affd5a7d8cd9766ef', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:52:49</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:51', 3, 0, NULL, 7),
	('60fe3aa6986b08cbb55529b1e2f5893ab5400228', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:32', 3, 0, NULL, 7),
	('61fd228dd67a78cc30ee9654437c1e19010744c8', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:351:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:47:54', 3, 0, NULL, 7),
	('6456bfbcde933cca2510463b8096abbf312a9d6d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:21:24', 3, 0, NULL, 8),
	('65c6f6789c459dfb5f089c7c7aeb9a24e7baf290', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:52', 3, 0, NULL, 7),
	('65d5dda203fd7f3edc459e1b30362ac7dc7aa776', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:15', 3, 0, NULL, 7),
	('69eed2a4f41318ecbc93d1e5c7d386b2e59357dc', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 12:18:17', 3, 0, NULL, 5),
	('6bd7da6a42f8aca90fd580655621de3365c5b7fd', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 12:25:15', 3, 0, NULL, 5),
	('6f771f951271035264be80779bec736aae9f981b', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:02', 3, 0, NULL, 7),
	('71bed2fbc474c3c9d488d3f06f5986449506fdf8', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 13:33:06</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:59', 3, 0, NULL, 7),
	('72638f6f4deccf31ab82c48c7070c0faf7355eb3', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:27', 3, 0, NULL, 7),
	('756fbae10a6b6ed5530db4573a22adfa9b54072d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 13:51:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 10:09:46', 3, 0, NULL, 8),
	('7fedd425bb55cc526c03ffa724ca709c2ac8a0ad', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:363:"<p>\r\n	          Hi! Kabucho Kabucho Kiruri,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 16:18:45', 3, 0, NULL, 12),
	('81409eb12dce399e220e69ba7f61814c15e6f1c9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-19 10:00:52</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 13:10:22', 3, 0, NULL, 17),
	('82e280964891ad931cfa5997c10bdf9b5bac766f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 11:36:03</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 14:47:14', 3, 0, NULL, 17),
	('86ae371b689a3d9f8c59013dc384de8acc94d26f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:19:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:04:20', 3, 0, NULL, 8),
	('8b758de5f5833530427465eac95aff9f9c9452e1', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:39', 3, 0, NULL, 7),
	('8b7e2e74efddd291ca7d5e256fb9c3fb9c80b5be', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:55:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:02', 3, 0, NULL, 7),
	('8c3161fd60c52910a9a5d3d3c315ebbde9ab1fc7', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:43', 3, 0, NULL, 7),
	('8dca92c839387dc8157dab28f8cbe22ec97a5431', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:54:23', 3, 0, NULL, 17),
	('9403ea1ed63d0071849e1714c6f938b16c2d4727', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:11', 3, 0, NULL, 7),
	('94b46e9eeea4ca0096c65ec10c056495ada50a04', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:02:38</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:27', 3, 0, NULL, 7),
	('951e9fd421cef5ab64c1e80f30b377f2d97aece0', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:01:04', 3, 0, NULL, 8),
	('967a82a5fcf271719fa96512d9a1673aca6c8043', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 15:44:08', 3, 0, NULL, 17),
	('96f60c2374a4a48a6d818524740c83b29f092255', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:21', 3, 0, NULL, 7),
	('98018d1066963b92db4946cb0869dba11d83b535', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:08', 3, 0, NULL, 7),
	('992f1955caf1bdb360377966ed88d99a5b8ffb44', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:04', 3, 0, NULL, 7),
	('9a8e62138912054d7e7d5051e23a0d9af239b8d7', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:43:28</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:11', 3, 0, NULL, 7),
	('9d66d9bcfd2f956862583c229b742fcfe69238dc', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:13', 3, 0, NULL, 7),
	('a02ac9bf86f3f4590f4339c6a23594dec4095e88', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:47:40</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:39', 3, 0, NULL, 7),
	('a14210e24eb1aae65fe6b63563e1b76b7b1cb184', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 11:29:47', 3, 0, NULL, 17),
	('a67f6b25dd9c51fa7e5ceab036d79bdf7d487ca6', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:00', 3, 0, NULL, 7),
	('aaff3afcea27fb1a5b79c238baff807d12e39465', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 17:55:42', 3, 0, NULL, 17),
	('ab0ee270a0ffc72328ab1a847ae9e5e8ac197746', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:29', 3, 0, NULL, 7),
	('ac1666ae9fa45921f4257d7df688f60f453daa7f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-18 08:18:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 11:29:25', 3, 0, NULL, 17),
	('ac60cabb42da963e249d20f8e6fb941001d4c0e9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:52', 3, 0, NULL, 7),
	('acc2bb085b0494f159a297cb929bd08d880d2bf0', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:359:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 10:40:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 15:00:57', 3, 0, NULL, 5),
	('ad9205ce5d09297561af6358ac64f9afe0a5e4bd', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:35:37</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 14:15:00', 3, 0, NULL, 17),
	('af647f7288df7ca3c97526fe7bfd4ed14df962a9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:05:16</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 13:17:06', 3, 0, NULL, 8),
	('afe28a53ecd64948e4767e2e8a75bcda1fb8a9d9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:43', 3, 0, NULL, 7),
	('b2ca545f7e5e966dc144e6c06c8377cba77ce960', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:21', 3, 0, NULL, 7),
	('b2ee9182bd69896f7a0ef1c8dfbb6448b0690303', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:00', 3, 0, NULL, 7),
	('b3f51292ddc3a5fdf3145d0a9900d92005f2f33b', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-17 11:53:20</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 14:56:24', 3, 0, NULL, 17),
	('b486c82324e4d18cee00486163f54fde3de186d0', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 06:57:01</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:21', 3, 0, NULL, 7),
	('b65779ad7f77b8974f746579c642410b2360d1d3', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-29 08:57:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:52', 3, 0, NULL, 7),
	('b65a1758598a8f0489fb25c9a5b0784a9368467e', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:19', 3, 0, NULL, 7),
	('b7af14df6877f43bae9aeb28fc2b7eb6608fa8a2', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:361:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Paul Wafula Sikuku</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-11 11:47:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 14:59:49', 3, 0, NULL, 5),
	('b9cc7dbc58b7248c8b118e02c95e76ab629c18d9', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-23 12:18:19</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:03', 3, 0, NULL, 7),
	('becbeffdde7d4f05152f236c9d096f1927f9a9a6', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:22:39</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-17 12:11:02', 3, 0, NULL, 17),
	('c0e724af244a8639ba2c726b97c1a4e7bcf25e99', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:22:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:43', 3, 0, NULL, 7),
	('c3620a360ca36e5cfdfb0fa3ae5ab550ecde3320', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-18 06:19:29</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 09:29:33', 3, 0, NULL, 17),
	('c38b8478b4e6a54e7d0f9a0be3080fa32d50b307', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:35', 3, 0, NULL, 7),
	('c4271e348a7711cc6513c782e629295f99387782', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-20 08:36:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-20 11:59:33', 3, 0, NULL, 17),
	('c63a23b63c1696c7807e303c970e487026eb9945', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:00:47</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 14:36:14', 3, 0, NULL, 7),
	('c73fa7a4e031c9e2e0e728f4cb221a7924504560', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:42:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:50:19', 3, 0, NULL, 7),
	('c9170fe2aff4728da617a9ce1d3a4f83ba14c429', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-18 08:53:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-18 12:09:36', 3, 0, NULL, 17),
	('c937f235ac9e28afa38ecfceada15e718b8c5918', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-12 12:21:36</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 15:24:19', 3, 0, NULL, 17),
	('ca7fede8e6cc6f0d33a639abdee18c69836d0f3e', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-09 12:25:15', 3, 0, NULL, 5),
	('ccaee63506a8fa497ef1c9600abba24a4beeab43', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:26', 3, 0, NULL, 7),
	('d15899c5fc0a513c8ff249be4e8afc1803953664', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 12:40:34</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:48:02', 3, 0, NULL, 8),
	('d3d6793229c0145bcf2dc8b755ba43b1d4b6f0c1', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:37', 3, 0, NULL, 7),
	('d4f1390d8826d660c554253f476a87146c16b50c', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 11:30:02</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:13', 3, 0, NULL, 7),
	('d56076af42824e1f2e65d0924ee96975cba5a442', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:48', 3, 0, NULL, 7),
	('d6a3da9e06128aef73715ae57ebf09ef443be4b8', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:45:42</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:53:26', 3, 0, NULL, 7),
	('d75a25f20a58701707c54526910bd6e0bae2c08d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:09:54</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:08', 3, 0, NULL, 7),
	('df811c2ef5f0482f4e75856282240f0444493e9c', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:37', 3, 0, NULL, 7),
	('e0c8501d9c2315ad1d9869fb2bd9dd717f0d43a3', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:351:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Nancy Lisi Ndeti</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-21 10:29:27</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:47:54', 3, 0, NULL, 7),
	('e0d146c3b61b0d52dd1b4362b83e196a1a915d35', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Esther Wanjiku Muthee</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-01-28 09:44:12</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:49:35', 3, 0, NULL, 7),
	('e17e702582635e0a279b221b7bb0ef90fc3ebd49', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 15:30:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 12:32:05', 3, 0, NULL, 17),
	('e21519eea1af1257e8ddb388a46542a59a124ee2', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:00:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:15', 3, 0, NULL, 7),
	('e80cf1c2aeda97c9ef4c3757c9aa439642788b10', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-19 07:05:11</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 12:58:24', 3, 0, NULL, 17),
	('ec166b181484d722ea376e3770712169e145ffeb', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:06:55</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:32', 3, 0, NULL, 7),
	('ee196134dd817477bb28d7b35cb0d6adb6169b24', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:50:14</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 11:18:48', 3, 0, NULL, 17),
	('ef9573a367f4e659058d86d0b31506ce3a1b6760', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:29', 3, 0, NULL, 7),
	('f15c3b0c536a95dd280f87f97c588d1450de440d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:370:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-10 14:57:31</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 18:01:40', 3, 0, NULL, 17),
	('f1627fc7b2903ffacc3a890f8e46f4116196f3cc', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:13:15</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-12 12:13:18', 3, 0, NULL, 8),
	('f181b44902b35d0f269a4e2d4afd85eb310ede7d', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:367:"<p>\r\n	          Hi! Patrick Muriithi Wanjiru,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Nelly Nafula Sumba</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-18 08:53:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-19 09:26:27', 3, 0, NULL, 17),
	('f584ea687b5cc089fc2b5eb2086e1e798af9897f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-04 07:57:17</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:54:29', 3, 0, NULL, 7),
	('f76f5d9ff109ab0f27c5bfcbabe92a338381f466', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-03 07:27:44</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:52:58', 3, 0, NULL, 7),
	('fc736093731951824101a05a6b1969feba8f106a', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"patrick@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 12:28:46</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:48', 3, 0, NULL, 7),
	('fe523108ec16a06904197899e4a319fcc9be66f1', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 10:56:25</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:00', 3, 0, NULL, 7),
	('ff1cf15c44672ba7b95d6ddc666ebdafb86c25a6', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://197.211.3.194/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:03:35</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:01:04', 3, 0, NULL, 8),
	('ff350eb18375bde9801bb86d43c3ef344028601f', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"lwambui@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:356:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://localhost/chimesgreen/req/req-requisition-headers/index">2015-02-02 11:04:59</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-10 08:51:37', 3, 0, NULL, 7),
	('ff785657159c15fc18c6349593fb033690c7bbb8', 'a:5:{s:9:"from_name";s:11:"Chimesgreen";s:10:"from_email";s:23:"info@felsoftsystems.com";s:8:"to_email";s:24:"kabucho@lifeskills.or.ke";s:7:"subject";s:11:"Requisition";s:7:"message";s:364:"<p>\r\n	          Hi! Lucy Wambui Mbugua,<br>\r\n	          <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">Linet Wairimu Njuguna</a> made a requisition on <a href="http://192.168.1.155/chimesgreen/req/req-requisition-headers/index">2015-02-06 14:19:09</a> that needs your approval.<br>\r\n	          Thanks<br>\r\n	          Chimesgreen\r\n</p>";}', 'sendEmail', NULL, 'Progress', '2015-02-11 11:04:20', 3, 0, NULL, 8);
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.queue_tasks
DROP TABLE IF EXISTS `queue_tasks`;
CREATE TABLE IF NOT EXISTS `queue_tasks` (
  `id` varchar(30) NOT NULL,
  `last_run` timestamp NULL DEFAULT NULL,
  `execution_type` enum('cron','continuous') NOT NULL DEFAULT 'cron',
  `status` varchar(10) NOT NULL DEFAULT 'Active',
  `threads` int(11) NOT NULL DEFAULT '0',
  `max_threads` int(11) NOT NULL DEFAULT '3',
  `sleep` int(11) NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.queue_tasks: ~3 rows (approximately)
DELETE FROM `queue_tasks`;
/*!40000 ALTER TABLE `queue_tasks` DISABLE KEYS */;
INSERT INTO `queue_tasks` (`id`, `last_run`, `execution_type`, `status`, `threads`, `max_threads`, `sleep`) VALUES
	('cleanUp', '2014-12-16 09:48:13', 'cron', 'Active', 0, 0, 0),
	('notificationManager', NULL, 'cron', 'Active', 0, 0, 0),
	('sendEmail', '2014-12-16 10:33:52', 'continuous', 'Active', 1, 2, 5);
/*!40000 ALTER TABLE `queue_tasks` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings
DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(64) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1 COMMENT='Stores systemwide settings';

-- Dumping data for table bremak_manager.settings: ~45 rows (approximately)
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`id`, `category`, `key`, `value`) VALUES
	(1, 'general', 'site_name', 's:11:"Chimesgreen";'),
	(2, 'general', 'company_name', 's:42:"Embassy of the Federal Republic of Somalia";'),
	(3, 'general', 'pagination', 's:2:"10";'),
	(6, 'payroll', 'payroll_personalrelief', 's:4:"1162";'),
	(7, 'payroll', 'payroll_pensionemployee', 's:3:"7.5";'),
	(8, 'payroll', 'payroll_pensionemployer', 's:2:"15";'),
	(9, 'payroll', 'payroll_pensionmaximum', 's:5:"20000";'),
	(10, 'payroll', 'payroll_nssfemployee', 's:1:"5";'),
	(11, 'payroll', 'payroll_nssfemployer', 's:1:"5";'),
	(12, 'payroll', 'payroll_nssfmaximum', 's:3:"400";'),
	(13, 'payroll', 'payroll_nssfvoluntarymaximum', 's:3:"600";'),
	(14, 'payroll', 'payroll_insurancepercentage', 's:2:"15";'),
	(15, 'payroll', 'payroll_insurancemaximum', 's:4:"5000";'),
	(16, 'payroll', 'payroll_pensionbaseon_basic_gross', 's:1:"2";'),
	(17, 'payroll', 'payroll_nssfbaseon_basic_gross', 's:1:"2";'),
	(18, 'payroll', 'payroll_owneroccupier_interest_jan_nov', 's:5:"12500";'),
	(19, 'payroll', 'payroll_owneroccupier_interest_december', 's:5:"12500";'),
	(20, 'payroll', 'payroll_fringe_benefit_january_march', 's:1:"7";'),
	(21, 'payroll', 'payroll_fringe_benefit_april_june', 's:1:"7";'),
	(22, 'payroll', 'payroll_fringe_benefit_july_september', 's:1:"6";'),
	(23, 'payroll', 'payroll_fringe_benefit_october_december', 's:1:"3";'),
	(24, 'payroll', 'payroll_fringe_corporate_tax_rate', 's:2:"30";'),
	(26, 'general', 'admin_email', 's:21:"admin@chimesgreen.com";'),
	(27, 'general', 'default_timezone', 's:14:"Africa/Nairobi";'),
	(28, 'email', 'email_mailer', 's:4:"smtp";'),
	(29, 'email', 'email_sendmail_command', 's:0:"";'),
	(30, 'email', 'email_host', 's:23:"gator4007.hostgator.com";'),
	(31, 'email', 'email_port', 's:3:"465";'),
	(32, 'email', 'email_username', 's:23:"info@felsoftsystems.com";'),
	(33, 'email', 'email_password', 's:9:"fel132!!!";'),
	(34, 'email', 'email_security', 's:3:"ssl";'),
	(35, 'email', 'email_master_theme', 's:27:"<p>\r\n	    {{content}}\r\n</p>";'),
	(36, 'general', 'company_email', 's:11:"info@hr.com";'),
	(37, 'google_map', 'google_map_api_key', 's:39:"AIzaSyDmaUPCeR6C_VEIx3HDKHaGfhSe2WELRqc";'),
	(38, 'google_map', 'google_map_default_center', 's:34:"0.39550467153201946,37.63916015625";'),
	(39, 'google_map', 'google_map_default_map_type', 's:7:"ROADMAP";'),
	(40, 'google_map', 'google_map_crowd_map_zoom', 's:1:"6";'),
	(41, 'google_map', 'google_map_single_view_zoom', 's:2:"12";'),
	(42, 'google_map', 'goole_map_direction_zoom', 's:1:"8";'),
	(43, 'general', 'currency_id', 's:1:"4";'),
	(44, 'general', 'default_location_id', 's:1:"2";'),
	(45, 'general', 'app_name', 's:11:"Doc Manager";'),
	(46, 'general', 'country_id', 's:1:"1";'),
	(47, 'general', 'items_per_page', 's:3:"100";'),
	(48, 'general', 'theme', 's:13:"smart-style-2";');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_city
DROP TABLE IF EXISTS `settings_city`;
CREATE TABLE IF NOT EXISTS `settings_city` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_city: ~28 rows (approximately)
DELETE FROM `settings_city`;
/*!40000 ALTER TABLE `settings_city` DISABLE KEYS */;
INSERT INTO `settings_city` (`id`, `name`, `country_id`, `latitude`, `longitude`, `date_created`, `created_by`) VALUES
	(1, 'Nairobi', 1, NULL, NULL, '2014-05-06 14:45:54', 1),
	(2, 'Mombasa', 1, NULL, NULL, '2014-05-06 14:46:28', 1),
	(3, 'Kisumu', 1, NULL, NULL, '2014-05-06 14:46:44', 1),
	(4, 'Nakuru', 1, NULL, NULL, '2014-05-06 14:47:00', 1),
	(5, 'Eldoret', 1, NULL, NULL, '2014-05-06 14:47:12', 1),
	(6, 'Kakamega', 1, NULL, NULL, '2014-05-06 14:47:29', 1),
	(7, 'Machakos', 1, NULL, NULL, '2014-05-06 14:47:41', 1),
	(8, 'Kisii', 1, NULL, NULL, '2014-05-06 14:47:50', 1),
	(9, 'Busia', 1, NULL, NULL, '2014-05-06 14:48:27', 1),
	(10, 'Mumias', 1, NULL, NULL, '2014-05-06 14:48:36', 1),
	(11, 'Homa-Bay', 1, NULL, NULL, '2014-05-06 14:48:47', 1),
	(12, 'Migori', 1, NULL, NULL, '2014-05-06 14:48:57', 1),
	(13, 'Malindi', 1, NULL, NULL, '2014-05-06 14:49:11', 1),
	(14, 'Voi', 1, NULL, NULL, '2014-05-06 14:49:24', 1),
	(15, 'Mlolongo', 1, NULL, NULL, '2014-05-06 14:49:40', 1),
	(16, 'Kericho', 1, NULL, NULL, '2014-05-06 14:49:49', 1),
	(17, 'Rongo', 1, NULL, NULL, '2014-05-06 14:49:59', 1),
	(18, 'Oyugis', 1, NULL, NULL, '2014-05-06 14:50:07', 1),
	(19, 'Sondu', 1, NULL, NULL, '2014-05-06 14:50:16', 1),
	(20, 'Kapsoit', 1, NULL, NULL, '2014-05-06 14:50:26', 1),
	(21, 'Meru Town', 1, NULL, NULL, '2014-05-06 14:50:42', 1),
	(22, 'Embu Town', 1, NULL, NULL, '2014-05-06 14:50:52', 1),
	(23, 'Nyeri Town', 1, NULL, NULL, '2014-05-06 14:51:04', 1),
	(24, 'Thika Town', 1, NULL, NULL, '2014-05-06 14:51:14', 1),
	(25, 'Ugunja', 1, NULL, NULL, '2014-05-09 23:00:56', 1),
	(27, 'Kiambu Town', 1, NULL, NULL, '2014-05-09 23:01:15', 1),
	(28, 'Kampala', 228, NULL, NULL, '2014-06-02 01:52:29', 1),
	(29, 'Kampala', 1, NULL, NULL, '2014-06-30 00:06:05', 1);
/*!40000 ALTER TABLE `settings_city` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_country
DROP TABLE IF EXISTS `settings_country`;
CREATE TABLE IF NOT EXISTS `settings_country` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unique ID of the county (system generated)',
  `name` varchar(128) NOT NULL COMMENT 'Country name',
  `country_code` varchar(4) DEFAULT NULL COMMENT 'Country code e.g 254 for Kenya',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_country: ~245 rows (approximately)
DELETE FROM `settings_country`;
/*!40000 ALTER TABLE `settings_country` DISABLE KEYS */;
INSERT INTO `settings_country` (`id`, `name`, `country_code`, `date_created`) VALUES
	(1, 'Kenya', '254', '2012-12-28 17:07:56'),
	(3, 'Andorra', NULL, '2013-01-30 12:56:12'),
	(4, 'United Arab Emirates', NULL, '2013-01-30 12:56:12'),
	(5, 'Afghanistan', NULL, '2013-01-30 12:56:12'),
	(6, 'Antigua and Barbuda', NULL, '2013-01-30 12:56:12'),
	(7, 'Anguilla', NULL, '2013-01-30 12:56:12'),
	(8, 'Albania', NULL, '2013-01-30 12:56:12'),
	(9, 'Armenia', NULL, '2013-01-30 12:56:12'),
	(10, 'Netherlands Antilles', NULL, '2013-01-30 12:56:12'),
	(11, 'Angola', NULL, '2013-01-30 12:56:12'),
	(12, 'Antarctica', NULL, '2013-01-30 12:56:12'),
	(13, 'Argentina', NULL, '2013-01-30 12:56:12'),
	(14, 'American Samoa', NULL, '2013-01-30 12:56:12'),
	(15, 'Austria', NULL, '2013-01-30 12:56:12'),
	(16, 'Australia', NULL, '2013-01-30 12:56:12'),
	(17, 'Aruba', NULL, '2013-01-30 12:56:12'),
	(18, 'Aland Islands', NULL, '2013-01-30 12:56:12'),
	(19, 'Azerbaijan', NULL, '2013-01-30 12:56:12'),
	(20, 'Bosnia and Herzegovina', NULL, '2013-01-30 12:56:12'),
	(21, 'Barbados', NULL, '2013-01-30 12:56:12'),
	(22, 'Bangladesh', NULL, '2013-01-30 12:56:12'),
	(23, 'Belgium', NULL, '2013-01-30 12:56:12'),
	(24, 'Burkina Faso', NULL, '2013-01-30 12:56:12'),
	(25, 'Bulgaria', NULL, '2013-01-30 12:56:12'),
	(26, 'Bahrain', NULL, '2013-01-30 12:56:12'),
	(27, 'Burundi', NULL, '2013-01-30 12:56:12'),
	(28, 'Benin', NULL, '2013-01-30 12:56:12'),
	(29, 'Bermuda', NULL, '2013-01-30 12:56:12'),
	(30, 'Brunei Darussalam', NULL, '2013-01-30 12:56:12'),
	(31, 'Bolivia', NULL, '2013-01-30 12:56:12'),
	(32, 'Brazil', NULL, '2013-01-30 12:56:12'),
	(33, 'Bahamas', NULL, '2013-01-30 12:56:12'),
	(34, 'Bhutan', NULL, '2013-01-30 12:56:12'),
	(35, 'Bouvet Island', NULL, '2013-01-30 12:56:12'),
	(36, 'Botswana', NULL, '2013-01-30 12:56:12'),
	(37, 'Belarus', NULL, '2013-01-30 12:56:12'),
	(38, 'Belize', NULL, '2013-01-30 12:56:12'),
	(39, 'Canada', NULL, '2013-01-30 12:56:12'),
	(40, 'Caribbean Nations', NULL, '2013-01-30 12:56:12'),
	(41, 'Cocos (Keeling) Islands', NULL, '2013-01-30 12:56:12'),
	(42, 'Democratic Republic of the Congo', NULL, '2013-01-30 12:56:12'),
	(43, 'Central African Republic', NULL, '2013-01-30 12:56:12'),
	(44, 'Congo', NULL, '2013-01-30 12:56:12'),
	(45, 'Switzerland', NULL, '2013-01-30 12:56:12'),
	(46, 'Cote D\'Ivoire', NULL, '2013-01-30 12:56:12'),
	(47, 'Cook Islands', NULL, '2013-01-30 12:56:12'),
	(48, 'Chile', NULL, '2013-01-30 12:56:12'),
	(49, 'Cameroon', NULL, '2013-01-30 12:56:12'),
	(50, 'China', NULL, '2013-01-30 12:56:12'),
	(51, 'Colombia', NULL, '2013-01-30 12:56:12'),
	(52, 'Costa Rica', NULL, '2013-01-30 12:56:12'),
	(53, 'Serbia and Montenegro', NULL, '2013-01-30 12:56:12'),
	(54, 'Cuba', NULL, '2013-01-30 12:56:12'),
	(55, 'Cape Verde', NULL, '2013-01-30 12:56:12'),
	(56, 'Christmas Island', NULL, '2013-01-30 12:56:12'),
	(57, 'Cyprus', NULL, '2013-01-30 12:56:12'),
	(58, 'Czech Republic', NULL, '2013-01-30 12:56:12'),
	(59, 'Germany', NULL, '2013-01-30 12:56:12'),
	(60, 'Djibouti', NULL, '2013-01-30 12:56:12'),
	(61, 'Denmark', NULL, '2013-01-30 12:56:12'),
	(62, 'Dominica', NULL, '2013-01-30 12:56:12'),
	(63, 'Dominican Republic', NULL, '2013-01-30 12:56:12'),
	(64, 'Algeria', NULL, '2013-01-30 12:56:12'),
	(65, 'Ecuador', NULL, '2013-01-30 12:56:12'),
	(66, 'Estonia', NULL, '2013-01-30 12:56:12'),
	(67, 'Egypt', NULL, '2013-01-30 12:56:12'),
	(68, 'Western Sahara', NULL, '2013-01-30 12:56:12'),
	(69, 'Eritrea', NULL, '2013-01-30 12:56:12'),
	(70, 'Spain', NULL, '2013-01-30 12:56:12'),
	(71, 'Ethiopia', NULL, '2013-01-30 12:56:12'),
	(72, 'Finland', NULL, '2013-01-30 12:56:12'),
	(73, 'Fiji', NULL, '2013-01-30 12:56:12'),
	(74, 'Falkland Islands (Malvinas)', NULL, '2013-01-30 12:56:12'),
	(75, 'Federated States of Micronesia', NULL, '2013-01-30 12:56:12'),
	(76, 'Faroe Islands', NULL, '2013-01-30 12:56:12'),
	(77, 'France', NULL, '2013-01-30 12:56:12'),
	(78, 'France, Metropolitan', NULL, '2013-01-30 12:56:12'),
	(79, 'Gabon', NULL, '2013-01-30 12:56:12'),
	(80, 'United Kingdom', NULL, '2013-01-30 12:56:12'),
	(81, 'Grenada', NULL, '2013-01-30 12:56:12'),
	(82, 'Georgia', NULL, '2013-01-30 12:56:12'),
	(83, 'French Guiana', NULL, '2013-01-30 12:56:12'),
	(84, 'Ghana', NULL, '2013-01-30 12:56:12'),
	(85, 'Gibraltar', NULL, '2013-01-30 12:56:12'),
	(86, 'Greenland', NULL, '2013-01-30 12:56:12'),
	(87, 'Gambia', NULL, '2013-01-30 12:56:12'),
	(88, 'Guinea', NULL, '2013-01-30 12:56:12'),
	(89, 'Guadeloupe', NULL, '2013-01-30 12:56:12'),
	(90, 'Equatorial Guinea', NULL, '2013-01-30 12:56:12'),
	(91, 'Greece', NULL, '2013-01-30 12:56:12'),
	(92, 'S. Georgia and S. Sandwich Islands', NULL, '2013-01-30 12:56:12'),
	(93, 'Guatemala', NULL, '2013-01-30 12:56:12'),
	(94, 'Guam', NULL, '2013-01-30 12:56:12'),
	(95, 'Guinea-Bissau', NULL, '2013-01-30 12:56:12'),
	(96, 'Guyana', NULL, '2013-01-30 12:56:12'),
	(97, 'Hong Kong', NULL, '2013-01-30 12:56:12'),
	(98, 'Heard Island and McDonald Islands', NULL, '2013-01-30 12:56:12'),
	(99, 'Honduras', NULL, '2013-01-30 12:56:12'),
	(100, 'Croatia', NULL, '2013-01-30 12:56:12'),
	(101, 'Haiti', NULL, '2013-01-30 12:56:12'),
	(102, 'Hungary', NULL, '2013-01-30 12:56:12'),
	(103, 'Indonesia', NULL, '2013-01-30 12:56:12'),
	(104, 'Ireland', NULL, '2013-01-30 12:56:12'),
	(105, 'Israel', NULL, '2013-01-30 12:56:12'),
	(106, 'India', NULL, '2013-01-30 12:56:12'),
	(107, 'British Indian Ocean Territory', NULL, '2013-01-30 12:56:12'),
	(108, 'Iraq', NULL, '2013-01-30 12:56:12'),
	(109, 'Iran', NULL, '2013-01-30 12:56:12'),
	(110, 'Iceland', NULL, '2013-01-30 12:56:12'),
	(111, 'Italy', NULL, '2013-01-30 12:56:12'),
	(112, 'Jamaica', NULL, '2013-01-30 12:56:12'),
	(113, 'Jordan', NULL, '2013-01-30 12:56:12'),
	(114, 'Japan', NULL, '2013-01-30 12:56:12'),
	(115, 'Kenya', NULL, '2013-01-30 12:56:12'),
	(116, 'Kyrgyzstan', NULL, '2013-01-30 12:56:12'),
	(117, 'Cambodia', NULL, '2013-01-30 12:56:12'),
	(118, 'Kiribati', NULL, '2013-01-30 12:56:12'),
	(119, 'Comoros', NULL, '2013-01-30 12:56:12'),
	(120, 'Saint Kitts and Nevis', NULL, '2013-01-30 12:56:12'),
	(121, 'Korea (North)', NULL, '2013-01-30 12:56:12'),
	(122, 'Korea', NULL, '2013-01-30 12:56:12'),
	(123, 'Kuwait', NULL, '2013-01-30 12:56:12'),
	(124, 'Cayman Islands', NULL, '2013-01-30 12:56:12'),
	(125, 'Kazakhstan', NULL, '2013-01-30 12:56:12'),
	(126, 'Laos', NULL, '2013-01-30 12:56:12'),
	(127, 'Lebanon', NULL, '2013-01-30 12:56:12'),
	(128, 'Saint Lucia', NULL, '2013-01-30 12:56:12'),
	(129, 'Liechtenstein', NULL, '2013-01-30 12:56:12'),
	(130, 'Sri Lanka', NULL, '2013-01-30 12:56:12'),
	(131, 'Liberia', NULL, '2013-01-30 12:56:12'),
	(132, 'Lesotho', NULL, '2013-01-30 12:56:12'),
	(133, 'Lithuania', NULL, '2013-01-30 12:56:12'),
	(134, 'Luxembourg', NULL, '2013-01-30 12:56:12'),
	(135, 'Latvia', NULL, '2013-01-30 12:56:12'),
	(136, 'Libya', NULL, '2013-01-30 12:56:12'),
	(137, 'Morocco', NULL, '2013-01-30 12:56:12'),
	(138, 'Monaco', NULL, '2013-01-30 12:56:12'),
	(139, 'Moldova', NULL, '2013-01-30 12:56:12'),
	(140, 'Madagascar', NULL, '2013-01-30 12:56:12'),
	(141, 'Marshall Islands', NULL, '2013-01-30 12:56:12'),
	(142, 'Macedonia', NULL, '2013-01-30 12:56:12'),
	(143, 'Mali', NULL, '2013-01-30 12:56:12'),
	(144, 'Myanmar', NULL, '2013-01-30 12:56:12'),
	(145, 'Mongolia', NULL, '2013-01-30 12:56:12'),
	(146, 'Macao', NULL, '2013-01-30 12:56:12'),
	(147, 'Northern Mariana Islands', NULL, '2013-01-30 12:56:12'),
	(148, 'Martinique', NULL, '2013-01-30 12:56:12'),
	(149, 'Mauritania', NULL, '2013-01-30 12:56:12'),
	(150, 'Montserrat', NULL, '2013-01-30 12:56:12'),
	(151, 'Malta', NULL, '2013-01-30 12:56:12'),
	(152, 'Mauritius', NULL, '2013-01-30 12:56:12'),
	(153, 'Maldives', NULL, '2013-01-30 12:56:12'),
	(154, 'Malawi', NULL, '2013-01-30 12:56:12'),
	(155, 'Mexico', NULL, '2013-01-30 12:56:12'),
	(156, 'Malaysia', NULL, '2013-01-30 12:56:12'),
	(157, 'Mozambique', NULL, '2013-01-30 12:56:12'),
	(158, 'Namibia', NULL, '2013-01-30 12:56:12'),
	(159, 'New Caledonia', NULL, '2013-01-30 12:56:12'),
	(160, 'Niger', NULL, '2013-01-30 12:56:12'),
	(161, 'Norfolk Island', NULL, '2013-01-30 12:56:12'),
	(162, 'Nigeria', NULL, '2013-01-30 12:56:12'),
	(163, 'Nicaragua', NULL, '2013-01-30 12:56:12'),
	(164, 'Netherlands', NULL, '2013-01-30 12:56:12'),
	(165, 'Norway', NULL, '2013-01-30 12:56:12'),
	(166, 'Nepal', NULL, '2013-01-30 12:56:12'),
	(167, 'Nauru', NULL, '2013-01-30 12:56:12'),
	(168, 'Niue', NULL, '2013-01-30 12:56:12'),
	(169, 'New Zealand', NULL, '2013-01-30 12:56:12'),
	(170, 'Sultanate of Oman', NULL, '2013-01-30 12:56:12'),
	(171, 'Other', NULL, '2013-01-30 12:56:12'),
	(172, 'Panama', NULL, '2013-01-30 12:56:12'),
	(173, 'Peru', NULL, '2013-01-30 12:56:12'),
	(174, 'French Polynesia', NULL, '2013-01-30 12:56:12'),
	(175, 'Papua New Guinea', NULL, '2013-01-30 12:56:12'),
	(176, 'Philippines', NULL, '2013-01-30 12:56:12'),
	(177, 'Pakistan', NULL, '2013-01-30 12:56:12'),
	(178, 'Poland', NULL, '2013-01-30 12:56:12'),
	(179, 'Saint Pierre and Miquelon', NULL, '2013-01-30 12:56:12'),
	(180, 'Pitcairn', NULL, '2013-01-30 12:56:12'),
	(181, 'Puerto Rico', NULL, '2013-01-30 12:56:12'),
	(182, 'Palestinian Territory', NULL, '2013-01-30 12:56:12'),
	(183, 'Portugal', NULL, '2013-01-30 12:56:12'),
	(184, 'Palau', NULL, '2013-01-30 12:56:12'),
	(185, 'Paraguay', NULL, '2013-01-30 12:56:12'),
	(186, 'Qatar', NULL, '2013-01-30 12:56:12'),
	(187, 'Reunion', NULL, '2013-01-30 12:56:12'),
	(188, 'Romania', NULL, '2013-01-30 12:56:12'),
	(189, 'Russian Federation', NULL, '2013-01-30 12:56:12'),
	(190, 'Rwanda', NULL, '2013-01-30 12:56:12'),
	(191, 'Saudi Arabia', NULL, '2013-01-30 12:56:12'),
	(192, 'Solomon Islands', NULL, '2013-01-30 12:56:12'),
	(193, 'Seychelles', NULL, '2013-01-30 12:56:12'),
	(194, 'Sudan', NULL, '2013-01-30 12:56:12'),
	(195, 'Sweden', NULL, '2013-01-30 12:56:12'),
	(196, 'Singapore', NULL, '2013-01-30 12:56:12'),
	(197, 'Saint Helena', NULL, '2013-01-30 12:56:12'),
	(198, 'Slovenia', NULL, '2013-01-30 12:56:12'),
	(199, 'Svalbard and Jan Mayen', NULL, '2013-01-30 12:56:12'),
	(200, 'Slovak Republic', NULL, '2013-01-30 12:56:12'),
	(201, 'Sierra Leone', NULL, '2013-01-30 12:56:12'),
	(202, 'San Marino', NULL, '2013-01-30 12:56:12'),
	(203, 'Senegal', NULL, '2013-01-30 12:56:12'),
	(204, 'Somalia', NULL, '2013-01-30 12:56:12'),
	(205, 'Suriname', NULL, '2013-01-30 12:56:12'),
	(206, 'Sao Tome and Principe', NULL, '2013-01-30 12:56:12'),
	(207, 'El Salvador', NULL, '2013-01-30 12:56:12'),
	(208, 'Syria', NULL, '2013-01-30 12:56:12'),
	(209, 'Swaziland', NULL, '2013-01-30 12:56:12'),
	(210, 'Turks and Caicos Islands', NULL, '2013-01-30 12:56:12'),
	(211, 'Chad', NULL, '2013-01-30 12:56:12'),
	(212, 'French Southern Territories', NULL, '2013-01-30 12:56:12'),
	(213, 'Togo', NULL, '2013-01-30 12:56:12'),
	(214, 'Thailand', NULL, '2013-01-30 12:56:12'),
	(215, 'Tajikistan', NULL, '2013-01-30 12:56:12'),
	(216, 'Tokelau', NULL, '2013-01-30 12:56:12'),
	(217, 'Timor-Leste', NULL, '2013-01-30 12:56:12'),
	(218, 'Turkmenistan', NULL, '2013-01-30 12:56:12'),
	(219, 'Tunisia', NULL, '2013-01-30 12:56:12'),
	(220, 'Tonga', NULL, '2013-01-30 12:56:12'),
	(221, 'East Timor', NULL, '2013-01-30 12:56:12'),
	(222, 'Turkey', NULL, '2013-01-30 12:56:12'),
	(223, 'Trinidad and Tobago', NULL, '2013-01-30 12:56:12'),
	(224, 'Tuvalu', NULL, '2013-01-30 12:56:12'),
	(225, 'Taiwan', NULL, '2013-01-30 12:56:12'),
	(226, 'Tanzania', NULL, '2013-01-30 12:56:12'),
	(227, 'Ukraine', NULL, '2013-01-30 12:56:12'),
	(228, 'Uganda', NULL, '2013-01-30 12:56:12'),
	(229, 'United States', NULL, '2013-01-30 12:56:12'),
	(230, 'Uruguay', NULL, '2013-01-30 12:56:12'),
	(231, 'Uzbekistan', NULL, '2013-01-30 12:56:12'),
	(232, 'Vatican City State (Holy See)', NULL, '2013-01-30 12:56:12'),
	(233, 'Saint Vincent and the Grenadines', NULL, '2013-01-30 12:56:12'),
	(234, 'Venezuela', NULL, '2013-01-30 12:56:12'),
	(235, 'Virgin Islands (British)', NULL, '2013-01-30 12:56:12'),
	(236, 'Virgin Islands (U.S.)', NULL, '2013-01-30 12:56:12'),
	(237, 'Viet Nam', NULL, '2013-01-30 12:56:12'),
	(238, 'Vanuatu', NULL, '2013-01-30 12:56:12'),
	(239, 'Wallis and Futuna', NULL, '2013-01-30 12:56:12'),
	(240, 'Samoa', NULL, '2013-01-30 12:56:12'),
	(241, 'Yemen', NULL, '2013-01-30 12:56:12'),
	(242, 'Mayotte', NULL, '2013-01-30 12:56:12'),
	(243, 'Yugoslavia', NULL, '2013-01-30 12:56:12'),
	(244, 'South Africa', NULL, '2013-01-30 12:56:12'),
	(245, 'Zambia', NULL, '2013-01-30 12:56:12'),
	(246, 'Zimbabwe', NULL, '2013-01-30 12:56:12');
/*!40000 ALTER TABLE `settings_country` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_currency
DROP TABLE IF EXISTS `settings_currency`;
CREATE TABLE IF NOT EXISTS `settings_currency` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(30) NOT NULL,
  `description` varchar(128) NOT NULL,
  `symbol` varchar(60) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `decimal_places` tinyint(4) NOT NULL DEFAULT '2',
  `decimal_separator` varchar(1) NOT NULL DEFAULT '.',
  `thousands_separator` char(1) NOT NULL DEFAULT ',',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_currency: ~4 rows (approximately)
DELETE FROM `settings_currency`;
/*!40000 ALTER TABLE `settings_currency` DISABLE KEYS */;
INSERT INTO `settings_currency` (`id`, `code`, `description`, `symbol`, `is_active`, `decimal_places`, `decimal_separator`, `thousands_separator`, `date_created`, `created_by`) VALUES
	(2, 'USD', 'US Dollars (USD)', '&dollar;', 1, 2, '.', ',', '2014-05-08 15:19:03', 1),
	(4, 'KES', 'Kenyan Shillings (KES)', 'Ksh', 1, 2, '.', ',', '2014-05-08 22:02:34', 1),
	(15, 'EUR', 'EURO', '&euro;', 1, 2, '.', ',', '2014-05-09 03:32:21', 1),
	(29, 'GBP', 'British Pound', '&pound;', 1, 2, '.', ',', '2014-05-10 17:44:44', 1);
/*!40000 ALTER TABLE `settings_currency` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_currency_conversion
DROP TABLE IF EXISTS `settings_currency_conversion`;
CREATE TABLE IF NOT EXISTS `settings_currency_conversion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `from_currency_id` int(11) unsigned NOT NULL,
  `to_currency_id` int(11) unsigned NOT NULL,
  `exchange_rate` decimal(20,8) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `from_currency_id` (`from_currency_id`),
  KEY `to_currency_id` (`to_currency_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_currency_conversion: ~12 rows (approximately)
DELETE FROM `settings_currency_conversion`;
/*!40000 ALTER TABLE `settings_currency_conversion` DISABLE KEYS */;
INSERT INTO `settings_currency_conversion` (`id`, `from_currency_id`, `to_currency_id`, `exchange_rate`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
	(5, 2, 4, 87.85000000, '2014-05-29 15:25:03', 1, '2014-05-29 15:40:19', 1),
	(6, 2, 15, 0.73000000, '2014-05-29 15:25:03', 1, '2014-05-29 15:40:19', 1),
	(7, 2, 29, 0.60000000, '2014-05-29 15:25:03', 1, '2014-05-29 15:40:20', 1),
	(8, 4, 2, 0.01100000, '2014-05-29 15:31:00', 1, '2014-05-29 15:42:05', 1),
	(9, 4, 15, 0.00840000, '2014-05-29 15:31:00', 1, '2014-05-29 15:42:06', 1),
	(10, 4, 29, 0.00680000, '2014-05-29 15:31:00', 1, '2014-05-29 15:42:06', 1),
	(11, 15, 2, 1.36000000, '2014-05-29 15:35:59', 1, NULL, NULL),
	(12, 15, 4, 119.63000000, '2014-05-29 15:35:59', 1, NULL, NULL),
	(13, 15, 29, 0.81000000, '2014-05-29 15:35:59', 1, NULL, NULL),
	(14, 29, 2, 1.67000000, '2014-05-29 15:38:06', 1, NULL, NULL),
	(15, 29, 4, 146.93000000, '2014-05-29 15:38:06', 1, NULL, NULL),
	(16, 29, 15, 1.23000000, '2014-05-29 15:38:07', 1, NULL, NULL);
/*!40000 ALTER TABLE `settings_currency_conversion` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_department
DROP TABLE IF EXISTS `settings_department`;
CREATE TABLE IF NOT EXISTS `settings_department` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_department: ~3 rows (approximately)
DELETE FROM `settings_department`;
/*!40000 ALTER TABLE `settings_department` DISABLE KEYS */;
INSERT INTO `settings_department` (`id`, `location_id`, `name`, `description`, `status`, `date_created`, `created_by`) VALUES
	(1, 0, 'Finance', NULL, 'Active', '2014-03-16 23:28:31', 1),
	(2, 0, 'Programs', NULL, 'Active', '2014-03-16 23:29:24', 1),
	(3, 0, 'Admin', NULL, 'Active', '2014-03-16 23:29:36', 1);
/*!40000 ALTER TABLE `settings_department` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_email_template
DROP TABLE IF EXISTS `settings_email_template`;
CREATE TABLE IF NOT EXISTS `settings_email_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `from` varchar(255) NOT NULL,
  `comments` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_email_template: ~4 rows (approximately)
DELETE FROM `settings_email_template`;
/*!40000 ALTER TABLE `settings_email_template` DISABLE KEYS */;
INSERT INTO `settings_email_template` (`id`, `key`, `description`, `subject`, `body`, `from`, `comments`, `date_created`, `created_by`) VALUES
	(2, 'admin_reset_password', 'Email sent to a user when the admin resets the user\'s password', 'New Password', '<p>\r\n	Hi {name},\r\n</p>\r\n<p>\r\n	You Password has been reset by the web master.Here are your new login details\r\n</p>\r\n<ul>\r\n	<li>Username: <strong>{username}</strong> or <strong>{email}</strong></li>\r\n	<li>Password: <strong>{password}</strong></li>\r\n</ul>\r\n      To login now please <a target="_blank" rel="nofollow" href="http://{link}">click here</a> or copy and paste this link to your browser: {link} <br>\r\n <br>\r\n<p>\r\n	Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', 'Placeholders: {name}=> The Admin full name, {username}=>admin username, {email}=>admin email,{password} => The new password, {link}=> Login link ', '2013-09-28 02:43:30', 2),
	(3, 'forgot_password', 'Email sent to a user who forgot his/her password to asssist in password recovery', 'Password Recovery', '<p>\r\n	 Hello {name},\r\n</p>\r\n<p>\r\n	     Please <a target="_blank" rel="nofollow" href="http://{link}">click here</a>   or copy and paste this link: {link} to your browser to change your password. If you never initiated this password recovery process, please just ignore this email.\r\n</p>\r\n<p>\r\n	  Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', 'Placehoders: {name}=>The name of the user, {link}=>link for reseting password', '2013-09-28 02:43:48', 2),
	(4, 'account_activation', 'Account activation email template', 'Activate your account', '<p>\r\n	    Hi {name}.\r\n</p>\r\n<p>\r\n	    Thank you for joining ME HEALTH.\r\n</p>\r\n<p>\r\n	 To activate you account please  <a href="{link}">click here</a>  or copy and paste this link to your browser: {link}.\r\n</p>\r\n<p>\r\n	 Chimesgreen Team\r\n</p>', 'noreply@felsoftsystems.com', NULL, '2013-10-11 12:10:11', 1),
	(5, 'RFQ', 'Request for Quotation', 'Request for Quotation', '<p>\r\n	 Dear <strong>{supplier}</strong>,\r\n</p>\r\n<p>\r\n	 You are hereby invited to submit your quotation for the supply of the following goods/services.\r\n</p>\r\n<h3>Requisition Details</h3>\r\n<p>\r\n	 <strong>{header} </strong>\r\n</p>\r\n<p>\r\n	 <strong>{details}</strong>\r\n</p>\r\n<p>\r\n	 Best Regards,\r\n</p>\r\n<p>\r\n	 <strong>{sender} </strong>\r\n</p>\r\n<p>\r\n	<strong>-------------------------------------------------</strong>\r\n</p>\r\n<p>\r\n	<strong>Chimesgreen Team</strong><br>\r\n	<strong></strong>\r\n</p>', 'nonereply@felsoftsystems.com', NULL, '2014-04-28 09:26:56', 2);
/*!40000 ALTER TABLE `settings_email_template` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_frequency
DROP TABLE IF EXISTS `settings_frequency`;
CREATE TABLE IF NOT EXISTS `settings_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `no_of_days` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_frequency: ~6 rows (approximately)
DELETE FROM `settings_frequency`;
/*!40000 ALTER TABLE `settings_frequency` DISABLE KEYS */;
INSERT INTO `settings_frequency` (`id`, `name`, `no_of_days`, `date_created`, `created_by`) VALUES
	(1, 'Weekly', 7, '2014-08-29 10:42:54', 1),
	(2, 'Monthly', 30, '2014-08-29 10:43:13', 1),
	(3, 'Quaterly', 90, '2014-08-29 10:43:30', 1),
	(4, 'Annually', 365, '2014-08-29 10:43:45', 1),
	(5, 'biennially', 730, '2014-08-29 10:46:55', 1),
	(6, 'biannually ', 180, '2014-08-29 10:48:16', 1);
/*!40000 ALTER TABLE `settings_frequency` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_location
DROP TABLE IF EXISTS `settings_location`;
CREATE TABLE IF NOT EXISTS `settings_location` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `latitude` varchar(30) DEFAULT NULL,
  `longitude` varchar(30) DEFAULT NULL,
  `country_id` int(11) unsigned DEFAULT NULL,
  `town_id` int(11) unsigned DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `status` enum('Active','Closed') NOT NULL DEFAULT 'Active',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `parent_location_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `town_id` (`town_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_location: ~2 rows (approximately)
DELETE FROM `settings_location`;
/*!40000 ALTER TABLE `settings_location` DISABLE KEYS */;
INSERT INTO `settings_location` (`id`, `name`, `description`, `latitude`, `longitude`, `country_id`, `town_id`, `email`, `phone`, `address`, `status`, `is_active`, `parent_location_id`, `date_created`, `created_by`) VALUES
	(1, 'Oyugis', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', 1, NULL, '2014-05-10 00:55:10', 1),
	(2, 'Nairobi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Active', 1, NULL, '2014-05-10 00:55:22', 1);
/*!40000 ALTER TABLE `settings_location` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_modules_enabled
DROP TABLE IF EXISTS `settings_modules_enabled`;
CREATE TABLE IF NOT EXISTS `settings_modules_enabled` (
  `id` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'Enabled',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_modules_enabled: ~17 rows (approximately)
DELETE FROM `settings_modules_enabled`;
/*!40000 ALTER TABLE `settings_modules_enabled` DISABLE KEYS */;
INSERT INTO `settings_modules_enabled` (`id`, `name`, `description`, `status`, `date_created`, `created_by`) VALUES
	('admin', 'General Administration', 'General Administration', 'Enabled', '2014-03-20 12:40:16', 1),
	('assets_register', 'Assets Register Module', NULL, 'Enabled', '2014-03-10 14:25:12', 1),
	('claims_management', 'Claims', 'Claims Management', 'Enabled', '2014-07-02 08:42:52', 1),
	('company_docs', 'Company Documents', 'Company documents', 'Enabled', '2014-03-13 23:11:20', 1),
	('consultancy', 'Consultancy Module', NULL, 'Enabled', '2014-03-10 14:26:26', 1),
	('doc_manager', 'Documents Manager', NULL, 'Enabled', '2016-07-03 04:12:33', 28),
	('employees', 'Employees', 'Employees as independent module', 'Enabled', '2014-06-04 16:19:15', 2),
	('events', 'Events Module', 'Utility Schedule and other calender events', 'Enabled', '2014-03-10 14:24:51', 1),
	('expense_advance_management', 'Expense Advance Management', NULL, 'Enabled', '2014-07-02 08:44:41', 1),
	('fleet_management', 'Fleet Management Module', NULL, 'Enabled', '2014-03-10 14:25:58', 1),
	('hr', 'Human Resource Module', NULL, 'Enabled', '2014-03-10 14:24:05', 1),
	('invoice_payment', 'Payment Module', 'Payment Module', 'Enabled', '2014-10-07 12:10:48', 1),
	('payroll', 'Payroll Module', NULL, 'Enabled', '2014-03-10 14:22:28', 1),
	('Program', 'Programs ', 'Monitoring and Evaluation module', 'Enabled', '2015-01-22 11:49:46', 1),
	('Requisition', 'Requisition', NULL, 'Enabled', '2014-04-24 19:02:02', 1),
	('stock_inventory', 'Stock Inventory Module', NULL, 'Enabled', '2014-03-10 14:23:31', 1),
	('TRAINING', 'TRAINING', NULL, 'Enabled', '2016-04-27 00:27:39', 28);
/*!40000 ALTER TABLE `settings_modules_enabled` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_numbering_format
DROP TABLE IF EXISTS `settings_numbering_format`;
CREATE TABLE IF NOT EXISTS `settings_numbering_format` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(60) NOT NULL,
  `description` varchar(255) NOT NULL,
  `next_number` int(11) NOT NULL DEFAULT '1',
  `min_digits` smallint(6) NOT NULL DEFAULT '4',
  `prefix` varchar(5) DEFAULT NULL,
  `suffix` varchar(5) DEFAULT NULL,
  `preview` varchar(128) DEFAULT NULL,
  `module` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_numbering_format: ~3 rows (approximately)
DELETE FROM `settings_numbering_format`;
/*!40000 ALTER TABLE `settings_numbering_format` DISABLE KEYS */;
INSERT INTO `settings_numbering_format` (`id`, `type`, `description`, `next_number`, `min_digits`, `prefix`, `suffix`, `preview`, `module`, `date_created`, `created_by`) VALUES
	(1, 'purchase_order', 'Purchase Order', 149, 3, 'PO/', NULL, 'PO/001', 'settings', '2014-06-16 14:30:51', 1),
	(2, 'procure_rfq', 'Request for Quotation', 13, 4, 'LISP/', NULL, 'LISP/0001', 'settings', '2014-11-29 09:42:43', 1),
	(3, 'procure_rfq_award_no', 'Bid Award Reference Number', 17, 4, 'LISP/', NULL, 'LISP/0001', 'settings', '2015-01-12 17:28:27', 1);
/*!40000 ALTER TABLE `settings_numbering_format` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_org
DROP TABLE IF EXISTS `settings_org`;
CREATE TABLE IF NOT EXISTS `settings_org` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(128) NOT NULL,
  `status` enum('Active','Blocked') NOT NULL DEFAULT 'Active',
  `logo` blob,
  `file_type` varchar(10) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_org: ~1 rows (approximately)
DELETE FROM `settings_org`;
/*!40000 ALTER TABLE `settings_org` DISABLE KEYS */;
INSERT INTO `settings_org` (`id`, `company_name`, `status`, `logo`, `file_type`, `date_created`, `created_by`) VALUES
	(1, 'LifeSkills Promoters', 'Active', _binary 0x6956424F5277304B47676F414141414E5355684555674141415A5141414142594541594141414343434E3259414141414247644254554541414C4750432F7868425141414141467A556B6443414B374F484F6B4141414167593068535451414165695941414943454141443641414141674F67414148557741414471594141414F706741414264776E4C70525041414141415A69533064452F2F2F2F2F2F2F2F43566A333341414141416C7753466C7A4141414F7777414144734D4278322B6F5A41414141416C326345466E414141426C41414141466741527A4131744141416741424A52454655654E72736657566746636E5737616F2B4669644B49466A51344F3775377535756737734E376A61347537737A754C74446341496B42496D37482B313650336F336433496D4449484C44484F2F312B73486D39507072693772717533466F43415678414E696537386E414D764871747539646E5650374A4E776264756A576A5A695272377239516A6250726275747630374A51654561545A705674584A65714D303530426B4A4F38704349783565507A73326974516F4543424167554B46436851384C384E34576458344776676E504E494A34434C6E456436614B3777474D346A686A7143633837444877476979486C553141393834544D45736A6B4154754633555633796D655755364D717662646D636B69396C54584B54725A4D7431537A4C78437A396D306B33712B372B375035526F4543424167554B464368516F4F442F4574512F75774C66424B61716A4367414A57324D634159735045484C4E674A4F4F6B6D513452795636633663524C637A7868686A332F434B48597A7065774B34682B4D6F62577650472F4D6E4B5471646A526A4B4F786D4B41377766587646454953654152584345376E2B774678556F554B4241675149464368516F2B4E666958323942595977783933694143597935682B7456466745516C30646F676550484D7063452B475165464C47716145356A76436E322F6F354A2F513037545135334B777A4A774A2F7776754644334D61494979306E50323446556E77734470782F2F583257646D4B4E6D4C6D414F4167467A586165305877773232774D56343167496A747457517177624B776A49426A6F397438414142652F763332576C76783830486A41386F6A664474766E58493037383943496E566E485351495879386B353535626850337355464368516F4543424167554B46436A345A2F4376463141346C3179344A4D7079716E494354722B684A64436B4B542B676E574E78354164696E30302F46707551654F54536A656D644570346C54726E55594D6B51336C4763474F77374B6A486D536D544568413241545637687761646358332B66705A656C76336F7549423632744C52306A56396C46737761793052786A4C425A645A5946415A7073366F4B71454A747131483071414F443130392B655A4E2B6B48767254556E7657757747434A7A3972666C7739722F476350737572456874484A50564E636E3459635736416D4E5753352B58783973304151466A456E4C694E2B553577334D38654451554B464368516F4543424167554B2F6C3738367755554B327746674A5348374F54484F7234725757766A65485377444F51525235756F736F6B566F6D32546168677A476A4A64764D3379476F59612F5639647276585339563747727175333539396D446B472B5A386D412B4A54487842623538677330707A586547544944367375716D626250452F4E7168367162322F666C467978334C5238744451485459444F4D70566D4D644465724352464166506F62594B5052755965764251427856504E6F74794E694E624648534D787632354B51644F36385938756D3854666A52682F395058383277794F6A665644373057594169442F6D4573715043546C4D57333532397974516F4543424167554B464368513850666958792B674D4D61596D35744D6556584747484D647A76585A7A68562F2F3645316F4A367155526661634B4342585658627267574F5053374B38754B7435537151734D3759354661544D68654E743858576A2F585464716A7641375658324C6B7A5638784E36766358372B737252754D3667444B7752776F67564D46706351484164616871715147497255527762384D6D3657367845697350734170666277666E6E4564704155514C35384B324159425149715A7A3263784A565A4C653363377273387051306A6A35326C46416D30663330765A334D555A37512F764559647A3138627768336D43552B415A4E4C586E6A6C77466D3877394F43714241675149464368516F554B424167594C2F48707A7A794B4454636E4138697A4D6E6D302F65586238794C72703264496D4A637A6C2F317A2F4D7076494D7A694E7A787933363153583575726A6363767065726B464E6B33496D754454796B70344C7266766E6369333344466B656A7762454634595472795931334243314D377A6879484C6D2F694548777038324F3836356F57444B6E444F4E46376D455A67374A33716B627750576352373734793370797A674675464F4E6635514D34352F75443978567159567873636A75323655486D6B4758687564724D346A793054786876636F397A7731334439524E584135397A4C70594D4B5A422F50756647616B396166785A4D65696F43696749464368516F554B4241675149462F324C77327A7738594A496B43487A53352B616D4F366247312B3764486865784C484A383378366342367444437A5630346A773564334C52625A56757533484F65667732393471633831782B61575433736777332B64792B41596A4C54596366585771344964493575762B41382B622B5164306A772B70753574775162327879617359756E535234714D74777A6E6C3474372B6F482B662B6E30704A39597439376E4C665A44435575586A6A397866686C794A6D39752F4B2B667537496646564263356A547353316E744D74715278664C527065444F79776E54666C764E4A51514B78766D424B682B646D39724543424167554B464368516F45444250344E2F765976583132417142593863726743416F39476950314F5856522F504F336459563065743765676178554B666939644678734F4235436A6A6B357558796C77794437524D664A4270646D4D412F7034764863667759587A64652B30664F6953332B7033644E6F414E556A65336D78423352643145647957504A7247696D45746334564D464D495562363670764A30765A7538425834684D41317A2F58792F4C4A4F4E5277414443664E706F57764142675A396E3233714765582F4C4A704C45333974522B5A6F777A6A6E3765444841365A486539386D484173597A44785372426832366A5039506D7A486C30496A596A61743951514469746D2B5A682B746D39724543424167554B464368516F454342676D384335357848444A4E647174517163794E4C3662755A4E32364C6E42537A2F7463476E482B63474C71317768544F592F736B544A3836797268617643522B656C69745578754C316A4C306B73306679676E6D357A34756C63714A4F614B64617A7872486E494866514A5478756D76586A6B7762615568642F4B313530337935452F7345642F397069474E656A53785849676F4431674B63463569414D41354C2F662B626175484B61565453757937464A6A7A5539586744593061636835324C2F7839317832636D782B616D6C30596655484C4F65637863374F57346D59652F37627A66317A44464368516F4543424167554B4643685138412F417446626345486B54344A7850386E3045694E7635674865624D69556B4C3068327678506665622F425075585272564839657641336649582F77534A6C784B6469726F634E763836343677654C32353858425978367A6F4D575A446C674B6D783075525335396D47554B754A6C2F304B6342346D684E64763135447735572F4C516B34363752524A6F626E4C4F65584161416F6534334E496A616845674872664D692B706D33347258466574477A765149454A7461624B4B532F6E772F48794365437A6769315450613371317053712F6B385766363359734E79525056714E737A7A674E586846367370755538595636533366724D43633035353266666C6130376B7A2F682F4F6C7877424B56596F36302B396D6A6F3043424167554B464368516F4544422F3263773552463949766F426E4276576A58384469426E466D713953666C3065577A7A615A304E6C697A6E38636C6A376B573663367876717733654D65394B5763383566337174615342496F644C6E34522F343039745758792B6463725061324863576D36484B344A303950726E3730386F5858736463544D712B4C695735767A6D757875374E71387A504F4F54665674306E6D6E5050674E4D7178754A7074776A686738544937687A4E564C30743763374E774435584A6F6A475A776F782F766C2B634B575A38505248676E41384E5046314A48564D72387647616C596E585067774A336C465835447A696365796F6962386B624462554D63322F717874656733504F6B3873365A4C53453834756863332F32714368516F4543424167554B46436851385038706A4863733938506241596274657076374777486A593850453531327258592B4A6A4B6D39366C58437071446D6F644F724E2B41385A457A453176716E4F592B666E44683377597649396961317966745337384F6A65427A6E722B49724F5667537851326248306D435347536C50372B48467A6664654E3065344E744635343872504E357A502F464377495843727A6E6E5048694E5978374F4F512F732B6550614A52626D56642B76412F67347A6F4D665A6A2B6C31787261585239386F5837632F7354534F2B4E6A2B3572647A614E75727432656D33504F343666716A6E484F2B62762B36532F2F633159776955376E6E5050344B4F65386E484D65342B42776833504F6F33762F374E46566F4543424167554B464368516F4F443777503737497634376942564634554D3941463152563166446957454A636E3459392B753578414D4A47652B34644A7961734333522B6371397247583561694662794354387074477262585073424F782B733939537164436262506158624F504B565274646D4233436D654A314C716E67492F374F505A4F377755396F6248706F6269756D41436C547351586241514771712B7365725A75326234396C5538757872586F3279436D38525244433463656565465A324C39312B6B4B586C2B59326E5062615563425166316E355939343270394744544A35455A376C547A4D6D38514F78745075473753744E4A6355546435666335746459625A4C6C4D75756E5259304C6C447853742B4C78304C4F4F354F69625563546E7957654E34386D3357507339672F64532F4A6D336D745A47465247643058495A6B50534A6E733453753274567A5435597061706D7171376D52344778374A4747505A6876363558314B37737248474147596E7A3354756950586D73324537717A635236335058304E4A56677657426C734D6837537557567539525638706F3933434964715836657555615136384377496462706D754D4D5A616A347338655A51554B464368516F4543424167554B2F736467336D2F70614F6F4D6D494D34353579397366517A354830396F637A764B547554317039633933524E54505059476C4D4378645A42786A446E3276476366317761466C6A39487563784958454E4A6C6C697A5962616570656A753379442B4677787736765477343861484D79756E5359426C677563522B7A46654975463836676F315941507A344D72424E5943776E565247543865634E416C7A7A4C636A48584C7348334268506B333932327375365870765559764A3336364E446676674A785A2B34636D56584C58754B7A70317050507944416B773937574F7A6E5058437654673437314C4C654B5679396166564478344E3139792F53357538786C7A6235626362654842347776646F567A7A684F633743744656597665482B6D463458484765444879795A2F6279376E704E756341373855626361364F344A787A537975376A7549597675466A6646577A6559586F2B6A546E4E49666B35536D626A78355A3943724670422B2B6466316437376A354D63746E746B343547336B3772484F33685A77483351394A624A696638356A6E636170563034496D634337366644446E4E3474447A5274656C2F335A6F367041675149464368516F554B42417762666870317451724D453562357A5345514151705930547067433447563035377776656941384E307A64366B314C615550662B794B47612B4558785A382F327958355876473861455051376F44716F336532344774414F31446E57376864345454745076627930667A6650784E6C4A4B2F3137585058786845656A616775415A387366646C646642346F4D4B625531367744676C2F686544646670477356642F5868622F6343386470522F462F397A7754657972444D4D30646533715166674C7172704F494453724B4D59416941434C385545514C6A4467766B4677446D6A38795A574279675456627061396E746E4E37635930694B7357756265742F705737722B73336F35775866536E79506578513431476F625451423476344F7674724C6B476E3677457148356A4C6C4B38365443776F326B5856366A636476586D462B4932354D70766D6D69614549392F716C415970654C484F7459737068386E3131532B414A5253425555734163775A6D546C6F4D614E594A6A6835504150554D6C53466651584E7457792B627674552B2B6A36777A32447A73637A474C6936384170396E79767A716E63706531533750727A39375642556F554B4241675149464368516F53422F2B64514B4B4E546A6E33464157534E6C6C76447631446144726F36335865556D64594D7361342F546767744E326D2B63616E487A664635345A39303776637A324C593751356A4A664C64674877364F3638747A586257564633554E752B326F544F483847426C4962344E47766C7441716E6177415A2B6D5A5935764336324D33742F6C766A54345966576674672B704E52496248655738534C66446366442F43564F474778415A4164584841455747644571764D432B4D677534776E4179714B42656A4341557549705378664177635042614A67454E4B37612B486E357872745679336374697875316430442F4F385875394970786931315650365A52574D4A306749316C3535307175685A49584A3130356B377A3335636B766B7A4F644B4A716862704D59414E66786748694B637437773257414E52434849444F67446454733836794C47465155617553596A4C376F706836566F3657593362364D336375635A7836357141594B76325775742F4B3053696355634C74306F697A755956376D6B78487663526C494C7351504363474D7565623532614F6F5149454342516F554B464367514548366F50375A4666676147474E4D642F63504D526B6445514F634F79644169377A503771526F4C6D6D7A5A7039556134764B594F4E513673773857324D5867796B3563304A757A526931364B342B7653746D51476952666F74513747587A6741615464774B2F447072796F59574C2B6B3466723134566C313866382F6235504C2B383731323974316A4F696236615677436955464134436141396E49534C4145516B49416E6748497937415142366F4250414E2F4C4233424651586359794D542B514D4351786D6E73446A77373565722B7233616261675A674439532F324F2B455A2B6947305964544E6E616972616C53696241644139516A513631306A6D626471746B61664B3946536E6C2F58644163305234537448746B423752744E4E303937774F36683764686932314D65366E4A725475653176534B7967366F5247534A32764742486D492F54793454352B494265747073656A41525137644F756F427341344A6D49473477785A7450735A342B614167554B464368516F4543424167586668332B394265564C73427A6E61734D496746764562536A445A6A412F59584473395477575953747561686248482B46362B4B566B43764D56416C6A544C4E48412B743372746C367041326A726135734A75547A667A373831373851572F2F5058586A56355754496D756E416E537A6555316D594473416C4A634B4B583846525542524D41734A3634436A4133766B6156425242616F62686C4C53424F596B62524872415662615A5A6467474E4F74647656504446622F6E33587A76734F4F50674B442F3959624E3332456C416E6142364A317877586D4F75496A3650657A44465365546D58794F4C36475A704A67734E6A66734454776F68724B2F4465664533595A6671706D7645303068554656577570323833784375686E4D456C726A4E6A6A486D4D2B396D39723043424167554B464368516F45444233344E2F7651586C533141315957626449674241523442506B71362B4151416672507A7A2F534643534B57494A59436C7557574475595A33534572466C4F4938523834796C6B306F6F466B495944727357523370587436594872496C797568663653444834377771774839485233346534444634796F734472444C3671613442526F4F786E2F67415344496B313941584B6432487A6D334A6336543967667433356869615A7A3354477158327841347555454D6F354F59374970636474456479522F4961374243415A636A4247474F4D4166424D566631642F377569704149464368516F554B4241675149463663662F724944797258672F376B5044594366416F72486B4E7539676E564947474F615A624A6748697347656C3851744A4341586E77344165497137394A4238737279612F6E5542414834595977416377304C65454D42525A4F58334158455A7A714D33674D32384D4A734C7850564B574A74796A4B304D5350492F465059657577506576702F7736536A51497062356C512B43435963426548412F656F4F66496F416F554B4241675149464368516F5550413343436879724D6A2B52337674376A634838746E346C4D7A6B596D2F377175744C7935754E4F63644872593163487464487656736F4B487853682F427266437765436463427A734178486F41742B476642414141456142414A34413232436A63424869487574587342694A505957373444554731556454515541495132596E74745666626159616A3954715245462B2F38766A75726E76796843355679634D6D30705A38757551506D6570623270714B3832725678317831654A6E456E52434D6961546B41653043374249415A7742304147756F644A77434841566967355A3041704F4146387757514136487350494358384F5165414D5969524777436344653845334D44756C39736274745569567559363250756C703435784D4E35352F756F38323044654533656C4F6637334C6F6E3467732B463143764D3965303341484D2F666B7776686C41595A76785768656D4343304B464368516F4543424167554B2F6A2F4444784E515444554D52546D5867746F5A412B5A636D5A31705A38743868626457335A6268575044306A6D4858777862452F6C3533766247432F7049787238704F2B4D4475737751654C3237444547453549505A435276595167424D343948386F32414956474941526141495667426B6F6F783446694D3649597830413453694C7364774732434C754974525337532F6D5571536A522B6648326D4A56533742634F356F46787265507A36345069304B4F5A726E6165507743324C2B7933364F39456C6A42706248726536465551443268464D7576716C6345596A562B584E6750594241655954654146726A444A674B7349395163414B75504B706131414E7777587830465941617157306F44584365384D2F55453049692F30756B41682B4F326931514359464E556531383464364E4F68746B4F645776584D72574B63597650752F4D49674A6F4178676A78786A3169725164486D683941423347764D616962687A714B4658475A65366170344B6F4B646362644A44364F31772B50536E674F344A5475546478382F6F426E63796F5850694846597651316432493978484F6F4B6B3769762B6D6D6D633970382F4E4A76422F72412F41453672566E756858324A7855425234454342516F554B464367514D482F476E365967484B73785A46376A2F5943352F576E652F6F39634F382F6838327A724B797A5A4F36746C72634C423478716B43453564306F4F6D30694D787A594D3475364155424D514459413443467434416F435A415070545964794B43674232416A6749774245514B674D6F685258696141446C57415176427941543579714F5261714D624C342B573548364C324E6650673170584B4248347341457635514A31782F6B796C6F77562B6237514931506C5576347A417A72303656556C3743354F552F6B65757638616C6A6330534C2B735A336976537752414E7A59437773446342633565434D414A2B436D366730676D6C75306251473851545A75442F417357475561436E4237766C64584155416750367671417552596D373252613747594B593159772F363166723835764A4A6E7865586C5667464346574666334767412F584450354A5933336A7A5A6D50484E7463565834304D54486C77356C48327A7778484E2F4D797A477735585652574F5A5A796547494733776D784E714E6D4444565A56635667664F6B687A556E4D763638313343327A5567714F3735666761646C53345A3570346F6270595544664A36586E2B37587753366D6C6A48725A42525842785153674463504A6E547934464368516F554B42416751494643723456503078414F6244786B504743432B435435444D363339756D57643738386B596232616265592F31617730436238716A4B764E67376D3449413338396E5759494150674F582B4232416463633263527A414C37444B664451414E53796667394D3541424E55454148576831396E4777426B77487A31436F44585A396C4D685146325163686E7A416577616D4B69646738517241764F6C334C4E366654444D3439382F463271784D316A387A30376662772B4B505A4A584579454C56737774657555506E7448384E48755A647A6275547176714F312F2B37567A524F55616A2B2B5A48695148476373564D78303050396330424667753347576E414E6979696E776C4948356B33553054414D78474F38737141476652574455415141412F70536F48654C683739425657416B5639533254505757395631494268677931312F57356D76322B34657A7368414543453244546343514145614D6F6D685041425A6B3956524A53575055615070487A5A4556386D75632B46454C556A316E434235334A32784562564A4F4D3541496C73737161707537653272325A5A566C3368616A434C5231334C3139714A45757968616C70734331554F586363385864314C615335714478512F75324F45773176746E4B70642B713831742B4C765030777739564D66596F757A662F725A303079424167554B464368516F454342677652422B46454658556D34377669304348445A2B56716A4A776C5A4C6B59345236307A5267745665564863565873436643326157693444324D757957315941764270374B413446654332307869344158586769697766516D5274593142396F4E35374D51674765425A567842754335735639594373436631314D58426E67562F704946414F49612F7173714B78434E6D4E636D4C2B424B335373643731616F796677712B42304D444D72594C594F4C6B374E446153463261504B776672554467554654426C2B6537527530726E6D68466A3431792F363674554C5643726D794633797A3353484176717A5A435A765A504978577A51475950552B7865516267562F36594C774E516B4564623767477137594A5233414634316656714C44777A5832323870536B76356E547765652F4166764D623931705A376E696C457A636543706145416B634B626C66315A316554397076616157376B4C4B362F594A347475707157326A36317A5644307765437A4C6B656333335675634F473576622F7A7A5535626F79376275547658626C496C747176644C343672616D746A442B747932447774664D4A3831316A5A464A6A38455443747430782B756432756B356A5457506C68734E64752F6233592B39644F61584F78436161635565474E32674A416D4A6650516C55473643775066766230416D4A69596D4B696F69415166556F30423145464368516F554B424167514946436C4C6868316C5149755A485A6B324F42754B764A6868663951634D2F5979354551797749386A5048514532457863744477472B48372B493951486B516D6E315A49446E5154696644694159435A4353426174675134564B4D52526D32414949776A7078427744676D615562414B435379675041472B346D2B414F6F4346764C5763416349363546582B443939592F42555234316878337366536A547738684246385A6A584D735766704F6A776D7046584E5758426F7775786C336E68774761425A6F4F5175634C435859336E565347794B5A583830374B59335254547A72776872316545746D78596275517479456C346C593465686F574738625A31384A3575794A32697A5532786963464B68566F6C6631715A4D5A38597772637A4A61347447694C7A4B316156643232635553354A57587165512B4C76574A70617448464F414F71646971447968664F2B697947595462373749642B62504465474F656372594975556C33417566325430396D503574426C50644F6D6A627169656C336873746E614354565A546C4E546353756D41556E4C5653727869475651306F53532B7779564E59552B724D3279533468583365587A532F64686738307239586D7A7478526E6952753046334B4E555074727332587A6556494C41506A4868484B6F68635871495869447A543933676A6B374F7A7537756B4A46412F70516F6A77482F666E397A3632646775394659754B795A53564B794C39304C535871733559756545766B56576D4A477138344F41775A38756A527A3636316772384C7165654476624E4566665A4A4E47573052462F336B4B686C6D44496676672B702B786E644A574C54567149654E79587146694A523238455346555A4A314341644E597A594478494E6D30756C5870516F6E36614D69774946437634742B484668314D39592F6A6132414A75467131673371515A76792B38796D2B6B5832577673453234444C41706C4C59554168434966376742384B6D616F5977452B46566B74745142735133582B4B774241684675716B6B317742514332336E494E41506746745A536331314F374551427754463847414642432B4167414C41364F675042575743444F4149714746633257636662625258336639392F5976464B334F5939556A314C656A4C675A7561377436756742522F38543148397A796F326D41586542334D647A643870593266624E67335950367233522B4167526F5646335973615832684A585079366A506A66764C77343132724D69727A66583961677A71577863324D4943563476736361336F2F354150467063425971366B65306C376F6F4B673158707168324B4C454B6474703346302B39576D632F2B64766575745054556736707250445964623237704D4B4E5737394A4338576633516632746632355A6C357A77744D364C556550645278765073675443545033755A5453776E70714362754537723572417734364850666446614970714D414A413431544557414A4B5065536341674C72302B3347575A70626D5362326A4B686B614742707143716B316549696E574D41653271327A5735357071726E723759315879775A6C63626E33315050467664763953322F784B4F6E5A772B6C77305A71467068624B5662446C6A697435312B57626D475669364E44454938626C4D6337514F3762514458474E677A4739303244626D3631724476774B61457471386D766D2B44782F5576444A513739336251763768377856525652584C59326646476372526F747242467368534865566736396A32396C746150676F7A425948416E43456A6267447744574D45596F41754D4E33436963414346416A4A52305645434241414F4148502F674266436D5738715541484B426C4F5144757836755A6177426D5834753341594348692F732B7439356F55734B72324A79435961614D5758706D5065526875586D715173734B7678573638576A5068726C72487430324A575159633348536F37497655596C315A665859655256742F4A62666E444E6B794F43654566662B32382F486976456761496C6D79537252444E46304955496952696B784E6D4972537A54794D6C332F54533768527A4D63716575704D6B7430484D553744624F58714668466F704D70716D7A7470722B725067702B4C6C4C50427A7636332B7972457532574B4E474531524964536D6E56442B2B566E31446D77312F44366E736A4161545162596B326D694452716E5479563235705234524C48346E616B673264645A616F63625A453435704939424F745937644955586C4D6C4F6A647A4249312B43766A6F30434267702B464835646D754171505A425541376F48614F4166674A4E3744412B414F53424B4C413977445639683641436245776835414D38525A7867463443317673416743635A73534779667032416F635A41486765565141416F4270475374644E6A7745415177564A54326442566744674B74674233466C386F786F41764B6E35326A6E69655A3452757A507536484C327737773876543731724E39303171437A6141734154327754617958646A396A444D72484B5167413669753877426B32535036586B62576A66794C5A45526C2B7042684E396763554167426D66612F564B496F596A70746A495A6568696E47734B4655665A46484777642F513175656976414C6A6F46576F3376632F546E6C4F5772477676653658646C513750546B384A434C675436426B3777754653364E46773179633569734C637A577A555855316574483774686C6139716F2F70617A2F5A376E64744B483968724A47794D326B537743375477593053446B6A4552442B6A55394E484F4234314C71706638673341626F4A74525A76714E746D4F622F6F3935575A5177524B583731784B50764369536132336544556F3845626A41367271366F634A706231323155717145316F2B5966764A556F6D6C65705A375A4A6A436C7746784D577058595956516B7463784A394A6249394D3744665454394C565365674E6941423976726864624D39764D374934356D356C486D764F59737A6A66727650726865506E6D74786258714C6E6D77332B5475454C485A61594F34696A4E464D4176672B682F447A416C75415675674A3846752F454E674B384276717A476741636F49626C472B616A47576255414A4349424E7744594949424A774534595A6959445142516758734371737243796D414E634E582F4D6E737A4458557A5863346B3246314B374A336E5475366E6D573176324A6463563670362F71464C6F497651784C6B33766E4856596F4D2B55525753333171387A524E5A44546A39734F386E46577A48536E514B615A7A62485A576F3879574A736E37553279316F2F462F54704F776B305950314A58725955324A776B6F762B50597947467A45345059394A314E307132716B6C7A64664E31795271724A4C656B6C4D7A5A674B744343375566706468456C57566F334A394A526F724D324355586C7A383950384C6732556C34456F7A4842346B474B6972536A5245746B31722F35352B7964644B6F703150534E533548644670456D30534B39482F4343687031392F2B484E5766786A76346F45534E66746231746D4C6779306F307978694A78732B55614B7976664D652F6654366B72616A49586B2B6967306B413652676F5553395A673948546973726F6E76716E6E62794F304F38637653566169583733496F766E666A754A7A6A306B3165646473662B562F6C4F67514D482F48667734415355476B6641674B6D753633774F494178414149416A76345558583565786345744F66416731526E373938773331472B6D4B5A4C655446415141426E3964626D59464E41566847746F513942765262394B2F51463369512B37374266306E6C637337396E4E68562F2B4F336B32596E462B4D726877305963483741343672747A673443554D70304A43556641496A52366565456A6230736A6332327547472F30586151347A78583764314639375A46334E4E3133784B32496466366D4B484C4C76653966506C787A37366C41352B2F483536597737596C7234567164705742754C377838793362674A7439626F312F33713374727055324B332B35557643472F5667326E7258497437356C314C576F554D34426649576C533269553249357A774F46332B7A304170707A4D653672452F627A353147644F6E617030762B54496D62643233697A3853745879784A746462334E2B764F7632796E6C69686E79364D4E617556612B57597970374C7A6A5A2B2F64653435726D48752B5849636E5679326D765A584B4B526E534C436C59505A6C4A34306977416E385769394B44766A6E3535752B51452B48624F4F512B4C785249416A2B5A73734451326277505731707438556E315A4D374769612B4B357738594C77356563665774363678632F4A33646453375446307A3454774B4A78576767426545476F54626B415A45416255574B76684D2F6A2F4731676E366B6541454D41327750414139745635774632484C6D452F45424B7436515270674D3447396F37624835304451653854486F5A454256584437366C6E73774F7946757566597775666B724B6E45304E426D77594F4C425433306D58373879374F2F5A56396D544A736A546E752B72314638676B4F54756949376C6D5A434E4745324E533335654271436652416B51626B6B5931497A476B697A387A506A2B577755676842384C774C424C4E5A665833454A4B6B4C574F2F5670495659305A66654B48314575314A4846533138784C4E4843745239575371423130506F3558466C7869715179326B63692B544A742B772B663850426973584D65596253594230486944523373386B2B6E6346707958736C476830714554643271582B6538682B3679657342464836332F69424575314B4B3839595370757965314C61444C794D36755461744A5A636C69374A6A4830466965705834312B4B744E74566A7236445264516646636D5638724F477967726D47684B39532B3339514A624C4650722B6444542B5875345339596D5861435A79765857354C39472B59524974534A62682F746B6C2B767A4433374F4F4B464367514D476638574D506170514644325A46355642383065702B726456764558384E6739567642364A6D4B316F4E34484E344C53454F344333676245344739432B4E59336B35654A366464653743342F335A5A767374663333372F5933567079374E75447A6D797279746473316A6D2B61746F2F393959506C4B5A57595762337933324C337074384A436C2B73376C356C637754505434502B384D6D78346347352B486E43656E6D455861716B6533527830772B766A327078587A2B6135324F2F346B4A72526433337537482B3874472F773432735079377737575477304F6D4E4D43664D6A56557665446A58736867485979504B61596746735A4E4771646B437751386A7A354171324C6C6675582B3339614861504B632F4750696B584758306736636D6B523350384B38665957336442516C6869596D51634B67725A4242395758524E6D66384A754C324D6D2F3939754C48543550572F444842654B5847782B31574E7078647656627A312B327939503168687A7A417A314A3953314C576158594C4D546461754E727A47745972464C5059624E47666D326439374635642F30444C67597137647379352B6F5459794D4242434439394361355A37576D7174624B764A7241444C42473556314A764D3979794C41767046366C2F6F3145464E504E5A6474685A333664737A65754C55526D3878546F5955574E7277307563366C4A4864496552683645515A686F6D6F6E437361576E6658376E44506450553847633239687165425A6F4D7A6D727076435474655A5977374E45626F446131567166686A394D5248415275526B52774141433454444E467379344C2B4241413241656E44696177446B787A377A727741505A4E7646496741657373376F416A417457364135412B69626D522B6F41344533384A2B666C4F4B38652F754D37566C50642B6F334C6E4A47544E366F51353830473071734E593759756272317579496638716173317666492B545437534E74662F7176362F514552697953366E3177367168476A5635677357626F4C456F30693137765870426B74334569696A7354513547364A7678575264474C52694F45533755356653764A55696136545977355766616D4574426D7A4F704C4B417374634A4A7166424342307336497979495646446D30715334786142784A7766694F47666459553658336D61582B7469662B4D5855546C3834583679582F34747A426F6164653749366B31716B73325A3569494D6339474B2B62664A61414555457A5A59497078614563435A43674A6A4776792F50587A627251444E4352584D4449456F5249787972752F38763436354A71556D7752304536316A727651644250394E3766352B7044312B4A5A704C64443346694253525658636B6D49545239323268466365727345546A615A364F7050586A4E6C6C454D454969416D6B773747704A4E4563626962596D53315666386D4877496B746F356359536E55666664356334696362386479767850396566314534574A46457575796F48664F2F336D2F5A3465532B5261434869514F3653785464692B4C396C6E666A4F397047466E6A32524B452B6D3679462F722B73776D6B744553346F6E4538315058755A7666692F744E35396A593673546666692F4E6F372F562F42446A2F4A723231622B33795269454B5A662F4B5943754E5876623632646250696F436941504849554B414C7752616E77427341734D356830416938422B375149416B3347496E514563426A6E3643682B41334F5A636D567A4F4A6D587A795A547670667563773336747137647358487671734E63386A4E75777656456C326B5231324679744D44446E7736785675325942486C58636256772B4E66513472447679374F61543159386672765239486A772F793846776333696D324C797151547A5330673158414436504C6458344176774658367535415743535544576C4D5143742B467834416341436A56414D794265553536434E6F2F3779324A5A6A4F725876336A6F323144506B6665544B4579312B4C543746732F4D66424B54722B6D744E4F51637136536F6642577A6E4C5A7932634D78784E486665586D76622B6A4F39356C39373866484668524358724E764E4979796C684F4541516748754442546155506A336A412B69624163764744713962624D57445A2F48506A2F3038644331513874434632667055674E49337052534971496E6F4C505852617162362B3448376E783351392F432B633436624C7839724750746C66374E416F344672572F565452786D436D456A7373396C2F705932647538652F314A75655956462B552B652F475851736D464E477138374E3172597A727954317963477074524936576D614A5161364F6A6B37754E56484B4F65636377357350626639384B4E32514D623947584D35586643496E765673706D464E6B375035627136394F636C2F626645496273744C713773436D4D3172635432417A61696C6F75336C7334417143374B434666336176456B4330414A5165514E4952446268486F427A47476A7143474141733750304278444A3972487941414951496451443247675556453046454D4A64444D3241556D644C3233724F5442462B655453675775764574756638432F71664330372B7666626B694B6D6D54734D426D7966714C64397A51476261473539417473675334524C396E526954544D636C657059323443376B366C53794E6C4553574137326B716A6676395A46493357373878426A655A426357596F534A323269594F7462354B70306C546A4E655070374A6F71424B52564C2F5557436D684E5A6F487A4A676C4B624E72796F46716C723455446C566944587343706B6B73726D4C46464F676B3441316538794D634C3369504533645031617636593976683555767871467150356C714E376B71685175787769516F4865442F7036516B505A62374A394B3942687473445770487A345267316D3767555439366E3270766C62314A4D31375A744B302B7842442B35623635314F682F33592B705835667A6D6F537656436566684F4476713676525075745437735552754F796B667174427A486D622B6E2B476A656F766C76543132366E69524B745168624C436951517531452F784E4A39392B6E365A574A736F7072494A61533358314B2F6C354745765A7A654D7A413639643050484355366872377A4C6C736B32703045686967535147765462392F67394C5758465A566F4C65726E315753787A464E636F6D61797850593549744574446236316E54384B56764F5446424D5A4B5771314C4931484B664B31794670586F6A716174776E302F542B6E372B6E5347596E366B617055725042743337484445496E754941745455374C6754534F58786D6B4F6E2B2F386C3679375676314872704F5A6148386F357972526B7152516B765643576E495A6A7138703053646B3262784D2F666432432F5666314C65324D2B31317354374E767A36307A6836686D4B767446656D4766662F6C65306A526C48323752477551497163556656664F64535361515050723858794A6E716478667264436F6A7A6C337A6D75396A51504B33744C744F495569576263497446342B733466725A506F4A624A346838584C4A6678623276566A4C536A66436D754252456455746F525968325A2F695347566F51575147384171674A314867756B4E7743616A6F435944674577386A323067774D5949373032654144764B6C69416353427951474345734178354E397530524D73332B6F33474E63585669574F307A466164573642347A50454E4F49564234722B6B6342597952386D454672416E4D486249504D4A635537374D4A336A623351682B3065716E4A6E6A6E38565868507A52674D45716F7742377558414D764D57686C654164794D7A6E77686741743478724D4377684475434146415366515468674B69414B6771415A5974356E716F596C50645A7157756E66337A6775663058564E2B437A6C2B346B394E764E4468597456394F344248593332664F5238767676566F395750624C34355A2B766F356631453974496D4879654A6B4F5778584655415378706B65414859566255745963674356484D732F4B627A30636B432F5637337656492B35505377734B494B306D356A2B2B6A7077662F684E4579734F654F37334C4F766935396C3362716235543762306E35333534713072374F6E55746D644469345A374735746F736C6779477566784B78696A4D326D7571593658776F4E706A2B362F614E72385A4C412B56423932614A50376A42307A437651306A352B2B7576417133656E62596A363533676B4A69596E52305543334F6C31615A4F7948437167445A4E6B6259377851384F4B78496D3243567A3751504E6A397A723034444458302F51553977467643573553576A7A4B69433432755A45394B6F58587A4E656C5A45726D7335355939374A4F524E69774144674E63596F4E4368523041504C465146514F7764707970587741736B70666B6A674165597142354538417643596630557748684936746D45776D386D5051734A734C4456727971752F724274304F664431744B62466F33584C6859397433546A796373553550766F696932664D2B6E6B4C5A475879545846693078696A5A64557A2F3169546261794E386C65727157564D35702B595A6C33314F5876304C6147306F6D457079365579794B5079336B682F7970343050545A37486F6C6C2B69736D41696B6B765161757254716553794531504B3673474D456E476B686263636254684E535653384C32742B725661635968736B4F706B5930586F553347322F7A6170384B30664C474C4A6B37614A367A736F757453666B512F7261575959456A586E306C38724E4A4B71524C615A4A71653950706A515A76314F374A69796B667036582B6A345069683349335945753049627A6B623651344962574E556D37667137464A647147337465486E6939437A2B386E6871396E6F5053383054763138376C4C53725154435570336948452F4936755174716539415772495A4B4A6254426449514D464E2F43565951596E61456550772B52736B675130683657743362724C387A4B5556704445785A6A5933306E367638614E457231454D32446A4B566E5A2F77666535516E48717234666B4B68633454714B50696B68304169566D663055375A6366723943414A6F72437A6F6C2B4558432B706E707730354F63485358513266596472364476546B715730436131484F32394A31465242656A346A43537A5A4A5673332F495A4B4E4748746A324A30556F2B58446257764458454967346E524C6B71437165377746347070542F314D677544486B64524F63744A6475564A36542F7A41394E58625A3439454B3732554B434D475077734A326F77735676786C2B74714655784C784A7065376D755361575967454865315A6962366D6354704F3551624B4F782F534A3544616B5144516B574C32426A6A546532696630663736686572534F737270753368483936386B6C384331486158334A445838766E485052392F70496D702F415670767A4A516361446439582B5A3936533078646275316C45796D765253396A46466B4B5378492F6174366D6E59702F4A31452F5568424D4963453446303579534C6632756F425774397A30486667306C47694C326D664D6B542B50643946747645536E6656576F69316A4A476F766E797734507658545A6D4977373854536E326D2F755059337559522F4F33364F67434B784362624563445A6B6B716474484A503047793563636D4170544779425150652F344A4B686277393534764D2F7559517841434C7345415467506D7878462B436A38463649413141533731564E414835565047592B427A4344734E7038477543654B49356241456F43326D47415745646359724D434D4A3452463641396F4C714346694435485459413738647A71454D41584D56466A5158414974785454515577474E76454345434D3549504E655143564F34617076414765416939784B59424139424972416E77494C3637654469416E416F554C41476F44346961414C38413774514F416E4E784250514E71473433744F6276575148495A5937484932656F58746E63317675723671736B7633766F654D796262325938634D793766797044784B2B396C766D743674385044586877764C6C5533426E43556153797641435477724B77636B4B6C54786B6D756138336C696A386F3872444168614F787242316A6A4A6C65477777475131515536702B7466726131766A684F3134757639377A584532447774594554653566747244702F2B734C675A774D372F5234305069524533416D6741754467412F424E764A6F7042354279315A444A7367663474504E5439355432726733336A396A72634B566F392F484A356F54724B52584F337433585A752B384D31764F2F576E6F4535636D46624D7079455936444C562F444A6866667467642B4F4A546A39744456622B7A5A44616A5956752B426765784155414D4B34444C41447168713167446744383855424A676F5A69764567436E7A49346A644F2F77786962574A6A4F36594B426C426B2F6D4958774F4C7149394C77734C3144436E327159354F45515932584632474E3573686A35452F3676784163346B6C556C636272774C38426C69426B306B674B31736A75556D774771772B65597841442B4C4254674A69423934494738434A48756C44424E7241322B6176336239734C783636584D6546317A66396975354D6E6C44796D4B6A396E70622F43317749775A52336C686B424A4D67494F373974764A2B4E44715342574D4F4D5867766963472F5366554C717648587A3375517061695256597A4363374B5A4C53414C51497A7A4678624D63476C42545342584C6C4B55346B4A3369584A66756B43755075564A3437364F366C6645366B536542474C34506849445943494E617A614B6F6E4D6C792F4241326A446453415039793336704872486C3071366E4E326C366C395047554D35446F686179704C796E4453535A474F5173704B46306F6D426D65587078797437556B7A62305A4F70767A2B4A30663950553777306B6954566C6F33776C39635A6D7530576964576A6A486B4B755246574A386450492B38527936682B4B6252444C576A57514E48572F4542314A41754931327068766B595571486D6C44525A59746451532B435977455332325A6233744F686A4F31667948315A334E714679664C5676415669635953592B354A4C6C5675464852566979795A69306D51625530435A7467585852746C70425955354B7662614635656F766B5152514A66764D37716364335879762B2B39353869776369504E4D31467041542F7945754369777378346B6E454B43346A5458636A2B72343255564B4D63585A5375536B306E78306F7532416C5970694B6B6573616F33352F5242616A61795151706C6974617A7236377361524258555561627A7479614A6E4A73765852356F486B624C6C6D4F3733494D312F5A6E707664684A3870784F6A37585748797239476A48615676326251664568776448314F46326A38583547464F786370364771644A30484F57667239686754774D79526F78464539323543414E5A6B456A674C3033516C7937434578374A77456D61363062763543436F76376F36775A53797642684F3662537630306D50595447396D56697362355061306E556652335276504D6B77545A54435367354B4A316651344A4C703630586B2F5A49623158337A6D646C6C70434F39726E4370424C496B6942394A446549394136706E6F6C50572F70547666642F6D76466B4A7271505A54326C386D6B3848456779366449416E674943524A684E4A36324E4237656C4877695033305079306A7870794B42782F6F736878796B324E6C4E372F4F6865546564424F466C516151596D4372396471643256794F4F78596657595432357474366B6465412B5758544D37314B2F7A35353858326654664F6C6357663444745963452B536A794650436737384F443771744533395653716B384C326E666533385A5078733852554B527453306561377246635967653879654E627A6663424150506D6B6A34744831384C414877336B2B543677307A364850536663796A4A656C457A674866516F78374175384D6F4A774F7A464152674153774341443261347A7A4175536971386750775144622B527270504C4142776754757166675734503761782F41435059324F4568582B6F65674B6669704D416D794432687975417558796934414B67457836694A4142333745457577474C414658344C7741546F455172674E344358416A67514B4451456F4145345451387A413367506A425A754133777A72344F7867474732495571384337432B57496C366C7034416B4F4755576478375A322F443757586254487A787934757841627362464445324E305270526947416255656765416E677A5667737A773667415A6F774E654236336458584A6A713458715947586A5A4F543637653333706F303645625A66426170395070334E78772B6D726D432F66662B41483370397A616348425A5A74337739714E6A46313170657A473464466A4F7044454143324F7A37633441346E6252782B41466F434D6D6968554144707853377758455A7A78415741394546592F494A765A3066787762477075536E47336B397467484356744F696A6376505433337A436E534F576D7A6B354F6A6F39766E394E4738645448377771373969774D5A686A6F56736C644865374736764343474174694334705A6F41414F77577473577747692B574855527746555759656F43714B36725070683641565658564374564A4F50394254552F317078572B4C6474446F6B4E4574636E467A626D3548665956457843427168682F47785241594447764246767A56383676484A3836726A4138314A4576776A626D4234467231783666656E59485833314967392B655A416A63476B6D563874476B354E744351432B2F4B43754E734262596F524A42444347562B4933414377412B427767715066486D35484E6E4D4A6633482B325075685155595438457049357250683134443961327838495A394B77617A4F6E766835355166376654394A306B436179734533717934363041646F575431387832584E4C4E4A7556707645427055734E5876793145744A755039394347395557366263334D5A534C53524E5A684453556C7277535055462F5830367547302B70485561797A42516C6A66344D6572344B4D54697479614A7969786A785A562F59674474306C3667736D496A45794B796D44583870615834547156394C53456B714D4A4F2B6E4A4C457746656759475976636B5634537779464532317374714E53767A65554744417A61517A7371623256534650666D307A3839576D6C63355274314F51364745546A737033657534515954485078314F2F522B5571307342777A515A59464A334C533144624158304B67353154583847326770424C71723537363941575845724C5A4E3552397A306C414F55534D375451616833426951484E375333514B6E66745464366C4569354776664C376D457632366743496A396677313033732B387947666B773338645A4B414834556F4571412F6B6F424A664467636959477970613156543478546C6C68714258304850536D6232786E7150356E4258454439334A493035485A575558744A354B4B316D4F62314E484C424D5A4F436F444D70516B61545973434F76726658772B6735696C3237514F4D535151797537427963695454615465693977334A4B31497359374435557631656B45466E3546553179486E4968464B67634F547451486B7047634A77457A5149557979506659434A5870643345474636704C744535564535477373776D6B754151545050616D52517047536D57717A52394C3950494262686A63596E472B61593954337054467449685A4E6E576B634C6C4F626E434C534B423877714E597951704B686A317178655632484B4252416554344A5252746D5152522F615379746C694E5638645359475668797A2F65556C686B5A4E635A547631536C316654674A6A4E324C5536394F34684A4E6735307639656E7930394A356E7373496B572B70793270446C64694A5A5042316F66434B70764E57303775346A685551514B5268736956467651665761536A3576377253657952624E3237526576615476566B635758533861463166614A3461547139673534767745386A785954504F684773312F7A5757714F466C34496D6839482B637430553157416B7031457068616B754A41506F37694649337A52424A7367306A786B3533342F676B6B4344616E787771535171445145596D2B7A342B666A4A2F7034705641726C72727554536356626B6B3735586E35774341522F4753414D4463756254644F704B464A5A4E4B756938517449783964676D54494549464942486935316745325745692F67392F422F34544569567232745541443059436F674373776E33634248414F4433486E4436567277436C6B566F414F674D306637446A532B37545141637747444471416A38522F6B674849765730444B6478527267634863427839735258677739683237672B496C2F6C5A3852487763585641647465382F485A652B4D77444E4A6E754F4E2B4E666E71306856336F6A4E444430622B71417A41506E527771415777436F6A5448414C6A674950734E5945574575767238674371373971694242617875306244567A6C4C5A6F2B612F37664C32736A347A54686B4D5A6E50456332424F344B2F7462673846504B396B6A74554E79565169376E5430536B76526E4D74465A307364327977597A346F774C35596251463457626E6F49414877394E775049687545714677435832455A4C5238446B594F36706D516945623475346C2F412B2F35587251322F63656A66472B575A69796353335273484B593058754257634161746A416876704142594444455945417A4F676D35674259435168434C59432F77304A6D44366847436348695669437856754C59714B763336672F764E2F786877327A4C5777456F426541776371593932546A6E5779775375396D475064414E77785A735A4E6B31727175633138353365567A4B4A62354D2F4C5339345676612B6B312B38547132702F632B6E70663959767347774733655635514D7072457344734275514730436F6974456A7A666C416935456E4139344743744D6A646F5A2B5574304477416E6350624866796F7578436A4C36626F734E504F6A776E3738753734466A445250446C61785A707855427078386C6E4838723874784A4962466C6977676F4955386C4654746F686266434B76307333516579784179515A53584C546F55363743564E70725264443236735A55476B67515A5571526A46476D38446C4F736A78645A5A4E7154686D77374D53347874454859456D4E51375654715772346E67574D787564494533453339642F6C325057336F5330676A2B3577734A4A486C55743876612F77305668597241326E4557784444324A763673777256303546386B554850767963426254395A6B6E625165353652527457794C57304E725544334F31673535334A694D44674A457269414E434851504763682B43597759754256737371715A7A6F66704847734C72753245414D56537A464C69326A68656E715A376963424C347A63593463545937755776674D396166342F6E4D662F4E42694E483175522B72715A786F335439786C503833734C4D57786C615A776479504C5768526A74316A52444F6C502F69735349685A4E6C785959734A3035624A64715258465057306E724153614165516E75744C4A6938702F576C4E776E38313254586E3456706139526A615A31365252727441474A4D313548673430714D58482F6138592F5275765778695258445434634D35435146696D797931644C383730594D7148794162626A73366B57754E2B3630506E596B68726346665A2B4F5579556153497A3047457032634A55346D567A553775576B304A45395861735259313265474F6F7A56754F5A4A354E4542354B4C6D79795976436247756764394C2F646B6A75674C4C736578394F73467A592F337044426251655075524F7631414C4B556E534C4C587A47614C324D6F4E716F343764495A71454431467A54316A4E596E4F54756C544F556B4B573170506E536A646130624D666F797735614C31705078744A34346B61415852642F7055467050397841484B4A354D766137524E4D5171736C43346B75412B6E574B4D3870436C75426E7478374A2B3753304A4676756F506952504977654E637939614E347251504B704E43696B546A55735972574E4F314838656C7958616D635A7644316C536B736D69573555555748593058354E70504A6151495076515372514F4979587153464B4D4F5A4C6772715031336638682F6955512F7673697676764E466E41414178417339414F594A306F4A567744454962504B486B41544E464731424E436131316458416C41424856524E4144414579626B357241535450384D3671356A613672634667427679794F6D4E575173416D324847416741316B52743141655343743377754E674267482F5A684838437634426175416C44426A386D43686765395666784B50644A4B42724154546341412F6862375951647765787947446A686E4F50666B536A69773475367934656658652B304E4F684E534C4336325448745458744E6F7A526D414C63564846516451424336714B49437477554B324368434F433333465755424D30646A5A555476666572454D676F507165574C50764D66794E625A392F4A395854386B333930436A433972754F6966645464743862426347347235714D574A78464C4D7842594144564B6742344277753834454177504C7A73674343344D54564145616870726764774B2F7348474941334D516C59546541346E7761477750674147367A744B5A3763375243637743394D425139415154414455454153714967506746347A416F5A4E6746387462424733787467545744677451484D346A30316A51486D497537584564756F4B6F4376776A54424E4D467649324263616C7A3650747177372F6E525A316541784B49445976754E715866796171734B4A3874574B54376C69722F74533775357643764147636F6D5651577767335579532B7968687055444D424E513977414D677777635963446A7772345433746F426237582B76345838625935574C6C614A6E73326B385936782B6661796669694963524465703734736B73615A707A4E746B757A374B31674A4F75592B3658762B6179693652714C747247624B48644C41545A48543436363333706A543170772B70504B7557666E3035695A474E377656775A30363076686C66703336656A68704A75577362563936337858535A4E59686E2F642B35424953613956664B6D4A3442474B777562644547314537747844443135432B53416469384E34345333516D755351304A49336857474A5148394F4266663852544C3441456853452B366B7669795467384B7234537A4261305958612B445A51576C3342373973655539484B6E3256673675747878474146665A352F616266374263562B4E616635304A5A382B74392F733044393734497A4D59786567314A666A794542494E4871657A394547756D625674462F7A59677837457961355753795A4D7768516151324D555274686B6E304A436C676268474446304F754E7257493053396B6C5635354D776E306E7757544C3171537261374853755149725A2F374271652B4F7A2B4E613755764B46627379494B595A55627136797069594A504A425849386D5A357145654E65697A6A5678584C4D41745644466B77346653644C53634C595478714B4D424B67626D3252364370717234572B5933754B54617A305231587148314350356D4D2B4F53615233724F4F46437A33354A6931395059666C6265585974474F396B6839643146797861704F375A554E5A6257493858596A77554F32584F735070583665564E4B34536558734A55764846564B63684A456969424F443730505A3538705A2B5A53336C79324173755757317475564E423537614A364A4C2F39363365646B6B547443766A5A6857314B2F7078525A786A546B3269575368574D6A72532F2B736F61574F4D4E2B4A4D6A57496745706E42525677306D417155483756453853554735515039326D38545A4D5243706B74564C344A4A4A6C3675506E624874706A32734178644B304A5265345A7153673874506A58344966613047785670544C6A4C6A6354626F2F3346636444666B32674F5846666E4D79774938694F343848574237597159344372437530366D634161754B7434414E7748344337535637426C7279516A4C59305054396E37354A44544758396E585661597250562F51426742783230414B49413367465139314C58562F734154754F64646A74744249514E7767627442674346494E6C356E76416E65414B67496C6244486F444149306A4D457A366E506635576D414863515351654133776B37676E464147364432327756384437487036683349774433306534484D6F51566359793353637871574A4E5235447378514473415941796C575465417A3855567978794158326574384262674876774F6D777749425669513970366D4A65656357776F6861394C374241733454674A415A435377636536794D6D2F396A46744B5A4334374F472B646A323374337A74787550735656366E554F66524E7934446E355056746D674B6F4A725A57747749342B466A52423443494461674649432F32434555417A546E4E4733306577486D57367A434E7A784E7A35644B5631336937522B64366E4F764A7061676F2B41456F2F73636D7330496F6A4549416C714169386744596879653443794172457441457746782B536C63507742752B57574D4763414139654275417265484E44506B416C706D76745567666E4C306C2F757464724A326A6E564E774C47437863423456686541434B497A6F61465964414678642B636D43566649337972637471723764475A757044365941695355534A78767941486A46746C767141496A46485851443841435A384474672B7332385538774542503861756947384B73435063426565395056366642386333464C2F747042494C4D6343665062422B4A6441646C30535A635A76775663656F484D5A344768312F624D4B3476757949736C6F514270614C3949516D736B43735A593070702B2B466A784F4B34754747436F6E2B6B73696253416741636565676D4E644F6C763142326D533954745458336569446374686930542F6B3577726458744659767A435A38494B56765773547051594C30366151544F747747717247576F697758417061577858455350497A66542B71562F76386652417048365473364639456154525A6C37344E7041724171762F625939786D672F3644366D763278456A3555774D316E2F34636174355341656A526C6B586641582F492F684345676461582F4C4D546E33394E54484D63566178596A4755764F497743523756695147336C5255724A434476496F337944474A4944623453665572304A6F3044493565665A464C4556434A4C6C346F5934786A53574A386D7979573670336439534B30687439433850456266535464534D4E6853386F6B794A4C6A73574A65364646745376475355733547516743774833792B69636865534B704166536633385367724F6230305744472F53594964517538354F5337752B387457374641735251516F4A4D704441687868754E5A6C574F436B794B7372724D4B31585961534A5032646E2F5A35766735454538364F2B456D31503637694F584D534B45582B35676851656232694269364A367669414665513853494F546B69736E4563452B672B584A6C43645753424E4C6335444C566741544377755461644955454E576353564A70617559712B704F39354D376B4F6972752F6B74534542474962556A697075315039724E5A7844314C30614B656C65677976616436666F396965334452753968517A4A362F5453366D644B366D646F48612B4A4E6537302F523341776B716C754B70333539695A586D334952644C6C7A396C653752714C2F585435325342502B796768422B464832644261516B7758774454415059555145654176514C676A554B34423641457576473641447244415873423573706538314F41522F614D36327733576F345633317238594535542F4B66734E584C457573777978375068776A37395167413734474F3644654145366F6C6E415069674A4549417445552F37674167443578354451414D734C6741304D42624841776741327278725142796F786466444541484E3869355750676661444B4D6E334F3850415A5530345575716F4741335369374A58594441496665447233732F2B675A7159494B4B674143424C4B492F436570724C566C3542764164374F384C4150413837464E714148774B4D784344734263326D786A75412B594A3169796D6E793872765044434D5646585254796F6F66364E734254454D3047414C774D52484D4F41432F3559664E6D5146786C65616B364272674F636336634B556570436738763358654A6E75472B706C786F2F6B3358317747384D4F7177594F776F50364F4F4866736471507571526255426C79496146757462366E5465486E754C5A507A6757634E6D4E6D2F4566635748706E63413338764C617A6F43454C426349316B7533724E56414B386F4A76477151505944325730794E45357558754239415964736C7A5973625A573556616B706E314A4F46583958724C68375770364D496977514156686767515541417763447747456D746A53556A514B51776972784D41414375325963434B414E6D32374D44724154724C423558546F36316771734A4144645A79713767687A6B6D564844556B472F444F76685A6D6B44734258736B64676351474F38684B525058634132415441696E7463476D4C4F775744774D36464A734271756E414A7034725A667731647735337776376C4E532F7A6354494A66317343386F5859436247515279547A67643869583549352F337068413078444A57734E4C754A744444626B54746562584A42714C6449327142616B5962784632724848424B4264354A6C36414A7441473262575A557242314E76536E303969526973467A56545838394E726D6574534F50476C76796747414E616B77526942492F5168722B454742554439597557326A4F4D2B723161686338742B614778446D59716E33386C57514B54672F7437662B4D4C584F582B2B37624852496F35656D706C3866496743304A48306B6872312F307A73523966786866656E396671743777447965664E484A576559786D736E302B37504539533351306C416357654E4E646D366F397A704E6D31664F456B3431766B7768526A3962337079614A3367425142687377796F3553614A6C4C4D52674A707947324A30632B394A33563534635142767266325A66704F76435662664A545665334C51504E532B53483364675454387A6C626E4D5156537A4D452B6B686A3472326B7A2F76476B735536326374454D49742B516F4B6C2F58643949556F4245573658357A4569436C5934454858735346484A61375238684A4342382B747242516E3943327531355465324F4F5A723665765A416956366D6F4F77784A49484D493848764C4C6C4B716E7974326B667A4C61686F3676636D556F7A4A343245536E5573576A61366B6B6E354D6A4C6F506C65746A5A63475070506C5868446A424F71576C3736446843346C3270485634465045464B386C313769686C707A7849333055324B30743842436B30544E59784E4351515871563562546C6F4E51363076787956592F5932573330584D795561533562726C4F4A706A38705471774E6F6E57686437304C395A61663632657658392B4B4843536A43446A674C577744564358674C6C514268496C54716D6741627A55512B475243533253517845464264776D525747386731317A73713434584946523033747576537350656B35506C7A46355471333770313332466A6838316F7657787179534B655258535A45384C4432544A5648334D7777464A553556675A6749316D446541414343464D3145384237437662325745665475554E7A7676475854432F64587675487152394A373744485662462F4248414E6C5A4539414877684C6E7778514379777776536447474942784346334C77354E5549483844486F797963446C69684C6E4355474D413878447A45502B5A7448775258415779793272416351493351306A514F77552B416D52304331544E3162565170516139523756576F4150646C757841463467383249426E675876424D584133776374716E47412B6A423837466E41494C516C58634149755A4766645133794672352B646A6E44563461536A35375669366F543456506A674E51426A6E5A586E575433472F79546E4739414F7A32326F4535683447422B77664F366442335232795A3330724E7A62743779526F7666362B4C4F703679544176644F644D52514656476254435042327875323834326C7746382F484B6464766F6C666B2F7A526F324E4661387464357051636B4B4E6E706B7543684E2F2F2F56456E3876664E5274316B41796C59525939674764386C4A674A344F75356E356758344A767767666B43346941634564656D762B6A342B4C693471436767615657434D2B754D7372486259672B6C42484C53384B677A6870344D76787231304C4F7A5959627875714531674C47596156344C34416C4F636B6D413552674B594375532B5552416136665A6F646F4C654E664A66737272424A43706E756337742F4A2F3177537874664B704E314E776D366E427435663151324639684C316350334B5273487A65796F685261696A52664C54785679634E61536B4B4A6861734E6A67583069786D6F69424970306353315461673873624A43322F614337414C6255546562314E6664795A4759436C745043664A6B6E4763524F6D397A535736696E7A72783945364B57665A4B55615744416661574F4C49785745724D5467767253786146684A5944684F6A6B455162705A6143713663516F394F504E6A6A6479422B376F636A352F4B655359447548736E476C30506A6B6F5931344F626C6D6C52795A65747A532B78376D54662F78546E3364534A5961692B39584370415A327752384732536E32756976335A673267335743424D3051387645484F57734F6F6C694358796E7471745073663964474C35524D2F56736B333334667374697449522B4458386E46716F5246716E38476370334B51427265386D52685730576D6F6C705773564A3353574E392B72507A637472392B496B5970524372494F5545636A6B4A304878622B7A53306A726859716637693654744B4F6678743558304A386654647056674A4441363037717079704C377554414B745865585531352B525A654C6A684F2B72523567637939546C722B394C6F653956587A6A316454763672534A4E765959735863365A55743858523635492B6E5434487151486362547A4765616E767534594B46476859397250325A416C77754E4936757578386E6C4466307276626358416B3075756144552B655A7A702F56627A75444A5A69412B523566514543553548616479326B30437A59496C454235444C614630536D484F536F4D706F7658354E6971364E5A504531766B6D376E57386F5669667556657272595451757764585332394E706633646E5362423635355436656739535945776E4477445865662B75396576722B4845576C4D4B7369486B48774F4E594D644E516742735161676B4165423955456559422F436D2F715455426D6F71614957496C494C4F48313357563038624669303873632B336F2F4E75364F6B363137543365334A6B36724F53774958554435322B7139716E616E6C4A6E447A5A334C4F56775772636634452F354E4F4D72674E644454334E2F774F6D706F396265695865714E36586570564C7454793263466A7A39514C2F59507361476465726E4C2F66722F6E654F526F66654B674F4153747844424943503342643741646A4267594C54356654466470414E74614541497541504D384166385676384A63446E38726C3837726433787A6642414D416242765961594473524C7351422B425668776A76414C6F3964674E3047514F656B433941566554394556553231516557582F415141654263412B3848354D6743505953633842646859566C6E77416643414E576447494B78725749366F343835326A3873387666333252593972414B43616276796F323672616D444C492F4A716646624E694D56447A624A567148757542776D3446524858744F4D63566D5A595A2B30395A554B7070544E507335537030714E3668555965624A616565624E4C705663652B4A534F6575335636313746706564582B58377259647431577A3675644F4358446A50413231565A504647716F34734F485A6C36585135504C6D633337786E345141466A41454136674F4853694877427648424E72416E4242542F594D7741322B5470554D59445143324F50304638303673573673464342475767614B4454484C49645968336E366778367933412F7837684C7A4956746A3367322B643531324C643030386D395462636754675A697A5744674D5167617951505852624159674432446E41746F61756A7A6F454B447539374963693159414379666E6235397A796430305174645753596946626A57583274356631497945484D7774574C6C78474D72576266464E667A304B61736431544A587147676D786E456B4F6C73347252364578426A5A644941332B4F474E68445A504A65516251764D66725A4B36522B33703563786C79735968504D784C426253464152694145535A456161596A746953494D655142765064544C68627953567854414B2B7135505763616D6B756261734437746A6551634D5951373548575842475233596B682B497A716232757332387138467348535045776C494A724C677A434D5867392B496B54465373475668386E5666544B3430336A5054562F376E39386870536130594E794E70386B547676333665797A372F472F4674494D4745442F752B2F6E6C456A4E414B476E635461534474416955366E6F4B675635486D4F386561487A4D752F7930457178676F6B574A42477042725A4639795A5A6C4F41745A702B70374F6B3442796E6C78796A704F436F53573578416A4541496653664A6B682B2F355077313869675A7971773630734F775A79666A5965774C654258474D4571344E56503665582B5545485461764967734B73736757614B52614A76303939335A34303464714B71612F376B365853365046393959696C646371382F4B2F7645796E62487265796F41676B69444C366E746C6C61702B564950755A372F74522F626546696D746C31582F643654382B61542B6E4A6757527252586A72716436362F6667752B424246695156575970415762424D737556506A6E6D557A772B693241343943645A685A506C2F515445374A366C6646354B4C616C6353324276533933486D382F6C6B61612F376B5752426A4C4F79504B62514F4A6E4C34622F435378726678594855446C4941366B6A41483059633741624B617063332F4E2B78666E30645079774752537A4C3767737641616767596A32416753796A2B4154415379777948774377466F6473497748424A4553726877434F7A523047616C7271442B45694150417776536F78304E4A4466324A413630465A6C68704E303731795A316E6F55666468612F735A647264746477477842654E584A52384730494D4E55793047436B346F6E447466314B633676365A4D57746E39334B777A4A557557344A346648395A496D6833584A2B72752F627350382F766D654457305573336E673539584342325339534A7268724F6F442F433276447A30414637684E5531542F746E7A6E514766486264633451595841426D5255543447376D39444D6F4379324D4C694164546C5756564A414A76455856586E67474970525A635864674B6777586E4C756863575638463559596149734E6A41697469613244556E59494E5737446D413873676E334162774F333648434167315762426742754A4C4A5451317667524F506A703137375A7A5862676D757864796336757939327A34756657335A703633323135345A39735257774431664F305679775657796D6D5A6471575A6D782B377A33636272723051563275787A614B46336438654C6162625A3150617166485A69355A4130314130317056576E564B2F41464B656D7250785954464E6862376D415A594B706734755A524841493452782B7072327578314B366D5447766E67362B38454D7741517A6967486F7847747251674157676B586138774145774667465147564D4D6D554132435168462B71777A63674F6F4267676E565450706E2F6C4462496D62766A64743365384135646D484C47787753616E772B56474F742B6666532F586139744376635551305555334755414435697455413141622F717733674E3238496D6F414749507A6C713241522F2B4D78677A3755626436785A7150432B755458554D58684E535057412B6377756E504874632F447649474B734E4332555045482B5469384C3251475165316C577557675453744A697658475A455951544F353167686B346C5A5244415373596738796B47556C4136786778526A496D644E4F456B50546E6A537341676B38617173677A6975304163306A4564644F446C4A336C6B677970572B4A4A633167424B574E6A4B514E4C356B3063364C4D534C68384C6269557A6F4D6777574171756559356B6D61754854484164715152486B594D52425A696D496154526A2B6B48583449394D52347A614F4F64616367302F36304831516C426E636D576251477A4B41443743623974612B3669697848716A7970723663736B61685A44757066386F55433550544C7A2F46746F486B6D706A73474A66573469445365793467426361586731554555444B326A2B64534A584457795550384E4941483135597976762F4648517141645332503159636748417A346C432B74482B693679307A7A4F4B46503567545A5742564D61306A664541452B67474A737A6E7757457678352F50576E734532584643626C4363624A5179425A7236344E507677514C7055564B6B686C7843673532494957476649446B6636746F7A53696E77375A4B3478354F343238616D2F71365061306257717565434A63564C4C62344C69535242742F796C5768575754355479576C5A534841555A5A636D57742F4D4A50416E57566C3048636931537A34682F6B736E473663586D556A4636394138396655776D6F2F792B5372792B69714430336F7657696D6D6D4F7A792B703075763170615630457569685A7134417861562B365371366B397A5538547262734A4A4E6845557A76437957495352346F3076667A68524E46336B45372B4F596E57463730634D30557561484A5342487872576E5872324352537147776742595537435A346A79524A745437456C4C5968366B656544374D4C38614F432F35574247612F777741595864346C36615041426D495967464176776876457A72414E786B4E55306A414F786D6A7377504D4277307472554A425949445167614B4B787150575642732F6F3054633763663362647833384D62592F337233652F785144766E4D6A417065464C6866657635596F41565933344170764768367163417872507937413567583859786A4F6E4D423579484F6E4F3761314635444D31534373646C30336E3352702F337454384737502B74384A4B4A423379656562464747494561576347666F6A70574136694459496F70452F434E4275652F637852594C5834502B77413835773769636F427435355034534B444F7164704E4370304648456F3662424847427566643848774E507A7A77547253326D6D613871556A4F474E4D5663332B314F3442336D4D4A69414E36414E324337414237436777516E414E745151584D42654F5037746B37593736367A7A3377345A5873783731796633767437446D6E5176334E38746B6D5A50726E393470632F62456E4D6734694C4C6B4A694D644D6B46496C5052447541563072656B796D543436554D4755487861796B3141575146557042514A7246784A4150344C5836557877745068544A434E454C4368714162323457586C74717773585341477050772B574E4D427A535131743737414A367957385A4F41422F4B5035693741396A48696C68634155744E4D5568374559675A474E504D624B6D58324E7A51645043306177664C4E422F617450694D57686A482B2F483143414A67412B3066747930326C5056455633446A46634D6A593155324C2B3547334B37457667555876783337746B48492B6E786434752F485A3146505A43755244594B71495941587147555A424C415A7A46574942495347676838504233434D507A485542567A6958434E3037563556394C7A6D2B534244345174367038564F2F65324B41646A36743879514A55524A4D384B496357642F7A39765344545735514F6973766951394D5A7147574C7041416B49774D5277646155484E54527258577251526A5359544E614D4E2B6A5274564D386F76555A5779674B566C6461744C43536F754E4E4362454D574434453247724F73755A583753585A74496C6575533852346D6D642B59574732796B61465450676843435847616844466F487769686E34674A554F774A774776586142456F366D657777783063727675657A655331427462497157376E45367543766D496F5A56445A6551596D32666B456A4C764C5230776C6966743932744A415743647843714A436A544A75726F6C6164644F4A495A5A6C4633796371657A57575168736D54394C2F75446E7039456D7363674F566958676C586471582B716B323134506D6E5375773667744B53722F706B4E586B577148787372526C67676A625238774F4D4E736D683170337158497570474D5141694D5637424A4A426570586D356977537746334B3271642F5431793652336D7532736D324C7843447A6278546B444E306C2B736E4B64632B64474B30734A46423934376D6566304A4A30767937797A5A2F6173647A45674173566C47554E76522B746458426F4D6D6658534F2F62783459534E4F5072365350313145574C36335666664B4273694970416F796B7341736D7755582B2F447970505A6C6F6E597739696E51696259313761524B456E4D6E434B4673736E73734373485061705A6C49454536776B70426B41645342787631625054355472464C3971386879385A3736362B786E6977324E6B2F7A37613562623574395945594B5A4C444D577A395458525249552B624A764B2B3850505A56614155626A4F4A765768574153564B6551414A695A31724E7931412B4C534542737630536959634F2B7478352F4633366367484B4874394D484171775437486769494937475275314767442F45665A74516742385366497A3267476A4562364950385072576D783168683075644F544C6C634F6A463654746144486F77784C316570764644584865356C6D70567663696B497847486C3933755632563034704745386E6F6662476272575461564B38446A6548357849664179392F4F3962784B7A354E70326366503273374D377866562B316A6535376F644E302F3332764B727A4E734578723864706A31596548777474654A5872566675774777423778556F6A414F412B38454E35415035346A2B7341414F477A6770622F3464392F457134413746677073534841736D4362325146414C72624474416C4972707734585841442B7558724E575632426B74412B656E6C3477726B4F2F58755264395861392F5061316334634D714858516B36396F7A315A6147715177433638674C4D434F41656648456451437555553030487842576D494B4575634F2F6B6E564C762B7061383448376362656D646876736E75613350324E797A66622F62446673304F464C307A5A31354D59506956774757675334726E416238565A5564377A6E3837733442714B47436F2F6766445932304B466641743050716477337377414673593766454A674332735461575351445572494F704A6D43704C46625672675365334873794E6268777A6B7650597038742F5667774A77432B476C55427A4961525339754745586151677538427741324C595163676D613055375A426B6E6D64357A77384263454D7833524941527554557467467768467373745146326B5464434E49417759614171427942653468764E467747486A77365A4C4765424C457579786A6A33327864662F325339412F6E7976462B383463796D6F4575535069724C6A353867386B497551304F6148485631756E446B523730703951626B526F79704238324641484C644D424B6A72364D463055466D534A5A514B61534A4E48354F4D45304C4B666B412B394E3769442B437339774F53724D49796770306C6A5457692B55546C65567A46756A397A6D534A384B4B734E5648454D4D59527736716A672F4F69794A5366685A377A70766E705475574770724D2F5A475169467A7362326F412F6B47416B766B706E6D6B34357A7A344A5A4A4F7555372B51356E6E324675722F37684B564259577473524C39516A62524C2B4D7272687768704F47655151785849564971654A497463426874724C65706E70652F55493474575A707372567838457167386352482B45685A6945437A66474676414B54755057646163666D755176665647542B3165536B6B50354E694A786351703553514C526C3279564E516952757062505A692B467872616F78797339697047333446494F3970746376473653346F415A2B6F5857784C454F416E436956547868454336486B72395958554F797463674D2B7936674E545854655469592F354741644A45776643506C30693048566C515047676556694E47314266703151436E2F703764795254526951514C676461626146724472303739516A7470764956637161392F6E6E2F7050714454717231796B50565858486674794A4A72637A33316454334637496E454542756F667834666B616A73305A535A58465972303772394B7433396C78715A3654767051496F4F325663696C4377584E7A36376C7157646A53794A4C4E564256756D424D3547676D49334749655162303361486B6B755469574A794E445276436E314B62776C574A394654756D317632732B696158324C2F7177672F4F742B3039472B706F354A6656315046746A2F33684D6964663861534B47316A766979414272345A6551796C35396962717051466A625A5657337A73482B624A6558484353677A6D436E5A42474177736C683641537733722B76734365414E6A334F59422F434C534971374257435455446C35494B42765A43676D624154656E487237322F767535512F37337664642F6252596E64746564544E375A7648506D75325A32394F467A2F3361334533716D4E78506677746774396B49645454416275453334536B51334369345248497837624B3975586348487A382B666F6A333278792B746F452B763537656548622F37596135717A3162382F54533237645A43724C7862436437425341627134435041453778587177346742797752576B416C65444C596748736F715354484F785035356A38335241416A4F4158635146674C354356647744594466365174775138557A4C6E5941773456487A2F6F2F744867527939632F544F6C50474579396D713538382B4C48462B7843637836465043766A6F514F346B75724366416337475A346A6B4141336768345259672F415A664E67385138714366756A5667624766614B72786C57552F3863764C466F7A6D46527365377871394C61724E7863716A36303463496E776B4C6536376F4D36435733374751714D775249354C4B57773739743033376C696B4553582B584A4F5857457566594277484943693062434B43786344465A414C43664665456441637361586C477342496A627A552F356359412F78446F38426668764B4D475441476768706A704A3367493939414161736875716877447244446431546743645549345A41567A6E61383050414C594F6A3941467744626B31517747554A595046776F4366435A726C3351654B4A4772354C73385A614F3274616E59746B666A336164714E586A513447484E2F6C6A632B306A504C4455362F5631646F3939432F79475476495A38776D31476655397061534831676D784C47734C4670454773546347763430676A4A797636374569543532496C6F455452426D73706A485242522B3336624245694151576647526F4B68695147534937746C4F6C2F5041465374794F61474C6258784B444A535748796B45392B4E577258336A397430476B4C4A73577050737549516645676C36762B46494E7A4A575071352F4C516874534E664A616630735A776741514F6B5677666A42537A735A356961584C53675A4B79764F5A79524B4B464B576A2F6A6C5551386C656873723651646A757655746166745454756B796A6F315A4E6330305A536D754B4867386A6C4B7A4631715936304554724A53524E49557864567950713961554D57544D776B57434B644C6D3263676B774E582F304370587037556978474C394A67526C47513752616146775A7669597055727178677A6B5157697058456B476B704E714545435334486E50435051436648566B576D7669365368554B554C537464364C7368432B526E51385477763664656A7053464C615056435758785A49464B6676753145744B656C786370316932534E4D33755A4E6E734A6164684A59755033306A7238744C2B6A6A4E516B504D45636F6D704A76636A4353676E4B542F6B6F793849476D70616677574B346671634E6530713067647552516E6979765139376A694D656B75326F4E44336C554472706B6B2B775A45456C484F303367776B56396F4D6375776557626F766B414C6A335476724E3658646636346B7545386D6C393379386E7A614970476A704968345074443653537658536E727649324C513556683656322B4A4E69564C337A314B6B7349587049397839714F596B4269614C374A6E566C30354751694E63394366584D69734476676C6C3742654A436850494148304E6D55743630633039697442414F3630726A6A4C46695653434D5251673031665863395431387556356D6450386D41776B73766B527572504A4E70764F50572F4C50394E4A3446324D3255393039483357737158726E2B7447763834666C695150422B48654A73504143626874743146414F4F776A4663472B425A554D6563423248522B524F4D46734A36386A506B6B6B473137746C614F4C614C52646E2B37734162744C2B757A743871654F2B6641505532615647685573657141345335315239652B58473334716934755A31774F3265674265505058346C79414D663453656B41375352656C4F51526B4B5A3874612B59797A3961355633594F64484737324D537074644E543535757838636A48467071614334665144564D78457342764B4935324143367847463463514863574A2F77474D443165432F4A326D6775414341624C6433584239794D4951465055464159437A4664736F57304E59436576727674443648484C7257314B6C473447544434367264364B4E6C484F4A6465586373315A5A665734624C396D43584E756C504B415A2B557457576B41746F4A67646746594C47746A61516D77776241492B77447A446E376558422B77724F637570693241596132687247594679334A74784E575237316A2B6A57757A7248453532484C6471796E464A6E7076506A77794950704B54416C4C4D3664732B794C33444834302F767561396332513253694A4463747233674F67506A366139674C734E37474A74695041796F73354D416867592F677A32414D38447A71674273443334727867447941545A716D61416E4446484656566F74554175474B52716862414A6E454856676667595479333551504148586C5434326B414D554976537A4B41616B4A2B586744414C53484A7241465964645A5A66786649586A65377A75464F79756E61723274334C4A313177594C32763762314C3166795872465779317356726248366D3176366A5569795767445646457A742B494E636A717A6852527132616D51697A75784E7636756E76693844425647375767567268744B4777394F5A42746C57547465374B2F5631793737305066386C6D496C685045306268495779634E6D51526D34386265695635577875586177324B504964726B7A39735A35637A4B6F516F353666737169562F454C43362F37454B45796B7246315436586B764B38464C2F73564A412F715367686C42473653733264576D567A437863726D7A66507A536A56626E72524344736F594568427457615652726B53576C79686661363036714861642B71612B487A6B7066746332553163633449483333662B343332706A3172394A336638736D457031464773515A354174654F4766612F534C6A4A576D4D4465367072327548666C74392F3176596B57754F6D3555726934555951387530627976765238474C7674647356676E5867346B786A4E643957336B79486848446464677137556F526172393834474244596D53396E6B6A556E56784C6378456A316F7759746D336B326A4B494E4D647945506C4C737177756F4F3955667A4C742B73674A355A6D5679303636466236306F347231724B3766534E2F6A7272514F4F46715A55714E706E5442616E59747870376C456637646146386F51343732574C417831336B6A396C766B53395A3938666766466D4C5169446E596E61654A374538504E746B6A556C2F6170785254386262715676765A634949593978436F6455522B613537307074736C726C465376334C4D6B326D457055547276785962576E37656B434C7072466474536A43783745796D6D4B6E4E4679676170546233754F354C695A5169316579355A37724E5475797552514F70706C6258735338684E36343262316269384A34757A36586A36797048526744525863306C544F494D38466B70396471464D652F33796F33303670556E71363170794E663673456676583449634A4B4B4974662B5059485241442B5A454D427747784350754944414147716D62463377523445644544643442735356355076475943376452745274663058374A715265794B3162334D72587232312F646231664C733834644C5931624D337A38304F6B516661356F527633466A622F55627A5279545A2F52493059666E34416241636F782F4E4C6342736E584E6B64476C7836645448624A3132647851393876352B694D614E5376593476546D4154634764756F345A656A3438726B726148316948366A5658595765756B414176766864317779414C61746973415767683844794137776F6E6A463565346D6B48766C427553335333336B41524353674A41414C777468434142622B6A7133353836326A2F555A3136746B586D4F5532383967766D383575724778625763672F3476674F753571327063526C414C614C3256524E4142374D68686E50412B496B48424A5441425248435038414942456C786634417372506656566B41307A317A4931746E344D4775523548686A647772624336357065796C5254507A6A74302F647348716D78753063515554476B5A366C6C77684261467234794E75527A5A5065506F3339594D417741434F4B41445032464252414E6835466D4E2B42504270624B57784D6941577830584C536B443077585268493841376F4B5336414941736D4D5179414D6942656441516453507143694158666F4D58774E5534495A5941554171444C6255423745574530415A67766646554A514B596A562F4D51774732576C6957504276496B3544587A324737586C566E624F3270356335507A3967335A35393972525973366E3730347A473342776D57624B35745864547362353876386156542F31595249354368364C65586C52356B6F51544F4C69395458326375715839376235476F6B35574A2F6D4E2B66424E6372432B517934356C324C6657504F32462B54674A556C6573664E6D4C555433336B635A744A7A4765432B6B45385732303065326A446253303151466474326B4450666D46684E654F566D6B386339414A36585649303672616C48706A64436156784F6673306354594A5A4F67347A384836514D4A5148497352324C6C644435484343464C7A5878797A596F6A687332574E75373858386A506C3464633632516666546D3938736352365875766B6362646B47355844416D63664B755430366B43644A4239336B6B6763614D76754B4776524855707163664668755A4659314A6C325A486D6D644E4D65374D4C2F796A6361463436394C587150324C516A4B4866567436334932334E656D46692F4E7973424E6A376C4F625565503172356370492F52306266535536723768457235644E6658645630756A766C633870497376742B53564579524E674433337654636E4371794547387A6E525162536550766C7343557437506547795A586778766773575769384E47314A66747A75597675657A3076656B73347270435A4B546B697850585738354E6D455757577A7579516F4C75714D4F61654950306A70336766727041765866425272506E5354513143664C6B706F59395565302F67306B793562665A38744A2B6977655432686562433075555A47795057596B7758495A7251635879574A302F724A4574354641755A49592F4D4C6B4970644D46733356564A396F57733846697648725177717145325170586B337233434A4B566E4B45424E505A56493773476D6B695157684864346B472B6C71334A4F33766F6853354F477249596D4F696466522B38322F724A786E3278412B6F53414233496F7465592B6F6E753265703636456868564844366E522F6E39546C76535A584D2F7A642B57712F475438757A66425539425372414568674E666C3941492B5258616741494967335A78634232364B3271793152514C474852582F4A6458525832584839782B33744D484C6665674349624A4A5958333147387A52696E5A6939305037434C4F64387750745372743175434A2F717248632B6265735655304B597953344B4A73416D336D61334E687451346D4A523542393659302B76486A32366C75386130734D55774E7362336B514D36445376732B756F393839794634307346706E37392B3135584B71354C484538415743534F4A6F3541786A43336C6F4741376A436C3241346749312F434D513844796C6D34596335766E30545A4D754E374F6F6B494F58504E31587655614E5176736E416C435A54757138746E645330584C734B77666C43787555756D61746B73787852543974706F6C55654F6D38414158797672692F41707768544C6163425645466D495275416E6E796A4F6A75414E6E43316A4150345072624E63676F514E6579553753666730393367327162664E66744F467A7854343937734E7146724E71353132523937624F336B795650713764772F492B6672636139733468626C71466571566646564B78794230455668777A344F686D74797870523355564749536D45476256515579356D63724E64485261577A3554496B515933424657432F386C6C734A3444586E4B736541396A4D4C36757541626A424D676C536A453146586764414375614A6B694E4E44556747386B704356364B7455744879516A4D412F7642524751476377304A745851416A4D5679394268424F4D7864574258412F346E62456270526F7274436E517036435435343937624B73322F456D5859624E6E62463578734E42525266702F4A4A657A777837627872565048757A364E4C2F6B454D4845477556766C464E4D39546C6D772F615368393853415231744472354F387871365331496D6874627372536B6B475642316D436C642B463174584A42347352674762643862777453767A6563474C64526C4D5649396F336D35424B556D57496632704947656952747942316A36652B6B57544C53786E4359676B35376B6B624E37777357696876556A79614B666245624A7447465579557138395054695148655137374E7252366B4C7563757566546337344F2F52417135694A6C705869545178756A336C65652B4E45356E4B4568364B576C434C354E4635584A41327155554A74633067655A464C476D4B33313945756D41674269726C57394F586B49744A516A6F4647356B78694B5832434D53676A534D4C795735535755326A7632386E5465554132574A42575944384B436A34346C6A386F33416E6874772B4B5058314A4C49454A66316A4B314E715A4B52367163696C4D705159775A50443544762B4F3939326632496365355067766F7463654F497052732B42356B312B456A694C55542F6B705056465279353549535149724366477543304A6568632F43315A666355576B644D43696466706A50644946457A4757306674545833656E3965672F307A39746874656231682B42424732524C4F702B6676684C7643544C55532B4B6E546C494676684545696763615A3072514E654C6B734354672F704C512B384A496F33535376713748475239382F4E354A4638623539522F6C374F314C5777753056586B67685A50763231494D504B6837394362766B4D3535736D50585054434236642B79786B5341436453663456524C496D4B424E4953704D6A7052786232345A523276695A5A686D776F5732595943614A546148786D6B694C4A63503472376152325A4C4A4B5A76474D58466D764E3852333452454A576D4679476D5A795252314D333974657372424E6F2F6D776C646276306552364B6443362F70376D37396E504670562F532B794A6A42385867394A54574B7050414B444251333443594F6535526A4D6334456E694E7530594946397A6E3370754B354A65744B76534B624A61766D304858616534506450712F56386169686C455070475077435432432B594456523556325664324C56417370636A315450574353787974752F2F747A594B3362722B2F463341316F477A757A7335464D6B78306D6D4E356C552F4D3253424C70394D54415544544C4779777A73303257753263306E334975694631756C7A4757507575396D397358487A7265703733765050416D504178596E366B633754474D527569784735714777433330524F53587446487047486A73736670793239762F7A2B4E4261454C322F57374436694F71613478396D3777704977546E7836734D6D4A6E614B635176387456316F6539572F662B5676683062302B4C697238525077413868546D716177486F77352B6F3177456F6732766D4B7744666A4152395977416A555165504155537A4C4377486F44636235724E62534879303564475255475242794E79672B6166486A786E33754F2B6A35514566367436746637544269687756706D78564C52664C756A323663747432754932337A6A32685A664A772F6369592B71705A5A6E2B7A76336D615358625669453133777751414F6744654F4D4D61414A694D336577476742544571626F416D494F56706E78416873595A547246736746732F6C3475365041427559615461426543624543305541714147777838464A424563726744365959697049674B54536961766966655073485637356C62445A567A5362336D4B3563365231536177576137477559746D7A58687351496D504A614D4C336478586F64756C7270387139417036485A453171695236595579314F6C556665662F54466A5A457962372B5A434957694546796B592B475050433977573157735365306F4E655258544757534D524D72674F5072545468526E70537A74727A6A487879487A2F45463544326875736D75796F51677935534C49502B4B2B634266437365456550656A68695274725442313662663373546F324E4C436E556A3376795142374368744243646F673474624B70656374752F3855624930794D473858616C6658456B792B5877656D3155615A4A444C32797579754579686A544C61797258494767394A34545250506E69514E745437365535616B626F64526D49415A314737467849446D6943374B736A4F7352524D725A652F44474C733739494A37322F53365A7157516F78546B737A776B34554B39756C3750735A61454A4D5A417975423577594A4A4D73707665644959697A74534D4D70383533572F4B654D59474B5970684A4439505A623077796C47326C2F4C7836556E5534576545474D567979315079465772756A6656612B30635A4973686C6E4A78657A6846714A3976367334664F6D3738714E792B354441555A6F592F59716B416339423635694B366846447269312B784544666F2B757653504E7563766D3264544E5A5075644A50722B4A727250423658796576732F585A42475170376B727A5838317255736D4B31633552677831447175442F634B4A55583943366248786566314E752F2B65457366546E5251435A656E37716B414B6D4777556F79445165364C496376754B474E35375A476C3654594B4B2B64575079536F5952594C746146705844394B36557055554C566C4949574368446E74467355636E4C6B763077356A557056746F48566C4C5476752B4E412F626B3057354E4C6E755A705264396D6842446963467A423371707A316B57626850436A764C6E6E53326C317754743541464A706E5730324F304C676335664C57454E5047512B4B7046564B2B4A737373664B51626C6377346179346F4B713678366B535134546164392F616C737150696E6778752B6968386D6F4B6748594943774742443738454F5745674257345471714145497231577965466368374E6D2B54724D6365327A5750624657705175687A392F6349757068694E74667A66704B3156735A5A2B4A79525063445A377A646A5A6D42303263484E317251537A7A635A32324258745549377A38643369526D54464E423056394A6B50665337593073566556506B6C38774462313162395076634671634B704268474E42363976464A64654F5A656D6E74456A6B6541647A7676596C35757675554C48536C793975727A6578622F33774C4B662F71394A704C6670575332725164416A577A734767417A706F6A53524676794F526E65503836417068394A53536B706B5A4741486B5A455262474E6C75663852617A4147366A4B73694C4F3138353764566E644E586C68775947564D55456F6B7A527134386541547747653855557A4256704D4B47612B44474141626D68734165454A3336734E4239686B33735855447844627752312B414D347A4C596F41724978774755554158684C33625145453377695A597271437561634454356437564C73346E6B633876662B362F79356D364A4A6B4E422B3965714A2B3066714C717777614E4B50573467617A733437346543486C764F464B346B64516D6B75635346666A354E6C344430417938706B2F4156694E4B6B494641456145437738426468484D6B4142557A31724E72336A4A6F424539786E513730377A436A4837786B784B4B4A77794D663472473742567A5A4546776749672F667634573647454C786C58495A596E447857656A5879783950757A46566464493139555A576F52643765765135315A4C6C77544847765561754733596B2F686B5361636C5479724534776C6A6A5032644C6C79704E3437385A494559514445634F6E496C634357475854776955586B39795545436752745A4249775870664A4D7042457A6B302B2B534270485267752B6A6F4C714D70434E72684274524F31704957736B7034756B3450586E354A4A773363715373353347395233355972386E686A6F346E656D5074635177754D7452795049426972546A78386B72376672306C66646E704A30394B34673271735865456C31444D30574F36564548556E395366386654686D5A4D2B564B353174656C393851746B61364F49464835486A4677726368354D7864747946706E69556152527649366A6363363275436564507253653630735255516E5761634E33663766395A755242424F6A66454557544F543553777A794571727662574B556E704D7254587736737A667061643546395570396E6374525871582B2B766B494F56714F73735642447670396B766F2B4939563344735649505A66504F61476B45374947336F3563354F4A49774C6C506A504147456E43757930484B64762B733574474C5845325956544336664F354A43726E473442743932394F50744E7637686C7A36527639443730756D574B6D724A4C6A2F4B555A3937312B5843752F767138306E306D44486B71417568364C596B75494932363056526C6143416A475768796C4975686135444E34686874727768653946493538494C682B675370722F383551573230394F492F656E31503570393138694C52675879534C774A304F6E374D7A3970577875656647446B4C702B636A61797930684E5952335457426270677169532B6C304F69626C4E7955766B63323973536443556E65715461542B4E4A3473777A356265377A76742B323653774878547676444E4D57757035342B463975386C354672366D7461704C6D52424C53536E61615A7853794446697939784768737039756F537856687979372F4E63694C6A68776B6F6C6934386839674F77427A6D43303841445868652B414F32646A623964586F6738786A50575737393769363169395447415347743759625A62575670654B702F36686E734644674D614F585634554B39492B34526D6E63716A66416F764B506562507A4E38484A55765A5137687644456B7A56583244433747654A327A7758464235545375713539762B726D6A70745A4170766F58366E326F7053684242433649336A2F3230696E366F30754E656863786548475376472B7061486D616F37674D2F66503675374F7962303549547A2B4665734238417334717A6F4438435563664449414D797979352F652F483670462B6B78364C2F4D6A4D59467A7A6E6C526B594B757A6D596230334C73384A305A523277354A523476646176626E445A2B6E2F78376849626B32472B755A467A4942674D3478507171446742694E474A35497741315748376546734138766C796C416442484E4B6739414E614A662B4C564161453454716E4F41655939357176713630444139634344535966742B65594E323065637A644D416752354244304A5431673856733275664E442F66503175644437577146537A67487873624778635847636D4A416543327A73345A4D72682F53512F4D495232557151563461315247477742324D4B4158674B567368636B4251446A4B7334714139716B6D727841517436715A5A38734C7865346674455459684330434970646C394D2B306D66583879303572675858346A303568456F43624B44496574446351693877364D7A62344878645561354E47634C433136765A2B327664334A6331504452494D6F6B6C6A476B386253784C4644706A6F3732724B57754E434331523273686C6D6F34334E33697047493545304C45766F532F33302B5141396155474C6F36777A6E2B5850394A3935417743774A5263566436767A4C7049434A5272387735307472525A69656F3873642F7A4A702F4B37672F537442425669614E59513437476442415A584F73394352597938664642624E476C6F4C586F713532503633767250773070416F67337A6D487868367265565A695A422F41355A6C4A72535049333630386E63615775476E354176664353354F7353546A3778655074687A7231553659624B633743564C7A314869304E7A49315556444849552B582B70366D424B6F6E483834396B52476C6A707058333946395452335458395A4372346462386A79634A7863692B715336316E414E305A6E58695A4C5230334B6A686848383151736E7662384E7049465979637050443551724D5A71737277594D2F7862476332664461746B4A4754356B624D6A7833334A776A73532F7A4A594B5934365376506A4350302B52516F544E3971516465535A6F4366584E546B4E762F48542F386F382B58456E79653947764D55415149577A4F414A674B686373737744376B665A463746714B41545A5074497674504736632B69576D373768316E53784C3169786450362B662F352F4C4F5876757A4B744C6B7748623058596C626530616D5078442F587347724A7271326D35306877584E4F6E544D647A6E585A64327473325631323970744F3333383152376237763137444F76516F4F46456331314C505A626274334E6B6C736879305738422B3169486C7A614E696930386548542F6C74507A68393179334F48347775765A57366542447764656265386432572F4C794B326E7A6E5A7A58787363454C7739566B37324A2B6B4A4F55716B7339452F416662327472622F5966444E4D734D344C486D69766E7434637743393042534E624976503935353373506E62637938382B6D5534356A6731597644353056637133556C616F4834302B75456B2F314C464630556E52342B79444D4151377348587347344166346C63776E55415931676F62774C773972793232414E514F654D556677596748424F77466342576E6B73344476442B374B50754978437A4C5059337351317734656D466C592B4F31317A71387436746E754F7846556439577559726C7A4F732F374C736837496463664D4D724A7A676B4B6A396D6F4D4B524568706E6E55416A69465547417667453936714A55506C4D4A4D72774266796F36724B674F7141716F7A4E5336306B6B46555361707247574F6F4357495966374254307A2B49537158694F6B7971774C4446454C71544231637157424F49416E636A3135303853395750384E5753584836747A4B537930775834674D5730524C58413735586C3237736375624A384631366D70727A386C6A626C2F73522F3170703846712F366947496B6B3072516D79646437666C4F682F3465514E694F32736268455130687776563479666158356B73445853393659796555685367346D2F7178525433736536306B672B687A6138584F694562384B6B336671337762794B5838552B396674552F433953443150396552614F356F7369624F715331512B6342572B3654735079554957377041765A71314C2B2F7334314A326F3742713656686E762F7A2B526574774E704758396B32666E796653582B4F2F436A317541753249446367416F796E65694B4341305A426E454B59417756616872354348445756643230765475385937564A395A74626A695032546966644A6F354D5A6F337A6A3039542B7463682F432B7A2B31666A76613041344A6E427665504B776B34486E5A73623445574479496539506450794C5737324E36536B794F3244473530506646572F4B756574664E617A68767A384C463864735A334753393654544C73744C53794C4C5373416F356C5054726B5A6C2B67547374362B347257664E333455347550516E7A32714B34766E3734613857354A69657665692F4D39797551684C6F6F2F6C626732756951344D37507872447759427A2F4B704A504D3158382B4E65446644353658313049456743485969706E71676E703334386B5570696B7865744F4539553058586443555846547551743778725836356E4F31693255642B517878756C4C35652F3858474E6D636648336B364A47436631375759465448587A57724D344E565A633830324149364D69526B41537A522B35564D414953764B6D75594466442F75435561412F594A35717359415738567356555741754471787656516A67524D506A375634734C5857696B7739504D2B34435630377A6D417A574B6466707A305152346F546545344176324532766851384C3173734F50302F4151414434386E30317949414C44416A415941525A767A7846414142726A393744503537504366586C67356B7576656D44533850435249353665416C4C394C7375464E36514765797644674553745357664B45315A4C4A576B6157446B342B7459596C453551504A33704D4A2B4248357246366A2B774D6F566B4C3839652F5A414F4F702F45586B63764F4B736A2F74494531687A49394C34714867583433553879754D5843412B4A2B554B534E397A4A6B7175634D78616F4D6D482F31505954306B64696C4B2F424A4772306258715037746D2F39655265723746557252716244706A70483755657A2B37377631744C6E774B46507762384D4D45464345657456676F774471786A6D674169426446423667417236315A6D32595578664B437337713471566E7277372F6B37684F384E686572744E702F7664634D76376B46316E6D733272362F6E2F67655A654145414737395851325A7A674575363130504F523944664E79484F456565485A6C58506C69353557694C746B4836464D50746C4A6E71505931694738387164324A4439724A31792B664B374F546635343768626B4C454A4B4258564F387A626459424A537156736E556446484237673237646F7A4E4E4C303936657668463636693633533350357A38644746526147495436716F3369466F41563477767461667379646766416F634B626E7A30733339482F39595836624447412B716750784A41476E6E58526878736A492B2F78374B564F6C337956653257417364625A576D634B7A426D704F355630637062664C3576616E757877717636465A333330463861657A2F4C77654674315148762F6C3346524763324741484E4F6D3159416534577171704F416542714839463441666F4772715450414C4C77324B7750775972777250776567494836783851464331614537396139563538364F4F5866303576504F7333644F3358336961764C4F6A2B7336626D782F647476626250674E51502F76622B662F4E61544E2B43645275737A6E4D705531614C496D6847493342416F65564A4E6C526142306D734A6C6F6D546168587741474D31736B5679367A484C61512F4B4A35726570506C582F37765A5365796757354444352B683439517655375958322F4167582F50794A74446670445368766169697879466A4C356D5034485657734B464368516B445A2B6E41556C476A47434E384372386946384663414C3434544B453841453158784E524261626D787475383165687338666237646361574E445238774443414F62584936473355473869446E644D364249664E51637446712F394C66374B5863426C674F74487078683961525969394E4D6B63366658393135662F6E52504531532B51666C375071344231326F6671484F6E374977467737666633376E73396E7A6A6D63375A4F6D33504D594C56686764677167542F76705836396C7177305079726A324E426C65667759374750696A364A66647573363769674B30483756525A684C727372396C48504246675A5068572F30704775397743593861667A58564E422F7273636E696A2B49595261376B31356D78442B384D776672315053304D38355A6F79513075767950377A3747302B7A742F585565586A384A32534D67695635447576374F4467344C4959473967312F39566E39784E5467646B50345950534154664762796C7A71636B46374B4F662B484265384A75652B31767261714A65546938394D47703773617A654F6452436455563956427542506543477A445141766A47557241617A434453457A494F374341584D31414B567869646B41727766364F5962567A6A506879494B44445336667147752B7272385265712F3057317A71662B48477035724138414E443338784D58394D5534453970436242784141416B72556C455156534D4F6C6C4D52474A55506763765A2F2F43343932746671667A344C782F72443055362F463530763972597934554B50675A735070654B4E766335367932653736355141554B46436A346C2B4F487556437734617265366E67414C5957726241444144724D6E327158416B78565068446535686546335A74356837346F436962614A702B3357654B375A6B374C4C634D637536326C4E44613239557A613277744C566B6C7459424C77392F47354234466A675564596E6A353447337A2B5263736F34517238795175636B4F7131575651454B4F6855386C756E7044753957743573486C316A34356B79486A423132354A7745384A7541616C7A2B7733776C6F447161592B3377724350574E73384F74475A74327457652B304C6E452B337A4B63756254394F5A426B412F674C58444C72457677467167706F48307950774A414431452F465773524155773245504B634B3046344D58316E484C7838434D41764A414A4677446B6765527837673450316736414C5A7A3444514246416534476F415559326C475A5A77453243474677414F414B415178672B6643335A424D5438774D683577484C4565446A457577584C55424D486E33726E6B3439373958596675523575307274744257334E746C65725833566B336B3237686D514964357073576B7478714937503231784264414B336749482B44706B305751442B43433830746B424F4D46475758774254424165734939412F4A7A3476594957434F7363386A6270614E75686C39335071586633646F62326F756F312B2F3348743075424167554B464368516F4544422F773338754A506B6A34744E6D4438675A68457A71423443634F5342346E6241754348466D6130436A46574E4F6457336747636E58315A373531542B774E34392B7859636E542B7644674477526171435777647479334265414D6F356C2F63763977796F5061684F3131712F4255566B6A637461317158506B375535442B5273377651362B6B4B4A733858485A367439634F48696E49736D37383444434C4F4675344962674C63416D2F35754C413443516F616732675832356C635836776C742B56506C61755839474473303939727334626B634C6C7930713258585152674B38453873317251623446467347704E44314F547A6F464F2B33453568506F3678545943776743385535674873412B59792B6679556851444C67614A384463426359634F4C4147776C74714566674B485977347344374358616D5030416F5455476D514D4256682F673177446846684C59634142546B496A7341494A672B4778702B5946512B54486D56516451743241732B334259564772475850302F52345834426A3049577056514D616853717757744A396271504D533752476A4A46726B326E52697176614E7478726F41724A5577587777426D4B50777748775A514762734E45304373496958467A77427442594843386D41654E67535A636F495249324E6E4273337556542F4B3265756564795A566D7A36375A6737797839312B50487455714241675149464368516F5550422F417A2F4D78597650463265497177484577707458423741573355514C7750596A58736743384C58777756776738574869385252334944456D735652795964644141474456325569507A68344774396C41682B414F787974614D4B49774C37792B5161624531713164576D317077545955537A695832446175656533726E5677366E6D6C78317539695975624534676B71514E57644D5A6650783866705A5646686C666D6C666E4C6B556A525637644E4E4136495035527466634772574C6B644F5A7579622B5A496D643565596430377678754F4F4F70454E775344315577417A386175354141414746646F4253454163456742454978702F544C3561465A7359413141654A6951434B49693137496A304A3859426C49594B2B5142596F474B3241447751687738414D7543534D4248674E66425961412F41483648734863425041386743694F66687146344A59435666796A59414F49617A6A493566776F652F66794C49353332496E54695058676F775839523336526A314C695863504D444765586D583936552B4A752B715639484776365A2F6A6168654C6963776B555768446741584E74627946454179643263416B42646D684149734B77344B7859486F41394654457A30636967536133706350366C4E695375445964377544336C3842356D495066502F2B64696C516F4543424167554B46436A3433384B5079354C44556339794877426E4E63516C41457877353373424A43435A7A774577676F396879514475345144754166622B397466735A3864536B4B2F3453504442685A514E774F2F646A775664666C462B2B6569555551393631797265723239533738464E39687963337670315333334E51364D396A6C63396576545159385073624464794F44712F426A6A6E6E4A76526B2B697652484770786557416F4F7334397247612F343749797177347238614847795958366162313065375433565A3152456D73317A3048774E68796B593766346E45414D694933334145475A73634D4147764F6D72506D66326A6E47697867427742326D64316831774657442F325A66483578577741503842615A4147677058653532314C4D4151485A4D744177414549537150442F414D37412B3876464859696A41642B47543552334173794249584150774A71686E716666505434676B742B523970697041636935393166417847585A317A395A6A644E57477669384B625331634F7366556836366159757061366F73414B384266436E7341714A444D65774C49416C746B414443596A63415441426C5972466F467849324B33326257415863613362332B764C31647156326A397271746A416643456B4C37524D3848644C593246625237314E506767597966443870556F4543424167554B4643685138503874666D4365647A5A5654414141724B63307A4F645A5A774163547A4153344E3135436651436848374364614559384B724671304C766874796B644B6F4F764E326C446E6B6242756A7A5437772F36656D53535631474A35394B3370783851472F6633647A443248725879457938452B6563783649697138784B4D3644692F496F374568344172497577416A484F5362737A374C492F74724B4435754F346A78552F6E584A3339436C51534D7A7164564F6A4B696363462F7047766A39652F7969374E47505174646544582F574B61636E30574D7A3736767742324C50485351784156335245666F416459592F596555416C71443670504142564631555839534167646E444D6F4B676F774E6E5A786156775A6D445779326C396C3466674B69367A7736494A7742684144415277417131345067436C30513837414E5A5638454665414F39345A56344C594130774677384270686547474E6341466C6961705151434B6B66564563306351436976336D6A3030723956523267506D6A3769793265342F6B327750324C585669324631726646754E676367704578786844367934372B335664356E707467763966756C6170544C6353636941757944414A5966615A6A7A77474D77314C634154414C4A7034623449316777387941635957704D6434425169355661364636326536633836306638366A4C4D3859597532437558614F6652365A356535326E6F7A386134536C3649786C503865436662624D4342516F554B464367514947436677392B35446B442F63545441446771573659415543453733776A4146694C38415578454E2F597277486467424C49432B6D43395130707679774A3674763671384F55357A716C797A37357675442F78592F765331774D48426F354D4E4E5471486449367243726E62705A626B58667578323444586C5637325431324B5643345438466E4C5534434A30372F66756E366963377239742F5A762B5053726D554C39353766582B52616F656E78463777766E5048744D37465868746F754553366A78556C38466D2B6B2B5358354D48666B53316B5967475257335641485950573572536F5A514435413841544D726D615973774E78462B4B75786430423469724556592B742F4A39474E726E56594579766F774472784677744F7A434B4C34484A64416241664D4338462B4456344739784255526E564462664138517A59726A514830425A6C4655584146517A56416546574D446D724359537A5243653557376D6B30364E67654C6E696A6A6D652F526F6B634E7A68334362736D644363374163723731652F34514A385945784E376650745044496D4A467A312F55474D6E52797474485A76366A74344F61345833415138364D2B706F724A4145627839734939414C6C67673563416F6C484B736758675470687432514B494E336C2F714943495531486E6F696F37425637726366766F6F7965436B2F772B6C6F50357347426D684F5A2F382F775A42516F554B4643675149454342543857503836436370494C6D7334416444444148384152334D63616748506D61696F416F4379366F6751675072625574727744504C646D32754D794B3676686149763968652B4F614E3136353562744A3437467A33373573745762624E45654C6963302B3357333863347972487675626C486A44693456657962324F4E70732B7643366436654533497964453346327761394C706F32366D792F7250732B397863386E2F784C387370726674756970366752544B557462585576676457362F346C467546664445365A6E345A6E385A6F2B6668544A6D647437374E614A76464A76653730626C674B7A69736436774B7841324A475734754438444C41737459514678714B576365414F67484A6D645032514D49747349697334316B4F58467A412B59646D354E352F31776755366A6E47486674363869716C3675682B4D504867524746496D636158727262386A303867462F687A7756506F524B37426D6876324562794E537854797055556C63456A714A7078717A37464F4F644E7031797463797A31794275374E744F6C54433163596D397362444B77386543365478374D5058336E58495954525639486A46347A386B4C48692B6F466B5A4868435245724C464F6C54755A4A3775345A4D3370342F484D5478466C30506D586E41326766614E617246674F61385A70322F424F4156526768324144734A6E78564251476378574E4C4C6F4258594A6646645144363869455141497A41496E3446304F74534A68716141323963583373477A766A4443324952693054452F3256715A77554B464368516F4543424167582F332B43484353684D514250684163427977686C52674A674D4452594147494B6C676859514D6770764C586B4274394E75703233727034546E44636E627966745655506A5677646671504370567350694C4779393366397164385870736E3651427167384146734448556C5744447830435634643371443034644732776638522B352B57353975544F6B376D4F7A6654565256655A443831624E75326831794F48647845466335737169745759477342746C6C395647666955383950652B4F32362B5365366E69783170554C54795355545375624E6E753349464C644174796A48497437506B35377032396B757A4A6468333831395236376B416977724C4A5651454E4350536D365A584277492B506A323272767467484244754B4175514933304276724844756A5365697A67464F51304444676656336C5A39614F6C4A74562B2F756E3578316F52626533716D704A4E4A58686E484E594F305161716F7744584B52377A31424D78327A32332B7935334957482B52765675762B75476D4A345446673265564773797742674C5A777964466D454A3441596B5279567469707349714D2B72544E6F534E6C6B746E5332506A64754E6A5868666A4F505A6A50762B36516C79705053685675635741505771316550567032514B5342366472444757454E5969414731553967427534525366444C42346E47614241472B474F70613941477544514D306341502B767666734F712B7259467A372B6E56336F5251455252525156455168713749494E613478694C4C46456A5A72596A5255316D6867544E576F307868366A735A6359724C46476A614B78592B3964696C685170417473594F2F4E586E502F414E397A54753537336E744F726B6E4F38377A7A2B5766744257764E7A4A715A39547A37743262746D6633346155664175614E5452576362564373586B5058477139665764767A5A56364D6F6971496F69714C38703374397333674E4664644D325343626930597945416A546A746A334256796B30616B796C4A6E6B4F56463346546F4864776D74643235647777366862626330723379625754766E324B324F69687153473247717273734358513878797A3466744735614D2F4E39634F6E73464F2F306E66476A4E376656506C7439597056623234667432506A72344745647A736247566E71343971327142594D4B2B386C62494C714B42575142736470302F566977314442586C337668796F704C3266457A576A52766232752F49614A647A4F6A386279786134754F304B2F74757238373639617641466B555862534E735677467638636A614855776976346C2B56716D75546F38643764303368513637314F39436C33765045704653786B6A3574396D75364D64636544555056586F6B45416141302F2B6C6369617A56456F7063334E674D714E6F6944674E59446F715636532F6D33457178324B59787A797857487A6F754F7275316A76746E3430336E557555447A2B386177793658386266613776372F4352666F364E6452655056644F44502B5958473666777A6E3654556838614F3461504B586A4475366E766C2F616846566472747977352B4F644E576A39456B3636635756514F35554973302B41482B637064644438424A727450584164774945384E416630505855657348666D3958614F42544F2B395330775868336F477032676A576C4754556C2F366941353052364E674C694F4A3159496F37315A3978705971694B4971694B4D702F6B74663247785464565632735853385178396C7176784C45742B496155654163362B425839414B616E326C5372756139666330476252365945506C7737643331527A626333394E6D387246622B3234336633724776343257777A6D444D3867495674726D675847777755504D68484B7266516F71324D37724731594B503150642F7336744A354F66476A4D2F437871572B32487542734D6F45464E467544346438434B456342435057455156344161464449566E63354D5861434675675373614C2F4D36486A7872344B7238356665502B7A65716E334C2B2B533661412B6E79674577457356436330667442586B787552333255672B6D4649665754374F6B54496C5A6B725A7335385A6571445837734572336B2F4B69535763502B6A532F4F386D335A567534732F757A69716A7638744766794A6B735839356137622B304F6A5776525A4D66586238354F3268777A362F477973642B4652362F714E376A4F3848706C4130764A6B356648587878307631625856707343666A682B4E474A4D3631492F75766C586637504D77754C383961485A3562494F70645A6B64565A57566C5A474271346C323938744E5455314E545556557031536E564B64644C6F6D546F336E2B4679456554336D58397635344F33764C305265714852765A4D754B685A304B2F5A3361416E6435626C774F4E4356526A6764713834436C494F707A5772634A64413946674A594C37692F6431687133673838636E30326C5770377537724443376B79727930564767507033495366365A644F386F615A64504F4D514C3447375043455A7346465A564146415534474B6F69694B6F696A4B2F7A39653379746551614B70677866494A744B64436143582B673135546144577546727A792B312B4D4C315A7A576233337A67317366726363334F57724F38363874444A3061646E33752F6273704D704A332B58626945413632554B30496A414969746F3532316A325136506B3539635336726F2F303773376450797A684C6E66573432313945366332616959615478656435467345367A546A4A734256364B6379496670432F33744641515063523632526E4D62617A31696E6269647A48346B74764E6C75353735474E75792B33414B4F4667507757496B796B69444553793247483843637A6E725A58307653486D384E464A467A7962646B79396B50485469317562686A327A542F34713564432B62624F37664756634F4639756257696F6632535942562B586736346258623442646D4B553053415079307774434972474653327A396F6251623050642B6B79556C7A78506548337059613651347072744D733668586F4D33557A394B715646774B3344793037374A48372B34376C3676355A335778327132573563716842676E684D775A62786631624D6D79593848582B393938646E76577272646D445A6F64746672544F6E363937667234645969656572534470634F54477463765079327566566D6870426E752F74373263334E77652B44534865784E39695A486B2F3342626259644B792F307239646731646C5669335A555776446577774E4A73656B39536A5567686E5333727341513231464D494E7451326E494E6D4D7775305130344C63765145396A48476573344B4E656E664D307967314B5731647059793177392B386A516158576E372F316F4232785A466C333279466C344F65486C70447A3766413965454541644949466E70414E2B564B527138635768416141723262344B712B56726E654A4255525246555252462B592F7732674955726251576D48734A35477A4E714776434A4C38612F76487577356A5676397141387048564E37393575667135646E63337657302B31764359512F7733483336577676696C6B3230386E52676B4B686A57416A2F49686E774C624A4C72355176516D6C42505078484D76596F61692B73564C532B4C386A346F334F52334B5852567944666C4F7332346E31576C6A644D6275373976644B375632522B53743151366C37346F633364654A524165596F326343484934522B3357415376454D664E313050786C63374552324D647A426F46594C5159576467485255584E79484166435532646E2F524C6B54624763702F42696471717A4A524F4F654D64636539693555624C423376434664726F526A42484F425133704C72356B7475556C365061497A39504B416E745A4B5138444254776A4165514136636B4B734C575477375154384F44444231376D596144743041626157744658666D6E4C63636741665864446E48595772424F73646869594A365730536D6E6F6C39447855624F4D36576B663333737259554C3634365434765976324C6237776676744C7A2B756B6E4D6F4A726549623633583236766D674D546D7A306D6657486C546E784F72734C3135366D494C4E372F32373758622F38594E506B7275446735744445366674734772656D6B3148766D7657646B2F63376A4F6E502F712B394B6B6A4A394B534B76746E46546E62456F324E5148694947336B5451486A4C5059594E6F50576D504575415448796C4233434A723255494F4C52326A4E4C35516343327176482B396A733744787330584E66326C317652414A6C6A474C666C3278396A59714A5977447773334B595A6C3968504A694452795250465A5A4F5734713646437741767141694141306B4136486E796A3133775437706E46455652464556526C442F51613373475853476933454C33737541357358516E683155694D7A77772F47474E4B7763644930544C777A55762F694B65646E6A2B666E72335954657A58584C387A492B63626F6D7449675741594C7A6C4551415335515241593446754C45676E49417853356A2F6655746A4A345A755A6474507A6F68664E76484B6930636D7963642B4D61566639666C44764E304B30475145644167623664515679614B36314241726C474659414B3252462B52436B76375A4A647764344A6F4D4E476A42436A6A5265424E6C4F2B3951774562544E724E4C476757625147737355304B35706869494C79415A61764B45763243625A56746A4E41584F672B57336A486A43764C647A763142774B33796F30473170412F726346505856356B4F39576743455538764D4B4875717A6F63433330456B2F476978487A575A375A37412B746835773641433249625A513533306763786A4A665743384E497568344A546B394A337A75724B375371707A563956396C55353654723065574774636A53484237362F63376A7A4179646478527448624A3575634C4866335546447931735062736F3546626E7253762F55486238794F2B324C2B386364484C7A327155376C6E7953746F377A78716C64524E31767676375A52574B2B32384A666256713271364D516E6E452F726C6267357848564E7A7A4D35565057663157764C4274774F3235717739637669486D4A69626C2F327A7A4B4D4B4F396E76415247717A64462F41417953396B77447552526B585241754F6A2F645064417446756531664E446C6772552F564F73516B46764F3855573769504D5256787648725A31536E4C74327A4E7A494F6B434C4538757562377A64646E383265433333664D76395A3963443546474A653441505074707A77453630744A5546494A763641484B556269554172615176414D396B376A39635774652F364235534645565246455652587150584E6F497959756277794534763457483778483650566A38393369363051356E774565754E676438474C436E58396D6C432B4B6A77696146756336626D787074575A2F70506D33536C3335556E7A3439566D4738615A4F7072764B4972596A4A3778424B6744463755415846426A4A534E49657654374B4C386E2F5476586C313070576E756B516276367133363779324C34456A416B6339764E774B74514662534477457855745156423044757746327A4148566B65777141592F51574334724C714430764B65774F34426C7252426E414872534A494A486F7167492F416D5941536A4D4E5741365756302F715338596E524C2B532F566668335455676C576A654C766E2F5434417234414269495465307979416C30725963324531704D526E6B45334853556741306B676C734174316933525844426631484A536D3662676D4D486E637056513675543232433833377769703061334F4269657263315A33786952796431616B7069544D4A616132535A556C6B44302B6664394A68736531493671564E4F6C78374A682B5966506C2F3235656248675575446A31614A767851567658647A76374D78567638585735375879586A6737442F713031457A762F3752346243316D7A564F2B36376C6C705250557A78797872642B386D44662F5130767176706479476966636452386E395A6161526C6F7477444559495A70345541497355564677414173326C365161574B6E3941527852317A575077584F79746E36723643304C47577865302B754352785962566E5A395173506A4E34635662665A695374424C2F4A532B724656786A6D36325466774F6743365071797248674346383831584C654D736D3451507A5854764138746C5939314734435942386B4D4178746D4B7078386F454D556A4B76586C44554379542F37394970616133464C7971657166644F386F6971496F6971496F663444584671424574496D4966534D4F65765470556454303435314257324B32424F326673617A4E376F632F4C6671317A4F68773933576C6A6E6F764F426A62616B4C72716D316D372B2B546C5A4A352B577A5A44323763387235394E4D6E4F7354675244544352783033416A394C59673467573633674835434B7830446747624747616C304D4C6B4F6D797258594B354364636B4237417A7A4A5969774961696248794D7042485A666B51674C32695A4C313650483554364B4B5362626D536266342F715A3351332F7A3974386639566845674B4A3646536F2F34502F6C364179417042526777345165344342634D6F504D574E61674935426650444E5A6B52764F5264653941784D6374524C637673744A377A7574784D4C4C466E4D31506E4A2F4D7A7178526F304F43622B4B682F47616C394A6C755752626163766E6B6A564E313433344F34504976562B7665622F39357650735539344F5870396F47474A7362377A6E34325459553362446B325762624F6552396C756452614D51726631442B4A657430447076644C626C614B64795A49567272757442474E4B534433514C51786246656C414A792B6454364F54415850376B645A444E4D2B6A41676733546967563831437850425073702B724F5572694B6A53636C47642B4E686678395964722B766D764F314E61797662346F78683775374F2B57344A7467505A6D50726E5476507543625A686375535265444667334E536F5357734D4952346E35353232767838474F484C593641734D594B3474485A6A4E534E732B414B7270626F4C7778316C3742725A46326D686255564870346F715643305745615065362B724B694B4971694B49727931336C744155702B52584F676651576F334B35614B652B412F4E3758507274685464376C342F427A394E346C563563327A6A4836327431776252356551317779584B6139356C3477306A7A616E4B515041734B5A44734142416F4339374F55707945675A795849676779653441554E70716A55486D534736465955446B30577132414E3879756643453368425335304F734D6A705841514B694A516C347848532F6E566637652B676F6145426E766A6744534B63637A774447684E41426C6A384C6132734630714F44594F384F6E6B462B6536776264725769617447515A30427463743633447A38656661556A48376D495439752B616E427A6C556E356F336F6B3571566C6D304F41466D50643578715130364E484A316D5430424F2B3578416136722B4A4966416571313469585A5A48736847702B3850314354445954427755726A62484942445971796C486442576E724A47677568476C6D343479465232614D6B67316F6B2B44674E412F77374E6A4734676C386B4A6C6A4277477553595A2F305677753646483631362F754A62766478376257783966655349734979774F745553486B37494C5442396B4E7051484F4F6F746B3347366E613531485478547A6E754942694731536643587069324646684D74304E634C4D616970755965494D4A465654456257436E5879697951315A676F4D77416833446B466842416F786B4468447758765754314E392B67447A4330364C584F30566B4156494F347662475646555252465552546C662B6E317A594E6B3567492F2F39312B4F72503031384536762B67486E52356544736E7A734C694C6D396B487338645A312B6F486D37504D4438794F36476A424B6D313779546C75514437356C414B32735931377746484F63684E5977774E744135416F6C6D7458674265303034594450664855686F486F4A53664A366944637153496C38423758354E636C3664722B6F7130737157456A414149377741316E416F4539496C2F6B6778784E504476417373347975636A68623957583279743373456E334B6A4342666647377864303952573938304C622F357136645A31393871324F624776572F504C6939394862336530514459665332624151537856773867666F364A336B466545756373523443454E6D57506B436157477A4F424561494E504D3634434632424145447465344F6A69433779565337766142565A497832456D52627470454F33434E5050784A594A7174624A6F4C37584E666F2F4667356F37564C3639504274362F384F6E6E6E6C426D446273363539573547747A723139742F6262377053344A5A6C3472676F4C644E496C6136696A6567684B7650554F64375A5773626246726431316E62664B3664614C4C6A696469557459554C74515A62626C6966694A5042532F435433413847306C2B6341535A61754443446C43743165344C6E7371713844684C4E444E394B6C6239616C6A494867654354584F33644637766E583170735652564555525647557638687247314F773839566C47563846424750423841745064566442353871446F6954514C786672724A3841336A4C542F676367546C767350427130564A70627034414D424F6B4A75414142663565774248494146786C7347413155747133543951594743793935437351446556556243734B647A32556D3842343637545377474138427946445352435267683062686E31436A72344953587941426549704F66674E55414459437566686F516342444C736D52514A413877775951455A68314A34483777464451766175375A787863736A446B7862386C2F2B325352656D4846795576376E32677A34473350687732577A644A373237376555586851654F6870706648743074383066434676626B5A49634A52766D506646486A4563627675494E505A4C445941325A68747659484B7A4C66744173614C6543304E4D416850575173344A59666F4B774F7A634E52584246495961666B556449363643766D726F46706931516D65466B3366337158644A343039447953304F64587563503276786F774C6537657873336455566D53616C76354C6972742B4575484538347956726761587765556551704F72596162526964726B7A6C4F3739497677713754747345764D2B4E69766F7A363755654861694F524F446E624D34524F585347432B66454E72413351693156596453474D5755774154496349503542544F696A636830545778375A4D6D465933625137636C785279742F2B37644E58663945316273475549664876344A7261776F6971496F69714C385156356267464B6D5A2F6E6C7A6A4541394953696D643774793839792B766E652F744A33764C595A30316F6B767169666D6D30704337495871363244514A796B6B64596569434A523177544558704233536B706B4C456E3031514A3978534D532B37537A67416B76755147496C73674E674365312B52536B46386A6C67412B496863427763686B4A346A48494A762B517A6A2F334B723958343071764A7134562F2B5A35646958584D514949354A7032414B674674744841624B7062566F47734B30645147615337434B4954614537614F6E4541674367414C63526D70332F3233374D616347426B6F7A707477576D552F71353937786454796C2F787539747A34346A316A7332646C68724B44416B3557796B323632374C495138534E695575793135562B6B58655A644E706E4941764A49614677485636695837414F68476B48777145796A753245434162482B736B4543664532355A507750476F5537774238504C7A744865706C2F392B2B64592B753978536A677A7045744735644D76725734634F384276516F6550656D4C7133563933736B7571576C706C544D366469345135385532756C54436E4D5A3658625774644E4C67754D47556557486E3533663757517673306152697974644B2F7A33613274743058747164306A4E74452F7356486D717044755265396248787648674A6A4D34767A56674C39457541506E514263453467356F473446573343734B4275484458483647684F544571636D484847647353646F576362445A324C373936765639314F4C306B2F684832354C71506D313265336632386179707076336D436257573158366E2B6F375831637356525645555256475550397072433143712B31632F345645446C6755736658796F716261306F57756A6D76552B6E545A654468665374596F63657566386E6647504D7A71474A4E6753664A344E4D4F374F6D355A62312F794258464555554251712B6F5034694D45696E654A67495065665A424A4663544177684F4A70742B784B396A634357736B725641426641337241516E4851384C75584C2F78644250614150535953414B767371533048755A4E55773037515875707146773446335377713667364149553730347151683258366D5853664454356D766E7634667A687562732B336C4A7741305A395466456A644846313753764B41675332347A5654636E316E675732714F6952324C2F5A537A2F4C71724E3539596C3459735848527933782B5061684A752B38513444333076364B696B334C616E6C6B726A31392F4E544E6366706158505462727A73494D645970397463624C50416459584C59366633774C74306D576E756655524D4F58776E75387130436B45664238386F4E2F6A656E624C6E79365A3650766E7857756A6B5948326736664378394D4430727A4D7A433561582B634B6E676A4736704642354A64764E4A4A397666506242725443776657414C73773276554E73556D4E637166387169487931317A42364631304A7349543244742F703745524E3849756A446B4F505034326772542B716D6F576E486B664967677232694B76554141356C694A784449594F6B4B346773576969675163544A564E775A7746434D73675867347A48494D7339594D30737770356B5454704B572B3665585439566E7278345158686865307468792F434D762B314C5A5846455652464556522F70662B703747426631764A4F68796B643039664275427730654542474937664E643339304252512F73374F334A2F6B796236474D546357584338567035506E54654E4D575059434538517030517177595362724E3657542F384D2B2F2B4A7866385A35456F456A554A6F633252584544466C5052494263674E6B77444F517358652F433261413343364D2B4833512F344B62466953564E366A644E714B626C79326D6C7672523069556870656D58347064435870526C55392F76367430746C2F2B763162765972756746672F385251457779755A364D76464F627338626D34326E646C39345076327258656E374F2F315A4775636D39575A50615A6C2F6C514B2B504E6432705949584A512B2B75746C6F6E426B5273364A6A64596C6A65386A7675626D77337A73354B4B553765324B636D6D755242436950394872376C372B2F61704A336C414E48766C4E2F71646F72746F344C6A6339374C4455767432626B734D502B7476364C6659395A5272395948366A77436850365633423741357971634173706B5943594354754161416D54304134686B314162546C525A45412B6E5036496B4261544E5A457379633143304A4E735A6E655255643161335266467A314B3861595066555258613533417343412F76782F2F706136724B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4971694B4D6F6635373841724B62444B73537871574541414141415355564F524B35435949493D, 'image/png', '2014-03-14 00:52:40', 1);
/*!40000 ALTER TABLE `settings_org` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_payment_method
DROP TABLE IF EXISTS `settings_payment_method`;
CREATE TABLE IF NOT EXISTS `settings_payment_method` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_payment_method: ~8 rows (approximately)
DELETE FROM `settings_payment_method`;
/*!40000 ALTER TABLE `settings_payment_method` DISABLE KEYS */;
INSERT INTO `settings_payment_method` (`id`, `name`, `description`, `date_created`, `created_by`) VALUES
	(1, 'CASH', 'Cash Payment', '2014-05-11 00:06:38', 1),
	(2, 'MPESA', 'Payment via MPESA', '2014-05-11 00:06:52', 1),
	(3, 'PAYPAL', 'Payment via Paypal', '2014-05-11 00:07:11', 1),
	(4, 'WESTERN UNION', '', '2014-05-11 00:07:23', 1),
	(5, 'VISA', '', '2014-05-11 00:07:36', 1),
	(6, 'AIRTEL MONEY', '', '2014-05-11 00:08:03', 1),
	(7, 'CHEQUE', 'Cheque payments', '2014-09-25 10:58:19', 1),
	(8, 'WIRE_TRANSFER', 'Wire transfer', '2014-09-25 10:58:47', 1);
/*!40000 ALTER TABLE `settings_payment_method` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_timezone
DROP TABLE IF EXISTS `settings_timezone`;
CREATE TABLE IF NOT EXISTS `settings_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=462 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table bremak_manager.settings_timezone: ~460 rows (approximately)
DELETE FROM `settings_timezone`;
/*!40000 ALTER TABLE `settings_timezone` DISABLE KEYS */;
INSERT INTO `settings_timezone` (`id`, `name`) VALUES
	(2, 'Africa/Abidjan'),
	(3, 'Africa/Accra'),
	(4, 'Africa/Addis_Ababa'),
	(5, 'Africa/Algiers'),
	(6, 'Africa/Asmara'),
	(7, 'Africa/Asmera'),
	(8, 'Africa/Bamako'),
	(9, 'Africa/Bangui'),
	(10, 'Africa/Banjul'),
	(11, 'Africa/Bissau'),
	(12, 'Africa/Blantyre'),
	(13, 'Africa/Brazzaville'),
	(14, 'Africa/Bujumbura'),
	(15, 'Africa/Cairo'),
	(16, 'Africa/Casablanca'),
	(17, 'Africa/Ceuta'),
	(18, 'Africa/Conakry'),
	(19, 'Africa/Dakar'),
	(20, 'Africa/Dar_es_Salaam'),
	(21, 'Africa/Djibouti'),
	(22, 'Africa/Douala'),
	(23, 'Africa/El_Aaiun'),
	(24, 'Africa/Freetown'),
	(25, 'Africa/Gaborone'),
	(26, 'Africa/Harare'),
	(27, 'Africa/Johannesburg'),
	(28, 'Africa/Kampala'),
	(29, 'Africa/Khartoum'),
	(30, 'Africa/Kigali'),
	(31, 'Africa/Kinshasa'),
	(32, 'Africa/Lagos'),
	(33, 'Africa/Libreville'),
	(34, 'Africa/Lome'),
	(35, 'Africa/Luanda'),
	(36, 'Africa/Lubumbashi'),
	(37, 'Africa/Lusaka'),
	(38, 'Africa/Malabo'),
	(39, 'Africa/Maputo'),
	(40, 'Africa/Maseru'),
	(41, 'Africa/Mbabane'),
	(42, 'Africa/Mogadishu'),
	(43, 'Africa/Monrovia'),
	(44, 'Africa/Nairobi'),
	(45, 'Africa/Ndjamena'),
	(46, 'Africa/Niamey'),
	(47, 'Africa/Nouakchott'),
	(48, 'Africa/Ouagadougou'),
	(49, 'Africa/Porto-Novo'),
	(50, 'Africa/Sao_Tome'),
	(51, 'Africa/Timbuktu'),
	(52, 'Africa/Tripoli'),
	(53, 'Africa/Tunis'),
	(54, 'Africa/Windhoek'),
	(55, 'America/Adak'),
	(56, 'America/Anchorage'),
	(57, 'America/Anguilla'),
	(58, 'America/Antigua'),
	(59, 'America/Araguaina'),
	(60, 'America/Argentina/Buenos_Aires'),
	(61, 'America/Argentina/Catamarca'),
	(62, 'America/Argentina/ComodRivadavia'),
	(63, 'America/Argentina/Cordoba'),
	(64, 'America/Argentina/Jujuy'),
	(65, 'America/Argentina/La_Rioja'),
	(66, 'America/Argentina/Mendoza'),
	(67, 'America/Argentina/Rio_Gallegos'),
	(68, 'America/Argentina/Salta'),
	(69, 'America/Argentina/San_Juan'),
	(70, 'America/Argentina/San_Luis'),
	(71, 'America/Argentina/Tucuman'),
	(72, 'America/Argentina/Ushuaia'),
	(73, 'America/Aruba'),
	(74, 'America/Asuncion'),
	(75, 'America/Atikokan'),
	(76, 'America/Atka'),
	(77, 'America/Bahia'),
	(78, 'America/Bahia_Banderas'),
	(79, 'America/Barbados'),
	(80, 'America/Belem'),
	(81, 'America/Belize'),
	(82, 'America/Blanc-Sablon'),
	(83, 'America/Boa_Vista'),
	(84, 'America/Bogota'),
	(85, 'America/Boise'),
	(86, 'America/Buenos_Aires'),
	(87, 'America/Cambridge_Bay'),
	(88, 'America/Campo_Grande'),
	(89, 'America/Cancun'),
	(90, 'America/Caracas'),
	(91, 'America/Catamarca'),
	(92, 'America/Cayenne'),
	(93, 'America/Cayman'),
	(94, 'America/Chicago'),
	(95, 'America/Chihuahua'),
	(96, 'America/Coral_Harbour'),
	(97, 'America/Cordoba'),
	(98, 'America/Costa_Rica'),
	(99, 'America/Cuiaba'),
	(100, 'America/Curacao'),
	(101, 'America/Danmarkshavn'),
	(102, 'America/Dawson'),
	(103, 'America/Dawson_Creek'),
	(104, 'America/Denver'),
	(105, 'America/Detroit'),
	(106, 'America/Dominica'),
	(107, 'America/Edmonton'),
	(108, 'America/Eirunepe'),
	(109, 'America/El_Salvador'),
	(110, 'America/Ensenada'),
	(111, 'America/Fortaleza'),
	(112, 'America/Fort_Wayne'),
	(113, 'America/Glace_Bay'),
	(114, 'America/Godthab'),
	(115, 'America/Goose_Bay'),
	(116, 'America/Grand_Turk'),
	(117, 'America/Grenada'),
	(118, 'America/Guadeloupe'),
	(119, 'America/Guatemala'),
	(120, 'America/Guayaquil'),
	(121, 'America/Guyana'),
	(122, 'America/Halifax'),
	(123, 'America/Havana'),
	(124, 'America/Hermosillo'),
	(125, 'America/Indiana/Indianapolis'),
	(126, 'America/Indiana/Knox'),
	(127, 'America/Indiana/Marengo'),
	(128, 'America/Indiana/Petersburg'),
	(129, 'America/Indianapolis'),
	(130, 'America/Indiana/Tell_City'),
	(131, 'America/Indiana/Vevay'),
	(132, 'America/Indiana/Vincennes'),
	(133, 'America/Indiana/Winamac'),
	(134, 'America/Inuvik'),
	(135, 'America/Iqaluit'),
	(136, 'America/Jamaica'),
	(137, 'America/Jujuy'),
	(138, 'America/Juneau'),
	(139, 'America/Kentucky/Louisville'),
	(140, 'America/Kentucky/Monticello'),
	(141, 'America/Knox_IN'),
	(142, 'America/La_Paz'),
	(143, 'America/Lima'),
	(144, 'America/Los_Angeles'),
	(145, 'America/Louisville'),
	(146, 'America/Maceio'),
	(147, 'America/Managua'),
	(148, 'America/Manaus'),
	(149, 'America/Marigot'),
	(150, 'America/Martinique'),
	(151, 'America/Matamoros'),
	(152, 'America/Mazatlan'),
	(153, 'America/Mendoza'),
	(154, 'America/Menominee'),
	(155, 'America/Merida'),
	(156, 'America/Metlakatla'),
	(157, 'America/Mexico_City'),
	(158, 'America/Miquelon'),
	(159, 'America/Moncton'),
	(160, 'America/Monterrey'),
	(161, 'America/Montevideo'),
	(162, 'America/Montreal'),
	(163, 'America/Montserrat'),
	(164, 'America/Nassau'),
	(165, 'America/New_York'),
	(166, 'America/Nipigon'),
	(167, 'America/Nome'),
	(168, 'America/Noronha'),
	(169, 'America/North_Dakota/Beulah'),
	(170, 'America/North_Dakota/Center'),
	(171, 'America/North_Dakota/New_Salem'),
	(172, 'America/Ojinaga'),
	(173, 'America/Panama'),
	(174, 'America/Pangnirtung'),
	(175, 'America/Paramaribo'),
	(176, 'America/Phoenix'),
	(177, 'America/Port-au-Prince'),
	(178, 'America/Porto_Acre'),
	(179, 'America/Port_of_Spain'),
	(180, 'America/Porto_Velho'),
	(181, 'America/Puerto_Rico'),
	(182, 'America/Rainy_River'),
	(183, 'America/Rankin_Inlet'),
	(184, 'America/Recife'),
	(185, 'America/Regina'),
	(186, 'America/Resolute'),
	(187, 'America/Rio_Branco'),
	(188, 'America/Rosario'),
	(189, 'America/Santa_Isabel'),
	(190, 'America/Santarem'),
	(191, 'America/Santiago'),
	(192, 'America/Santo_Domingo'),
	(193, 'America/Sao_Paulo'),
	(194, 'America/Scoresbysund'),
	(195, 'America/Shiprock'),
	(196, 'America/Sitka'),
	(197, 'America/St_Barthelemy'),
	(198, 'America/St_Johns'),
	(199, 'America/St_Kitts'),
	(200, 'America/St_Lucia'),
	(201, 'America/St_Thomas'),
	(202, 'America/St_Vincent'),
	(203, 'America/Swift_Current'),
	(204, 'America/Tegucigalpa'),
	(205, 'America/Thule'),
	(206, 'America/Thunder_Bay'),
	(207, 'America/Tijuana'),
	(208, 'America/Toronto'),
	(209, 'America/Tortola'),
	(210, 'America/Vancouver'),
	(211, 'America/Virgin'),
	(212, 'America/Whitehorse'),
	(213, 'America/Winnipeg'),
	(214, 'America/Yakutat'),
	(215, 'America/Yellowknife'),
	(216, 'Antarctica/Casey'),
	(217, 'Antarctica/Davis'),
	(218, 'Antarctica/DumontDUrville'),
	(219, 'Antarctica/Macquarie'),
	(220, 'Antarctica/Mawson'),
	(221, 'Antarctica/McMurdo'),
	(222, 'Antarctica/Palmer'),
	(223, 'Antarctica/Rothera'),
	(224, 'Antarctica/South_Pole'),
	(225, 'Antarctica/Syowa'),
	(226, 'Antarctica/Vostok'),
	(227, 'Arctic/Longyearbyen'),
	(228, 'Asia/Aden'),
	(229, 'Asia/Almaty'),
	(230, 'Asia/Amman'),
	(231, 'Asia/Anadyr'),
	(232, 'Asia/Aqtau'),
	(233, 'Asia/Aqtobe'),
	(234, 'Asia/Ashgabat'),
	(235, 'Asia/Ashkhabad'),
	(236, 'Asia/Baghdad'),
	(237, 'Asia/Bahrain'),
	(238, 'Asia/Baku'),
	(239, 'Asia/Bangkok'),
	(240, 'Asia/Beirut'),
	(241, 'Asia/Bishkek'),
	(242, 'Asia/Brunei'),
	(243, 'Asia/Calcutta'),
	(244, 'Asia/Choibalsan'),
	(245, 'Asia/Chongqing'),
	(246, 'Asia/Chungking'),
	(247, 'Asia/Colombo'),
	(248, 'Asia/Dacca'),
	(249, 'Asia/Damascus'),
	(250, 'Asia/Dhaka'),
	(251, 'Asia/Dili'),
	(252, 'Asia/Dubai'),
	(253, 'Asia/Dushanbe'),
	(254, 'Asia/Gaza'),
	(255, 'Asia/Harbin'),
	(256, 'Asia/Ho_Chi_Minh'),
	(257, 'Asia/Hong_Kong'),
	(258, 'Asia/Hovd'),
	(259, 'Asia/Irkutsk'),
	(260, 'Asia/Istanbul'),
	(261, 'Asia/Jakarta'),
	(262, 'Asia/Jayapura'),
	(263, 'Asia/Jerusalem'),
	(264, 'Asia/Kabul'),
	(265, 'Asia/Kamchatka'),
	(266, 'Asia/Karachi'),
	(267, 'Asia/Kashgar'),
	(268, 'Asia/Kathmandu'),
	(269, 'Asia/Katmandu'),
	(270, 'Asia/Kolkata'),
	(271, 'Asia/Krasnoyarsk'),
	(272, 'Asia/Kuala_Lumpur'),
	(273, 'Asia/Kuching'),
	(274, 'Asia/Kuwait'),
	(275, 'Asia/Macao'),
	(276, 'Asia/Macau'),
	(277, 'Asia/Magadan'),
	(278, 'Asia/Makassar'),
	(279, 'Asia/Manila'),
	(280, 'Asia/Muscat'),
	(281, 'Asia/Nicosia'),
	(282, 'Asia/Novokuznetsk'),
	(283, 'Asia/Novosibirsk'),
	(284, 'Asia/Omsk'),
	(285, 'Asia/Oral'),
	(286, 'Asia/Phnom_Penh'),
	(287, 'Asia/Pontianak'),
	(288, 'Asia/Pyongyang'),
	(289, 'Asia/Qatar'),
	(290, 'Asia/Qyzylorda'),
	(291, 'Asia/Rangoon'),
	(292, 'Asia/Riyadh'),
	(293, 'Asia/Saigon'),
	(294, 'Asia/Sakhalin'),
	(295, 'Asia/Samarkand'),
	(296, 'Asia/Seoul'),
	(297, 'Asia/Shanghai'),
	(298, 'Asia/Singapore'),
	(299, 'Asia/Taipei'),
	(300, 'Asia/Tashkent'),
	(301, 'Asia/Tbilisi'),
	(302, 'Asia/Tehran'),
	(303, 'Asia/Tel_Aviv'),
	(304, 'Asia/Thimbu'),
	(305, 'Asia/Thimphu'),
	(306, 'Asia/Tokyo'),
	(307, 'Asia/Ujung_Pandang'),
	(308, 'Asia/Ulaanbaatar'),
	(309, 'Asia/Ulan_Bator'),
	(310, 'Asia/Urumqi'),
	(311, 'Asia/Vientiane'),
	(312, 'Asia/Vladivostok'),
	(313, 'Asia/Yakutsk'),
	(314, 'Asia/Yekaterinburg'),
	(315, 'Asia/Yerevan'),
	(316, 'Atlantic/Azores'),
	(317, 'Atlantic/Bermuda'),
	(318, 'Atlantic/Canary'),
	(319, 'Atlantic/Cape_Verde'),
	(320, 'Atlantic/Faeroe'),
	(321, 'Atlantic/Faroe'),
	(322, 'Atlantic/Jan_Mayen'),
	(323, 'Atlantic/Madeira'),
	(324, 'Atlantic/Reykjavik'),
	(325, 'Atlantic/South_Georgia'),
	(326, 'Atlantic/Stanley'),
	(327, 'Atlantic/St_Helena'),
	(328, 'Australia/ACT'),
	(329, 'Australia/Adelaide'),
	(330, 'Australia/Brisbane'),
	(331, 'Australia/Broken_Hill'),
	(332, 'Australia/Canberra'),
	(333, 'Australia/Currie'),
	(334, 'Australia/Darwin'),
	(335, 'Australia/Eucla'),
	(336, 'Australia/Hobart'),
	(337, 'Australia/LHI'),
	(338, 'Australia/Lindeman'),
	(339, 'Australia/Lord_Howe'),
	(340, 'Australia/Melbourne'),
	(341, 'Australia/North'),
	(342, 'Australia/NSW'),
	(343, 'Australia/Perth'),
	(344, 'Australia/Queensland'),
	(345, 'Australia/South'),
	(346, 'Australia/Sydney'),
	(347, 'Australia/Tasmania'),
	(348, 'Australia/Victoria'),
	(349, 'Australia/West'),
	(350, 'Australia/Yancowinna'),
	(351, 'Europe/Amsterdam'),
	(352, 'Europe/Andorra'),
	(353, 'Europe/Athens'),
	(354, 'Europe/Belfast'),
	(355, 'Europe/Belgrade'),
	(356, 'Europe/Berlin'),
	(357, 'Europe/Bratislava'),
	(358, 'Europe/Brussels'),
	(359, 'Europe/Bucharest'),
	(360, 'Europe/Budapest'),
	(361, 'Europe/Chisinau'),
	(362, 'Europe/Copenhagen'),
	(363, 'Europe/Dublin'),
	(364, 'Europe/Gibraltar'),
	(365, 'Europe/Guernsey'),
	(366, 'Europe/Helsinki'),
	(367, 'Europe/Isle_of_Man'),
	(368, 'Europe/Istanbul'),
	(369, 'Europe/Jersey'),
	(370, 'Europe/Kaliningrad'),
	(371, 'Europe/Kiev'),
	(372, 'Europe/Lisbon'),
	(373, 'Europe/Ljubljana'),
	(374, 'Europe/London'),
	(375, 'Europe/Luxembourg'),
	(376, 'Europe/Madrid'),
	(377, 'Europe/Malta'),
	(378, 'Europe/Mariehamn'),
	(379, 'Europe/Minsk'),
	(380, 'Europe/Monaco'),
	(381, 'Europe/Moscow'),
	(382, 'Europe/Nicosia'),
	(383, 'Europe/Oslo'),
	(384, 'Europe/Paris'),
	(385, 'Europe/Podgorica'),
	(386, 'Europe/Prague'),
	(387, 'Europe/Riga'),
	(388, 'Europe/Rome'),
	(389, 'Europe/Samara'),
	(390, 'Europe/San_Marino'),
	(391, 'Europe/Sarajevo'),
	(392, 'Europe/Simferopol'),
	(393, 'Europe/Skopje'),
	(394, 'Europe/Sofia'),
	(395, 'Europe/Stockholm'),
	(396, 'Europe/Tallinn'),
	(397, 'Europe/Tirane'),
	(398, 'Europe/Tiraspol'),
	(399, 'Europe/Uzhgorod'),
	(400, 'Europe/Vaduz'),
	(401, 'Europe/Vatican'),
	(402, 'Europe/Vienna'),
	(403, 'Europe/Vilnius'),
	(404, 'Europe/Volgograd'),
	(405, 'Europe/Warsaw'),
	(406, 'Europe/Zagreb'),
	(407, 'Europe/Zaporozhye'),
	(408, 'Europe/Zurich'),
	(409, 'Indian/Antananarivo'),
	(410, 'Indian/Chagos'),
	(411, 'Indian/Christmas'),
	(412, 'Indian/Cocos'),
	(413, 'Indian/Comoro'),
	(414, 'Indian/Kerguelen'),
	(415, 'Indian/Mahe'),
	(416, 'Indian/Maldives'),
	(417, 'Indian/Mauritius'),
	(418, 'Indian/Mayotte'),
	(419, 'Indian/Reunion'),
	(420, 'Pacific/Apia'),
	(421, 'Pacific/Auckland'),
	(422, 'Pacific/Chatham'),
	(423, 'Pacific/Chuuk'),
	(424, 'Pacific/Easter'),
	(425, 'Pacific/Efate'),
	(426, 'Pacific/Enderbury'),
	(427, 'Pacific/Fakaofo'),
	(428, 'Pacific/Fiji'),
	(429, 'Pacific/Funafuti'),
	(430, 'Pacific/Galapagos'),
	(431, 'Pacific/Gambier'),
	(432, 'Pacific/Guadalcanal'),
	(433, 'Pacific/Guam'),
	(434, 'Pacific/Honolulu'),
	(435, 'Pacific/Johnston'),
	(436, 'Pacific/Kiritimati'),
	(437, 'Pacific/Kosrae'),
	(438, 'Pacific/Kwajalein'),
	(439, 'Pacific/Majuro'),
	(440, 'Pacific/Marquesas'),
	(441, 'Pacific/Midway'),
	(442, 'Pacific/Nauru'),
	(443, 'Pacific/Niue'),
	(444, 'Pacific/Norfolk'),
	(445, 'Pacific/Noumea'),
	(446, 'Pacific/Pago_Pago'),
	(447, 'Pacific/Palau'),
	(448, 'Pacific/Pitcairn'),
	(449, 'Pacific/Pohnpei'),
	(450, 'Pacific/Ponape'),
	(451, 'Pacific/Port_Moresby'),
	(452, 'Pacific/Rarotonga'),
	(453, 'Pacific/Saipan'),
	(454, 'Pacific/Samoa'),
	(455, 'Pacific/Tahiti'),
	(456, 'Pacific/Tarawa'),
	(457, 'Pacific/Tongatapu'),
	(458, 'Pacific/Truk'),
	(459, 'Pacific/Wake'),
	(460, 'Pacific/Wallis'),
	(461, 'Pacific/Yap');
/*!40000 ALTER TABLE `settings_timezone` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.settings_uom
DROP TABLE IF EXISTS `settings_uom`;
CREATE TABLE IF NOT EXISTS `settings_uom` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `unit` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit` (`unit`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.settings_uom: ~10 rows (approximately)
DELETE FROM `settings_uom`;
/*!40000 ALTER TABLE `settings_uom` DISABLE KEYS */;
INSERT INTO `settings_uom` (`id`, `unit`, `description`, `date_created`, `created_by`) VALUES
	(1, 'kg', 'kilogram (kg)', '2014-05-11 01:59:58', 1),
	(2, 'g', 'gram (g)', '2014-05-11 02:00:11', 1),
	(3, 'm', 'metre (m)', '2014-05-11 02:00:34', 1),
	(4, 'km', 'kilometer (km)', '2014-05-11 02:04:34', 1),
	(5, 'cm', 'centimeter (cm)', '2014-05-11 02:07:56', 1),
	(6, 'l', 'liter (l)', '2014-05-11 02:08:38', 1),
	(7, 'ml', 'milliliter (ml)', '2014-05-11 02:09:08', 1),
	(8, 'ea', 'Each (ea)', '2014-05-11 02:10:20', 1),
	(9, '%', 'Percent(%)', '2014-08-29 11:39:19', 1),
	(10, 'people', 'People', '2014-08-29 11:39:41', 1);
/*!40000 ALTER TABLE `settings_uom` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.status_tracker
DROP TABLE IF EXISTS `status_tracker`;
CREATE TABLE IF NOT EXISTS `status_tracker` (
  `id` double DEFAULT NULL,
  `path` tinyint(1) DEFAULT NULL,
  `item_id` double DEFAULT NULL,
  `resource_id` varchar(384) DEFAULT NULL,
  `comment` varchar(768) DEFAULT NULL,
  `latest` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.status_tracker: ~12 rows (approximately)
DELETE FROM `status_tracker`;
/*!40000 ALTER TABLE `status_tracker` DISABLE KEYS */;
INSERT INTO `status_tracker` (`id`, `path`, `item_id`, `resource_id`, `comment`, `latest`, `date_created`, `created_by`) VALUES
	(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by Lucy Wambui Mbugua', 0, '2015-01-29 11:13:17', 7),
	(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by Lucy Wambui Mbugua', 0, '2015-01-29 11:13:17', 7),
	(NULL, 0, 5, 'REQUISITION', 'Payment of  was paid by FelSoft Systems Ltd', 0, '2015-01-30 16:39:50', 1),
	(NULL, 0, 5, 'REQUISITION', 'Requisition #5 was fully paid by FelSoft Systems Ltd', 0, '2015-01-30 16:41:15', 1),
	(NULL, 0, 5, 'REQUISITION', 'Payment of 10000 was paid by FelSoft Systems Ltd', 1, '2015-01-30 16:41:15', 1),
	(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by FelSoft Systems Ltd', 0, '2015-01-31 13:54:19', 1),
	(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by FelSoft Systems Ltd', 0, '2015-01-31 13:54:19', 1),
	(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by FelSoft Systems Ltd', 0, '2015-01-31 13:58:51', 1),
	(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by FelSoft Systems Ltd', 0, '2015-01-31 13:58:51', 1),
	(NULL, 0, 1, 'REQUISITION', 'Payment of 6000 was paid by FelSoft Systems Ltd', 0, '2015-01-31 14:01:08', 1),
	(NULL, 0, 1, 'REQUISITION', 'Requisition #1 was fully paid by Martin Mwangi Mwaniki', 0, '2015-02-02 12:44:06', 16),
	(NULL, 0, 1, 'REQUISITION', 'Payment of 14400.00 was paid by Martin Mwangi Mwaniki', 1, '2015-02-02 12:44:06', 16);
/*!40000 ALTER TABLE `status_tracker` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.tbl_documents
DROP TABLE IF EXISTS `tbl_documents`;
CREATE TABLE IF NOT EXISTS `tbl_documents` (
  `DocID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) NOT NULL,
  `Date_departure` datetime NOT NULL,
  `letter_date` date NOT NULL,
  `letter_ref_no` varchar(100) NOT NULL,
  `Salutation` varchar(20) NOT NULL,
  `Purpose` varchar(50) NOT NULL,
  `CreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Embassy_name` varchar(100) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `ministry_name` varchar(100) NOT NULL,
  `School_name` varchar(100) NOT NULL,
  `Degree_type` varchar(100) NOT NULL,
  `Academic_year` varchar(100) NOT NULL,
  `Applicant_name` varchar(100) NOT NULL,
  `Passport_no` varchar(100) NOT NULL,
  `Occupation` varchar(100) NOT NULL,
  `Destination` varchar(100) NOT NULL,
  `Author` int(11) NOT NULL,
  `Doc_template` int(11) DEFAULT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Approver` int(11) DEFAULT NULL,
  `PrintCount` int(11) DEFAULT '0',
  `Reference` varchar(25) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DocID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.tbl_documents: ~4 rows (approximately)
DELETE FROM `tbl_documents`;
/*!40000 ALTER TABLE `tbl_documents` DISABLE KEYS */;
INSERT INTO `tbl_documents` (`DocID`, `Title`, `Date_departure`, `letter_date`, `letter_ref_no`, `Salutation`, `Purpose`, `CreateDate`, `Embassy_name`, `Type`, `ministry_name`, `School_name`, `Degree_type`, `Academic_year`, `Applicant_name`, `Passport_no`, `Occupation`, `Destination`, `Author`, `Doc_template`, `Status`, `Approver`, `PrintCount`, `Reference`, `Subject`) VALUES
	(1, 'N/A', '0000-00-00 00:00:00', '0000-00-00', '', 'N/A', 'N/A', '2016-09-22 14:18:40', 'N/A', 'N/A', '', '', '', '', 'N/A', 'N/A', 'N/A', 'N/A', 28, 5, 'Saved', NULL, 0, '2016/09/18/0041', NULL),
	(2, 'N/A', '2016-09-30 00:00:00', '0000-00-00', '', 'Imam', '<p>\r\n	To visit the annual Hajj Mubarak at Saudi Ar', '2016-09-22 14:18:53', 'Somalia Embassy', 'Entry', '', '', '', '', 'Yussuf Hajj', '9039398445', 'Teacher', 'Hajj', 28, 4, 'Saved', NULL, 0, '2016/09/18/004', NULL),
	(3, 'N/A', '0000-00-00 00:00:00', '0000-00-00', '', 'N/A', 'N/A', '2016-09-22 14:18:48', 'N/A', 'N/A', '', '', '', '', 'N/A', 'N/A', 'N/A', 'N/A', 28, 5, 'Saved', NULL, 0, '2016/09/18/0010', NULL),
	(6, '', '0000-00-00 00:00:00', '0000-00-00', '', 'Mr', '', '2016-09-22 14:18:57', 'South sudan Embassy', '', '', 'Buba University', 'BSc in Medicine', '2017', 'Janet Yussuf', '18173930', '', 'Juba', 28, 7, 'Saved', NULL, 0, '2016/09/18/0044', NULL);
/*!40000 ALTER TABLE `tbl_documents` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.tbl_immigrants
DROP TABLE IF EXISTS `tbl_immigrants`;
CREATE TABLE IF NOT EXISTS `tbl_immigrants` (
  `imm_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '0',
  `td_no` varchar(100) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `doc_id` int(11) NOT NULL,
  PRIMARY KEY (`imm_id`),
  KEY `FK_tbl_immigrants_tbl_documents` (`doc_id`),
  CONSTRAINT `FK_tbl_immigrants_tbl_documents` FOREIGN KEY (`doc_id`) REFERENCES `tbl_documents` (`DocID`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table bremak_manager.tbl_immigrants: ~8 rows (approximately)
DELETE FROM `tbl_immigrants`;
/*!40000 ALTER TABLE `tbl_immigrants` DISABLE KEYS */;
INSERT INTO `tbl_immigrants` (`imm_id`, `name`, `td_no`, `date`, `doc_id`) VALUES
	(1, 'Makau', 'ESR/001/016', '2016-09-21 10:54:37', 1),
	(2, 'martin Muriithi', 'ESR/002/016', '2016-09-21 10:54:37', 1),
	(3, 'Mathew Juma', 'ESR/003/016', '2016-09-21 10:54:37', 1),
	(4, 'Allan Koskei', 'ESR/004/016', '2016-09-21 10:54:37', 1),
	(5, 'Reagan Inganji', 'ESR/005/016', '2016-09-21 10:54:37', 1),
	(18, 'Abubakar Yussuf', 'ESR/00001', '2016-09-22 10:35:22', 3),
	(19, 'Allan Koskei', 'ESR/00002', '2016-09-22 10:35:22', 3),
	(20, 'Fatuma Fugicha', 'ESR/00003', '2016-09-22 10:35:22', 3);
/*!40000 ALTER TABLE `tbl_immigrants` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.tbl_migration
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE IF NOT EXISTS `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.tbl_migration: ~4 rows (approximately)
DELETE FROM `tbl_migration`;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1418723772),
	('m141216_095436_create_table_delivery_reject', 1418723932),
	('m141216_100109_create_notifications_core', 1418724110),
	('m141216_100446_create_notifications_core', 1418724311);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_level` (`user_level`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.users: ~1 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `email`, `status`, `timezone`, `password`, `salt`, `password_reset_code`, `password_reset_date`, `password_reset_request_date`, `activation_code`, `user_level`, `role_id`, `date_created`, `created_by`, `last_modified`, `last_modified_by`, `last_login`) VALUES
	(28, 'jacjimus', 'jacjimus@gmail.com', 'Active', 'Africa/Nairobi', '735a2c577ed4a3d1bbfb0952f6537704434d711276d2ed2068e7a02499fac15c', '735a2c577ed4a3d1bbfb0952f6537704', '6e8dc770ff0265c25772a11559e96b02', NULL, '2016-07-03 06:18:08', NULL, 'ENGINEER', 13, '2016-01-30 09:15:44', 1, '2016-09-30 07:30:10', 28, '2016-10-04 13:48:07');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.users_copy
DROP TABLE IF EXISTS `users_copy`;
CREATE TABLE IF NOT EXISTS `users_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(128) NOT NULL,
  `status` enum('Pending','Active','Blocked') NOT NULL DEFAULT 'Pending',
  `timezone` varchar(60) DEFAULT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL,
  `password_reset_code` varchar(128) DEFAULT NULL,
  `password_reset_date` timestamp NULL DEFAULT NULL,
  `password_reset_request_date` timestamp NULL DEFAULT NULL,
  `activation_code` varchar(128) DEFAULT NULL,
  `user_level` varchar(30) NOT NULL,
  `role_id` int(11) unsigned DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) unsigned DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `user_level` (`user_level`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.users_copy: ~14 rows (approximately)
DELETE FROM `users_copy`;
/*!40000 ALTER TABLE `users_copy` DISABLE KEYS */;
INSERT INTO `users_copy` (`id`, `username`, `email`, `status`, `timezone`, `password`, `salt`, `password_reset_code`, `password_reset_date`, `password_reset_request_date`, `activation_code`, `user_level`, `role_id`, `date_created`, `created_by`, `last_modified`, `last_modified_by`, `last_login`) VALUES
	(1, 'mconyango', 'mconyango@gmail.com', 'Active', 'Africa/Nairobi', 'werwsa453dsss5648bd8a859690b9e779633714ee5ff072', 'werwsa453dsss56', NULL, '2014-03-07 23:41:00', '2014-03-07 23:41:00', NULL, 'ENGINEER', NULL, '2014-02-06 00:56:19', NULL, '2014-03-07 23:41:00', 1, '2014-09-02 15:45:43'),
	(2, 'felixaduol', 'felix@felsoftsystems.com', 'Active', 'Africa/Nairobi', '517f494cbafaa4430b2492661fd87aa45f4dcc3b5aa765d61d8327deb882cf99', '517f494cbafaa4430b2492661fd87aa4', NULL, '2014-03-07 23:42:14', '2014-03-07 23:42:14', NULL, 'ENGINEER', NULL, '2014-02-07 17:12:33', NULL, '2014-03-07 23:42:14', NULL, '2014-09-03 13:19:08'),
	(3, 'owango', 'felixaduol@yahoo.com', 'Active', 'Africa/Nairobi', '517f494cbafaa4430b2492661fd87aa45f4dcc3b5aa765d61d8327deb882cf99', '517f494cbafaa4430b2492661fd87aa4', NULL, '2014-03-07 23:42:26', '2014-03-07 23:42:26', NULL, 'ADMIN', 1, '2014-02-21 15:33:33', 1, '2014-06-03 20:13:16', 1, '2014-09-04 08:58:23'),
	(4, 'ewachira', 'ewachira@lifekills.or.ke', 'Active', 'Africa/Nairobi', '82fe5099459bc706fb8f00ad7a3259dd5f4dcc3b5aa765d61d8327deb882cf99', '82fe5099459bc706fb8f00ad7a3259dd', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-03 16:46:31', 3, NULL, NULL, '2014-09-03 16:57:22'),
	(5, 'kabucho', 'kabucho@lifeskills.or.ke', 'Active', 'Africa/Nairobi', 'e4eedb2e8cc77d239d5b87f51855b2025f4dcc3b5aa765d61d8327deb882cf99', 'e4eedb2e8cc77d239d5b87f51855b202', NULL, NULL, NULL, NULL, 'ADMIN', 2, '2014-09-04 09:25:50', 3, NULL, NULL, NULL),
	(7, 'lwambui', 'lwambui@lifeskills.or.ke', 'Active', 'Africa/Nairobi', 'da4cbb816b4911b888ef1508a34d49ab5f4dcc3b5aa765d61d8327deb882cf99', 'da4cbb816b4911b888ef1508a34d49ab', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 09:30:46', 3, NULL, NULL, NULL),
	(8, 'patrick', 'patrick@lifeskills.or.ke', 'Active', 'Africa/Nairobi', '41e01b4a196c7220c4bd9c60a59109c45f4dcc3b5aa765d61d8327deb882cf99', '41e01b4a196c7220c4bd9c60a59109c4', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 09:34:51', 3, NULL, NULL, NULL),
	(9, 'mary', 'mary@lifeskills.or.ke', 'Active', 'Africa/Nairobi', '82a4be5f3088e3e76711452d245dab435f4dcc3b5aa765d61d8327deb882cf99', '82a4be5f3088e3e76711452d245dab43', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 09:38:44', 3, NULL, NULL, NULL),
	(10, 'george', 'george@lifeskills.or.ke', 'Active', 'Africa/Nairobi', 'a2f89cd13a139abfdcd87d3de800eb0e5f4dcc3b5aa765d61d8327deb882cf99', 'a2f89cd13a139abfdcd87d3de800eb0e', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 10:09:46', 3, NULL, NULL, NULL),
	(11, 'palsikwaf', 'palsikwaf@yahoo.com', 'Active', 'Africa/Nairobi', '916825650cb2b85180616cd7accb29a45f4dcc3b5aa765d61d8327deb882cf99', '916825650cb2b85180616cd7accb29a4', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 10:12:44', 3, NULL, NULL, NULL),
	(12, 'nlisi', 'nlisi@lifeskills.or.ke', 'Active', 'Africa/Nairobi', '66dc9a9d574c49dd419ef1557663c9405f4dcc3b5aa765d61d8327deb882cf99', '66dc9a9d574c49dd419ef1557663c940', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 10:16:54', 3, NULL, NULL, NULL),
	(13, 'esywanjik', 'esywanjik@yahoo.com', 'Active', 'Africa/Nairobi', '7f5b3fb6ef04dcc8bb3b4903ae722af15f4dcc3b5aa765d61d8327deb882cf99', '7f5b3fb6ef04dcc8bb3b4903ae722af1', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 10:19:43', 3, NULL, NULL, NULL),
	(14, 'wmajanga', 'wmajanga@gmail.com', 'Active', 'Africa/Nairobi', 'd394c432fc85c0409637a500cba46f145f4dcc3b5aa765d61d8327deb882cf99', 'd394c432fc85c0409637a500cba46f14', NULL, NULL, NULL, NULL, 'ADMIN', 1, '2014-09-04 10:23:58', 3, NULL, NULL, NULL),
	(15, 'mmwangi', 'mmwangi@lifeskills.or.ke', 'Active', 'Africa/Nairobi', '2bcfc41320d250f01ba36651383f2dec5f4dcc3b5aa765d61d8327deb882cf99', '2bcfc41320d250f01ba36651383f2dec', NULL, NULL, NULL, NULL, 'ADMIN', NULL, '2014-09-04 10:40:14', 3, NULL, NULL, NULL);
/*!40000 ALTER TABLE `users_copy` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.users_view
DROP VIEW IF EXISTS `users_view`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `users_view` (
	`id` INT(11) UNSIGNED NOT NULL,
	`username` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`email` VARCHAR(128) NOT NULL COLLATE 'latin1_swedish_ci',
	`status` ENUM('Pending','Active','Blocked') NOT NULL COLLATE 'latin1_swedish_ci',
	`timezone` VARCHAR(60) NULL COLLATE 'latin1_swedish_ci',
	`password` VARCHAR(128) NOT NULL COLLATE 'latin1_swedish_ci',
	`salt` VARCHAR(128) NOT NULL COLLATE 'latin1_swedish_ci',
	`password_reset_code` VARCHAR(128) NULL COLLATE 'latin1_swedish_ci',
	`password_reset_date` TIMESTAMP NULL,
	`password_reset_request_date` TIMESTAMP NULL,
	`activation_code` VARCHAR(128) NULL COLLATE 'latin1_swedish_ci',
	`user_level` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`role_id` INT(11) UNSIGNED NULL,
	`date_created` TIMESTAMP NOT NULL,
	`created_by` INT(11) NULL,
	`last_modified` TIMESTAMP NULL,
	`last_modified_by` INT(11) UNSIGNED NULL,
	`last_login` TIMESTAMP NULL,
	`name` VARCHAR(61) NOT NULL COLLATE 'latin1_swedish_ci',
	`gender` ENUM('Male','Female') NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.user_activity
DROP TABLE IF EXISTS `user_activity`;
CREATE TABLE IF NOT EXISTS `user_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `type` enum('login','create','update','delete') NOT NULL,
  `description` text NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=721 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.user_activity: ~717 rows (approximately)
DELETE FROM `user_activity`;
/*!40000 ALTER TABLE `user_activity` DISABLE KEYS */;
INSERT INTO `user_activity` (`id`, `user_id`, `type`, `description`, `ip_address`, `datetime`) VALUES
	(1, 4, 'login', 'ewachira signed in successfully', '::1', '2014-09-03 16:57:22'),
	(3, 1, 'login', 'mconyango signed in successfully', '::1', '2014-09-04 17:03:20'),
	(4, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-05 10:48:03'),
	(5, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-05 13:27:54'),
	(6, 1, 'login', 'felsoft signed in successfully', '192.168.1.129', '2014-09-08 14:42:52'),
	(7, 1, 'login', 'felsoft signed in successfully', '192.168.1.139', '2014-09-08 14:45:32'),
	(8, 1, 'login', 'felsoft signed in successfully', '192.168.1.111', '2014-09-08 16:40:17'),
	(9, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-18 12:27:26'),
	(10, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-18 12:29:32'),
	(11, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-23 11:02:47'),
	(12, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 11:03:51'),
	(13, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 11:52:40'),
	(14, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-23 11:58:39'),
	(15, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 12:02:11'),
	(16, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 12:56:31'),
	(17, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-23 12:59:50'),
	(18, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 13:00:56'),
	(19, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-23 13:01:13'),
	(20, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 13:04:25'),
	(21, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-23 13:05:08'),
	(22, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-23 13:20:01'),
	(23, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-23 13:21:07'),
	(24, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-24 15:38:09'),
	(25, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-24 15:38:32'),
	(26, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-24 16:03:26'),
	(27, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-24 17:35:28'),
	(28, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-24 17:50:10'),
	(29, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-25 19:53:55'),
	(30, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 08:47:51'),
	(31, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 08:59:23'),
	(32, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 09:19:58'),
	(33, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 09:22:14'),
	(34, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 09:24:30'),
	(35, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 09:25:06'),
	(36, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-26 09:26:43'),
	(37, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-26 09:27:04'),
	(38, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-26 09:27:51'),
	(39, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 09:28:12'),
	(40, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 09:28:48'),
	(41, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 09:52:38'),
	(42, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-26 09:52:47'),
	(43, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 10:44:26'),
	(44, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-26 13:50:06'),
	(45, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-26 13:51:27'),
	(46, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-26 14:01:18'),
	(47, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 15:41:28'),
	(48, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 15:42:05'),
	(49, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:26:19'),
	(50, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 16:29:13'),
	(51, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 16:32:21'),
	(52, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:37:34'),
	(53, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 16:44:13'),
	(54, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 16:44:46'),
	(55, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:46:47'),
	(56, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 16:47:41'),
	(57, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:49:50'),
	(58, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 16:50:20'),
	(59, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:54:58'),
	(60, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 16:55:39'),
	(61, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 16:56:10'),
	(62, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 16:58:00'),
	(63, 8, 'login', 'patrick signed in successfully', '::1', '2014-09-29 16:58:37'),
	(64, 5, 'login', 'kabucho signed in successfully', '::1', '2014-09-29 16:59:28'),
	(65, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-09-29 18:34:28'),
	(66, 1, 'login', 'felsoft signed in successfully', '::1', '2014-09-30 16:49:17'),
	(67, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-01 10:42:34'),
	(68, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-02 12:36:45'),
	(69, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-02 12:37:35'),
	(70, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-02 12:38:03'),
	(71, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-04 12:17:54'),
	(72, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-04 12:18:34'),
	(73, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-04 12:19:02'),
	(74, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-07 09:59:09'),
	(75, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-07 10:02:03'),
	(76, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-07 10:03:55'),
	(77, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-07 11:02:16'),
	(78, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-07 11:02:55'),
	(79, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-07 11:03:22'),
	(80, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-07 12:06:33'),
	(81, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-08 18:59:42'),
	(82, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-08 19:02:41'),
	(83, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-08 19:03:20'),
	(84, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-08 19:03:47'),
	(85, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-15 13:11:51'),
	(86, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-15 13:13:26'),
	(87, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-15 13:20:31'),
	(88, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-17 09:58:27'),
	(89, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-24 13:18:25'),
	(90, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-24 14:55:14'),
	(91, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-24 18:37:29'),
	(92, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-24 18:37:50'),
	(93, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-27 10:45:39'),
	(94, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-29 16:15:48'),
	(95, 5, 'login', 'kabucho signed in successfully', '::1', '2014-10-29 16:16:30'),
	(96, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-29 16:16:55'),
	(97, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-30 16:32:46'),
	(98, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-30 16:35:06'),
	(99, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-10-31 07:31:27'),
	(100, 8, 'login', 'patrick signed in successfully', '::1', '2014-10-31 07:33:25'),
	(101, 1, 'login', 'felsoft signed in successfully', '::1', '2014-10-31 08:00:58'),
	(102, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-04 13:01:28'),
	(103, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-04 13:08:17'),
	(104, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-04 13:16:26'),
	(105, 5, 'login', 'kabucho signed in successfully', '::1', '2014-11-04 13:17:01'),
	(106, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-04 13:17:55'),
	(107, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-05 12:22:42'),
	(108, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 12:33:16'),
	(109, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-05 14:11:56'),
	(110, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-05 14:16:34'),
	(111, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-05 14:16:55'),
	(112, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-05 14:17:30'),
	(113, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 14:22:21'),
	(114, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-05 14:24:25'),
	(115, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-05 14:24:50'),
	(116, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 14:41:36'),
	(117, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-05 14:44:02'),
	(118, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-05 14:44:30'),
	(119, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 15:26:36'),
	(120, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-05 15:30:19'),
	(121, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 19:22:54'),
	(122, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-05 19:25:01'),
	(123, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-05 19:33:21'),
	(124, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-05 19:36:22'),
	(125, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-06 07:49:43'),
	(126, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 08:02:34'),
	(127, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-06 08:03:18'),
	(128, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 08:26:20'),
	(129, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 08:41:12'),
	(130, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-06 08:45:41'),
	(131, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 08:55:07'),
	(132, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-06 08:55:19'),
	(133, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 08:57:21'),
	(134, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-06 12:11:16'),
	(135, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 12:13:13'),
	(136, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-06 12:14:55'),
	(137, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-06 12:46:29'),
	(138, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-06 16:47:16'),
	(139, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-06 17:20:21'),
	(140, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-07 13:00:10'),
	(141, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-08 10:23:00'),
	(142, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-10 15:03:29'),
	(143, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-10 15:07:52'),
	(144, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-10 15:09:13'),
	(145, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-10 15:09:53'),
	(146, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-11 13:48:07'),
	(147, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 12:40:37'),
	(148, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 12:40:50'),
	(149, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 12:41:11'),
	(150, 5, 'login', 'kabucho signed in successfully', '::1', '2014-11-17 14:28:39'),
	(151, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 14:29:18'),
	(152, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 14:29:55'),
	(153, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 14:30:38'),
	(154, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 14:32:22'),
	(155, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 14:32:49'),
	(156, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 14:33:20'),
	(157, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-17 14:33:31'),
	(158, 5, 'login', 'kabucho signed in successfully', '::1', '2014-11-17 14:33:52'),
	(159, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 14:35:06'),
	(160, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 14:35:19'),
	(161, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-17 15:11:15'),
	(162, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-17 15:14:22'),
	(163, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-17 15:19:00'),
	(164, 5, 'login', 'kabucho signed in successfully', '::1', '2014-11-17 15:22:00'),
	(165, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-17 15:23:01'),
	(166, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-18 08:01:52'),
	(167, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-18 08:21:15'),
	(168, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-18 08:21:53'),
	(169, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-18 08:45:52'),
	(170, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-18 09:10:26'),
	(171, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-20 13:28:42'),
	(172, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-20 15:40:08'),
	(173, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-21 11:38:58'),
	(174, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-21 11:40:31'),
	(175, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-21 11:46:57'),
	(176, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-21 15:27:57'),
	(177, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-22 09:34:45'),
	(178, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-22 10:13:30'),
	(179, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-22 10:28:22'),
	(180, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-22 10:36:23'),
	(181, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-22 11:13:43'),
	(182, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-22 11:37:37'),
	(183, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-22 11:49:49'),
	(184, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-22 12:58:12'),
	(185, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-22 16:49:16'),
	(186, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-22 16:49:27'),
	(187, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-22 16:50:33'),
	(188, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-22 16:53:22'),
	(189, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-22 16:53:44'),
	(190, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-24 18:14:15'),
	(191, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-24 18:14:38'),
	(192, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-24 18:15:30'),
	(193, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-24 18:17:47'),
	(194, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-24 18:20:53'),
	(195, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-24 18:21:27'),
	(196, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-24 18:24:20'),
	(197, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-24 19:16:18'),
	(198, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-25 14:29:42'),
	(199, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-25 14:32:27'),
	(200, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 14:32:53'),
	(201, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-25 14:33:33'),
	(202, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 14:40:29'),
	(203, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-25 14:40:50'),
	(204, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 14:42:48'),
	(205, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-25 14:43:10'),
	(206, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 14:44:26'),
	(207, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-25 15:06:49'),
	(208, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 15:07:38'),
	(209, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-25 15:08:13'),
	(210, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-25 15:09:05'),
	(211, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-25 16:36:45'),
	(212, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-25 16:37:04'),
	(213, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-25 17:02:12'),
	(214, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-25 18:35:23'),
	(215, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-26 09:06:22'),
	(216, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 09:52:48'),
	(217, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 10:02:47'),
	(218, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 10:59:48'),
	(219, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 11:08:42'),
	(220, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-26 11:10:15'),
	(221, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 11:14:14'),
	(222, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 11:15:12'),
	(223, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-26 11:44:44'),
	(224, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-26 11:47:34'),
	(225, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-26 15:03:16'),
	(226, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 18:00:05'),
	(227, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 18:01:33'),
	(228, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-26 18:01:57'),
	(229, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 18:48:04'),
	(230, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 18:49:55'),
	(231, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-26 18:50:26'),
	(232, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-26 18:57:49'),
	(233, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-26 19:01:36'),
	(234, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-26 19:02:01'),
	(235, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-27 11:52:20'),
	(236, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-28 10:07:53'),
	(237, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-28 10:14:45'),
	(238, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-28 10:17:26'),
	(239, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-28 10:27:39'),
	(240, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-11-29 10:38:28'),
	(241, 8, 'login', 'patrick signed in successfully', '::1', '2014-11-29 10:43:02'),
	(242, 7, 'login', 'lwambui signed in successfully', '::1', '2014-11-29 11:08:33'),
	(243, 1, 'login', 'felsoft signed in successfully', '::1', '2014-11-29 11:45:47'),
	(244, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-03 13:50:54'),
	(245, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-03 13:54:20'),
	(246, 7, 'login', 'lwambui signed in successfully', '::1', '2014-12-03 13:54:58'),
	(247, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-03 13:55:39'),
	(248, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-08 11:54:26'),
	(249, 1, 'login', 'felsoft signed in successfully', '127.0.0.1', '2014-12-09 14:27:32'),
	(250, 1, 'login', 'felsoft signed in successfully', '127.0.0.1', '2014-12-09 14:37:09'),
	(251, 7, 'login', 'lwambui signed in successfully', '::1', '2014-12-11 16:49:03'),
	(252, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-11 17:06:22'),
	(253, 7, 'login', 'lwambui signed in successfully', '::1', '2014-12-11 17:25:55'),
	(254, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-11 17:29:07'),
	(255, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-11 17:32:54'),
	(256, 7, 'login', 'lwambui signed in successfully', '::1', '2014-12-11 17:50:41'),
	(257, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-11 18:08:58'),
	(258, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-11 18:09:22'),
	(259, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-11 18:12:01'),
	(260, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-11 18:12:14'),
	(261, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-11 18:14:12'),
	(262, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-11 18:15:23'),
	(263, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-11 18:25:36'),
	(264, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-11 18:33:22'),
	(265, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-11 18:37:59'),
	(266, 17, 'login', 'lwairimu signed in successfully', '::1', '2014-12-11 18:56:22'),
	(267, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-11 19:35:33'),
	(268, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-13 13:03:52'),
	(269, 8, 'login', 'patrick signed in successfully', '::1', '2014-12-15 16:01:10'),
	(270, 7, 'login', 'lwambui signed in successfully', '::1', '2014-12-15 16:01:24'),
	(271, 1, 'login', 'felsoft signed in successfully', '::1', '2014-12-15 16:01:56'),
	(272, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-06 15:17:43'),
	(273, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-12 17:01:32'),
	(274, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-13 09:48:06'),
	(275, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-13 09:48:27'),
	(276, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 09:55:00'),
	(277, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 10:08:02'),
	(278, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-13 10:29:12'),
	(279, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-13 10:30:59'),
	(280, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-13 10:34:24'),
	(281, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 11:01:22'),
	(282, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-13 11:31:04'),
	(283, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 11:33:44'),
	(284, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-13 16:17:42'),
	(285, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-13 16:21:53'),
	(286, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-13 16:52:48'),
	(287, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 16:54:31'),
	(288, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-13 17:34:25'),
	(289, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-13 17:36:48'),
	(290, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-13 17:37:30'),
	(291, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-13 17:48:13'),
	(292, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-14 08:31:07'),
	(293, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-14 16:02:45'),
	(294, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-14 16:23:59'),
	(295, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-14 16:55:52'),
	(296, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-15 07:16:51'),
	(297, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-15 07:23:58'),
	(298, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-15 07:33:34'),
	(299, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-15 07:35:23'),
	(300, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-16 16:53:40'),
	(301, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-17 10:12:15'),
	(302, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-17 10:21:59'),
	(303, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-17 10:22:51'),
	(304, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-17 17:36:55'),
	(305, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-19 10:39:16'),
	(306, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-19 10:51:04'),
	(307, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-19 10:52:45'),
	(308, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-19 10:57:34'),
	(309, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-19 15:38:41'),
	(310, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-19 15:42:01'),
	(311, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-19 15:44:21'),
	(312, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-19 15:46:28'),
	(313, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-19 15:48:08'),
	(314, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-19 15:52:49'),
	(315, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-19 16:22:02'),
	(316, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-20 16:33:33'),
	(317, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-20 16:38:14'),
	(318, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-20 16:40:17'),
	(319, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-20 16:40:47'),
	(320, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-20 16:45:10'),
	(321, 17, 'login', 'lwairimu signed in successfully', '::1', '2015-01-20 16:57:49'),
	(322, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-20 16:59:29'),
	(323, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-20 16:59:51'),
	(324, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-20 17:00:21'),
	(325, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 07:27:40'),
	(326, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 07:29:46'),
	(327, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 08:21:29'),
	(328, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 08:21:50'),
	(329, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 08:22:37'),
	(330, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 09:33:06'),
	(331, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 10:07:30'),
	(332, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 10:09:31'),
	(333, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 10:09:49'),
	(334, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 10:20:08'),
	(335, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 10:20:15'),
	(336, 7, 'login', 'lwambui signed in successfully', '::1', '2015-01-21 11:17:17'),
	(337, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 11:22:40'),
	(338, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 11:33:31'),
	(339, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 11:33:52'),
	(340, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 11:55:21'),
	(341, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 11:56:24'),
	(342, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-21 12:04:52'),
	(343, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 12:06:37'),
	(344, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 12:08:30'),
	(345, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 12:32:46'),
	(346, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 12:33:54'),
	(347, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 12:34:46'),
	(348, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 12:35:47'),
	(349, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 12:39:27'),
	(350, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 12:48:03'),
	(351, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-21 12:48:38'),
	(352, 18, 'login', 'cnganga signed in successfully', '::1', '2015-01-21 12:51:35'),
	(353, 8, 'login', 'patrick signed in successfully', '::1', '2015-01-21 12:59:56'),
	(354, 12, 'login', 'nlisi signed in successfully', '192.168.1.74', '2015-01-21 13:28:13'),
	(355, 1, 'login', 'felsoft signed in successfully', '192.168.1.74', '2015-01-21 13:40:42'),
	(356, 12, 'login', 'nlisi signed in successfully', '192.168.1.74', '2015-01-21 13:46:13'),
	(357, 17, 'login', 'lwairimu signed in successfully', '192.168.1.4', '2015-01-21 14:01:44'),
	(358, 7, 'login', 'lwambui signed in successfully', '192.168.1.39', '2015-01-21 15:51:29'),
	(359, 17, 'login', 'lwairimu signed in successfully', '192.168.1.60', '2015-01-21 16:25:09'),
	(360, 10, 'login', 'george signed in successfully', '192.168.1.74', '2015-01-21 16:29:32'),
	(361, 7, 'login', 'lwambui signed in successfully', '192.168.1.39', '2015-01-21 16:36:01'),
	(362, 16, 'login', 'mmwangi signed in successfully', '192.168.1.41', '2015-01-21 16:49:52'),
	(363, 18, 'login', 'cnganga signed in successfully', '192.168.1.28', '2015-01-22 10:04:10'),
	(364, 17, 'login', 'lwairimu signed in successfully', '192.168.1.28', '2015-01-22 10:05:57'),
	(365, 12, 'login', 'nlisi signed in successfully', '192.168.1.28', '2015-01-22 10:11:00'),
	(366, 18, 'login', 'cnganga signed in successfully', '192.168.1.28', '2015-01-22 10:18:39'),
	(367, 1, 'login', 'felsoft signed in successfully', '192.168.1.28', '2015-01-22 10:25:50'),
	(368, 12, 'login', 'nlisi signed in successfully', '192.168.1.28', '2015-01-22 10:28:19'),
	(369, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-22 11:32:31'),
	(370, 10, 'login', 'george signed in successfully', '82.145.221.230', '2015-01-22 13:53:46'),
	(371, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 11:26:45'),
	(372, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 11:37:16'),
	(374, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 11:40:27'),
	(375, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 11:41:18'),
	(376, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 11:42:58'),
	(377, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 11:47:28'),
	(379, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 11:50:20'),
	(380, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 11:54:27'),
	(381, 13, 'login', 'emuthee signed in successfully', '82.145.220.72', '2015-01-23 12:01:02'),
	(382, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 12:01:05'),
	(383, 12, 'login', 'nlisi signed in successfully', '192.168.1.5', '2015-01-23 12:02:10'),
	(384, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 12:03:14'),
	(385, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 12:04:31'),
	(386, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 12:06:25'),
	(387, 13, 'login', 'emuthee signed in successfully', '192.168.1.11', '2015-01-23 12:10:18'),
	(388, 13, 'login', 'emuthee signed in successfully', '192.168.1.45', '2015-01-23 12:11:14'),
	(389, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 12:32:14'),
	(390, 10, 'login', 'george signed in successfully', '192.168.1.5', '2015-01-23 12:33:19'),
	(391, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 12:34:49'),
	(392, 13, 'login', 'emuthee signed in successfully', '41.215.28.162', '2015-01-23 13:02:38'),
	(393, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 13:08:53'),
	(394, 13, 'login', 'emuthee signed in successfully', '41.215.28.162', '2015-01-23 13:58:37'),
	(395, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 14:05:03'),
	(396, 1, 'login', 'felsoft signed in successfully', '192.168.1.155', '2015-01-23 14:20:49'),
	(397, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 14:33:56'),
	(398, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 14:35:50'),
	(399, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-23 14:47:52'),
	(400, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 15:13:21'),
	(401, 1, 'login', 'felsoft signed in successfully', '192.168.1.155', '2015-01-23 15:13:25'),
	(402, 17, 'login', 'lwairimu signed in successfully', '192.168.1.155', '2015-01-23 15:13:58'),
	(403, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 16:16:53'),
	(404, 5, 'login', 'kabucho signed in successfully', '192.168.1.5', '2015-01-23 16:18:26'),
	(405, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-23 16:19:55'),
	(406, 5, 'login', 'kabucho signed in successfully', '192.168.1.5', '2015-01-23 16:23:15'),
	(407, 1, 'login', 'felsoft signed in successfully', '192.168.1.5', '2015-01-23 16:24:48'),
	(408, 13, 'login', 'emuthee signed in successfully', '41.215.28.162', '2015-01-23 16:34:46'),
	(409, 5, 'login', 'kabucho signed in successfully', '41.215.28.162', '2015-01-23 16:35:36'),
	(410, 13, 'login', 'emuthee signed in successfully', '41.215.28.162', '2015-01-23 16:36:16'),
	(411, 5, 'login', 'kabucho signed in successfully', '41.215.28.162', '2015-01-23 16:37:21'),
	(412, 13, 'login', 'emuthee signed in successfully', '41.215.28.162', '2015-01-23 16:45:49'),
	(413, 5, 'login', 'kabucho signed in successfully', '41.215.28.162', '2015-01-23 16:46:54'),
	(414, 1, 'login', 'felsoft signed in successfully', '41.215.28.162', '2015-01-23 17:29:56'),
	(415, 1, 'login', 'felsoft signed in successfully', '82.145.221.116', '2015-01-24 20:05:15'),
	(416, 5, 'login', 'kabucho signed in successfully', '192.168.1.37', '2015-01-26 10:46:01'),
	(417, 13, 'login', 'emuthee signed in successfully', '192.168.1.37', '2015-01-26 11:15:33'),
	(418, 8, 'login', 'patrick signed in successfully', '192.168.1.60', '2015-01-26 16:42:33'),
	(419, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-01-27 08:31:53'),
	(420, 18, 'login', 'cnganga signed in successfully', '192.168.1.15', '2015-01-27 08:33:42'),
	(421, 16, 'login', 'mmwangi signed in successfully', '192.168.1.86', '2015-01-27 08:35:08'),
	(422, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-01-27 10:02:17'),
	(423, 13, 'login', 'emuthee signed in successfully', '192.168.1.51', '2015-01-27 10:03:25'),
	(424, 5, 'login', 'kabucho signed in successfully', '154.70.39.34', '2015-01-27 12:30:02'),
	(425, 13, 'login', 'emuthee signed in successfully', '154.70.39.34', '2015-01-27 12:30:34'),
	(426, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-01-27 12:31:55'),
	(427, 13, 'login', 'emuthee signed in successfully', '154.70.39.34', '2015-01-27 12:34:20'),
	(428, 13, 'login', 'emuthee signed in successfully', '154.70.39.34', '2015-01-27 12:46:22'),
	(429, 5, 'login', 'kabucho signed in successfully', '197.237.243.169', '2015-01-28 02:59:14'),
	(430, 1, 'login', 'felsoft signed in successfully', '192.168.1.37', '2015-01-28 11:00:43'),
	(431, 1, 'login', 'felsoft signed in successfully', '192.168.1.115', '2015-01-28 11:36:13'),
	(432, 26, 'login', 'hmakori signed in successfully', '192.168.1.115', '2015-01-28 11:51:47'),
	(433, 26, 'login', 'hmakori signed in successfully', '192.168.1.6', '2015-01-28 12:03:03'),
	(434, 9, 'login', 'mary signed in successfully', '192.168.1.6', '2015-01-28 12:04:17'),
	(435, 12, 'login', 'nlisi signed in successfully', '192.168.1.5', '2015-01-28 12:28:51'),
	(436, 1, 'login', 'felsoft signed in successfully', '192.168.1.115', '2015-01-28 12:35:29'),
	(437, 13, 'login', 'emuthee signed in successfully', '192.168.1.76', '2015-01-28 12:37:06'),
	(438, 26, 'login', 'hmakori signed in successfully', '192.168.1.115', '2015-01-28 12:48:36'),
	(439, 9, 'login', 'mary signed in successfully', '192.168.1.6', '2015-01-28 12:54:57'),
	(440, 26, 'login', 'hmakori signed in successfully', '192.168.1.16', '2015-01-28 14:41:15'),
	(441, 12, 'login', 'nlisi signed in successfully', '192.168.1.16', '2015-01-28 14:41:51'),
	(442, 18, 'login', 'cnganga signed in successfully', '192.168.1.16', '2015-01-28 14:42:51'),
	(443, 26, 'login', 'hmakori signed in successfully', '192.168.1.16', '2015-01-28 14:43:44'),
	(444, 18, 'login', 'cnganga signed in successfully', '192.168.1.16', '2015-01-28 14:46:17'),
	(445, 1, 'login', 'felsoft signed in successfully', '192.168.1.16', '2015-01-28 14:47:20'),
	(446, 18, 'login', 'cnganga signed in successfully', '192.168.1.16', '2015-01-28 14:48:35'),
	(447, 7, 'login', 'lwambui signed in successfully', '41.220.127.114', '2015-01-28 14:53:16'),
	(448, 7, 'login', 'lwambui signed in successfully', '197.182.102.112', '2015-01-28 17:04:20'),
	(449, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-01-29 10:04:43'),
	(450, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-01-29 10:11:13'),
	(451, 7, 'login', 'lwambui signed in successfully', '192.168.1.86', '2015-01-29 10:43:29'),
	(452, 7, 'login', 'lwambui signed in successfully', '192.168.1.15', '2015-01-29 10:52:23'),
	(453, 16, 'login', 'mmwangi signed in successfully', '192.168.1.86', '2015-01-29 10:52:33'),
	(454, 1, 'login', 'felsoft signed in successfully', '192.168.1.15', '2015-01-29 10:53:48'),
	(455, 7, 'login', 'lwambui signed in successfully', '192.168.1.15', '2015-01-29 10:56:32'),
	(456, 26, 'login', 'hmakori signed in successfully', '192.168.1.5', '2015-01-29 11:11:34'),
	(457, 7, 'login', 'lwambui signed in successfully', '192.168.1.5', '2015-01-29 11:12:05'),
	(458, 18, 'login', 'cnganga signed in successfully', '192.168.1.5', '2015-01-29 11:45:38'),
	(459, 18, 'login', 'cnganga signed in successfully', '192.168.1.5', '2015-01-29 11:47:32'),
	(460, 26, 'login', 'hmakori signed in successfully', '192.168.1.5', '2015-01-29 11:48:17'),
	(461, 13, 'login', 'emuthee signed in successfully', '192.168.1.5', '2015-01-29 11:56:27'),
	(462, 5, 'login', 'kabucho signed in successfully', '192.168.1.5', '2015-01-29 11:58:59'),
	(463, 7, 'login', 'lwambui signed in successfully', '192.168.1.5', '2015-01-29 12:00:05'),
	(464, 18, 'login', 'cnganga signed in successfully', '192.168.1.5', '2015-01-29 12:00:56'),
	(465, 7, 'login', 'lwambui signed in successfully', '192.168.1.5', '2015-01-29 12:01:37'),
	(466, 5, 'login', 'kabucho signed in successfully', '192.168.1.5', '2015-01-29 12:03:13'),
	(467, 26, 'login', 'hmakori signed in successfully', '192.168.1.5', '2015-01-29 12:04:04'),
	(468, 8, 'login', 'patrick signed in successfully', '192.168.1.5', '2015-01-29 12:04:46'),
	(469, 18, 'login', 'cnganga signed in successfully', '192.168.1.5', '2015-01-29 12:05:16'),
	(470, 13, 'login', 'emuthee signed in successfully', '::1', '2015-01-30 12:43:43'),
	(471, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-30 12:51:38'),
	(472, 1, 'login', 'felsoft signed in successfully', '::1', '2015-01-30 14:13:47'),
	(473, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-02 09:02:40'),
	(474, 7, 'login', 'lwambui signed in successfully', '192.168.1.21', '2015-02-02 10:01:57'),
	(475, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-02 10:14:55'),
	(476, 1, 'login', 'felsoft signed in successfully', '192.168.1.58', '2015-02-02 10:31:19'),
	(477, 26, 'login', 'hmakori signed in successfully', '192.168.1.16', '2015-02-02 10:35:19'),
	(478, 17, 'login', 'lwairimu signed in successfully', '192.168.1.58', '2015-02-02 10:41:07'),
	(479, 1, 'login', 'felsoft signed in successfully', '192.168.1.58', '2015-02-02 11:03:07'),
	(480, 12, 'login', 'nlisi signed in successfully', '192.168.1.52', '2015-02-02 11:38:52'),
	(481, 26, 'login', 'hmakori signed in successfully', '192.168.1.115', '2015-02-02 11:51:45'),
	(482, 7, 'login', 'lwambui signed in successfully', '192.168.1.116', '2015-02-02 11:55:28'),
	(483, 1, 'login', 'felsoft signed in successfully', '192.168.1.116', '2015-02-02 11:56:24'),
	(484, 7, 'login', 'lwambui signed in successfully', '192.168.1.32', '2015-02-02 12:23:20'),
	(485, 1, 'login', 'felsoft signed in successfully', '192.168.1.32', '2015-02-02 12:25:14'),
	(486, 13, 'login', 'emuthee signed in successfully', '192.168.1.56', '2015-02-02 12:27:38'),
	(487, 18, 'login', 'cnganga signed in successfully', '192.168.1.51', '2015-02-02 12:29:51'),
	(488, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 12:31:14'),
	(489, 12, 'login', 'nlisi signed in successfully', '192.168.1.51', '2015-02-02 12:31:58'),
	(490, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 12:33:52'),
	(491, 18, 'login', 'cnganga signed in successfully', '192.168.1.51', '2015-02-02 12:58:49'),
	(492, 18, 'login', 'cnganga signed in successfully', '192.168.1.15', '2015-02-02 13:43:26'),
	(493, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 14:12:37'),
	(494, 7, 'login', 'lwambui signed in successfully', '192.168.1.116', '2015-02-02 14:41:32'),
	(495, 12, 'login', 'nlisi signed in successfully', '192.168.1.51', '2015-02-02 15:54:02'),
	(496, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 15:55:05'),
	(497, 26, 'login', 'hmakori signed in successfully', '192.168.1.52', '2015-02-02 15:58:48'),
	(498, 12, 'login', 'nlisi signed in successfully', '192.168.1.52', '2015-02-02 16:00:27'),
	(499, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-02-02 16:01:21'),
	(500, 12, 'login', 'nlisi signed in successfully', '192.168.1.51', '2015-02-02 16:05:57'),
	(501, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 16:08:31'),
	(502, 12, 'login', 'nlisi signed in successfully', '192.168.1.51', '2015-02-02 16:14:02'),
	(503, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-02 16:15:28'),
	(504, 1, 'login', 'felsoft signed in successfully', '192.168.1.52', '2015-02-02 16:17:29'),
	(505, 12, 'login', 'nlisi signed in successfully', '192.168.1.52', '2015-02-02 16:19:12'),
	(506, 1, 'login', 'felsoft signed in successfully', '192.168.1.21', '2015-02-03 08:42:23'),
	(507, 1, 'login', 'felsoft signed in successfully', '192.168.1.47', '2015-02-03 09:25:05'),
	(508, 17, 'login', 'lwairimu signed in successfully', '192.168.1.47', '2015-02-03 09:27:33'),
	(509, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-03 09:29:05'),
	(510, 26, 'login', 'hmakori signed in successfully', '192.168.1.47', '2015-02-03 10:04:44'),
	(511, 13, 'login', 'emuthee signed in successfully', '192.168.1.56', '2015-02-03 11:10:31'),
	(512, 16, 'login', 'mmwangi signed in successfully', '192.168.1.86', '2015-02-03 11:56:22'),
	(513, 7, 'login', 'lwambui signed in successfully', '192.168.1.115', '2015-02-03 12:21:46'),
	(514, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-03 12:26:00'),
	(515, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-03 13:57:06'),
	(516, 12, 'login', 'nlisi signed in successfully', '192.168.1.21', '2015-02-03 14:03:55'),
	(517, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-03 14:23:55'),
	(518, 8, 'login', 'patrick signed in successfully', '192.168.1.32', '2015-02-03 15:39:58'),
	(519, 7, 'login', 'lwambui signed in successfully', '192.168.1.116', '2015-02-04 09:14:50'),
	(520, 12, 'login', 'nlisi signed in successfully', '192.168.1.52', '2015-02-04 09:17:52'),
	(521, 16, 'login', 'mmwangi signed in successfully', '192.168.1.21', '2015-02-04 10:28:36'),
	(522, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-04 10:29:42'),
	(523, 8, 'login', 'patrick signed in successfully', '154.76.5.2', '2015-02-04 10:36:52'),
	(524, 1, 'login', 'felsoft signed in successfully', '154.76.5.2', '2015-02-04 10:39:55'),
	(525, 17, 'login', 'lwairimu signed in successfully', '154.76.5.2', '2015-02-04 10:51:33'),
	(526, 1, 'login', 'felsoft signed in successfully', '154.76.5.2', '2015-02-04 10:52:41'),
	(527, 17, 'login', 'lwairimu signed in successfully', '154.76.5.2', '2015-02-04 10:56:23'),
	(528, 7, 'login', 'lwambui signed in successfully', '192.168.1.116', '2015-02-05 11:25:54'),
	(529, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 15:54:32'),
	(530, 16, 'login', 'mmwangi signed in successfully', '192.168.1.86', '2015-02-05 15:56:19'),
	(531, 18, 'login', 'cnganga signed in successfully', '192.168.1.15', '2015-02-05 15:57:16'),
	(532, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 15:58:03'),
	(533, 5, 'login', 'kabucho signed in successfully', '192.168.1.15', '2015-02-05 15:59:21'),
	(534, 1, 'login', 'felsoft signed in successfully', '192.168.1.15', '2015-02-05 16:00:51'),
	(535, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 16:06:38'),
	(536, 16, 'login', 'mmwangi signed in successfully', '192.168.1.15', '2015-02-05 16:08:54'),
	(537, 17, 'login', 'lwairimu signed in successfully', '192.168.1.15', '2015-02-05 16:12:32'),
	(538, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 16:22:29'),
	(539, 7, 'login', 'lwambui signed in successfully', '192.168.1.15', '2015-02-05 16:28:16'),
	(540, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 16:34:19'),
	(541, 26, 'login', 'hmakori signed in successfully', '192.168.1.15', '2015-02-05 16:40:16'),
	(542, 1, 'login', 'felsoft signed in successfully', '192.168.1.15', '2015-02-05 16:44:39'),
	(543, 16, 'login', 'mmwangi signed in successfully', '192.168.1.51', '2015-02-06 10:32:17'),
	(544, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-06 10:33:41'),
	(545, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-09 10:33:53'),
	(546, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-09 10:41:56'),
	(547, 7, 'login', 'lwambui signed in successfully', '192.168.1.96', '2015-02-09 10:54:18'),
	(548, 17, 'login', 'lwairimu signed in successfully', '154.70.39.34', '2015-02-09 10:55:43'),
	(549, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-09 11:07:41'),
	(550, 18, 'login', 'cnganga signed in successfully', '192.168.1.106', '2015-02-09 11:37:06'),
	(551, 18, 'login', 'cnganga signed in successfully', '192.168.1.115', '2015-02-09 11:55:55'),
	(552, 26, 'login', 'hmakori signed in successfully', '192.168.1.115', '2015-02-09 12:18:52'),
	(553, 17, 'login', 'lwairimu signed in successfully', '192.168.1.134', '2015-02-09 12:22:24'),
	(554, 5, 'login', 'kabucho signed in successfully', '192.168.1.134', '2015-02-09 12:23:56'),
	(555, 17, 'login', 'lwairimu signed in successfully', '192.168.1.106', '2015-02-09 12:29:25'),
	(556, 1, 'login', 'felsoft signed in successfully', '192.168.1.106', '2015-02-09 12:29:54'),
	(557, 18, 'login', 'cnganga signed in successfully', '192.168.1.115', '2015-02-09 12:37:06'),
	(558, 18, 'login', 'cnganga signed in successfully', '192.168.1.106', '2015-02-09 12:39:19'),
	(559, 26, 'login', 'hmakori signed in successfully', '192.168.1.115', '2015-02-09 13:46:53'),
	(560, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.116', '2015-02-09 14:20:08'),
	(561, 26, 'login', 'hmakori signed in successfully', '192.168.1.116', '2015-02-09 14:31:47'),
	(562, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.116', '2015-02-09 14:34:13'),
	(563, 26, 'login', 'hmakori signed in successfully', '192.168.1.116', '2015-02-09 14:35:23'),
	(564, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.116', '2015-02-09 14:40:12'),
	(565, 7, 'login', 'lwambui signed in successfully', '::1', '2015-02-10 08:47:27'),
	(566, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-10 08:56:00'),
	(567, 5, 'login', 'kabucho signed in successfully', '154.70.39.34', '2015-02-10 09:16:18'),
	(568, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-10 09:17:51'),
	(569, 17, 'login', 'lwairimu signed in successfully', '154.70.39.34', '2015-02-10 09:18:31'),
	(570, 18, 'login', 'cnganga signed in successfully', '154.70.39.34', '2015-02-10 09:18:57'),
	(571, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-10 09:20:01'),
	(572, 16, 'login', 'mmwangi signed in successfully', '192.168.1.51', '2015-02-10 10:01:52'),
	(573, 18, 'login', 'cnganga signed in successfully', '192.168.1.51', '2015-02-10 10:02:38'),
	(574, 17, 'login', 'lwairimu signed in successfully', '192.168.1.51', '2015-02-10 10:03:21'),
	(575, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-10 10:05:52'),
	(576, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-10 10:14:43'),
	(577, 7, 'login', 'lwambui signed in successfully', '192.168.1.96', '2015-02-10 10:23:42'),
	(578, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-10 11:52:15'),
	(579, 16, 'login', 'mmwangi signed in successfully', '192.168.1.51', '2015-02-10 11:54:00'),
	(580, 17, 'login', 'lwairimu signed in successfully', '192.168.1.106', '2015-02-10 11:59:00'),
	(581, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-10 13:05:48'),
	(582, 18, 'login', 'cnganga signed in successfully', '192.168.1.106', '2015-02-10 13:57:10'),
	(583, 18, 'login', 'cnganga signed in successfully', '192.168.1.106', '2015-02-10 15:03:16'),
	(584, 17, 'login', 'lwairimu signed in successfully', '192.168.1.106', '2015-02-10 15:16:44'),
	(585, 7, 'login', 'lwambui signed in successfully', '192.168.1.96', '2015-02-10 16:20:16'),
	(586, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-10 17:41:14'),
	(587, 17, 'login', 'lwairimu signed in successfully', '154.70.39.34', '2015-02-10 17:44:01'),
	(588, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-11 07:44:44'),
	(589, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-11 09:50:21'),
	(590, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-11 10:41:12'),
	(591, 8, 'login', 'patrick signed in successfully', '154.70.39.34', '2015-02-11 11:00:23'),
	(592, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-11 11:03:24'),
	(593, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-11 11:15:50'),
	(594, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-11 11:18:55'),
	(595, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-11 11:20:22'),
	(596, 18, 'login', 'cnganga signed in successfully', '154.70.39.34', '2015-02-11 11:26:07'),
	(597, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-11 11:54:46'),
	(598, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-11 11:56:14'),
	(599, 7, 'login', 'lwambui signed in successfully', '192.168.1.21', '2015-02-11 12:32:56'),
	(600, 8, 'login', 'patrick signed in successfully', '192.168.1.21', '2015-02-11 12:39:03'),
	(601, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-11 13:15:35'),
	(602, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-11 13:34:42'),
	(603, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-11 13:38:28'),
	(604, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.116', '2015-02-11 14:41:02'),
	(605, 5, 'login', 'kabucho signed in successfully', '192.168.1.51', '2015-02-11 15:24:06'),
	(606, 5, 'login', 'kabucho signed in successfully', '192.168.1.21', '2015-02-11 15:27:06'),
	(607, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-11 15:28:51'),
	(608, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-11 15:29:53'),
	(609, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-12 09:01:56'),
	(610, 18, 'login', 'cnganga signed in successfully', '192.168.1.51', '2015-02-12 09:15:57'),
	(611, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-12 12:11:53'),
	(612, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-12 12:17:54'),
	(613, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.15', '2015-02-12 12:52:29'),
	(614, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-12 12:56:48'),
	(615, 26, 'login', 'hmakori signed in successfully', '192.168.1.21', '2015-02-12 13:44:16'),
	(616, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-12 13:48:47'),
	(617, 7, 'login', 'lwambui signed in successfully', '192.168.1.33', '2015-02-12 13:57:23'),
	(618, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-12 14:02:51'),
	(619, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-02-12 14:16:54'),
	(620, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-12 14:22:20'),
	(621, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-02-12 14:29:03'),
	(622, 1, 'login', 'felsoft signed in successfully', '192.168.1.51', '2015-02-12 14:47:16'),
	(623, 5, 'login', 'kabucho signed in successfully', '154.70.39.34', '2015-02-12 14:48:09'),
	(624, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-12 14:51:35'),
	(625, 7, 'login', 'lwambui signed in successfully', '192.168.1.33', '2015-02-12 14:58:41'),
	(626, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-12 15:00:12'),
	(627, 17, 'login', 'lwairimu signed in successfully', '192.168.1.21', '2015-02-12 15:03:18'),
	(628, 17, 'login', 'nsumba signed in successfully', '192.168.1.21', '2015-02-12 15:25:02'),
	(629, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-12 15:28:19'),
	(630, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-12 15:28:20'),
	(631, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-12 15:35:04'),
	(632, 18, 'login', 'cnganga signed in successfully', '154.70.39.34', '2015-02-12 19:13:23'),
	(633, 17, 'login', 'nsumba signed in successfully', '192.168.1.21', '2015-02-13 07:52:15'),
	(634, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-13 08:43:09'),
	(635, 18, 'login', 'cnganga signed in successfully', '192.168.1.155', '2015-02-13 09:05:26'),
	(636, 17, 'login', 'nsumba signed in successfully', '192.168.1.155', '2015-02-13 09:08:21'),
	(637, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-13 09:27:41'),
	(638, 11, 'login', 'palsikwaf signed in successfully', '192.168.1.15', '2015-02-13 11:27:45'),
	(639, 5, 'login', 'kabucho signed in successfully', '192.168.1.134', '2015-02-13 13:26:01'),
	(640, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-13 14:51:44'),
	(641, 17, 'login', 'nsumba signed in successfully', '192.168.1.111', '2015-02-16 08:27:55'),
	(642, 5, 'login', 'kabucho signed in successfully', '154.70.39.34', '2015-02-16 08:28:36'),
	(643, 17, 'login', 'nsumba signed in successfully', '154.70.39.34', '2015-02-16 08:30:18'),
	(644, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-16 08:32:43'),
	(645, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-16 08:39:00'),
	(646, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-16 09:55:41'),
	(647, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-16 10:41:24'),
	(648, 1, 'login', 'felsoft signed in successfully', '154.70.39.34', '2015-02-16 11:31:03'),
	(649, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 12:25:29'),
	(650, 26, 'login', 'hmakori signed in successfully', '192.168.1.141', '2015-02-16 12:32:15'),
	(651, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 12:49:37'),
	(652, 26, 'login', 'hmakori signed in successfully', '192.168.1.141', '2015-02-16 12:53:05'),
	(653, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 12:54:44'),
	(654, 26, 'login', 'hmakori signed in successfully', '192.168.1.141', '2015-02-16 12:55:45'),
	(655, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 12:59:25'),
	(656, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 13:00:05'),
	(657, 26, 'login', 'hmakori signed in successfully', '192.168.1.141', '2015-02-16 13:00:39'),
	(658, 26, 'login', 'hmakori signed in successfully', '192.168.1.51', '2015-02-16 14:21:39'),
	(659, 18, 'login', 'cnganga signed in successfully', '192.168.1.94', '2015-02-16 15:10:05'),
	(660, 14, 'login', 'wmajanga signed in successfully', '192.168.1.141', '2015-02-16 15:29:09'),
	(661, 5, 'login', 'kabucho signed in successfully', '192.168.1.82', '2015-02-17 09:37:40'),
	(662, 12, 'login', 'nlisi signed in successfully', '192.168.1.138', '2015-02-17 11:10:55'),
	(663, 26, 'login', 'hmakori signed in successfully', '192.168.1.111', '2015-02-18 08:56:12'),
	(664, 17, 'login', 'nsumba signed in successfully', '192.168.1.111', '2015-02-18 08:57:53'),
	(665, 8, 'login', 'patrick signed in successfully', '192.168.1.93', '2015-02-19 08:51:26'),
	(666, 5, 'login', 'kabucho signed in successfully', '154.70.39.34', '2015-02-19 08:51:58'),
	(667, 12, 'login', 'nlisi signed in successfully', '192.168.1.132', '2015-02-19 10:15:48'),
	(668, 26, 'login', 'hmakori signed in successfully', '192.168.1.111', '2015-02-19 13:02:15'),
	(669, 17, 'login', 'nsumba signed in successfully', '192.168.1.111', '2015-02-19 13:03:08'),
	(670, 26, 'login', 'hmakori signed in successfully', '192.168.1.111', '2015-02-19 14:34:06'),
	(671, 17, 'login', 'nsumba signed in successfully', '192.168.1.111', '2015-02-19 14:34:46'),
	(672, 26, 'login', 'hmakori signed in successfully', '192.168.1.113', '2015-02-19 16:11:47'),
	(673, 26, 'login', 'hmakori signed in successfully', '192.168.1.67', '2015-02-19 16:13:34'),
	(674, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-24 15:14:44'),
	(675, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-24 17:09:45'),
	(676, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-24 17:52:16'),
	(677, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-27 08:32:23'),
	(678, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-27 16:10:21'),
	(679, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-27 19:25:11'),
	(680, 1, 'login', 'felsoft signed in successfully', '::1', '2015-02-28 13:52:29'),
	(681, 1, 'login', 'felsoft signed in successfully', '::1', '2015-03-19 12:16:35'),
	(682, 1, 'login', 'felsoft signed in successfully', '::1', '2015-03-19 15:16:58'),
	(683, 1, 'login', 'felsoft signed in successfully', '::1', '2015-03-19 15:41:11'),
	(684, 25, 'login', 'ekhalembi signed in successfully', '::1', '2015-03-19 15:41:38'),
	(685, 1, 'login', 'felsoft signed in successfully', '::1', '2015-03-19 15:41:50'),
	(686, 1, 'login', 'felsoft signed in successfully', '::1', '2015-04-20 21:32:16'),
	(687, 1, 'login', 'felsoft signed in successfully', '127.0.0.1', '2015-09-02 22:38:59'),
	(688, 1, 'login', 'felsoft signed in successfully', '::1', '2015-12-19 12:23:03'),
	(689, 1, 'login', 'felsoft signed in successfully', '::1', '2016-01-30 07:35:28'),
	(690, 1, 'login', 'felsoft signed in successfully', '::1', '2016-01-30 08:58:23'),
	(691, 1, 'login', 'felsoft signed in successfully', '::1', '2016-02-26 21:32:52'),
	(692, 1, 'login', 'felsoft signed in successfully', '::1', '2016-02-26 21:54:46'),
	(693, 1, 'login', 'felsoft signed in successfully', '::1', '2016-02-28 15:25:18'),
	(694, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-09 23:22:03'),
	(695, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-10 00:03:07'),
	(696, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-11 00:23:31'),
	(697, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-11 20:20:35'),
	(698, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-13 21:28:27'),
	(699, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-15 21:13:03'),
	(700, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-15 22:29:23'),
	(701, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-18 06:15:16'),
	(702, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-18 06:19:51'),
	(703, 1, 'login', 'felsoft signed in successfully', '::1', '2016-03-18 06:25:57'),
	(704, 1, 'login', 'felsoft signed in successfully', '::1', '2016-04-24 11:25:45'),
	(705, 28, 'login', 'johnerick89 signed in successfully', '::1', '2016-04-27 00:17:02'),
	(706, 28, 'login', 'johnerick89 signed in successfully', '41.89.65.100', '2016-04-27 21:14:58'),
	(707, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-04-28 19:59:21'),
	(708, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-04-28 22:37:55'),
	(709, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-04-28 23:26:12'),
	(710, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-04-28 23:45:46'),
	(711, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-05-06 10:11:57'),
	(712, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-05-06 10:23:04'),
	(713, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-05-11 11:46:42'),
	(714, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-07-03 03:26:04'),
	(715, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-08-02 08:59:48'),
	(716, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-08-02 15:19:13'),
	(717, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-09-13 08:20:34'),
	(718, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-09-19 08:14:52'),
	(719, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-09-22 11:24:53'),
	(720, 28, 'login', 'jacjimus signed in successfully', '::1', '2016-10-04 13:48:07');
/*!40000 ALTER TABLE `user_activity` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.user_levels
DROP TABLE IF EXISTS `user_levels`;
CREATE TABLE IF NOT EXISTS `user_levels` (
  `id` varchar(30) NOT NULL,
  `description` varchar(255) NOT NULL,
  `banned_resources` text,
  `banned_resources_inheritance` varchar(30) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='user types (non-functional requirement)';

-- Dumping data for table bremak_manager.user_levels: ~4 rows (approximately)
DELETE FROM `user_levels`;
/*!40000 ALTER TABLE `user_levels` DISABLE KEYS */;
INSERT INTO `user_levels` (`id`, `description`, `banned_resources`, `banned_resources_inheritance`, `rank`) VALUES
	('ADMIN', 'Admin', NULL, 'SUPERADMIN', 3),
	('ENGINEER', 'System Engineer', NULL, NULL, 1),
	('SUPERADMIN', 'Super Admin', 'MODULES_ENABLED,QUEUEMANAGER,USER_LEVELS,USER_RESOURCES', 'ENGINEER', 2),
	('User', 'Users', 'EMPLOYEE_DETAILS,HR_SETTINGS,ORG_COMPANY,PROGRAMS,SETTINGS,USERS,USER_ACTIVITY,WORKFLOW', 'ADMIN', 0);
/*!40000 ALTER TABLE `user_levels` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.user_resources
DROP TABLE IF EXISTS `user_resources`;
CREATE TABLE IF NOT EXISTS `user_resources` (
  `id` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `viewable` int(11) NOT NULL DEFAULT '1',
  `createable` int(11) NOT NULL DEFAULT '1',
  `updateable` int(11) NOT NULL DEFAULT '1',
  `deleteable` int(11) NOT NULL DEFAULT '1',
  `approveable` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='system resources-(non-functional requirement)';

-- Dumping data for table bremak_manager.user_resources: ~113 rows (approximately)
DELETE FROM `user_resources`;
/*!40000 ALTER TABLE `user_resources` DISABLE KEYS */;
INSERT INTO `user_resources` (`id`, `description`, `viewable`, `createable`, `updateable`, `deleteable`, `approveable`) VALUES
	('ACTIVITY_TYPES', 'Activity types', 1, 1, 1, 1, 0),
	('APPROVE_INVOICE', 'APPROVE INVOICE FOR PAYMENT', 1, 1, 1, 1, 0),
	('AUDIT_TRAIL', 'Audit trail', 1, 1, 1, 1, 0),
	('BACKUP', 'Database Backup', 1, 1, 1, 1, 0),
	('BUDGET_PERIODS', 'Budget Period', 1, 1, 1, 1, 0),
	('CASH_PAYMENTS', 'CASH PAYMENTS', 1, 1, 1, 1, 0),
	('CHEQUE_VOUCHER', 'Cheque Voucher for Advance Cash', 1, 1, 1, 1, 0),
	('COMPANY_DOCS', 'Company Documents', 1, 1, 1, 1, 0),
	('CONSULTANCY', 'Consultancy module', 1, 1, 1, 1, 0),
	('CONSULTANTS', 'Consultants', 1, 1, 1, 1, 0),
	('CON_ASC_ASSIGNMENTS', 'Associate consultants assignments', 1, 1, 1, 1, 0),
	('CON_ASC_CONTRACTS', 'Associate consultants contracts', 1, 1, 1, 1, 0),
	('CON_ASC_INVOICE', 'Associate consultants invoices', 1, 1, 1, 1, 0),
	('CON_ASC_JOBS', 'Associate consultants jobs', 1, 1, 1, 1, 0),
	('CON_ASC_JOB_REPORTS', 'Consultancy Reports', 1, 1, 1, 1, 0),
	('CON_ASC_JOB_TYPES', 'Associate consultants job types', 1, 1, 1, 1, 0),
	('CON_ASC_PAYMENTS', 'Associate consultants payments', 1, 1, 1, 1, 0),
	('CON_ASSIGNMENTS', 'Consultancy Assignment', 1, 1, 1, 1, 0),
	('CON_ASSIGNMENT_REPORTS', 'Consultancy Assignment Reports', 1, 1, 1, 1, 0),
	('CON_ENGAGEMENT', 'Consultancy Engagement', 1, 1, 1, 1, 0),
	('CON_INVOICES', 'Consultancy Invoices', 1, 1, 1, 1, 0),
	('CON_MAIN_REPORTS', 'Consultancy Main Reports', 1, 1, 1, 1, 0),
	('CON_OPPORTUNITIES', 'Consultancy Opportunities', 1, 1, 1, 1, 0),
	('CON_PAYMENTS', 'Consultancy Payments', 1, 1, 1, 1, 0),
	('CON_RESPONSES', 'Consultancy  Responses', 1, 1, 1, 1, 0),
	('COUNTRY', 'Country', 1, 1, 1, 1, 0),
	('COUNTRY_LOCATIONS', 'Country Locations', 1, 1, 1, 1, 0),
	('COUNTRY_PROJECTS', 'Country projects', 1, 1, 1, 1, 0),
	('CREATE_INVOICE', 'CREATE INVOICE FOR PAYMENT', 1, 1, 1, 1, 0),
	('CREATE_RECONCILIATION', 'Make Reconciliation', 1, 1, 1, 1, 0),
	('EMPLOYEE_DETAILS', 'Employee Details', 1, 1, 1, 1, 0),
	('EMPLOYEE_LIST', 'Employees List', 1, 1, 1, 1, 0),
	('EMPLOYMENT_CATEGORIES', 'Employment Categories', 1, 1, 1, 1, 0),
	('EMPLOYMENT_CLASS', 'Employment Class', 1, 1, 1, 1, 0),
	('EVENT_REMINDERS', 'Event Reminders', 1, 1, 1, 1, 0),
	('EXPENSE_ADVANCE', 'Expense Advance', 1, 1, 1, 1, 0),
	('EXPENSE_ITEMS', 'Expense Items', 1, 1, 1, 1, 0),
	('FLEET', 'Manage Fleet/Company Vehicles', 1, 1, 1, 1, 0),
	('FLEET_ODOMETER_READINGS', 'FLEET ODOMETER READINGS', 1, 1, 1, 1, 0),
	('FLEET_VEHICLE_ASSIGNMENTS', 'FLEET VEHICLE ASSIGNMENTS', 1, 1, 1, 1, 0),
	('FLEET_VEHICLE_BOOKING', 'FLEET VEHICLE BOOKING', 1, 1, 1, 1, 0),
	('FLEET_VEHICLE_FUELLING', 'FLEET VEHICLE FUELLING', 1, 1, 1, 1, 0),
	('FLEET_VEHICLE_REQUISITIONS', 'FLEET VEHICLE REQUISITIONS', 1, 1, 1, 1, 0),
	('FLEET_VEHICLE_USAGE', 'FLEET VEHICLE USAGE', 1, 1, 1, 1, 0),
	('HR_LEAVES', 'Employees leaves', 1, 1, 1, 1, 1),
	('HR_SETTINGS', 'HR Settings', 1, 1, 1, 1, 0),
	('INDICATORS', 'Indicators setup', 1, 1, 1, 1, 0),
	('INDICATOR_TRANSACTIONS', 'Project Indicator Transactions', 1, 1, 1, 1, 0),
	('INDICATOR_TYPES', 'Indicator Types', 1, 1, 1, 1, 0),
	('INVENTORY', 'Inventory Management', 1, 1, 1, 1, 0),
	('INVOICE_MAKE_PAYMENT', 'INVOICE MAKE PAYMENT ', 1, 1, 1, 1, 0),
	('INVOICE_PAYMENT', 'Invoice Payment', 1, 1, 1, 1, 0),
	('LEAVE', 'Leave Management', 1, 1, 1, 1, 0),
	('LOCAL_PURCHASE_ORDER', 'Local Purchase Order', 1, 1, 1, 1, 0),
	('MAKE_ADVANCE_PAYMENT', 'Advance Payment', 1, 1, 1, 1, 0),
	('MENU_RFQ_AWARD', 'MENU RFQ AWARD', 1, 1, 1, 1, 0),
	('MENU_RFQ_BID_ANALYSIS', 'MENU RFQ BID ANALYSIS', 1, 1, 1, 1, 0),
	('ME_ACTIVITY_PLANS', 'M&E: Activity Plans', 1, 1, 1, 1, 0),
	('MODULES_ENABLED', 'Modules Enabled', 1, 1, 1, 1, 0),
	('MONITORING_REPORTS', 'M&E reports', 1, 1, 1, 1, 0),
	('ORG_COMPANY', 'Company', 1, 1, 1, 1, 0),
	('ORG_STRUCTURE', 'Organization Structure', 1, 1, 1, 1, 0),
	('PAYMENT_LIST', 'Advance Payment List', 1, 1, 1, 1, 0),
	('PAYMENT_PAYTYPE', 'Payment Type', 1, 1, 1, 1, 0),
	('PAYROLL_SETTINGS', 'Payroll settings', 1, 1, 1, 1, 0),
	('PAYROLL_TRANSACTIONS', 'Payroll Transactions', 1, 1, 1, 1, 0),
	('PENDING_ORDER_ISSUE', 'Pending Order Issue', 1, 1, 1, 1, 0),
	('PERFORMANCE', 'Performance Appraisal', 1, 1, 1, 1, 0),
	('PERFORMANCE_SETTINGS', 'Performance Appraisal settings', 1, 1, 1, 1, 0),
	('PF_CRITICAL_PERFORMANCE_FACTORS', 'CRITICAL PERFORMANCE FACTORS', 1, 1, 1, 1, 0),
	('PF_KEY_PERFORMANCE_AREAS', 'KEY PERFORMANCE AREAS', 1, 1, 1, 1, 0),
	('PF_RATING_DESCRIPTION', 'PERFORMANCE RATING DESCRIPTION', 1, 1, 1, 1, 0),
	('PF_REVIEWS', 'PERFORMANCE REVIEWS', 1, 1, 1, 1, 0),
	('PF_REVIEW_REPORTS', 'PERFORMANCE REVIEW REPORTS', 1, 1, 1, 1, 0),
	('PROGRAM', 'PROGRAMS', 1, 1, 1, 1, 0),
	('PROGRAMS', 'Programs Management', 1, 1, 1, 1, 0),
	('PROJECT', 'PROJECTS', 1, 1, 1, 1, 0),
	('PROJECT_ACTIVITIES', 'Project Activities', 1, 1, 1, 1, 0),
	('PROJECT_BUDGETS', 'Project Budget', 1, 1, 1, 1, 0),
	('PROJECT_DONATIONS', 'Project Donations', 1, 1, 1, 1, 0),
	('PROJECT_DONORS', 'Project Donors', 1, 1, 1, 1, 0),
	('PROJECT_DONOR_PAYMENTS', 'Project donor payments', 1, 1, 1, 1, 0),
	('PROJECT_EXPENDITURE', 'Project Expenses', 1, 1, 1, 1, 0),
	('PUBLIC_HOLIDAYS', 'Public Holidays', 1, 1, 1, 1, 0),
	('QUEUEMANAGER', 'Queue Management', 1, 1, 1, 1, 0),
	('REQUEST_FOR_QUOTATION', 'Request for Quotation', 1, 1, 1, 1, 0),
	('REQUISITION', 'Requisition', 1, 1, 1, 1, 1),
	('REQUISITION_REPORTS', 'Requisition Reports', 1, 1, 1, 1, 0),
	('RES_DOC', 'Documents Manager', 1, 1, 1, 1, 0),
	('RES_RFQ_EVAL_CRITERIA', 'Evaluation Criteria', 1, 1, 1, 1, 0),
	('RES_TRAINING', 'TRAINING', 1, 1, 1, 1, 0),
	('RFQ', 'Request for Quotation', 1, 1, 1, 1, 0),
	('RFQ_AWARD', 'Bid Award', 1, 1, 1, 1, 0),
	('RFQ_BID_ANALYSIS', 'Bid Analysis', 1, 1, 1, 1, 0),
	('RFQ_CRITERIA', 'Mandatory Requirements', 1, 1, 1, 1, 0),
	('RFQ_REQUIREMENTS', 'Request for Quotation Requirements', 1, 1, 1, 1, 0),
	('RFQ_RESPONSE', 'Request for Quotation Response', 1, 1, 1, 1, 0),
	('RFQ_SUPPLIERS', 'Invite Suppliers to RFQ', 1, 1, 1, 1, 0),
	('RFQ_SUPPLIER_RESPONSE', 'RFQ Supplier Response', 1, 1, 1, 1, 0),
	('RFQ_VALUATION_DATA', 'Valuation Data', 1, 1, 1, 1, 0),
	('SETTINGS', 'System Settings', 1, 1, 1, 1, 0),
	('STRATEGIC_OBJECTIVES', 'The organization\'s strategic objectives', 1, 1, 1, 1, 0),
	('TIMESHEETS', 'Timesheet', 1, 1, 1, 1, 0),
	('TIMESHEET_REPORTS', 'Timesheet Reports', 1, 1, 1, 1, 0),
	('TIMESHEET_TIMEOFF_ITEMS', 'Timesheet Time off items', 1, 1, 1, 1, 0),
	('TRAVEL_REQUISITION', 'Travel Requisition', 1, 1, 1, 1, 0),
	('USERS', 'Users Management', 1, 1, 1, 1, 0),
	('USER_ACTIVITY', 'Uses activity log', 1, 0, 0, 1, 0),
	('USER_LEVELS', 'User Levels', 1, 1, 1, 1, 0),
	('USER_RESOURCES', 'System resources', 1, 1, 1, 1, 0),
	('USER_ROLES', 'System Roles', 1, 1, 1, 1, 0),
	('VALUATION_PARAMETERS', 'Bid Valuation Parameters', 1, 1, 1, 1, 0),
	('WORKFLOW', 'Workflow Management', 1, 1, 1, 1, 0);
/*!40000 ALTER TABLE `user_resources` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.user_roles
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `readonly` int(11) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='System roles for admin users';

-- Dumping data for table bremak_manager.user_roles: ~12 rows (approximately)
DELETE FROM `user_roles`;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` (`id`, `name`, `description`, `readonly`, `date_created`, `created_by`) VALUES
	(1, 'Admin Manager', 'Administration members.', 1, '2013-09-29 00:39:40', 1),
	(2, 'Program Managers', 'Users who manage programs', 0, '2013-10-17 17:38:57', 1),
	(4, 'Human Resource', 'Human Resource management', 0, '2013-10-17 17:58:49', 1),
	(5, 'Fleet Managers', 'Users that manage fleet.', 0, '2014-04-04 13:16:14', 1),
	(6, 'Consultant', NULL, 0, '2014-09-25 11:23:55', 1),
	(7, 'Users', 'Ordinary users(Default)', 0, '2014-11-22 16:55:30', 1),
	(8, 'Finance Manager', 'For finance manager', 0, '2015-01-21 11:23:17', 1),
	(9, 'Finance Senior', 'Finance Senior', 0, '2015-01-28 11:11:56', 1),
	(10, 'Finance Users', 'Finance Users', 0, '2015-01-28 11:17:20', 1),
	(11, 'Program Users', 'Users who works under program', 0, '2015-01-28 11:23:31', 1),
	(12, 'Admin Users', 'Admin Users', 0, '2015-01-28 11:33:11', 1),
	(13, 'System Admin', 'System Administrator', 0, '2015-01-28 11:40:10', 1);
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.user_roles_on_resources
DROP TABLE IF EXISTS `user_roles_on_resources`;
CREATE TABLE IF NOT EXISTS `user_roles_on_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '1',
  `create` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0',
  `approve` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  KEY `resource_id` (`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=969 DEFAULT CHARSET=latin1 COMMENT='roles on resources (non-functional requirement)';

-- Dumping data for table bremak_manager.user_roles_on_resources: ~920 rows (approximately)
DELETE FROM `user_roles_on_resources`;
/*!40000 ALTER TABLE `user_roles_on_resources` DISABLE KEYS */;
INSERT INTO `user_roles_on_resources` (`id`, `role_id`, `resource_id`, `view`, `create`, `update`, `delete`, `approve`) VALUES
	(1, 1, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
	(2, 1, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
	(11, 1, 'USER_ROLES', 1, 1, 1, 1, 0),
	(12, 1, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
	(18, 1, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
	(19, 1, 'USER_ADMIN', 0, 0, 0, 0, 0),
	(20, 1, 'USER_DEFAULT', 1, 1, 1, 1, 0),
	(21, 2, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
	(22, 2, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
	(23, 2, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
	(32, 2, 'USER_ROLES', 1, 1, 1, 1, 0),
	(33, 2, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
	(40, 2, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
	(41, 2, 'USER_DEFAULT', 1, 1, 1, 1, 0),
	(42, 4, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
	(43, 4, 'SETTINGS_EMAIL', 1, 1, 1, 1, 0),
	(44, 4, 'SETTINGS_GENERAL', 1, 1, 1, 1, 0),
	(53, 4, 'USER_ROLES', 1, 1, 1, 1, 0),
	(54, 4, 'SETTINGS_RUNTIME', 1, 1, 1, 1, 0),
	(61, 4, 'USER_ACTIVITY', 1, 0, 0, 1, 0),
	(62, 4, 'USER_DEFAULT', 1, 1, 1, 1, 0),
	(63, 2, 'USER_LEVELS', 0, 0, 0, 0, 0),
	(64, 2, 'USER_RESOURCES', 0, 0, 0, 0, 0),
	(65, 4, 'MESSAGE', 0, 0, 0, 0, 0),
	(66, 4, 'SETTINGS_TOWN', 0, 0, 0, 0, 0),
	(67, 2, 'MESSAGE', 1, 1, 1, 1, 0),
	(68, 2, 'SETTINGS_TOWN', 1, 1, 1, 1, 0),
	(69, 2, 'INVENTORY_GOODS_RECEIVED_NOTE', 1, 1, 1, 1, 0),
	(70, 2, 'INVENTORY_INVOICE', 1, 1, 1, 1, 0),
	(71, 2, 'INVENTORY_ITEMS', 1, 1, 1, 1, 0),
	(72, 2, 'INVENTORY_LPO', 1, 1, 1, 1, 0),
	(73, 2, 'INVENTORY_PURCHASE', 1, 1, 1, 1, 0),
	(74, 2, 'INVENTORY_REQUISITION', 1, 1, 1, 1, 0),
	(75, 2, 'INVENTORY_STOCK', 1, 1, 1, 1, 0),
	(77, 2, 'SETTINGS_UNITS_OF_MEASURE', 1, 1, 1, 1, 0),
	(78, 1, 'HELP_DOCUMENTATION', 1, 1, 1, 1, 0),
	(79, 1, 'INVENTORY_GOODS_RECEIVED_NOTE', 1, 1, 1, 1, 0),
	(80, 1, 'INVENTORY_INVOICE', 1, 1, 1, 1, 0),
	(81, 1, 'INVENTORY_ITEMS', 1, 1, 1, 1, 0),
	(82, 1, 'INVENTORY_LPO', 1, 1, 1, 1, 0),
	(83, 1, 'INVENTORY_PURCHASE', 1, 1, 1, 1, 0),
	(84, 1, 'INVENTORY_REQUISITION', 1, 1, 1, 1, 0),
	(85, 1, 'INVENTORY_STOCK', 1, 1, 1, 1, 0),
	(92, 1, 'MESSAGE', 1, 1, 1, 1, 0),
	(93, 1, 'QUEUEMANAGER', 1, 1, 1, 0, 0),
	(94, 1, 'SETTINGS_TAXES', 1, 1, 1, 1, 0),
	(95, 1, 'SETTINGS_TOWN', 1, 1, 1, 1, 0),
	(96, 1, 'SETTINGS_UNITS_OF_MEASURE', 1, 1, 1, 1, 0),
	(97, 1, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
	(98, 2, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
	(99, 4, 'HUMANRESOURCES_LEAVES', 1, 1, 1, 1, 1),
	(100, 2, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
	(101, 2, 'EMPLOYEES_BANKS', 0, 0, 0, 0, 0),
	(102, 2, 'EMPLOYEES_CONTACTS', 0, 0, 0, 0, 0),
	(103, 2, 'EMPLOYEES_DEPENDANTS', 0, 0, 0, 0, 0),
	(104, 2, 'EMPLOYEES_EMPLOYEES', 0, 0, 0, 0, 0),
	(105, 2, 'EMPLOYEES_HOUSING', 0, 0, 0, 0, 0),
	(106, 2, 'EMPLOYEES_JOBDETAILS', 0, 0, 0, 0, 0),
	(107, 2, 'EMPLOYEES_QUALIFICATIONS', 0, 0, 0, 0, 0),
	(108, 2, 'EMPLOYEES_SKILLS', 0, 0, 0, 0, 0),
	(109, 2, 'EMPLOYEES_WORK_EXPERIENCE', 0, 0, 0, 0, 0),
	(110, 2, 'FLEET', 1, 1, 1, 1, 0),
	(111, 2, 'HUMANRESOURCES', 0, 0, 0, 0, 0),
	(112, 2, 'ORG_COMPANY', 1, 1, 1, 1, 0),
	(113, 2, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
	(114, 2, 'PAYROLL_EMPSTATUTORY', 0, 0, 0, 0, 0),
	(115, 2, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
	(116, 2, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
	(117, 2, 'PROCUREMENT', 0, 0, 0, 0, 0),
	(118, 2, 'PROCUREMENT_REQUISITION', 1, 1, 1, 1, 0),
	(119, 2, 'PROCUREMENT_SUPPLIERS', 1, 1, 1, 1, 0),
	(120, 2, 'PROCUREMENT_TRAVELREQUEST', 1, 1, 1, 1, 0),
	(121, 2, 'PROGRAM_BUDGETS', 0, 0, 0, 0, 0),
	(122, 2, 'PROGRAM_INDICATORS', 0, 0, 0, 0, 0),
	(123, 2, 'PROGRAM_INDICATOR_ACTIVITIES', 0, 0, 0, 0, 0),
	(124, 2, 'PROGRAM_PROGRAMS', 0, 0, 0, 0, 0),
	(125, 2, 'PROGRAM_PROJECTS', 0, 0, 0, 0, 0),
	(126, 2, 'QUEUEMANAGER', 0, 0, 0, 0, 0),
	(127, 2, 'REQUISITION', 1, 1, 1, 1, 0),
	(128, 2, 'SETTINGS_BANKS', 0, 0, 0, 0, 0),
	(129, 2, 'SETTINGS_BANKS_BRANCHES', 0, 0, 0, 0, 0),
	(130, 2, 'SETTINGS_HOLIDAYS', 0, 0, 0, 0, 0),
	(131, 2, 'SETTINGS_LEAVE_SETTINGS', 0, 0, 0, 0, 0),
	(132, 2, 'SETTINGS_LEAVE_TYPE', 0, 0, 0, 0, 0),
	(133, 2, 'SETTINGS_NOTIFICATION', 0, 0, 0, 0, 0),
	(134, 2, 'SETTINGS_TAXES', 0, 0, 0, 0, 0),
	(135, 2, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
	(136, 2, 'WORKFLOW', 1, 1, 1, 1, 0),
	(137, 2, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
	(138, 2, 'HR_LEAVES', 1, 1, 1, 1, 0),
	(139, 2, 'HR_SETTINGS', 1, 1, 1, 1, 0),
	(140, 2, 'INVENTORY', 1, 1, 1, 1, 0),
	(141, 2, 'PROGRAMS', 1, 1, 1, 1, 0),
	(142, 2, 'SETTINGS', 1, 1, 1, 1, 0),
	(143, 2, 'USERS', 1, 1, 1, 1, 0),
	(144, 1, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
	(145, 1, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
	(146, 1, 'FLEET', 1, 1, 1, 1, 0),
	(147, 1, 'HR_LEAVES', 1, 1, 1, 1, 0),
	(148, 1, 'HR_SETTINGS', 1, 1, 1, 1, 0),
	(149, 1, 'INVENTORY', 1, 1, 1, 1, 0),
	(150, 1, 'ORG_COMPANY', 1, 1, 1, 1, 0),
	(151, 1, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
	(152, 1, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
	(153, 1, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
	(154, 1, 'PROCUREMENT_REQUISITION', 1, 1, 1, 1, 0),
	(155, 1, 'PROCUREMENT_SUPPLIERS', 1, 1, 1, 1, 0),
	(156, 1, 'PROCUREMENT_TRAVELREQUEST', 1, 1, 1, 1, 0),
	(157, 1, 'PROGRAMS', 1, 1, 1, 1, 0),
	(158, 1, 'REQUISITION', 1, 1, 1, 1, 0),
	(159, 1, 'SETTINGS', 1, 1, 1, 1, 0),
	(160, 1, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
	(161, 1, 'USERS', 1, 1, 1, 1, 0),
	(162, 1, 'WORKFLOW', 1, 1, 1, 1, 0),
	(163, 1, 'CLAIMS', 0, 0, 0, 0, 0),
	(164, 1, 'EMPLOYMENT_CATEGORIES', 1, 1, 1, 1, 0),
	(165, 1, 'EMPLOYMENT_CLASS', 1, 1, 1, 1, 0),
	(166, 1, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
	(167, 1, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
	(168, 1, 'LEAVE', 1, 1, 1, 1, 0),
	(169, 1, 'LOCAL_PURCHASE_ORDER', 1, 1, 1, 1, 0),
	(170, 1, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
	(171, 1, 'PERFORMANCE', 1, 1, 1, 1, 0),
	(172, 1, 'PERFORMANCE_SETTINGS', 1, 1, 1, 1, 0),
	(173, 1, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
	(174, 1, 'RFQ_CRITERIA', 1, 1, 1, 1, 0),
	(175, 1, 'RFQ_RESPONSE', 1, 1, 1, 1, 0),
	(176, 1, 'RFQ_SUPPLIERS', 1, 1, 1, 1, 0),
	(177, 1, 'RFQ_VALUATION_DATA', 1, 1, 1, 1, 0),
	(178, 1, 'TIMESHEETS', 1, 1, 1, 1, 0),
	(179, 1, 'VALUATION_PARAMETERS', 1, 1, 1, 1, 0),
	(180, 4, 'CLAIMS', 0, 0, 0, 0, 0),
	(181, 4, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(182, 4, 'EMPLOYEE_DETAILS', 0, 0, 0, 0, 0),
	(183, 4, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(184, 4, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(185, 4, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
	(186, 4, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(187, 4, 'FLEET', 0, 0, 0, 0, 0),
	(188, 4, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(189, 4, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(190, 4, 'INVENTORY', 0, 0, 0, 0, 0),
	(191, 4, 'LEAVE', 0, 0, 0, 0, 0),
	(192, 4, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(193, 4, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(194, 4, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(195, 4, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
	(196, 4, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(197, 4, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(198, 4, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(199, 4, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(200, 4, 'PROGRAMS', 0, 0, 0, 0, 0),
	(201, 4, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
	(202, 4, 'REQUISITION', 0, 0, 0, 0, 0),
	(203, 4, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
	(204, 4, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
	(205, 4, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
	(206, 4, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
	(207, 4, 'SETTINGS', 0, 0, 0, 0, 0),
	(208, 4, 'TIMESHEETS', 0, 0, 0, 0, 0),
	(209, 4, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(210, 4, 'USERS', 0, 0, 0, 0, 0),
	(211, 4, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
	(212, 4, 'WORKFLOW', 0, 0, 0, 0, 0),
	(213, 2, 'CLAIMS', 0, 0, 0, 0, 0),
	(214, 2, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(215, 2, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(216, 2, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
	(217, 2, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(218, 2, 'LEAVE', 1, 1, 1, 1, 0),
	(219, 2, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(220, 2, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
	(221, 2, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(222, 2, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(223, 2, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
	(224, 2, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
	(225, 2, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
	(226, 2, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
	(227, 2, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
	(228, 2, 'TIMESHEETS', 0, 0, 0, 0, 0),
	(229, 2, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
	(230, 2, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(231, 2, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(232, 2, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
	(233, 2, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(234, 2, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(235, 2, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(236, 2, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(237, 2, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(238, 2, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(239, 2, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(240, 2, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(241, 2, 'COUNTRY', 0, 0, 0, 0, 0),
	(242, 2, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(243, 2, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(244, 2, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(245, 2, 'INDICATORS', 0, 0, 0, 0, 0),
	(246, 2, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(247, 2, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(248, 2, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(249, 2, 'PROGRAM', 0, 0, 0, 0, 0),
	(250, 2, 'PROJECT', 0, 0, 0, 0, 0),
	(251, 2, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(252, 2, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
	(253, 2, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(254, 2, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(255, 2, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(256, 2, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(257, 2, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(258, 1, 'ACTIVITY_TYPES', 1, 1, 1, 1, 0),
	(259, 1, 'APPROVE_INVOICE', 1, 1, 1, 1, 0),
	(260, 1, 'AUDIT_TRAIL', 1, 1, 1, 1, 0),
	(261, 1, 'BUDGET_PERIODS', 1, 1, 1, 1, 0),
	(262, 1, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
	(263, 1, 'CONSULTANCY', 1, 1, 1, 1, 0),
	(264, 1, 'CONSULTANTS', 1, 1, 1, 1, 0),
	(265, 1, 'CON_ASC_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(266, 1, 'CON_ASC_CONTRACTS', 1, 1, 1, 1, 0),
	(267, 1, 'CON_ASC_INVOICE', 1, 1, 1, 1, 0),
	(268, 1, 'CON_ASC_JOBS', 1, 1, 1, 1, 0),
	(269, 1, 'CON_ASC_JOB_REPORTS', 1, 1, 1, 1, 0),
	(270, 1, 'CON_ASC_JOB_TYPES', 1, 1, 1, 1, 0),
	(271, 1, 'CON_ASC_PAYMENTS', 1, 1, 1, 1, 0),
	(272, 1, 'CON_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(273, 1, 'CON_ASSIGNMENT_REPORTS', 1, 1, 1, 1, 0),
	(274, 1, 'CON_ENGAGEMENT', 1, 1, 1, 1, 0),
	(275, 1, 'CON_INVOICES', 1, 1, 1, 1, 0),
	(276, 1, 'CON_MAIN_REPORTS', 1, 1, 1, 1, 0),
	(277, 1, 'CON_OPPORTUNITIES', 1, 1, 1, 1, 0),
	(278, 1, 'CON_PAYMENTS', 1, 1, 1, 1, 0),
	(279, 1, 'CON_RESPONSES', 1, 1, 1, 1, 0),
	(280, 1, 'COUNTRY', 1, 1, 1, 1, 0),
	(281, 1, 'COUNTRY_LOCATIONS', 1, 1, 1, 1, 0),
	(282, 1, 'COUNTRY_PROJECTS', 1, 1, 1, 1, 0),
	(283, 1, 'CREATE_INVOICE', 1, 1, 1, 1, 0),
	(284, 1, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
	(285, 1, 'EXPENSE_ITEMS', 1, 1, 1, 1, 0),
	(286, 1, 'FLEET_ODOMETER_READINGS', 1, 1, 1, 1, 0),
	(287, 1, 'FLEET_VEHICLE_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(288, 1, 'FLEET_VEHICLE_BOOKING', 1, 1, 1, 1, 0),
	(289, 1, 'FLEET_VEHICLE_FUELLING', 1, 1, 1, 1, 0),
	(290, 1, 'FLEET_VEHICLE_REQUISITIONS', 1, 1, 1, 1, 0),
	(291, 1, 'FLEET_VEHICLE_USAGE', 1, 1, 1, 1, 0),
	(292, 1, 'INDICATORS', 1, 1, 1, 1, 0),
	(293, 1, 'INDICATOR_TRANSACTIONS', 1, 1, 1, 1, 0),
	(294, 1, 'INDICATOR_TYPES', 1, 1, 1, 1, 0),
	(295, 1, 'INVOICE_MAKE_PAYMENT', 1, 1, 1, 1, 0),
	(296, 1, 'INVOICE_PAYMENT', 1, 1, 1, 1, 0),
	(297, 1, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
	(298, 1, 'MONITORING_REPORTS', 1, 1, 1, 1, 0),
	(299, 1, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
	(300, 1, 'PROGRAM', 1, 1, 1, 1, 0),
	(301, 1, 'PROJECT', 1, 1, 1, 1, 0),
	(302, 1, 'PROJECT_ACTIVITIES', 1, 1, 1, 1, 0),
	(303, 1, 'PROJECT_BUDGETS', 1, 1, 1, 1, 0),
	(304, 1, 'PROJECT_DONATIONS', 1, 1, 1, 1, 0),
	(305, 1, 'PROJECT_DONORS', 1, 1, 1, 1, 0),
	(306, 1, 'PROJECT_DONOR_PAYMENTS', 1, 1, 1, 1, 0),
	(307, 1, 'PROJECT_EXPENDITURE', 1, 1, 1, 1, 0),
	(308, 1, 'REQUISITION_REPORTS', 1, 1, 1, 1, 0),
	(309, 1, 'STRATEGIC_OBJECTIVES', 1, 1, 1, 1, 0),
	(310, 8, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(311, 8, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
	(312, 8, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(313, 8, 'BUDGET_PERIODS', 1, 0, 0, 0, 0),
	(314, 8, 'CASH_PAYMENTS', 1, 1, 1, 0, 0),
	(315, 8, 'CLAIMS', 0, 0, 0, 0, 0),
	(316, 8, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(317, 8, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(318, 8, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(319, 8, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(320, 8, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(321, 8, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(322, 8, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(323, 8, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
	(324, 8, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(325, 8, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(326, 8, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(327, 8, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
	(328, 8, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
	(329, 8, 'CON_INVOICES', 0, 0, 0, 0, 0),
	(330, 8, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
	(331, 8, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
	(332, 8, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
	(333, 8, 'CON_RESPONSES', 0, 0, 0, 0, 0),
	(334, 8, 'COUNTRY', 0, 0, 0, 0, 0),
	(335, 8, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(336, 8, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(337, 8, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
	(338, 8, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
	(339, 8, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
	(340, 8, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(341, 8, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(342, 8, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
	(343, 8, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
	(344, 8, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(345, 8, 'FLEET', 0, 0, 0, 0, 0),
	(346, 8, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
	(347, 8, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(348, 8, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
	(349, 8, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
	(350, 8, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
	(351, 8, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
	(352, 8, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(353, 8, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(354, 8, 'INDICATORS', 0, 0, 0, 0, 0),
	(355, 8, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(356, 8, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(357, 8, 'INVENTORY', 0, 0, 0, 0, 0),
	(358, 8, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
	(359, 8, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
	(360, 8, 'LEAVE', 0, 0, 0, 0, 0),
	(361, 8, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(362, 8, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
	(363, 8, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
	(364, 8, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
	(365, 8, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(366, 8, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(367, 8, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(368, 8, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
	(369, 8, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
	(370, 8, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(371, 8, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(372, 8, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
	(373, 8, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(374, 8, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(375, 8, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
	(376, 8, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
	(377, 8, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
	(378, 8, 'PF_REVIEWS', 0, 0, 0, 0, 0),
	(379, 8, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
	(380, 8, 'PROGRAM', 0, 0, 0, 0, 0),
	(381, 8, 'PROGRAMS', 0, 0, 0, 0, 0),
	(382, 8, 'PROJECT', 0, 0, 0, 0, 0),
	(383, 8, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(384, 8, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
	(385, 8, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(386, 8, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(387, 8, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(388, 8, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(389, 8, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
	(390, 8, 'REQUEST_FOR_QUOTATION', 1, 0, 1, 1, 0),
	(391, 8, 'REQUISITION', 1, 1, 1, 1, 0),
	(392, 8, 'REQUISITION_REPORTS', 0, 0, 0, 0, 0),
	(393, 8, 'RES_RFQ_EVAL_CRITERIA', 0, 0, 0, 0, 0),
	(394, 8, 'RFQ', 1, 0, 0, 0, 0),
	(395, 8, 'RFQ_AWARD', 1, 0, 0, 0, 0),
	(396, 8, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
	(397, 8, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
	(398, 8, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
	(399, 8, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
	(400, 8, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
	(401, 8, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
	(402, 8, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
	(403, 8, 'SETTINGS', 0, 0, 0, 0, 0),
	(404, 8, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(405, 8, 'TIMESHEETS', 0, 0, 0, 0, 0),
	(406, 8, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
	(407, 8, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
	(408, 8, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(409, 8, 'USERS', 0, 0, 0, 0, 0),
	(410, 8, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(411, 8, 'USER_ROLES', 0, 0, 0, 0, 0),
	(412, 8, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
	(413, 8, 'WORKFLOW', 0, 0, 0, 0, 0),
	(414, 1, 'EMPLOYEE_LIST', 1, 1, 1, 1, 0),
	(415, 1, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
	(416, 1, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
	(417, 1, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
	(418, 1, 'PF_CRITICAL_PERFORMANCE_FACTORS', 1, 1, 1, 1, 0),
	(419, 1, 'PF_KEY_PERFORMANCE_AREAS', 1, 1, 1, 1, 0),
	(420, 1, 'PF_RATING_DESCRIPTION', 1, 1, 1, 1, 0),
	(421, 1, 'PF_REVIEWS', 1, 1, 1, 1, 0),
	(422, 1, 'PF_REVIEW_REPORTS', 1, 1, 1, 1, 0),
	(423, 1, 'PUBLIC_HOLIDAYS', 1, 1, 1, 1, 0),
	(424, 1, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 1, 0),
	(425, 1, 'RFQ', 1, 1, 1, 1, 0),
	(426, 1, 'RFQ_AWARD', 1, 1, 1, 1, 0),
	(427, 1, 'RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
	(428, 1, 'RFQ_REQUIREMENTS', 1, 1, 1, 1, 0),
	(429, 1, 'RFQ_SUPPLIER_RESPONSE', 1, 1, 1, 1, 0),
	(430, 1, 'TIMESHEET_REPORTS', 1, 1, 1, 1, 0),
	(431, 1, 'TIMESHEET_TIMEOFF_ITEMS', 1, 1, 1, 1, 0),
	(432, 8, 'EMPLOYEE_LIST', 0, 0, 0, 0, 0),
	(433, 9, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(434, 9, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
	(435, 9, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(436, 9, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
	(437, 9, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
	(438, 9, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(439, 9, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(440, 9, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(441, 9, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(442, 9, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(443, 9, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(444, 9, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(445, 9, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
	(446, 9, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(447, 9, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(448, 9, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(449, 9, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
	(450, 9, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
	(451, 9, 'CON_INVOICES', 0, 0, 0, 0, 0),
	(452, 9, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
	(453, 9, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
	(454, 9, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
	(455, 9, 'CON_RESPONSES', 0, 0, 0, 0, 0),
	(456, 9, 'COUNTRY', 0, 0, 0, 0, 0),
	(457, 9, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(458, 9, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(459, 9, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
	(460, 9, 'CREATE_RECONCILIATION', 0, 0, 0, 0, 0),
	(461, 9, 'EMPLOYEE_DETAILS', 1, 0, 0, 0, 0),
	(462, 9, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
	(463, 9, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(464, 9, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(465, 9, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
	(466, 9, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(467, 9, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(468, 9, 'FLEET', 0, 0, 0, 0, 0),
	(469, 9, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
	(470, 9, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(471, 9, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
	(472, 9, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
	(473, 9, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
	(474, 9, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
	(475, 9, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(476, 9, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(477, 9, 'INDICATORS', 0, 0, 0, 0, 0),
	(478, 9, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(479, 9, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(480, 9, 'INVENTORY', 0, 0, 0, 0, 0),
	(481, 9, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
	(482, 9, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
	(483, 9, 'LEAVE', 0, 0, 0, 0, 0),
	(484, 9, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(485, 9, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
	(486, 9, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
	(487, 9, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
	(488, 9, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(489, 9, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(490, 9, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(491, 9, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
	(492, 9, 'PAYMENT_PAYTYPE', 1, 1, 1, 0, 0),
	(493, 9, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(494, 9, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(495, 9, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
	(496, 9, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(497, 9, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(498, 9, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
	(499, 9, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
	(500, 9, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
	(501, 9, 'PF_REVIEWS', 0, 0, 0, 0, 0),
	(502, 9, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
	(503, 9, 'PROGRAM', 0, 0, 0, 0, 0),
	(504, 9, 'PROGRAMS', 0, 0, 0, 0, 0),
	(505, 9, 'PROJECT', 0, 0, 0, 0, 0),
	(506, 9, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(507, 9, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
	(508, 9, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(509, 9, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(510, 9, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(511, 9, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(512, 9, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
	(513, 9, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
	(514, 9, 'REQUISITION', 1, 1, 1, 1, 0),
	(515, 9, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
	(516, 9, 'RES_RFQ_EVAL_CRITERIA', 1, 0, 0, 0, 0),
	(517, 9, 'RFQ', 1, 0, 0, 0, 0),
	(518, 9, 'RFQ_AWARD', 1, 0, 0, 0, 0),
	(519, 9, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
	(520, 9, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
	(521, 9, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
	(522, 9, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
	(523, 9, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
	(524, 9, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
	(525, 9, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
	(526, 9, 'SETTINGS', 0, 0, 0, 0, 0),
	(527, 9, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(528, 9, 'TIMESHEETS', 1, 1, 1, 1, 0),
	(529, 9, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
	(530, 9, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
	(531, 9, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(532, 9, 'USERS', 0, 0, 0, 0, 0),
	(533, 9, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(534, 9, 'USER_ROLES', 0, 0, 0, 0, 0),
	(535, 9, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
	(536, 9, 'WORKFLOW', 0, 0, 0, 0, 0),
	(537, 10, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(538, 10, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
	(539, 10, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(540, 10, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
	(541, 10, 'CASH_PAYMENTS', 1, 1, 1, 0, 0),
	(542, 10, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(543, 10, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(544, 10, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(545, 10, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(546, 10, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(547, 10, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(548, 10, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(549, 10, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
	(550, 10, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(551, 10, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(552, 10, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(553, 10, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
	(554, 10, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
	(555, 10, 'CON_INVOICES', 0, 0, 0, 0, 0),
	(556, 10, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
	(557, 10, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
	(558, 10, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
	(559, 10, 'CON_RESPONSES', 0, 0, 0, 0, 0),
	(560, 10, 'COUNTRY', 0, 0, 0, 0, 0),
	(561, 10, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(562, 10, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(563, 10, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
	(564, 10, 'CREATE_RECONCILIATION', 0, 0, 0, 0, 0),
	(565, 10, 'EMPLOYEE_DETAILS', 1, 0, 0, 0, 0),
	(566, 10, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
	(567, 10, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(568, 10, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(569, 10, 'EVENT_REMINDERS', 1, 1, 0, 0, 0),
	(570, 10, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(571, 10, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(572, 10, 'FLEET', 0, 0, 0, 0, 0),
	(573, 10, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
	(574, 10, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(575, 10, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
	(576, 10, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
	(577, 10, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
	(578, 10, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
	(579, 10, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(580, 10, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(581, 10, 'INDICATORS', 0, 0, 0, 0, 0),
	(582, 10, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(583, 10, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(584, 10, 'INVENTORY', 0, 0, 0, 0, 0),
	(585, 10, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
	(586, 10, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
	(587, 10, 'LEAVE', 0, 0, 0, 0, 0),
	(588, 10, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(589, 10, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 0, 0),
	(590, 10, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
	(591, 10, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
	(592, 10, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(593, 10, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(594, 10, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(595, 10, 'PAYMENT_LIST', 1, 1, 1, 0, 0),
	(596, 10, 'PAYMENT_PAYTYPE', 1, 1, 0, 0, 0),
	(597, 10, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(598, 10, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(599, 10, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
	(600, 10, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(601, 10, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(602, 10, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
	(603, 10, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
	(604, 10, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
	(605, 10, 'PF_REVIEWS', 0, 0, 0, 0, 0),
	(606, 10, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
	(607, 10, 'PROGRAM', 0, 0, 0, 0, 0),
	(608, 10, 'PROGRAMS', 0, 0, 0, 0, 0),
	(609, 10, 'PROJECT', 0, 0, 0, 0, 0),
	(610, 10, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(611, 10, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
	(612, 10, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(613, 10, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(614, 10, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(615, 10, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(616, 10, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
	(617, 10, 'REQUEST_FOR_QUOTATION', 1, 0, 0, 0, 0),
	(618, 10, 'REQUISITION', 0, 0, 0, 0, 0),
	(619, 10, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
	(620, 10, 'RES_RFQ_EVAL_CRITERIA', 1, 0, 0, 0, 0),
	(621, 10, 'RFQ', 1, 0, 0, 0, 0),
	(622, 10, 'RFQ_AWARD', 1, 0, 0, 0, 0),
	(623, 10, 'RFQ_BID_ANALYSIS', 1, 0, 0, 0, 0),
	(624, 10, 'RFQ_CRITERIA', 1, 0, 0, 0, 0),
	(625, 10, 'RFQ_REQUIREMENTS', 1, 0, 0, 0, 0),
	(626, 10, 'RFQ_RESPONSE', 1, 0, 0, 0, 0),
	(627, 10, 'RFQ_SUPPLIERS', 1, 0, 0, 0, 0),
	(628, 10, 'RFQ_SUPPLIER_RESPONSE', 1, 0, 0, 0, 0),
	(629, 10, 'RFQ_VALUATION_DATA', 1, 0, 0, 0, 0),
	(630, 10, 'SETTINGS', 0, 0, 0, 0, 0),
	(631, 10, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(632, 10, 'TIMESHEETS', 0, 0, 0, 0, 0),
	(633, 10, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
	(634, 10, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
	(635, 10, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(636, 10, 'USERS', 0, 0, 0, 0, 0),
	(637, 10, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(638, 10, 'USER_ROLES', 0, 0, 0, 0, 0),
	(639, 10, 'VALUATION_PARAMETERS', 1, 0, 0, 0, 0),
	(640, 10, 'WORKFLOW', 0, 0, 0, 0, 0),
	(641, 11, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(642, 11, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
	(643, 11, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(644, 11, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
	(645, 11, 'CASH_PAYMENTS', 0, 0, 0, 0, 0),
	(646, 11, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(647, 11, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(648, 11, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(649, 11, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(650, 11, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(651, 11, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(652, 11, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(653, 11, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
	(654, 11, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(655, 11, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(656, 11, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(657, 11, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
	(658, 11, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
	(659, 11, 'CON_INVOICES', 0, 0, 0, 0, 0),
	(660, 11, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
	(661, 11, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
	(662, 11, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
	(663, 11, 'CON_RESPONSES', 0, 0, 0, 0, 0),
	(664, 11, 'COUNTRY', 0, 0, 0, 0, 0),
	(665, 11, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(666, 11, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(667, 11, 'CREATE_INVOICE', 1, 0, 0, 0, 0),
	(668, 11, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
	(669, 11, 'EMPLOYEE_DETAILS', 0, 0, 1, 0, 0),
	(670, 11, 'EMPLOYEE_LIST', 1, 0, 0, 0, 0),
	(671, 11, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(672, 11, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(673, 11, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
	(674, 11, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(675, 11, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(676, 11, 'FLEET', 0, 0, 0, 0, 0),
	(677, 11, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
	(678, 11, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(679, 11, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
	(680, 11, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
	(681, 11, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
	(682, 11, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
	(683, 11, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(684, 11, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(685, 11, 'INDICATORS', 0, 0, 0, 0, 0),
	(686, 11, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(687, 11, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(688, 11, 'INVENTORY', 0, 0, 0, 0, 0),
	(689, 11, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
	(690, 11, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
	(691, 11, 'LEAVE', 0, 0, 0, 0, 0),
	(692, 11, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(693, 11, 'MAKE_ADVANCE_PAYMENT', 0, 0, 0, 0, 0),
	(694, 11, 'MENU_RFQ_AWARD', 0, 0, 0, 0, 0),
	(695, 11, 'MENU_RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
	(696, 11, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(697, 11, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(698, 11, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(699, 11, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
	(700, 11, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
	(701, 11, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(702, 11, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(703, 11, 'PENDING_ORDER_ISSUE', 0, 0, 0, 0, 0),
	(704, 11, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(705, 11, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(706, 11, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
	(707, 11, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
	(708, 11, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
	(709, 11, 'PF_REVIEWS', 0, 0, 0, 0, 0),
	(710, 11, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
	(711, 11, 'PROGRAM', 0, 0, 0, 0, 0),
	(712, 11, 'PROGRAMS', 0, 0, 0, 0, 0),
	(713, 11, 'PROJECT', 0, 0, 0, 0, 0),
	(714, 11, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(715, 11, 'PROJECT_BUDGETS', 1, 0, 0, 0, 0),
	(716, 11, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(717, 11, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(718, 11, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(719, 11, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(720, 11, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
	(721, 11, 'REQUEST_FOR_QUOTATION', 0, 0, 0, 0, 0),
	(722, 11, 'REQUISITION', 1, 1, 1, 1, 0),
	(723, 11, 'REQUISITION_REPORTS', 1, 0, 0, 0, 0),
	(724, 11, 'RES_RFQ_EVAL_CRITERIA', 0, 0, 0, 0, 0),
	(725, 11, 'RFQ', 0, 0, 0, 0, 0),
	(726, 11, 'RFQ_AWARD', 0, 0, 0, 0, 0),
	(727, 11, 'RFQ_BID_ANALYSIS', 0, 0, 0, 0, 0),
	(728, 11, 'RFQ_CRITERIA', 0, 0, 0, 0, 0),
	(729, 11, 'RFQ_REQUIREMENTS', 0, 0, 0, 0, 0),
	(730, 11, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
	(731, 11, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
	(732, 11, 'RFQ_SUPPLIER_RESPONSE', 0, 0, 0, 0, 0),
	(733, 11, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
	(734, 11, 'SETTINGS', 0, 0, 0, 0, 0),
	(735, 11, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(736, 11, 'TIMESHEETS', 1, 0, 0, 0, 0),
	(737, 11, 'TIMESHEET_REPORTS', 1, 0, 0, 0, 0),
	(738, 11, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
	(739, 11, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(740, 11, 'USERS', 0, 0, 0, 0, 0),
	(741, 11, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(742, 11, 'USER_ROLES', 0, 0, 0, 0, 0),
	(743, 11, 'VALUATION_PARAMETERS', 0, 0, 0, 0, 0),
	(744, 11, 'WORKFLOW', 0, 0, 0, 0, 0),
	(745, 12, 'ACTIVITY_TYPES', 0, 0, 0, 0, 0),
	(746, 12, 'APPROVE_INVOICE', 0, 0, 0, 0, 0),
	(747, 12, 'AUDIT_TRAIL', 0, 0, 0, 0, 0),
	(748, 12, 'BUDGET_PERIODS', 0, 0, 0, 0, 0),
	(749, 12, 'CASH_PAYMENTS', 0, 0, 0, 0, 0),
	(750, 12, 'COMPANY_DOCS', 0, 0, 0, 0, 0),
	(751, 12, 'CONSULTANCY', 0, 0, 0, 0, 0),
	(752, 12, 'CONSULTANTS', 0, 0, 0, 0, 0),
	(753, 12, 'CON_ASC_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(754, 12, 'CON_ASC_CONTRACTS', 0, 0, 0, 0, 0),
	(755, 12, 'CON_ASC_INVOICE', 0, 0, 0, 0, 0),
	(756, 12, 'CON_ASC_JOBS', 0, 0, 0, 0, 0),
	(757, 12, 'CON_ASC_JOB_REPORTS', 0, 0, 0, 0, 0),
	(758, 12, 'CON_ASC_JOB_TYPES', 0, 0, 0, 0, 0),
	(759, 12, 'CON_ASC_PAYMENTS', 0, 0, 0, 0, 0),
	(760, 12, 'CON_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(761, 12, 'CON_ASSIGNMENT_REPORTS', 0, 0, 0, 0, 0),
	(762, 12, 'CON_ENGAGEMENT', 0, 0, 0, 0, 0),
	(763, 12, 'CON_INVOICES', 0, 0, 0, 0, 0),
	(764, 12, 'CON_MAIN_REPORTS', 0, 0, 0, 0, 0),
	(765, 12, 'CON_OPPORTUNITIES', 0, 0, 0, 0, 0),
	(766, 12, 'CON_PAYMENTS', 0, 0, 0, 0, 0),
	(767, 12, 'CON_RESPONSES', 0, 0, 0, 0, 0),
	(768, 12, 'COUNTRY', 0, 0, 0, 0, 0),
	(769, 12, 'COUNTRY_LOCATIONS', 0, 0, 0, 0, 0),
	(770, 12, 'COUNTRY_PROJECTS', 0, 0, 0, 0, 0),
	(771, 12, 'CREATE_INVOICE', 0, 0, 0, 0, 0),
	(772, 12, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
	(773, 12, 'EMPLOYEE_DETAILS', 0, 0, 0, 0, 0),
	(774, 12, 'EMPLOYEE_LIST', 0, 0, 0, 0, 0),
	(775, 12, 'EMPLOYMENT_CATEGORIES', 0, 0, 0, 0, 0),
	(776, 12, 'EMPLOYMENT_CLASS', 0, 0, 0, 0, 0),
	(777, 12, 'EVENT_REMINDERS', 0, 0, 0, 0, 0),
	(778, 12, 'EXPENSE_ADVANCE', 0, 0, 0, 0, 0),
	(779, 12, 'EXPENSE_ITEMS', 0, 0, 0, 0, 0),
	(780, 12, 'FLEET', 0, 0, 0, 0, 0),
	(781, 12, 'FLEET_ODOMETER_READINGS', 0, 0, 0, 0, 0),
	(782, 12, 'FLEET_VEHICLE_ASSIGNMENTS', 0, 0, 0, 0, 0),
	(783, 12, 'FLEET_VEHICLE_BOOKING', 0, 0, 0, 0, 0),
	(784, 12, 'FLEET_VEHICLE_FUELLING', 0, 0, 0, 0, 0),
	(785, 12, 'FLEET_VEHICLE_REQUISITIONS', 0, 0, 0, 0, 0),
	(786, 12, 'FLEET_VEHICLE_USAGE', 0, 0, 0, 0, 0),
	(787, 12, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(788, 12, 'HR_SETTINGS', 0, 0, 0, 0, 0),
	(789, 12, 'INDICATORS', 0, 0, 0, 0, 0),
	(790, 12, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(791, 12, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(792, 12, 'INVENTORY', 0, 0, 0, 0, 0),
	(793, 12, 'INVOICE_MAKE_PAYMENT', 0, 0, 0, 0, 0),
	(794, 12, 'INVOICE_PAYMENT', 0, 0, 0, 0, 0),
	(795, 12, 'LEAVE', 0, 0, 0, 0, 0),
	(796, 12, 'LOCAL_PURCHASE_ORDER', 0, 0, 0, 0, 0),
	(797, 12, 'MAKE_ADVANCE_PAYMENT', 0, 0, 0, 0, 0),
	(798, 12, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
	(799, 12, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
	(800, 12, 'MONITORING_REPORTS', 0, 0, 0, 0, 0),
	(801, 12, 'ORG_COMPANY', 0, 0, 0, 0, 0),
	(802, 12, 'ORG_STRUCTURE', 0, 0, 0, 0, 0),
	(803, 12, 'PAYMENT_LIST', 0, 0, 0, 0, 0),
	(804, 12, 'PAYMENT_PAYTYPE', 0, 0, 0, 0, 0),
	(805, 12, 'PAYROLL_SETTINGS', 0, 0, 0, 0, 0),
	(806, 12, 'PAYROLL_TRANSACTIONS', 0, 0, 0, 0, 0),
	(807, 12, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
	(808, 12, 'PERFORMANCE', 0, 0, 0, 0, 0),
	(809, 12, 'PERFORMANCE_SETTINGS', 0, 0, 0, 0, 0),
	(810, 12, 'PF_CRITICAL_PERFORMANCE_FACTORS', 0, 0, 0, 0, 0),
	(811, 12, 'PF_KEY_PERFORMANCE_AREAS', 0, 0, 0, 0, 0),
	(812, 12, 'PF_RATING_DESCRIPTION', 0, 0, 0, 0, 0),
	(813, 12, 'PF_REVIEWS', 0, 0, 0, 0, 0),
	(814, 12, 'PF_REVIEW_REPORTS', 0, 0, 0, 0, 0),
	(815, 12, 'PROGRAM', 0, 0, 0, 0, 0),
	(816, 12, 'PROGRAMS', 0, 0, 0, 0, 0),
	(817, 12, 'PROJECT', 0, 0, 0, 0, 0),
	(818, 12, 'PROJECT_ACTIVITIES', 0, 0, 0, 0, 0),
	(819, 12, 'PROJECT_BUDGETS', 0, 0, 0, 0, 0),
	(820, 12, 'PROJECT_DONATIONS', 0, 0, 0, 0, 0),
	(821, 12, 'PROJECT_DONORS', 0, 0, 0, 0, 0),
	(822, 12, 'PROJECT_DONOR_PAYMENTS', 0, 0, 0, 0, 0),
	(823, 12, 'PROJECT_EXPENDITURE', 0, 0, 0, 0, 0),
	(824, 12, 'PUBLIC_HOLIDAYS', 0, 0, 0, 0, 0),
	(825, 12, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
	(826, 12, 'REQUISITION', 1, 1, 1, 1, 0),
	(827, 12, 'REQUISITION_REPORTS', 1, 1, 1, 0, 0),
	(828, 12, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 0, 0),
	(829, 12, 'RFQ', 1, 1, 1, 0, 0),
	(830, 12, 'RFQ_AWARD', 1, 1, 1, 0, 0),
	(831, 12, 'RFQ_BID_ANALYSIS', 1, 1, 1, 0, 0),
	(832, 12, 'RFQ_CRITERIA', 1, 1, 1, 0, 0),
	(833, 12, 'RFQ_REQUIREMENTS', 1, 1, 1, 0, 0),
	(834, 12, 'RFQ_RESPONSE', 1, 1, 1, 0, 0),
	(835, 12, 'RFQ_SUPPLIERS', 1, 1, 1, 0, 0),
	(836, 12, 'RFQ_SUPPLIER_RESPONSE', 1, 1, 1, 0, 0),
	(837, 12, 'RFQ_VALUATION_DATA', 1, 1, 1, 0, 0),
	(838, 12, 'SETTINGS', 0, 0, 0, 0, 0),
	(839, 12, 'STRATEGIC_OBJECTIVES', 0, 0, 0, 0, 0),
	(840, 12, 'TIMESHEETS', 0, 0, 0, 0, 0),
	(841, 12, 'TIMESHEET_REPORTS', 0, 0, 0, 0, 0),
	(842, 12, 'TIMESHEET_TIMEOFF_ITEMS', 0, 0, 0, 0, 0),
	(843, 12, 'TRAVEL_REQUISITION', 0, 0, 0, 0, 0),
	(844, 12, 'USERS', 0, 0, 0, 0, 0),
	(845, 12, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(846, 12, 'USER_ROLES', 0, 0, 0, 0, 0),
	(847, 12, 'VALUATION_PARAMETERS', 1, 1, 1, 0, 0),
	(848, 12, 'WORKFLOW', 0, 0, 0, 0, 0),
	(849, 13, 'ACTIVITY_TYPES', 1, 1, 1, 1, 0),
	(850, 13, 'APPROVE_INVOICE', 1, 1, 1, 1, 0),
	(851, 13, 'AUDIT_TRAIL', 1, 1, 1, 1, 0),
	(852, 13, 'BUDGET_PERIODS', 1, 1, 1, 1, 0),
	(853, 13, 'CASH_PAYMENTS', 1, 1, 1, 1, 0),
	(854, 13, 'COMPANY_DOCS', 1, 1, 1, 1, 0),
	(855, 13, 'CONSULTANCY', 1, 1, 1, 1, 0),
	(856, 13, 'CONSULTANTS', 1, 1, 1, 1, 0),
	(857, 13, 'CON_ASC_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(858, 13, 'CON_ASC_CONTRACTS', 1, 1, 1, 1, 0),
	(859, 13, 'CON_ASC_INVOICE', 1, 1, 1, 1, 0),
	(860, 13, 'CON_ASC_JOBS', 1, 1, 1, 1, 0),
	(861, 13, 'CON_ASC_JOB_REPORTS', 1, 1, 1, 1, 0),
	(862, 13, 'CON_ASC_JOB_TYPES', 1, 1, 1, 1, 0),
	(863, 13, 'CON_ASC_PAYMENTS', 1, 1, 1, 1, 0),
	(864, 13, 'CON_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(865, 13, 'CON_ASSIGNMENT_REPORTS', 1, 1, 1, 1, 0),
	(866, 13, 'CON_ENGAGEMENT', 1, 1, 1, 1, 0),
	(867, 13, 'CON_INVOICES', 1, 1, 1, 1, 0),
	(868, 13, 'CON_MAIN_REPORTS', 1, 1, 1, 1, 0),
	(869, 13, 'CON_OPPORTUNITIES', 1, 1, 1, 1, 0),
	(870, 13, 'CON_PAYMENTS', 1, 1, 1, 1, 0),
	(871, 13, 'CON_RESPONSES', 1, 1, 1, 1, 0),
	(872, 13, 'COUNTRY', 1, 1, 1, 1, 0),
	(873, 13, 'COUNTRY_LOCATIONS', 1, 1, 1, 1, 0),
	(874, 13, 'COUNTRY_PROJECTS', 1, 1, 1, 1, 0),
	(875, 13, 'CREATE_INVOICE', 1, 1, 1, 1, 0),
	(876, 13, 'CREATE_RECONCILIATION', 1, 1, 1, 1, 0),
	(877, 13, 'EMPLOYEE_DETAILS', 1, 1, 1, 1, 0),
	(878, 13, 'EMPLOYEE_LIST', 1, 1, 1, 1, 0),
	(879, 13, 'EMPLOYMENT_CATEGORIES', 1, 1, 1, 1, 0),
	(880, 13, 'EMPLOYMENT_CLASS', 1, 1, 1, 1, 0),
	(881, 13, 'EVENT_REMINDERS', 1, 1, 1, 1, 0),
	(882, 13, 'EXPENSE_ADVANCE', 1, 1, 1, 1, 0),
	(883, 13, 'EXPENSE_ITEMS', 1, 1, 1, 1, 0),
	(884, 13, 'FLEET', 1, 1, 1, 1, 0),
	(885, 13, 'FLEET_ODOMETER_READINGS', 1, 1, 1, 1, 0),
	(886, 13, 'FLEET_VEHICLE_ASSIGNMENTS', 1, 1, 1, 1, 0),
	(887, 13, 'FLEET_VEHICLE_BOOKING', 1, 1, 1, 1, 0),
	(888, 13, 'FLEET_VEHICLE_FUELLING', 1, 1, 1, 1, 0),
	(889, 13, 'FLEET_VEHICLE_REQUISITIONS', 1, 1, 1, 1, 0),
	(890, 13, 'FLEET_VEHICLE_USAGE', 1, 1, 1, 1, 0),
	(891, 13, 'HR_LEAVES', 0, 0, 0, 0, 0),
	(892, 13, 'HR_SETTINGS', 1, 1, 1, 1, 0),
	(893, 13, 'INDICATORS', 1, 1, 1, 1, 0),
	(894, 13, 'INDICATOR_TRANSACTIONS', 0, 0, 0, 0, 0),
	(895, 13, 'INDICATOR_TYPES', 0, 0, 0, 0, 0),
	(896, 13, 'INVENTORY', 1, 1, 1, 1, 0),
	(897, 13, 'INVOICE_MAKE_PAYMENT', 1, 1, 1, 1, 0),
	(898, 13, 'INVOICE_PAYMENT', 1, 1, 1, 1, 0),
	(899, 13, 'LEAVE', 1, 1, 1, 1, 0),
	(900, 13, 'LOCAL_PURCHASE_ORDER', 1, 1, 1, 1, 0),
	(901, 13, 'MAKE_ADVANCE_PAYMENT', 1, 1, 1, 1, 0),
	(902, 13, 'MENU_RFQ_AWARD', 1, 1, 1, 1, 0),
	(903, 13, 'MENU_RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
	(904, 13, 'MONITORING_REPORTS', 1, 1, 1, 1, 0),
	(905, 13, 'ORG_COMPANY', 1, 1, 1, 1, 0),
	(906, 13, 'ORG_STRUCTURE', 1, 1, 1, 1, 0),
	(907, 13, 'PAYMENT_LIST', 1, 1, 1, 1, 0),
	(908, 13, 'PAYMENT_PAYTYPE', 1, 1, 1, 1, 0),
	(909, 13, 'PAYROLL_SETTINGS', 1, 1, 1, 1, 0),
	(910, 13, 'PAYROLL_TRANSACTIONS', 1, 1, 1, 1, 0),
	(911, 13, 'PENDING_ORDER_ISSUE', 1, 1, 1, 1, 0),
	(912, 13, 'PERFORMANCE', 1, 1, 1, 1, 0),
	(913, 13, 'PERFORMANCE_SETTINGS', 1, 1, 1, 1, 0),
	(914, 13, 'PF_CRITICAL_PERFORMANCE_FACTORS', 1, 1, 1, 1, 0),
	(915, 13, 'PF_KEY_PERFORMANCE_AREAS', 1, 1, 1, 1, 0),
	(916, 13, 'PF_RATING_DESCRIPTION', 1, 1, 1, 1, 0),
	(917, 13, 'PF_REVIEWS', 1, 1, 1, 1, 0),
	(918, 13, 'PF_REVIEW_REPORTS', 1, 1, 1, 1, 0),
	(919, 13, 'PROGRAM', 1, 1, 1, 1, 0),
	(920, 13, 'PROGRAMS', 1, 1, 1, 1, 0),
	(921, 13, 'PROJECT', 1, 1, 1, 1, 0),
	(922, 13, 'PROJECT_ACTIVITIES', 1, 1, 1, 1, 0),
	(923, 13, 'PROJECT_BUDGETS', 1, 1, 1, 1, 0),
	(924, 13, 'PROJECT_DONATIONS', 1, 1, 1, 1, 0),
	(925, 13, 'PROJECT_DONORS', 1, 1, 1, 1, 0),
	(926, 13, 'PROJECT_DONOR_PAYMENTS', 1, 1, 1, 1, 0),
	(927, 13, 'PROJECT_EXPENDITURE', 1, 1, 1, 1, 0),
	(928, 13, 'PUBLIC_HOLIDAYS', 1, 1, 1, 1, 0),
	(929, 13, 'REQUEST_FOR_QUOTATION', 1, 1, 1, 1, 0),
	(930, 13, 'REQUISITION', 1, 1, 1, 1, 0),
	(931, 13, 'REQUISITION_REPORTS', 1, 1, 1, 1, 0),
	(932, 13, 'RES_RFQ_EVAL_CRITERIA', 1, 1, 1, 1, 0),
	(933, 13, 'RFQ', 1, 1, 1, 1, 0),
	(934, 13, 'RFQ_AWARD', 1, 1, 1, 1, 0),
	(935, 13, 'RFQ_BID_ANALYSIS', 1, 1, 1, 1, 0),
	(936, 13, 'RFQ_CRITERIA', 1, 1, 1, 1, 0),
	(937, 13, 'RFQ_REQUIREMENTS', 1, 1, 1, 1, 0),
	(938, 13, 'RFQ_RESPONSE', 0, 0, 0, 0, 0),
	(939, 13, 'RFQ_SUPPLIERS', 0, 0, 0, 0, 0),
	(940, 13, 'RFQ_SUPPLIER_RESPONSE', 0, 0, 0, 1, 0),
	(941, 13, 'RFQ_VALUATION_DATA', 0, 0, 0, 0, 0),
	(942, 13, 'SETTINGS', 0, 0, 0, 0, 0),
	(943, 13, 'STRATEGIC_OBJECTIVES', 1, 1, 1, 1, 0),
	(944, 13, 'TIMESHEETS', 1, 1, 1, 1, 0),
	(945, 13, 'TIMESHEET_REPORTS', 1, 1, 1, 1, 0),
	(946, 13, 'TIMESHEET_TIMEOFF_ITEMS', 1, 1, 1, 1, 0),
	(947, 13, 'TRAVEL_REQUISITION', 1, 1, 1, 1, 0),
	(948, 13, 'USERS', 1, 1, 1, 0, 0),
	(949, 13, 'USER_ACTIVITY', 0, 0, 0, 0, 0),
	(950, 13, 'USER_ROLES', 1, 1, 1, 1, 0),
	(951, 13, 'VALUATION_PARAMETERS', 1, 1, 1, 1, 0),
	(952, 13, 'WORKFLOW', 1, 1, 1, 1, 0),
	(953, 11, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
	(954, 12, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
	(955, 1, 'CHEQUE_VOUCHER', 1, 1, 1, 1, 0),
	(956, 9, 'CHEQUE_VOUCHER', 1, 1, 1, 1, 0),
	(957, 9, 'BACKUP', 0, 0, 0, 0, 0),
	(958, 10, 'BACKUP', 0, 0, 0, 0, 0),
	(959, 10, 'CHEQUE_VOUCHER', 1, 1, 1, 0, 0),
	(960, 11, 'BACKUP', 0, 0, 0, 0, 0),
	(961, 8, 'BACKUP', 0, 0, 0, 0, 0),
	(962, 8, 'CHEQUE_VOUCHER', 1, 1, 1, 0, 0),
	(963, 13, 'BACKUP', 0, 0, 0, 0, 0),
	(964, 13, 'CHEQUE_VOUCHER', 0, 0, 0, 0, 0),
	(965, 13, 'RES_DOCS', 1, 1, 1, 1, 1),
	(966, 13, 'ME_ACTIVITY_PLANS', 0, 0, 0, 0, 0),
	(967, 13, 'RES_DOC', 1, 1, 1, 1, 0),
	(968, 13, 'RES_TRAINING', 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `user_roles_on_resources` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.wf_actionhistory
DROP TABLE IF EXISTS `wf_actionhistory`;
CREATE TABLE IF NOT EXISTS `wf_actionhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `approver_id` int(11) NOT NULL,
  `resource_id` varchar(64) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `levels` (`item_id`),
  KEY `actions` (`action_id`),
  KEY `emp` (`approver_id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.wf_actionhistory: ~161 rows (approximately)
DELETE FROM `wf_actionhistory`;
/*!40000 ALTER TABLE `wf_actionhistory` DISABLE KEYS */;
INSERT INTO `wf_actionhistory` (`id`, `item_id`, `action_id`, `reason`, `approver_id`, `resource_id`, `date_created`, `created_by`) VALUES
	(1, 3, 1, 'okay', 7, 'REQUISITION', '2015-01-21 11:17:31', 7),
	(2, 1, 1, 'Activity can go on for the training of 130 Pee Educators', 5, 'REQUISITION', '2015-01-21 13:58:52', 5),
	(3, 1, 1, 'Activity can go on for the training of 130 Pee Educators', 5, 'REQUISITION', '2015-01-21 15:08:49', 5),
	(4, 1, 1, 'Approved', 7, 'REQUISITION', '2015-01-21 15:54:06', 7),
	(5, 2, 1, 'Approved', 5, 'REQUISITION', '2015-01-21 16:34:40', 5),
	(6, 3, 2, 'Rejected', 5, 'REQUISITION', '2015-01-21 16:35:16', 5),
	(7, 2, 2, 'Test', 7, 'REQUISITION', '2015-01-21 16:37:27', 7),
	(8, 5, 1, 'Approved', 5, 'REQUISITION', '2015-01-23 14:57:05', 5),
	(9, 7, 2, 'At the activity, please indicate the number of teachers targetted', 5, 'REQUISITION', '2015-01-23 14:58:39', 5),
	(10, 8, 2, 'Reject', 5, 'REQUISITION', '2015-01-23 15:55:36', 5),
	(11, 8, 1, 'Approved although the number of teachers was not indicated.  ', 5, 'REQUISITION', '2015-01-26 15:11:52', 5),
	(12, 9, 1, 'Cycle 6 training', 8, 'REQUISITION', '2015-01-26 16:43:56', 8),
	(13, 5, 1, 'Facilitate volunteer transport', 7, 'REQUISITION', '2015-01-29 10:57:39', 7),
	(14, 8, 1, 'Facilitate Transport', 7, 'REQUISITION', '2015-01-29 10:58:46', 7),
	(15, 9, 1, 'Approved', 7, 'REQUISITION', '2015-01-29 11:00:04', 7),
	(16, 18, 1, 'okay', 5, 'REQUISITION', '2015-01-29 11:59:44', 5),
	(17, 18, 1, 'test', 7, 'REQUISITION', '2015-01-29 12:00:30', 7),
	(18, 10, 2, 'Please talk to George', 5, 'REQUISITION', '2015-01-29 13:09:15', 5),
	(19, 15, 1, 'Approved', 5, 'REQUISITION', '2015-01-29 13:10:22', 5),
	(20, 16, 2, 'Consult George', 5, 'REQUISITION', '2015-01-29 13:11:20', 5),
	(21, 17, 2, 'Consult George', 5, 'REQUISITION', '2015-01-29 13:11:56', 5),
	(22, 15, 1, 'I am assuming Martin had not approved this while i was away', 7, 'REQUISITION', '2015-02-02 14:42:44', 7),
	(23, 25, 1, 'This was a data casual ', 7, 'REQUISITION', '2015-02-02 15:15:55', 7),
	(24, 24, 2, 'Get the names corect', 5, 'REQUISITION', '2015-02-02 17:15:50', 5),
	(25, 19, 1, 'Approved', 5, 'REQUISITION', '2015-02-02 17:16:37', 5),
	(26, 24, 1, 'Approved', 5, 'REQUISITION', '2015-02-03 08:49:09', 5),
	(27, 19, 2, 'This project was closed last year in December. IAPF new budget does not have his votehead. Please discuss with kabucho', 7, 'REQUISITION', '2015-02-03 09:08:17', 7),
	(28, 24, 1, 'Please go ahead and prepare the payment although we can only pay it once KEPSA has paid us.', 7, 'REQUISITION', '2015-02-03 09:09:36', 7),
	(29, 29, 1, 'Approved', 5, 'REQUISITION', '2015-02-03 13:20:12', 5),
	(30, 20, 1, 'Part of the stationery used during life skills youth training for CAA project.', 8, 'REQUISITION', '2015-02-03 15:48:11', 8),
	(31, 21, 1, 'Engagement during KEPSA cycle 6 training.', 8, 'REQUISITION', '2015-02-03 15:49:44', 8),
	(32, 22, 2, 'The payment is meant to be for cycle 5 not 6. Ammend appropriately. ', 8, 'REQUISITION', '2015-02-03 15:53:37', 8),
	(33, 23, 1, 'Support during Cycle 6 training and data verification after the training.', 8, 'REQUISITION', '2015-02-03 15:55:21', 8),
	(34, 26, 1, 'Engagement for Cycle 6 training.', 8, 'REQUISITION', '2015-02-03 15:56:38', 8),
	(35, 27, 1, 'IT Support during KYEP cycle 6 training. ', 8, 'REQUISITION', '2015-02-03 15:57:52', 8),
	(36, 28, 1, 'Driving services.', 8, 'REQUISITION', '2015-02-03 15:58:28', 8),
	(37, 30, 1, 'Ok.', 8, 'REQUISITION', '2015-02-03 16:01:14', 8),
	(38, 31, 1, 'Office P/Cash', 8, 'REQUISITION', '2015-02-03 16:01:43', 8),
	(39, 33, 1, 'Participants Manuals for cycle 6 training.', 8, 'REQUISITION', '2015-02-03 16:02:47', 8),
	(40, 34, 1, 'Office utility', 8, 'REQUISITION', '2015-02-03 16:03:14', 8),
	(41, 35, 1, 'Monthly staff airtime', 8, 'REQUISITION', '2015-02-03 16:04:28', 8),
	(42, 36, 1, 'Staff meals', 8, 'REQUISITION', '2015-02-03 16:05:09', 8),
	(43, 37, 1, 'Car services', 8, 'REQUISITION', '2015-02-03 16:06:08', 8),
	(44, 38, 1, 'Staff medical', 8, 'REQUISITION', '2015-02-03 16:06:43', 8),
	(45, 39, 1, 'Office tea', 8, 'REQUISITION', '2015-02-03 16:07:51', 8),
	(46, 40, 1, 'Refund', 8, 'REQUISITION', '2015-02-03 16:08:25', 8),
	(47, 32, 1, 'Utility Bill.', 8, 'REQUISITION', '2015-02-03 16:09:13', 8),
	(48, 20, 1, 'CAA Activity', 7, 'REQUISITION', '2015-02-04 09:17:15', 7),
	(49, 21, 1, 'You need to attach the contract', 7, 'REQUISITION', '2015-02-04 09:17:55', 7),
	(50, 23, 1, 'Please attach the contract and invoice as usual', 7, 'REQUISITION', '2015-02-04 09:19:23', 7),
	(51, 26, 1, 'Attach contract and invoice..We must show the work done in the eleven days', 7, 'REQUISITION', '2015-02-04 09:20:20', 7),
	(52, 28, 1, 'Carol please do not print as we have the hard copy processed', 7, 'REQUISITION', '2015-02-04 09:20:59', 7),
	(53, 29, 1, 'Internship', 7, 'REQUISITION', '2015-02-04 09:21:30', 7),
	(54, 27, 1, 'In the next cycle can we try to synchronize the current system and spss. I think there was double work as the data had to be keyed in in both systems.', 7, 'REQUISITION', '2015-02-04 09:25:25', 7),
	(55, 30, 1, 'We have purchased two toners this week,is this correct?', 7, 'REQUISITION', '2015-02-04 09:26:29', 7),
	(56, 31, 1, 'Carol process and remember to close Linet\'s account. Zero balance', 7, 'REQUISITION', '2015-02-04 09:27:20', 7),
	(57, 32, 1, 'Okay', 7, 'REQUISITION', '2015-02-04 09:27:46', 7),
	(58, 33, 1, 'Trainees and trainers manuals', 7, 'REQUISITION', '2015-02-04 09:29:32', 7),
	(59, 34, 2, 'Month of March----Its a prepaid services', 7, 'REQUISITION', '2015-02-04 09:31:16', 7),
	(60, 35, 1, 'In future please give the requisition as detailed as before.', 7, 'REQUISITION', '2015-02-04 09:32:06', 7),
	(61, 36, 1, 'Okay', 7, 'REQUISITION', '2015-02-04 09:32:39', 7),
	(62, 37, 2, 'Details of which car should be included.If its Wezesha vehicle the budget code is different', 7, 'REQUISITION', '2015-02-04 09:34:02', 7),
	(63, 38, 1, 'We need to tell Felix to give you the option to attach some of the invoices ', 7, 'REQUISITION', '2015-02-04 09:36:55', 7),
	(64, 39, 1, 'Okay', 7, 'REQUISITION', '2015-02-04 09:40:38', 7),
	(65, 40, 1, 'Okay', 7, 'REQUISITION', '2015-02-04 09:41:21', 7),
	(66, 22, 1, 'Cleared by Program manager.', 8, 'REQUISITION', '2015-02-04 10:53:59', 8),
	(67, 34, 1, 'Okay thank you', 7, 'REQUISITION', '2015-02-05 11:35:01', 7),
	(68, 41, 1, 'Staff Lunch', 8, 'REQUISITION', '2015-02-05 16:06:34', 8),
	(69, 37, 2, 'Rejected', 5, 'REQUISITION', '2015-02-06 10:17:32', 5),
	(70, 41, 1, 'Okay', 7, 'REQUISITION', '2015-02-09 10:55:31', 7),
	(71, 41, 1, 'Okay', 7, 'REQUISITION', '2015-02-09 10:55:46', 7),
	(72, 37, 2, 'This needs to be broken down further', 5, 'REQUISITION', '2015-02-09 12:16:19', 5),
	(73, 37, 2, 'This needs to be broken down further', 5, 'REQUISITION', '2015-02-09 12:18:07', 5),
	(74, 37, 2, 'This needs to be broken down further', 5, 'REQUISITION', '2015-02-09 12:18:17', 5),
	(75, 37, 2, 'Breakdown the costs', 5, 'REQUISITION', '2015-02-09 12:25:15', 5),
	(76, 1, 1, 'okay', 7, 'REQUISITION', '2015-02-10 08:47:53', 7),
	(77, 5, 1, 'okay', 7, 'REQUISITION', '2015-02-10 08:48:44', 7),
	(78, 8, 1, 'okay', 7, 'REQUISITION', '2015-02-10 08:49:03', 7),
	(79, 9, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:49:19', 7),
	(80, 15, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:49:35', 7),
	(81, 18, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:49:51', 7),
	(82, 20, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:50:06', 7),
	(83, 21, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:50:19', 7),
	(84, 22, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:50:38', 7),
	(85, 23, 1, 'okay', 7, 'REQUISITION', '2015-02-10 08:51:00', 7),
	(86, 24, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:51:15', 7),
	(87, 25, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:51:27', 7),
	(88, 26, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:51:37', 7),
	(89, 28, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:51:48', 7),
	(90, 29, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:51:58', 7),
	(91, 27, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:52:08', 7),
	(92, 30, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:52:20', 7),
	(93, 31, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:52:31', 7),
	(94, 32, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:52:43', 7),
	(95, 33, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:52:58', 7),
	(96, 34, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:53:10', 7),
	(97, 35, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:53:26', 7),
	(98, 36, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:53:39', 7),
	(99, 38, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:53:51', 7),
	(100, 39, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:54:02', 7),
	(101, 40, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:54:12', 7),
	(102, 41, 1, 'Okay', 7, 'REQUISITION', '2015-02-10 08:54:29', 7),
	(103, 42, 1, 'Security services for the month of Janaury 2015', 8, 'REQUISITION', '2015-02-11 10:09:45', 8),
	(104, 43, 1, 'Ok', 8, 'REQUISITION', '2015-02-11 10:12:43', 8),
	(105, 42, 1, 'Services for Jan 2015', 8, 'REQUISITION', '2015-02-11 10:19:36', 8),
	(106, 43, 1, 'Ok', 8, 'REQUISITION', '2015-02-11 10:23:54', 8),
	(107, 44, 1, 'okay', 8, 'REQUISITION', '2015-02-11 11:01:04', 8),
	(108, 42, 1, 'Services for the month of January 2015', 8, 'REQUISITION', '2015-02-11 11:01:44', 8),
	(109, 47, 1, 'Wezesha vehicle', 8, 'REQUISITION', '2015-02-11 11:04:20', 8),
	(110, 48, 1, 'Wezesha Project', 8, 'REQUISITION', '2015-02-11 11:07:12', 8),
	(111, 49, 1, 'CPAK', 8, 'REQUISITION', '2015-02-11 11:21:24', 8),
	(112, 49, 1, 'CPAK', 8, 'REQUISITION', '2015-02-11 11:21:40', 8),
	(113, 50, 1, 'LTC ', 8, 'REQUISITION', '2015-02-11 11:22:37', 8),
	(114, 52, 1, 'Admin', 8, 'REQUISITION', '2015-02-11 11:48:02', 8),
	(115, 55, 1, 'okay', 8, 'REQUISITION', '2015-02-11 12:39:46', 8),
	(116, 49, 1, 'LTC', 8, 'REQUISITION', '2015-02-11 13:11:12', 8),
	(117, 56, 1, 'Admin stationery', 8, 'REQUISITION', '2015-02-11 13:17:06', 8),
	(118, 61, 2, 'Please justify the activity.  What is the activity?', 5, 'REQUISITION', '2015-02-11 14:59:49', 5),
	(119, 58, 1, 'Approved', 5, 'REQUISITION', '2015-02-11 15:00:57', 5),
	(120, 58, 1, 'Approved', 5, 'REQUISITION', '2015-02-11 15:02:32', 5),
	(121, 58, 1, 'Approved', 5, 'REQUISITION', '2015-02-11 15:09:00', 5),
	(122, 46, 1, 'Ok', 8, 'REQUISITION', '2015-02-12 12:13:18', 8),
	(123, 46, 1, 'Ok', 8, 'REQUISITION', '2015-02-12 14:23:38', 8),
	(124, 43, 1, 'Okay', 7, 'REQUISITION', '2015-02-12 14:36:14', 7),
	(125, 60, 1, 'Ok', 8, 'REQUISITION', '2015-02-12 14:56:04', 8),
	(126, 62, 1, 'Ok.', 8, 'REQUISITION', '2015-02-12 14:56:31', 8),
	(127, 59, 2, 'Check figures again ', 8, 'REQUISITION', '2015-02-12 15:01:30', 8),
	(128, 48, 1, 'Attach the detailed invoice', 7, 'REQUISITION', '2015-02-12 15:01:35', 7),
	(129, 44, 1, 'Pray guys to stop falling sick', 7, 'REQUISITION', '2015-02-12 15:02:16', 7),
	(130, 42, 1, 'Okay', 7, 'REQUISITION', '2015-02-12 15:05:57', 7),
	(131, 47, 1, 'Attached the detailed invoice please', 7, 'REQUISITION', '2015-02-12 15:06:47', 7),
	(132, 49, 2, 'This is for CPAK  as we pay mileage', 7, 'REQUISITION', '2015-02-12 15:09:49', 7),
	(133, 50, 1, 'I think we should consider to sell this car.', 7, 'REQUISITION', '2015-02-12 15:10:54', 7),
	(134, 52, 1, 'Okay', 7, 'REQUISITION', '2015-02-12 15:11:46', 7),
	(135, 55, 1, 'Board meeting and Mckinsey proposal meeting', 7, 'REQUISITION', '2015-02-12 15:12:56', 7),
	(136, 56, 1, 'Board meeting stationery as well as normal office supply', 7, 'REQUISITION', '2015-02-12 15:14:13', 7),
	(137, 58, 2, 'Can we request for all the staff who need dairies including Wezesha and Admin', 7, 'REQUISITION', '2015-02-12 15:15:32', 7),
	(138, 57, 1, 'KYEP Cycle 6 Training Materials', 8, 'REQUISITION', '2015-02-13 13:12:36', 8),
	(139, 59, 1, 'Ok.', 8, 'REQUISITION', '2015-02-13 13:13:31', 8),
	(140, 63, 1, 'Ok', 8, 'REQUISITION', '2015-02-13 13:19:24', 8),
	(141, 19, 2, 'Attach Contract ', 8, 'REQUISITION', '2015-02-13 13:20:58', 8),
	(142, 61, 2, 'This is supposed to be approved  programs manager, please re-direct', 8, 'REQUISITION', '2015-02-13 13:22:26', 8),
	(143, 58, 1, 'Approved', 5, 'REQUISITION', '2015-02-13 13:39:10', 5),
	(144, 66, 1, 'Approved', 5, 'REQUISITION', '2015-02-17 09:38:46', 5),
	(145, 49, 2, 'Rejected.  CPAK should be the one paying for this.', 5, 'REQUISITION', '2015-02-17 09:59:13', 5),
	(146, 49, 2, 'Hold this requisition for the time being', 8, 'REQUISITION', '2015-02-19 08:52:53', 8),
	(147, 67, 1, 'Rent for Wezesha office ', 8, 'REQUISITION', '2015-02-19 08:54:02', 8),
	(148, 68, 1, 'Ok', 8, 'REQUISITION', '2015-02-19 08:54:59', 8),
	(149, 69, 1, 'Ok.', 8, 'REQUISITION', '2015-02-19 08:56:23', 8),
	(150, 70, 1, 'Ok', 8, 'REQUISITION', '2015-02-19 08:59:05', 8),
	(151, 71, 1, 'Ok.', 8, 'REQUISITION', '2015-02-19 08:59:37', 8),
	(152, 72, 1, 'Hall Hire for KYEP cycle6, Kisumu Training ', 8, 'REQUISITION', '2015-02-19 09:00:46', 8),
	(153, 73, 2, 'Change  vote head to LTC', 8, 'REQUISITION', '2015-02-19 09:02:45', 8),
	(154, 49, 2, 'This should be paid by CPAK.  We have discussed with Finance and Admin', 5, 'REQUISITION', '2015-02-19 12:12:50', 5),
	(155, 73, 1, 'Contract approved.', 8, 'REQUISITION', '2015-02-20 15:00:16', 8),
	(156, 74, 1, 'Ok.', 8, 'REQUISITION', '2015-02-20 15:02:22', 8),
	(157, 75, 1, 'Ok.', 8, 'REQUISITION', '2015-02-20 15:02:55', 8),
	(158, 76, 1, 'Ok.', 8, 'REQUISITION', '2015-02-20 15:03:18', 8),
	(159, 77, 2, 'Ignore this requisition! Thanks.', 8, 'REQUISITION', '2015-02-20 15:04:30', 8),
	(160, 79, 1, 'Ok.', 8, 'REQUISITION', '2015-02-20 15:05:24', 8),
	(161, 80, 1, 'Cycle 5 Certificates.', 8, 'REQUISITION', '2015-02-20 15:07:22', 8);
/*!40000 ALTER TABLE `wf_actionhistory` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.wf_actions
DROP TABLE IF EXISTS `wf_actions`;
CREATE TABLE IF NOT EXISTS `wf_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `act_name` varchar(128) NOT NULL,
  `action_color` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.wf_actions: ~2 rows (approximately)
DELETE FROM `wf_actions`;
/*!40000 ALTER TABLE `wf_actions` DISABLE KEYS */;
INSERT INTO `wf_actions` (`id`, `act_name`, `action_color`, `date_created`, `created_by`) VALUES
	(1, 'Approved', '#FFFFFF', '2013-02-09 20:35:29', 4),
	(2, 'Rejected', '#99ebbe', '2013-02-09 20:35:41', 4);
/*!40000 ALTER TABLE `wf_actions` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.wf_approval_levels
DROP TABLE IF EXISTS `wf_approval_levels`;
CREATE TABLE IF NOT EXISTS `wf_approval_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(64) NOT NULL,
  `leve_desc` varchar(255) DEFAULT NULL,
  `order_no` int(11) NOT NULL,
  `status_color` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.wf_approval_levels: ~4 rows (approximately)
DELETE FROM `wf_approval_levels`;
/*!40000 ALTER TABLE `wf_approval_levels` DISABLE KEYS */;
INSERT INTO `wf_approval_levels` (`id`, `level_name`, `leve_desc`, `order_no`, `status_color`, `date_created`, `created_by`) VALUES
	(1, 'Departmental Approval', 'Pending Admin\'s Approval', 1, NULL, '2014-09-18 06:55:52', 4),
	(2, 'Admin Approval', 'Pending Director\'s Approval', 2, NULL, '2014-09-18 06:56:27', 4),
	(3, 'Director\'s Approval', 'Approved', 3, NULL, '2014-09-18 06:56:43', 4),
	(4, 'Rejected/Cancelled', 'Rejected/Cancelled', 4, NULL, '2014-06-07 06:59:30', 4);
/*!40000 ALTER TABLE `wf_approval_levels` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.wf_approvers_above_me
DROP VIEW IF EXISTS `wf_approvers_above_me`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_approvers_above_me` (
	`id` INT(11) NOT NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`leve_desc` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`order_no` INT(11) NOT NULL,
	`workflow_name` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_checkapprover_item_level
DROP VIEW IF EXISTS `wf_checkapprover_item_level`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_checkapprover_item_level` (
	`id` INT(11) NOT NULL,
	`workflow_id` INT(11) NOT NULL,
	`level_id` INT(11) NOT NULL,
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL,
	`workflow_name` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`is_valid` TINYINT(1) NOT NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`order_no` INT(11) NOT NULL,
	`emp_id` INT(11) NOT NULL,
	`item_id` INT(11) NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_first_levelof_approval
DROP VIEW IF EXISTS `wf_first_levelof_approval`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_first_levelof_approval` (
	`id` INT(11) NOT NULL,
	`workflow_id` INT(11) NOT NULL,
	`emp_id` VARCHAR(130) NULL COLLATE 'latin1_swedish_ci',
	`level_id` INT(11) NOT NULL,
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL,
	`disp_message` VARCHAR(245) NULL COLLATE 'latin1_swedish_ci',
	`date_created` TIMESTAMP NOT NULL,
	`created_by` INT(11) NOT NULL,
	`order_no` INT(11) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_getinitialworkflowstate
DROP VIEW IF EXISTS `wf_getinitialworkflowstate`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_getinitialworkflowstate` (
	`id` INT(11) NOT NULL,
	`lsa` INT(11) NULL,
	`level_id` INT(11) NOT NULL,
	`workflow_id` INT(11) NOT NULL,
	`final` TINYINT(4) NOT NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`leve_desc` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`order_no` INT(11) NOT NULL,
	`status_color` VARCHAR(128) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_getlevel_below_me
DROP VIEW IF EXISTS `wf_getlevel_below_me`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_getlevel_below_me` (
	`id` INT(11) NOT NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`leve_desc` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`item_id` INT(11) NULL,
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`emp_id` INT(11) NOT NULL,
	`order_no` INT(11) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_getmyorder_number_in_approvallevel
DROP VIEW IF EXISTS `wf_getmyorder_number_in_approvallevel`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_getmyorder_number_in_approvallevel` (
	`id` INT(11) NOT NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`leve_desc` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`item_id` INT(11) NULL,
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`emp_id` INT(11) NOT NULL,
	`order_no` INT(11) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_getstatus
DROP VIEW IF EXISTS `wf_getstatus`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_getstatus` (
	`id` INT(11) NOT NULL,
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`workflow_id` INT(11) NULL,
	`item_id` INT(11) NULL,
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci',
	`leve_desc` VARCHAR(255) NULL COLLATE 'latin1_swedish_ci',
	`order_no` INT(11) NOT NULL,
	`closed` TINYINT(1) NULL,
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL
) ENGINE=MyISAM;


-- Dumping structure for view bremak_manager.wf_get_my_level_details
DROP VIEW IF EXISTS `wf_get_my_level_details`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_get_my_level_details` (
	`id` INT(11) NOT NULL,
	`workflow_id` INT(11) NOT NULL,
	`emp_id` INT(11) NOT NULL,
	`level_id` INT(11) NOT NULL,
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL,
	`disp_message` VARCHAR(245) NULL COLLATE 'latin1_swedish_ci',
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`is_valid` TINYINT(1) NOT NULL,
	`workflow_desc` TEXT NULL COLLATE 'utf8_unicode_ci',
	`workflow_name` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`item_id` INT(11) NULL,
	`status` VARCHAR(25) NULL COLLATE 'latin1_swedish_ci',
	`approval_level_id` INT(11) NULL,
	`last_modified` TIMESTAMP NULL,
	`closed` TINYINT(1) NULL
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.wf_link_users
DROP TABLE IF EXISTS `wf_link_users`;
CREATE TABLE IF NOT EXISTS `wf_link_users` (
  `link_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.wf_link_users: ~17 rows (approximately)
DELETE FROM `wf_link_users`;
/*!40000 ALTER TABLE `wf_link_users` DISABLE KEYS */;
INSERT INTO `wf_link_users` (`link_id`, `emp_id`) VALUES
	(7, 2),
	(8, 3),
	(9, 3),
	(10, 1),
	(12, 6),
	(11, 2),
	(14, 2),
	(15, 2),
	(16, 2),
	(13, 1),
	(2, 8),
	(4, 8),
	(5, 8),
	(6, 5),
	(3, 7),
	(1, 5),
	(1, 8);
/*!40000 ALTER TABLE `wf_link_users` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.wf_my_possible_levels_per_resource
DROP VIEW IF EXISTS `wf_my_possible_levels_per_resource`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_my_possible_levels_per_resource` (
	`id` INT(11) NOT NULL,
	`emp_id` INT(11) NOT NULL,
	`resource_id` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`workflow_name` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci'
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.wf_workflow
DROP TABLE IF EXISTS `wf_workflow`;
CREATE TABLE IF NOT EXISTS `wf_workflow` (
  `workflow_id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `workflow_desc` text COLLATE utf8_unicode_ci,
  `resource_id` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`workflow_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table bremak_manager.wf_workflow: 2 rows
DELETE FROM `wf_workflow`;
/*!40000 ALTER TABLE `wf_workflow` DISABLE KEYS */;
INSERT INTO `wf_workflow` (`workflow_id`, `workflow_name`, `workflow_desc`, `resource_id`, `is_valid`, `date_created`, `created_by`) VALUES
	(1, 'Leave Management', '<p>\r\n	 <strong>Leave </strong><em>Management </em><strong>Workflow </strong>\r\n</p>', 'LEAVE', 1, '2014-03-06 12:47:08', 1),
	(3, 'Requisition', 'Requisitions workflow', 'REQUISITION', 1, '2014-03-24 11:14:42', 1);
/*!40000 ALTER TABLE `wf_workflow` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.wf_workflow_items
DROP TABLE IF EXISTS `wf_workflow_items`;
CREATE TABLE IF NOT EXISTS `wf_workflow_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `approval_level_id` int(11) DEFAULT '0',
  `last_modified` timestamp NULL DEFAULT NULL,
  `closed` tinyint(1) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.wf_workflow_items: ~71 rows (approximately)
DELETE FROM `wf_workflow_items`;
/*!40000 ALTER TABLE `wf_workflow_items` DISABLE KEYS */;
INSERT INTO `wf_workflow_items` (`id`, `workflow_id`, `item_id`, `status`, `approval_level_id`, `last_modified`, `closed`, `date_created`, `created_by`) VALUES
	(1, 3, 1, '3', 0, NULL, 1, '2015-01-21 13:38:43', 12),
	(2, 3, 2, '0', 0, NULL, 0, '2015-01-21 16:32:01', 10),
	(3, 3, 3, '0', 0, NULL, 0, '2015-01-21 16:34:11', 10),
	(4, 3, 5, '3', 0, NULL, 1, '2015-01-23 14:02:03', 13),
	(5, 3, 7, '0', 0, NULL, 0, '2015-01-23 14:38:48', 13),
	(6, 3, 8, '3', 0, NULL, 1, '2015-01-23 15:28:59', 13),
	(7, 3, 9, '3', 0, NULL, 1, '2015-01-26 16:36:29', 17),
	(8, 1, 10, '0', 0, NULL, 0, '2015-01-27 18:08:22', 10),
	(9, 3, 10, '0', 0, NULL, 0, '2015-01-28 12:40:17', 9),
	(10, 3, 15, '3', 0, NULL, 1, '2015-01-28 12:45:38', 13),
	(11, 3, 16, '0', 0, NULL, 0, '2015-01-28 13:02:39', 9),
	(12, 3, 17, '0', 0, NULL, 0, '2015-01-28 13:14:47', 9),
	(13, 3, 18, '3', 0, NULL, 1, '2015-01-29 11:58:32', 13),
	(14, 3, 19, '0', 0, NULL, 0, '2015-02-02 11:50:22', 12),
	(15, 3, 20, '3', 0, NULL, 1, '2015-02-02 13:41:23', 17),
	(16, 3, 21, '3', 0, NULL, 1, '2015-02-02 13:47:25', 17),
	(17, 3, 22, '3', 0, NULL, 1, '2015-02-02 13:53:32', 17),
	(18, 3, 23, '3', 0, NULL, 1, '2015-02-02 13:57:53', 17),
	(19, 3, 24, '3', 0, NULL, 1, '2015-02-02 14:01:09', 17),
	(20, 3, 25, '3', 0, NULL, 1, '2015-02-02 14:03:50', 17),
	(21, 3, 26, '3', 0, NULL, 1, '2015-02-02 14:05:56', 17),
	(22, 3, 28, '3', 0, NULL, 1, '2015-02-02 15:30:08', 17),
	(23, 3, 29, '3', 0, NULL, 1, '2015-02-02 16:34:19', 17),
	(24, 3, 27, '3', 0, NULL, 1, '2015-02-02 16:35:10', 17),
	(25, 3, 30, '3', 0, NULL, 1, '2015-02-03 10:04:43', 17),
	(26, 3, 31, '3', 0, NULL, 1, '2015-02-03 10:17:39', 17),
	(27, 3, 32, '3', 0, NULL, 1, '2015-02-03 10:23:10', 17),
	(28, 3, 33, '3', 0, NULL, 1, '2015-02-03 10:41:32', 17),
	(29, 3, 34, '3', 0, NULL, 1, '2015-02-03 10:44:02', 17),
	(30, 3, 35, '3', 0, NULL, 1, '2015-02-03 10:46:31', 17),
	(31, 3, 36, '3', 0, NULL, 1, '2015-02-03 10:48:31', 17),
	(32, 3, 37, '0', 0, NULL, 0, '2015-02-03 10:51:08', 17),
	(33, 3, 38, '3', 0, NULL, 1, '2015-02-03 10:54:14', 17),
	(34, 3, 39, '3', 0, NULL, 1, '2015-02-03 11:07:39', 17),
	(35, 3, 40, '3', 0, NULL, 1, '2015-02-03 14:35:09', 17),
	(36, 3, 41, '3', 0, NULL, 1, '2015-02-04 11:01:35', 17),
	(37, 3, 42, '3', 0, NULL, 1, '2015-02-06 16:58:18', 17),
	(38, 3, 43, '3', 0, NULL, 1, '2015-02-06 17:02:08', 17),
	(39, 3, 44, '3', 0, NULL, 1, '2015-02-06 17:04:02', 17),
	(40, 3, 46, '1', 0, NULL, 0, '2015-02-06 17:14:51', 17),
	(41, 3, 47, '3', 0, NULL, 1, '2015-02-06 17:19:40', 17),
	(42, 3, 48, '3', 0, NULL, 1, '2015-02-06 17:21:14', 17),
	(43, 3, 49, '0', 0, NULL, 0, '2015-02-06 17:23:06', 17),
	(44, 3, 50, '3', 0, NULL, 1, '2015-02-06 17:25:01', 17),
	(45, 3, 52, '3', 0, NULL, 1, '2015-02-10 15:44:07', 17),
	(46, 3, 55, '3', 0, NULL, 1, '2015-02-10 18:01:40', 17),
	(47, 3, 56, '3', 0, NULL, 1, '2015-02-10 18:25:56', 17),
	(48, 3, 57, '1', 0, NULL, 0, '2015-02-11 11:54:23', 17),
	(49, 3, 58, '1', 0, NULL, 0, '2015-02-11 14:02:40', 12),
	(50, 3, 59, '1', 0, NULL, 0, '2015-02-11 14:14:05', 17),
	(51, 3, 61, '0', 0, NULL, 0, '2015-02-11 14:52:23', 11),
	(52, 3, 60, '1', 0, NULL, 0, '2015-02-12 14:14:59', 17),
	(53, 3, 62, '1', 0, NULL, 0, '2015-02-12 14:34:31', 17),
	(54, 3, 63, '1', 0, NULL, 0, '2015-02-12 15:24:19', 17),
	(55, 3, 64, '0', 0, NULL, 0, '2015-02-16 12:51:33', 14),
	(56, 3, 66, '1', 0, NULL, 0, '2015-02-16 15:41:46', 14),
	(57, 3, 67, '1', 0, NULL, 0, '2015-02-17 13:36:09', 17),
	(58, 3, 68, '1', 0, NULL, 0, '2015-02-17 14:47:14', 17),
	(59, 3, 69, '1', 0, NULL, 0, '2015-02-17 14:56:24', 17),
	(60, 3, 71, '1', 0, NULL, 0, '2015-02-18 09:29:33', 17),
	(61, 3, 70, '1', 0, NULL, 0, '2015-02-18 10:47:59', 17),
	(62, 3, 72, '1', 0, NULL, 0, '2015-02-18 11:29:25', 17),
	(63, 3, 73, '1', 0, NULL, 0, '2015-02-18 12:09:35', 17),
	(64, 3, 74, '1', 0, NULL, 0, '2015-02-19 09:48:10', 17),
	(65, 3, 75, '1', 0, NULL, 0, '2015-02-19 12:58:24', 17),
	(66, 3, 76, '1', 0, NULL, 0, '2015-02-19 13:10:22', 17),
	(67, 3, 77, '0', 0, NULL, 0, '2015-02-19 15:55:50', 17),
	(68, 3, 79, '1', 0, NULL, 0, '2015-02-19 17:16:56', 17),
	(69, 3, 80, '1', 0, NULL, 0, '2015-02-20 11:59:33', 17),
	(70, 3, 81, '0', 0, NULL, 0, '2015-02-23 11:44:32', 17),
	(71, 3, 82, '0', 0, NULL, 0, '2015-02-23 12:18:40', 17);
/*!40000 ALTER TABLE `wf_workflow_items` ENABLE KEYS */;


-- Dumping structure for table bremak_manager.wf_workflow_link
DROP TABLE IF EXISTS `wf_workflow_link`;
CREATE TABLE IF NOT EXISTS `wf_workflow_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `emp_id` varchar(130) DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `lsa` int(11) DEFAULT NULL,
  `final` tinyint(4) NOT NULL DEFAULT '0',
  `disp_message` varchar(245) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workflow_level` (`level_id`),
  KEY `workflow_employee` (`emp_id`),
  KEY `workflow_link` (`workflow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table bremak_manager.wf_workflow_link: ~6 rows (approximately)
DELETE FROM `wf_workflow_link`;
/*!40000 ALTER TABLE `wf_workflow_link` DISABLE KEYS */;
INSERT INTO `wf_workflow_link` (`id`, `workflow_id`, `emp_id`, `level_id`, `lsa`, `final`, `disp_message`, `date_created`, `created_by`) VALUES
	(0, 0, '0', 0, 0, 0, 'Submitted', '2014-08-21 15:27:27', 1),
	(1, 3, '5,8', 1, 1, 0, 'Pending Finance\'s Approval', '2014-09-23 11:15:58', 17),
	(3, 3, '7', 3, 1, 1, 'Approved', '2014-09-23 11:17:11', 17),
	(4, 1, '8', 1, 1, 0, 'Pending HR Approval', '2014-09-23 11:22:37', 17),
	(5, 1, '8', 2, 1, 0, 'Pending Directors Approval', '2014-09-23 11:24:10', 17),
	(6, 1, '5', 3, 1, 1, 'Approved', '2014-09-23 11:24:46', 17);
/*!40000 ALTER TABLE `wf_workflow_link` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.wf_workflow_link_view
DROP VIEW IF EXISTS `wf_workflow_link_view`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `wf_workflow_link_view` (
	`id` INT(11) NOT NULL,
	`workflow_name` VARCHAR(80) NOT NULL COLLATE 'utf8_unicode_ci',
	`workflow_id` INT(11) NOT NULL,
	`emp_id` VARCHAR(130) NULL COLLATE 'latin1_swedish_ci',
	`level_id` INT(11) NOT NULL,
	`lsa` INT(11) NULL,
	`final` TINYINT(4) NOT NULL,
	`disp_message` VARCHAR(245) NULL COLLATE 'latin1_swedish_ci',
	`date_created` TIMESTAMP NOT NULL,
	`created_by` INT(11) NOT NULL,
	`first_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`middle_name` VARCHAR(30) NULL COLLATE 'latin1_swedish_ci',
	`last_name` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`level_name` VARCHAR(64) NOT NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for table bremak_manager.wf_workflow_records_status
DROP TABLE IF EXISTS `wf_workflow_records_status`;
CREATE TABLE IF NOT EXISTS `wf_workflow_records_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `resource_id` varchar(128) NOT NULL,
  `action` varchar(128) NOT NULL,
  `action_desc` varchar(128) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;

-- Dumping data for table bremak_manager.wf_workflow_records_status: ~170 rows (approximately)
DELETE FROM `wf_workflow_records_status`;
/*!40000 ALTER TABLE `wf_workflow_records_status` DISABLE KEYS */;
INSERT INTO `wf_workflow_records_status` (`id`, `item_id`, `resource_id`, `action`, `action_desc`, `date_created`, `created_by`, `last_modified`, `last_modified_by`) VALUES
	(1, 1, 'REQUISITION', 'Approved', 'Approved', '2014-09-29 16:55:23', 17, '2015-02-10 08:47:54', 7),
	(2, 2, 'REQUISITION', 'Rejected', 'Approved', '2014-09-29 16:58:30', 17, '2015-01-21 16:37:27', 7),
	(3, 3, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-09-29 18:34:51', 17, '2015-01-21 16:35:16', 5),
	(4, 5, 'REQUISITION', 'Approved', 'Approved', '2014-09-30 08:21:24', 17, '2015-02-10 08:48:44', 7),
	(5, 4, 'REQUISITION', 'Approved', 'Approved', '2014-09-30 08:51:36', 17, '2014-11-26 18:50:37', 7),
	(6, 11, 'REQUISITION', 'Approved', 'Approved', '2014-10-02 12:36:24', 17, '2014-10-02 12:37:48', 5),
	(7, 14, 'REQUISITION', 'Approved', 'Approved', '2014-10-03 11:37:57', 17, '2014-10-07 10:02:37', 5),
	(8, 15, 'REQUISITION', 'Approved', 'Approved', '2014-10-04 12:17:17', 17, '2015-02-10 08:49:35', 7),
	(9, 16, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-10-07 09:58:36', 17, '2015-01-29 13:11:20', 5),
	(10, 17, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-10-07 11:02:00', 17, '2015-01-29 13:11:56', 5),
	(11, 18, 'REQUISITION', 'Approved', 'Approved', '2014-10-08 19:02:27', 17, '2015-02-10 08:49:52', 7),
	(12, 19, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-10-15 13:11:27', 17, '2015-02-13 13:20:58', 8),
	(13, 21, 'REQUISITION', 'Approved', 'Approved', '2014-10-29 16:15:29', 17, '2015-02-10 08:50:19', 7),
	(14, 3, 'LEAVE', 'Initialized', 'Initialized', '2014-10-31 07:32:35', 17, '0000-00-00 00:00:00', NULL),
	(15, 4, 'LEAVE', 'Initialized', 'Initialized', '2014-10-31 07:33:12', 17, '0000-00-00 00:00:00', NULL),
	(16, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-04 13:14:38', 17, '0000-00-00 00:00:00', NULL),
	(17, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-05 14:11:05', 17, '0000-00-00 00:00:00', NULL),
	(18, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-05 14:24:17', 17, '0000-00-00 00:00:00', NULL),
	(19, 3, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-05 14:43:39', 17, '0000-00-00 00:00:00', NULL),
	(20, 4, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-05 19:36:08', 17, '0000-00-00 00:00:00', NULL),
	(21, 5, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-06 08:01:49', 17, '0000-00-00 00:00:00', NULL),
	(22, 6, 'REQUISITION', 'Approved', 'Approved', '2014-11-06 08:54:55', 17, '2014-11-28 10:17:59', 7),
	(23, 7, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-11-06 08:57:09', 17, '2015-01-23 14:58:39', 5),
	(24, 8, 'REQUISITION', 'Approved', 'Approved', '2014-11-06 12:13:02', 17, '2015-02-10 08:49:03', 7),
	(25, 9, 'REQUISITION', 'Approved', 'Approved', '2014-11-17 14:32:12', 17, '2015-02-10 08:49:20', 7),
	(26, 10, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2014-11-17 14:36:39', 17, '2015-01-29 13:09:15', 5),
	(27, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-17 15:13:58', 17, '0000-00-00 00:00:00', NULL),
	(28, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-18 08:07:00', 17, '0000-00-00 00:00:00', NULL),
	(29, 3, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 11:38:42', 17, '0000-00-00 00:00:00', NULL),
	(30, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 15:44:34', 17, '0000-00-00 00:00:00', NULL),
	(31, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 16:04:24', 17, '0000-00-00 00:00:00', NULL),
	(32, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 16:05:20', 17, '0000-00-00 00:00:00', NULL),
	(33, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 16:17:16', 17, '0000-00-00 00:00:00', NULL),
	(34, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 17:54:24', 17, '0000-00-00 00:00:00', NULL),
	(35, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-21 17:54:49', 17, '0000-00-00 00:00:00', NULL),
	(36, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-22 10:10:31', 7, '0000-00-00 00:00:00', NULL),
	(37, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-22 10:26:34', 17, '0000-00-00 00:00:00', NULL),
	(38, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-24 18:20:12', 17, '0000-00-00 00:00:00', NULL),
	(39, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-25 14:32:21', 17, '0000-00-00 00:00:00', NULL),
	(40, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 10:02:34', 17, '0000-00-00 00:00:00', NULL),
	(41, 1, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 11:08:13', 17, '0000-00-00 00:00:00', NULL),
	(42, 2, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 11:15:03', 17, '0000-00-00 00:00:00', NULL),
	(43, 3, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 18:01:25', 17, '0000-00-00 00:00:00', NULL),
	(44, 4, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 18:49:47', 17, '0000-00-00 00:00:00', NULL),
	(45, 5, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-26 19:01:18', 17, '0000-00-00 00:00:00', NULL),
	(46, 6, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-28 10:13:34', 17, '0000-00-00 00:00:00', NULL),
	(47, 7, 'REQUISITION', 'Initialized', 'Initialized', '2014-11-29 10:42:54', 17, '0000-00-00 00:00:00', NULL),
	(48, 8, 'REQUISITION', 'Initialized', 'Initialized', '2014-12-03 13:54:08', 17, '0000-00-00 00:00:00', NULL),
	(49, 5, 'LEAVE', 'Initialized', 'Initialized', '2014-12-11 16:58:41', 7, '0000-00-00 00:00:00', NULL),
	(50, 6, 'LEAVE', 'Initialized', 'Initialized', '2014-12-11 17:27:41', 7, '0000-00-00 00:00:00', NULL),
	(51, 7, 'LEAVE', 'Initialized', 'Initialized', '2014-12-11 17:51:25', 7, '0000-00-00 00:00:00', NULL),
	(52, 8, 'LEAVE', 'Initialized', 'Initialized', '2014-12-11 18:04:20', 7, '0000-00-00 00:00:00', NULL),
	(53, 9, 'LEAVE', 'Initialized', 'Initialized', '2014-12-11 18:14:45', 17, '0000-00-00 00:00:00', NULL),
	(54, 10, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-13 10:30:36', 17, '0000-00-00 00:00:00', NULL),
	(55, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-13 16:20:45', 17, '0000-00-00 00:00:00', NULL),
	(56, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-13 17:35:57', 17, '0000-00-00 00:00:00', NULL),
	(57, 3, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-14 16:06:36', 17, '0000-00-00 00:00:00', NULL),
	(58, 3, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-14 16:56:08', 17, '0000-00-00 00:00:00', NULL),
	(59, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-17 10:16:25', 17, '0000-00-00 00:00:00', NULL),
	(60, 2, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-19 10:46:52', 17, '0000-00-00 00:00:00', NULL),
	(61, 2, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-19 10:50:26', 17, '0000-00-00 00:00:00', NULL),
	(62, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-19 15:41:47', 17, '0000-00-00 00:00:00', NULL),
	(63, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-19 15:47:54', 17, '0000-00-00 00:00:00', NULL),
	(64, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-20 16:37:06', 17, '0000-00-00 00:00:00', NULL),
	(65, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-20 16:38:23', 17, '0000-00-00 00:00:00', NULL),
	(66, 2, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-20 16:59:22', 17, '0000-00-00 00:00:00', NULL),
	(67, 3, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 11:16:52', 18, '0000-00-00 00:00:00', NULL),
	(68, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 13:38:43', 12, '0000-00-00 00:00:00', NULL),
	(69, 1, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 13:51:53', 12, '0000-00-00 00:00:00', NULL),
	(70, 2, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 16:32:01', 10, '0000-00-00 00:00:00', NULL),
	(71, 2, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 16:32:27', 10, '0000-00-00 00:00:00', NULL),
	(72, 3, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-21 16:34:11', 10, '0000-00-00 00:00:00', NULL),
	(73, 5, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 14:02:04', 13, '0000-00-00 00:00:00', NULL),
	(74, 7, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 14:38:48', 13, '0000-00-00 00:00:00', NULL),
	(75, 7, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 14:39:37', 13, '0000-00-00 00:00:00', NULL),
	(76, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 15:28:59', 13, '0000-00-00 00:00:00', NULL),
	(77, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 16:09:00', 13, '0000-00-00 00:00:00', NULL),
	(78, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 16:11:23', 13, '0000-00-00 00:00:00', NULL),
	(79, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 16:22:50', 13, '0000-00-00 00:00:00', NULL),
	(80, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 16:36:44', 13, '0000-00-00 00:00:00', NULL),
	(81, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-23 16:46:13', 13, '0000-00-00 00:00:00', NULL),
	(82, 8, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-26 12:19:32', 13, '0000-00-00 00:00:00', NULL),
	(83, 9, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-26 16:36:29', 17, '0000-00-00 00:00:00', NULL),
	(84, 10, 'LEAVE', 'Initialized', 'Initialized', '2015-01-27 18:08:22', 10, '0000-00-00 00:00:00', NULL),
	(85, 10, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 12:40:17', 9, '0000-00-00 00:00:00', NULL),
	(86, 15, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 12:45:38', 13, '0000-00-00 00:00:00', NULL),
	(87, 10, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 12:47:51', 9, '0000-00-00 00:00:00', NULL),
	(88, 10, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 12:48:53', 9, '0000-00-00 00:00:00', NULL),
	(89, 16, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 13:02:39', 9, '0000-00-00 00:00:00', NULL),
	(90, 17, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-28 13:14:47', 9, '0000-00-00 00:00:00', NULL),
	(91, 18, 'REQUISITION', 'Initialized', 'Initialized', '2015-01-29 11:58:32', 13, '0000-00-00 00:00:00', NULL),
	(92, 19, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-02 11:50:22', 12, '0000-00-00 00:00:00', NULL),
	(93, 20, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 13:41:23', 17, '2015-02-10 08:50:06', 7),
	(94, 21, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-02 13:47:25', 17, '0000-00-00 00:00:00', NULL),
	(95, 22, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 13:53:32', 17, '2015-02-10 08:50:38', 7),
	(96, 23, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 13:57:53', 17, '2015-02-10 08:51:00', 7),
	(97, 24, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 14:01:09', 17, '2015-02-10 08:51:15', 7),
	(98, 25, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 14:03:50', 17, '2015-02-10 08:51:27', 7),
	(99, 26, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 14:05:56', 17, '2015-02-10 08:51:37', 7),
	(100, 21, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-02 14:40:03', 17, '0000-00-00 00:00:00', NULL),
	(101, 28, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 15:30:09', 17, '2015-02-10 08:51:48', 7),
	(102, 29, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 16:34:19', 17, '2015-02-10 08:51:58', 7),
	(103, 27, 'REQUISITION', 'Approved', 'Approved', '2015-02-02 16:35:10', 17, '2015-02-10 08:52:08', 7),
	(104, 30, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:04:43', 17, '2015-02-10 08:52:21', 7),
	(105, 31, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:17:39', 17, '2015-02-10 08:52:32', 7),
	(106, 32, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:23:10', 17, '2015-02-10 08:52:43', 7),
	(107, 33, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:41:32', 17, '2015-02-10 08:52:58', 7),
	(108, 34, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:44:02', 17, '2015-02-10 08:53:10', 7),
	(109, 35, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:46:31', 17, '2015-02-10 08:53:26', 7),
	(110, 36, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:48:31', 17, '2015-02-10 08:53:39', 7),
	(111, 37, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2015-02-03 10:51:08', 17, '2015-02-09 12:25:15', 5),
	(112, 38, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 10:54:14', 17, '2015-02-10 08:53:51', 7),
	(113, 39, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 11:07:39', 17, '2015-02-10 08:54:02', 7),
	(114, 40, 'REQUISITION', 'Approved', 'Approved', '2015-02-03 14:35:09', 17, '2015-02-10 08:54:12', 7),
	(115, 22, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-03 16:14:36', 17, '0000-00-00 00:00:00', NULL),
	(116, 41, 'REQUISITION', 'Approved', 'Approved', '2015-02-04 11:01:35', 17, '2015-02-10 08:54:29', 7),
	(117, 42, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 16:58:18', 17, '2015-02-12 15:05:57', 7),
	(118, 43, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 17:02:08', 17, '2015-02-12 14:36:14', 7),
	(119, 44, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 17:04:02', 17, '2015-02-12 15:02:16', 7),
	(120, 46, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-06 17:14:51', 17, '2015-02-12 14:23:38', 8),
	(121, 47, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 17:19:40', 17, '2015-02-12 15:06:47', 7),
	(122, 48, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 17:21:14', 17, '2015-02-12 15:01:35', 7),
	(123, 49, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2015-02-06 17:23:06', 17, '2015-02-19 12:12:50', 5),
	(124, 50, 'REQUISITION', 'Approved', 'Approved', '2015-02-06 17:25:01', 17, '2015-02-12 15:10:54', 7),
	(125, 37, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-10 11:18:48', 17, '0000-00-00 00:00:00', NULL),
	(126, 37, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-10 15:24:10', 17, '0000-00-00 00:00:00', NULL),
	(127, 52, 'REQUISITION', 'Approved', 'Approved', '2015-02-10 15:44:07', 17, '2015-02-12 15:11:46', 7),
	(128, 55, 'REQUISITION', 'Approved', 'Approved', '2015-02-10 18:01:40', 17, '2015-02-12 15:12:56', 7),
	(129, 56, 'REQUISITION', 'Approved', 'Approved', '2015-02-10 18:25:56', 17, '2015-02-12 15:14:13', 7),
	(130, 52, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-10 18:26:48', 17, '0000-00-00 00:00:00', NULL),
	(131, 57, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-11 11:54:23', 17, '2015-02-13 13:12:37', 8),
	(132, 57, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-11 12:32:05', 17, '0000-00-00 00:00:00', NULL),
	(133, 58, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-11 14:02:40', 12, '2015-02-13 13:39:10', 5),
	(134, 59, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-11 14:14:05', 17, '2015-02-13 13:13:31', 8),
	(135, 57, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-11 14:20:28', 17, '0000-00-00 00:00:00', NULL),
	(136, 61, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2015-02-11 14:52:23', 11, '2015-02-13 13:22:26', 8),
	(137, 61, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-12 12:57:14', 11, '0000-00-00 00:00:00', NULL),
	(138, 60, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-12 14:14:59', 17, '2015-02-12 14:56:04', 8),
	(139, 62, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-12 14:34:31', 17, '2015-02-12 14:56:31', 8),
	(140, 59, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-12 15:04:29', 17, '0000-00-00 00:00:00', NULL),
	(141, 63, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-12 15:24:19', 17, '2015-02-13 13:19:24', 8),
	(142, 58, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-12 16:18:45', 12, '0000-00-00 00:00:00', NULL),
	(143, 63, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-12 16:35:10', 17, '0000-00-00 00:00:00', NULL),
	(144, 58, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-12 17:30:25', 12, '0000-00-00 00:00:00', NULL),
	(145, 64, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-16 12:51:33', 14, '0000-00-00 00:00:00', NULL),
	(146, 66, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-16 15:41:46', 14, '2015-02-17 09:38:46', 5),
	(147, 49, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-17 12:11:02', 17, '0000-00-00 00:00:00', NULL),
	(148, 67, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-17 13:36:09', 17, '2015-02-19 08:54:02', 8),
	(149, 68, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-17 14:47:14', 17, '2015-02-19 08:54:59', 8),
	(150, 69, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-17 14:56:24', 17, '2015-02-19 08:56:23', 8),
	(151, 69, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-17 15:28:29', 17, '0000-00-00 00:00:00', NULL),
	(152, 69, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-17 17:55:42', 17, '0000-00-00 00:00:00', NULL),
	(153, 68, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-18 09:10:27', 17, '0000-00-00 00:00:00', NULL),
	(154, 71, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-18 09:29:33', 17, '2015-02-19 08:59:37', 8),
	(155, 67, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-18 10:38:45', 17, '0000-00-00 00:00:00', NULL),
	(156, 67, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-18 10:43:19', 17, '0000-00-00 00:00:00', NULL),
	(157, 71, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-18 10:44:00', 17, '0000-00-00 00:00:00', NULL),
	(158, 70, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-18 10:47:59', 17, '2015-02-19 08:59:05', 8),
	(159, 72, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-18 11:29:25', 17, '2015-02-19 09:00:46', 8),
	(160, 49, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-18 11:29:47', 17, '0000-00-00 00:00:00', NULL),
	(161, 73, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-18 12:09:35', 17, '2015-02-20 15:00:17', 8),
	(162, 73, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-19 09:26:27', 17, '0000-00-00 00:00:00', NULL),
	(163, 74, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-19 09:48:10', 17, '2015-02-20 15:02:22', 8),
	(164, 75, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-19 12:58:24', 17, '2015-02-20 15:02:55', 8),
	(165, 76, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-19 13:10:22', 17, '2015-02-20 15:03:18', 8),
	(166, 77, 'REQUISITION', 'Rejected', 'Pending Finance\'s Approval', '2015-02-19 15:55:50', 17, '2015-02-20 15:04:30', 8),
	(167, 79, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-19 17:16:56', 17, '2015-02-20 15:05:24', 8),
	(168, 80, 'REQUISITION', 'Approved', 'Pending Finance\'s Approval', '2015-02-20 11:59:33', 17, '2015-02-20 15:07:22', 8),
	(169, 81, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-23 11:44:32', 17, '0000-00-00 00:00:00', NULL),
	(170, 82, 'REQUISITION', 'Initialized', 'Initialized', '2015-02-23 12:18:40', 17, '0000-00-00 00:00:00', NULL);
/*!40000 ALTER TABLE `wf_workflow_records_status` ENABLE KEYS */;


-- Dumping structure for view bremak_manager.empbanksview
DROP VIEW IF EXISTS `empbanksview`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `empbanksview`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `empbanksview` AS (select `hr_emp_banks`.`id` AS `id`,`hr_emp_banks`.`emp_id` AS `emp_id`,`hr_emp_banks`.`account_no` AS `account_no`,`hr_emp_banks`.`bank_id` AS `bank_id`,`hr_emp_banks`.`bank_branch_id` AS `bank_branch_id`,`hr_emp_banks`.`paying_acc` AS `paying_acc`,`hr_banks`.`bank_code` AS `bank_code`,`hr_banks`.`bank_name` AS `bank_name`,`hr_bank_branches`.`branch_code` AS `branch_code`,`hr_bank_branches`.`branch_name` AS `branch_name`,concat(`hr_banks`.`bank_name`,', ',`hr_bank_branches`.`branch_name`,', Acc_no: ',`hr_emp_banks`.`account_no`) AS `emp_bank_account` from ((`hr_emp_banks` join `hr_banks` on((`hr_emp_banks`.`bank_id` = `hr_banks`.`id`))) join `hr_bank_branches` on(((`hr_bank_branches`.`bank_id` = `hr_banks`.`id`) and (`hr_emp_banks`.`bank_branch_id` = `hr_bank_branches`.`id`))))); ;


-- Dumping structure for view bremak_manager.employeeslist
DROP VIEW IF EXISTS `employeeslist`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `employeeslist`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `employeeslist` AS (select `hr_employees`.`paygroup_id` AS `paygroup_id`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`id` AS `id`,`hr_employees`.`emp_code` AS `emp_code`,`person`.`marital_status_id` AS `marital_status_id`,concat(`person`.`first_name`,' ',`person`.`middle_name`,' ',`person`.`last_name`) AS `empname`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,`person`.`gender` AS `gender`,`person`.`birthdate` AS `birthdate`,`settings_country`.`name` AS `name`,`hr_employees`.`hire_date` AS `hire_date`,`hr_jobs_titles`.`job_title` AS `job_title`,`settings_department`.`name` AS `dept_name`,`hr_employees`.`work_hours_perday` AS `work_hours_perday`,`hr_employees`.`employment_class_id` AS `employment_class_id`,`hr_employmentclass`.`empclass` AS `empclass`,`hr_employement_categories`.`categoryname` AS `categoryname`,`hr_paytypes`.`pay_type_name` AS `pay_type_name`,`hr_employees`.`salary` AS `salary`,`hr_employees`.`employment_status` AS `status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`hr_employees`.`company_phone` AS `company_phone`,`hr_employees`.`company_phone_ext` AS `company_phone_ext`,`hr_employees`.`branch_id` AS `branch_id`,`hr_employees`.`currency_id` AS `currency_id` from (((((((`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`))) join `hr_jobs_titles` on((`hr_employees`.`job_title_id` = `hr_jobs_titles`.`id`))) join `settings_department` on((`hr_employees`.`department_id` = `settings_department`.`id`))) left join `hr_employmentclass` on((`hr_employees`.`employment_class_id` = `hr_employmentclass`.`id`))) left join `hr_employement_categories` on((`hr_employees`.`employment_cat_id` = `hr_employement_categories`.`id`))) left join `hr_paytypes` on((`hr_employees`.`pay_type_id` = `hr_paytypes`.`id`))) left join `settings_country` on((`person`.`country_id` = `settings_country`.`id`)))); ;


-- Dumping structure for view bremak_manager.empminorlist
DROP VIEW IF EXISTS `empminorlist`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `empminorlist`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `empminorlist` AS (select `hr_employees`.`id` AS `id`,`hr_employees`.`employment_class_id` AS `employment_class_id`,`hr_employees`.`employment_status` AS `employment_status`,`hr_employees`.`email` AS `email`,`hr_employees`.`mobile` AS `mobile`,`person`.`first_name` AS `first_name`,`person`.`middle_name` AS `middle_name`,`person`.`last_name` AS `last_name`,concat(`person`.`first_name`,' ',`person`.`last_name`) AS `name`,`person`.`gender` AS `gender` from (`hr_employees` join `person` on((`hr_employees`.`id` = `person`.`id`)))); ;


-- Dumping structure for view bremak_manager.users_view
DROP VIEW IF EXISTS `users_view`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `users_view`;
CREATE ALGORITHM=TEMPTABLE DEFINER=`root`@`localhost` VIEW `users_view` AS select `a`.`id` AS `id`,`a`.`username` AS `username`,`a`.`email` AS `email`,`a`.`status` AS `status`,`a`.`timezone` AS `timezone`,`a`.`password` AS `password`,`a`.`salt` AS `salt`,`a`.`password_reset_code` AS `password_reset_code`,`a`.`password_reset_date` AS `password_reset_date`,`a`.`password_reset_request_date` AS `password_reset_request_date`,`a`.`activation_code` AS `activation_code`,`a`.`user_level` AS `user_level`,`a`.`role_id` AS `role_id`,`a`.`date_created` AS `date_created`,`a`.`created_by` AS `created_by`,`a`.`last_modified` AS `last_modified`,`a`.`last_modified_by` AS `last_modified_by`,`a`.`last_login` AS `last_login`,concat(`b`.`first_name`,' ',`b`.`last_name`) AS `name`,`b`.`gender` AS `gender` from (`users` `a` join `person` `b` on((`a`.`id` = `b`.`id`))); ;


-- Dumping structure for view bremak_manager.wf_approvers_above_me
DROP VIEW IF EXISTS `wf_approvers_above_me`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_approvers_above_me`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_approvers_above_me` AS (select `wf_link_users`.`emp_id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_link_users` join `wf_workflow_link` on((`wf_link_users`.`link_id` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))); ;


-- Dumping structure for view bremak_manager.wf_checkapprover_item_level
DROP VIEW IF EXISTS `wf_checkapprover_item_level`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_checkapprover_item_level`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_checkapprover_item_level` AS (select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_items`.`item_id` AS `item_id` from ((((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`)))); ;


-- Dumping structure for view bremak_manager.wf_first_levelof_approval
DROP VIEW IF EXISTS `wf_first_levelof_approval`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_first_levelof_approval`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_first_levelof_approval` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow_link`.`date_created` AS `date_created`,`wf_workflow_link`.`created_by` AS `created_by`,`wf_approval_levels`.`order_no` AS `order_no` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) order by `wf_approval_levels`.`order_no` limit 1; ;


-- Dumping structure for view bremak_manager.wf_getinitialworkflowstate
DROP VIEW IF EXISTS `wf_getinitialworkflowstate`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_getinitialworkflowstate`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_getinitialworkflowstate` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`final` AS `final`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_approval_levels`.`status_color` AS `status_color` from (`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))); ;


-- Dumping structure for view bremak_manager.wf_getlevel_below_me
DROP VIEW IF EXISTS `wf_getlevel_below_me`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_getlevel_below_me`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_getlevel_below_me` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)))); ;


-- Dumping structure for view bremak_manager.wf_getmyorder_number_in_approvallevel
DROP VIEW IF EXISTS `wf_getmyorder_number_in_approvallevel`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_getmyorder_number_in_approvallevel`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_getmyorder_number_in_approvallevel` AS (select `wf_workflow_link`.`id` AS `id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_approval_levels`.`order_no` AS `order_no` from ((((`wf_workflow_link` join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`)))); ;


-- Dumping structure for view bremak_manager.wf_getstatus
DROP VIEW IF EXISTS `wf_getstatus`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_getstatus`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_getstatus` AS (select `wf_workflow_items`.`id` AS `id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow_items`.`workflow_id` AS `workflow_id`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_approval_levels`.`level_name` AS `level_name`,`wf_approval_levels`.`leve_desc` AS `leve_desc`,`wf_approval_levels`.`order_no` AS `order_no`,`wf_workflow_items`.`closed` AS `closed`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final` from (((`wf_workflow_items` join `wf_workflow_link` on((`wf_workflow_items`.`status` = `wf_workflow_link`.`id`))) join `wf_workflow` on((`wf_workflow_items`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))); ;


-- Dumping structure for view bremak_manager.wf_get_my_level_details
DROP VIEW IF EXISTS `wf_get_my_level_details`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_get_my_level_details`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_get_my_level_details` AS select `wf_workflow_link`.`id` AS `id`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`is_valid` AS `is_valid`,`wf_workflow`.`workflow_desc` AS `workflow_desc`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow_items`.`item_id` AS `item_id`,`wf_workflow_items`.`status` AS `status`,`wf_workflow_items`.`approval_level_id` AS `approval_level_id`,`wf_workflow_items`.`last_modified` AS `last_modified`,`wf_workflow_items`.`closed` AS `closed` from (((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_workflow_items` on((`wf_workflow_link`.`workflow_id` = `wf_workflow_items`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))); ;


-- Dumping structure for view bremak_manager.wf_my_possible_levels_per_resource
DROP VIEW IF EXISTS `wf_my_possible_levels_per_resource`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_my_possible_levels_per_resource`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_my_possible_levels_per_resource` AS select `wf_workflow_link`.`id` AS `id`,`wf_link_users`.`emp_id` AS `emp_id`,`wf_workflow`.`resource_id` AS `resource_id`,`wf_workflow`.`workflow_name` AS `workflow_name` from ((`wf_workflow_link` join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_link_users` on((`wf_workflow_link`.`id` = `wf_link_users`.`link_id`))); ;


-- Dumping structure for view bremak_manager.wf_workflow_link_view
DROP VIEW IF EXISTS `wf_workflow_link_view`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `wf_workflow_link_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `wf_workflow_link_view` AS (select `wf_workflow_link`.`id` AS `id`,`wf_workflow`.`workflow_name` AS `workflow_name`,`wf_workflow_link`.`workflow_id` AS `workflow_id`,`wf_workflow_link`.`emp_id` AS `emp_id`,`wf_workflow_link`.`level_id` AS `level_id`,`wf_workflow_link`.`lsa` AS `lsa`,`wf_workflow_link`.`final` AS `final`,`wf_workflow_link`.`disp_message` AS `disp_message`,`wf_workflow_link`.`date_created` AS `date_created`,`wf_workflow_link`.`created_by` AS `created_by`,`employeeslist`.`first_name` AS `first_name`,`employeeslist`.`middle_name` AS `middle_name`,`employeeslist`.`last_name` AS `last_name`,`wf_approval_levels`.`level_name` AS `level_name` from (((`wf_workflow_link` join `employeeslist` on((`wf_workflow_link`.`emp_id` = `employeeslist`.`id`))) join `wf_workflow` on((`wf_workflow_link`.`workflow_id` = `wf_workflow`.`workflow_id`))) join `wf_approval_levels` on((`wf_workflow_link`.`level_id` = `wf_approval_levels`.`id`)))); ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
