<div id="user-flash-messages">
      <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-block alert-success fade in">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <div><i class="fa-fw fa fa-check"></i> <?php echo CHtml::decode(Yii::app()->user->getFlash('success')); ?></div>
            </div>
      <?php endif; ?>
      <?php if (Yii::app()->user->hasFlash('notice')): ?>
            <div class="alert alert-block alert-warning">
                  <button type="button" class="close" data-dismiss="alert"> &times;</button>
                  <div><i class="fa-fw fa fa-warning"></i> <?php echo CHtml::decode(Yii::app()->user->getFlash('notice')); ?></div>
            </div>
      <?php endif; ?>
      <?php if (Yii::app()->user->hasFlash('info')): ?>
            <div class="alert alert-block alert-info">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <div><i class="fa-fw fa fa-info"></i><?php echo CHtml::decode(Yii::app()->user->getFlash('info')); ?></div>
            </div>
      <?php endif; ?>
      <?php if (Yii::app()->user->hasFlash('error')): ?>
            <div class="alert alert-block alert-danger">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <div><i class="fa-fw fa fa-warning"></i><?php echo CHtml::decode(Yii::app()->user->getFlash('error')); ?></div>
            </div>
      <?php endif; ?>
</div>