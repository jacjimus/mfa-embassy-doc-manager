<?php



/**

 * stores the import configurations

 * @author Fredrick <mconyango@gmail.com>

 * @since 1.0

 * @version 1.0

 * @package bluebound

 */

return array(

    'application.models.*',

    'application.models.forms.*',

    'application.components.*',

    'application.components.widgets.*',

    'ext.easyimage.EasyImage',

    'ext.Gmap.classes.*',

    'application.extensions.simpleWorkflow.*',

    //pdfFactory

    'ext.pdffactory.*',

    'application.pdf.docs.*',//the path where I place the EPdfFactoryDoc classes

    //backup module

    'application.modules.backup.models.*',

    'application.modules.backup.components.*',

    //user module

    'application.modules.users.models.*',

    'application.modules.users.components.*',

    'application.modules.users.components.behaviors.*',

    //help module

    'application.modules.help.models.*',

    'application.modules.help.components.*',
    //Event module

    'application.modules.event.models.*',

    'application.modules.event.components.*',

    //settings module

    'application.modules.settings.models.*',

    'application.modules.settings.forms.*',

    'application.modules.settings.components.*',

    //messaging module

    'application.modules.msg.models.*',

    'application.modules.msg.components.*',

    //Employees

    'application.modules.employees.models.*',

    'application.modules.employees.components.*',

    //Payroll

    'application.modules.payroll.models.*',

    'application.modules.payroll.components.*',

    //Hr

    'application.modules.hr.models.*',

    'application.modules.hr.components.*',

    //auth module

    'application.modules.auth.models.*',

    'application.modules.auth.components.*',

    

    

    //Notification module

    'application.modules.notif.models.*',

    'application.modules.notif.components.*',

    //Document Manager

    'application.modules.manager.models.*',

    'application.modules.manager.components.*',

    //documents module

    'application.modules.doc.models.*',

    'application.modules.doc.components.*',

    

    

    //reports

    'application.modules.reports.models.*',

    'application.modules.reports.components.*',



    

);

