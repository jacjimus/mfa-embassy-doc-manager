<?php

class m161117_114051_update_notif_table extends CDbMigration
{
	public function safeUp()
	{
            $this->addColumn(Notif::model()->tableName(), 'approve_reject', 'TINYINT(4) AFTER `is_Seen` ');
            
	}

	public function safeDown()
	{
		echo "m161117_114051_update_notif_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}