<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">

    <div class="col-md-12">
        <?php $this->renderPartial('_grid', array('model' => $model)); ?>
    </div>
</div>

