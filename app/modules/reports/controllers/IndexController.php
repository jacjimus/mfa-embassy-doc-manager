<?php

/**
 * Home controller
 * @author John<johnerick8@gmail.com>
 */
class IndexController extends Controller {

    
    public function init() {
        $this->resourceLabel = Lang::t('System Reports');
        $this->resource = ReportsModuleConstants::RES_REPORTS_ALL;
        $this->activeTab = ManagerModuleConstants::TAB_GENERAL;
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index' , 'logs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($reference = null, $Doc_template =  null, $type = null, $status = null) {
       $this->hasPrivilege(Acl::ACTION_VIEW);
      
      $this->pageTitle = Lang::t('Document Reports');
      $search=new Newdoc();
      $searchModel = Newdoc::model()->searchModel(
              array('Type'=>$type,
                    'Reference'=>$reference,
                    'Doc_template'=>$Doc_template,
                    'Status' => $status )
              , 50, 'createDate', array());
        $this->render('index', array(
            'model' => $searchModel,
            'reference'=>$reference,
            'type'=>$type,
            'status'=>$status,
            'Doc_template'=>$Doc_template,
            'search' => $search,
            
        ));
		
    }

	 /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
   public function actionLogs($reference = null, $action =  null, $user_id = null, $action_date = null) {
       $this->hasPrivilege(Acl::ACTION_VIEW);
      
      $this->pageTitle = Lang::t('Document logs reports');
      $search=new DocumentLogs();
      $searchModel = DocumentLogs::model()->searchModel(
              array('action'=>$action,
                    'id_model'=> Newdoc::model()->getScalar('DocID' , "Reference LIKE '" .$reference. "'"),
                    'user_id'=>$user_id,
                    'action'=>$action,
                    'action_date' => $action_date )
              , 50, 'action_date DESC', array());
        $this->render('logs', array(
            'model' => $searchModel,
            'reference'=> $reference,
            'action'=>$action,
            'user_id'=>$user_id,
            'action'=>$action,
            'action_date' => $action_date,
            'search' => $search,
            
        ));
		
    }
		
	
	public function actionCosts(){
		$this->activeTab = TrainingModuleConstants::TAB_TRAINING_READING_MATERIALS;
        $this->pageTitle = Lang::t('Training Costs Reports');
		$this->render('costs');
	}

}
