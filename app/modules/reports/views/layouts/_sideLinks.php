 <li>
             <a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent"><?php echo Lang::t('Document Reports') ?></span></a>
             <ul>
                 <?php if ($this->showLink(ReportsModuleConstants::RES_REPORTS_ALL)): ?>
                     <li class="<?php echo $this->activeMenu === ReportsModuleConstants::MENU_REPORTS_ALL ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('reports/index/') ?>"><i class="fa fa-lg fa-fw"></i> <span class="menu-item-parent"><?php echo Lang::t('All Documents Reports') ?></span></a></li>
                 <?php endif; ?>
                 <?php if ($this->showLink(ReportsModuleConstants::RES_REPORTS_LOGS)): ?>
                     <li class="<?php echo $this->activeMenu === ReportsModuleConstants::MENU_REPORTS_LOGS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('reports/index/logs') ?>"><i class="fa fa-lg fa-fw"></i> <span class="menu-item-parent"><?php echo Lang::t('Documents Logs') ?></span></a></li>
                 <?php endif; ?> 
             </ul>
            </li>