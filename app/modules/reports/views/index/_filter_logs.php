<?= CHtml::form(Yii::app()->createUrl($this->route, $this->actionParams), 'get', ['class' => '']) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::t('Filter By:') ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <?= CHtml::label('Reference', "", ['class' => 'control-label']) ?>
                <br/>
                <?= CHtml::textField('reference', $reference, ['class' => 'form-control']); ?>
            </div>
            <div class="col-md-2">
                <?= CHtml::label('Action type', "", ['class' => 'control-label']) ?>
                 <?php
             Yii::import('ext.chosen.Chosen');
                    $datas = ['CREATE' => 'Create' , 'UPDATE' => 'UPDATE', 'DELETE' => 'DELETE'];
                    $sigs = array();

                    foreach ($datas as $ds=>$v)
                        $sigs[$ds] = $v; 
                    echo Chosen::dropDownList('action', $action, $sigs, array('prompt' => 'Select action',  'class' => 'form-control',
                        ));?>
            </div>
            <div class="col-md-2">
                <?= CHtml::label('User', "", ['class' => 'control-label']) ?>
                 <?php
                $data = UsersView::model()->findAll();
                    $arr = array();

                    foreach ($data as $ds)
                        $arr[$ds->id] = $ds->name; 
                    echo Chosen::dropDownList('user_id', $user_id, $arr, array('prompt' => 'Select user',  'class' => 'form-control',
                        ));?>
            </div>

           
            
			<div class="col-md-1">
                <br/>
                <button class="btn btn-primary" type="submit"><?= Lang::t('Filter') ?></button>
            </div>
            <div class="col-md-1">
                <br/>
                <a class="btn btn-default"
                   href="<?= Yii::app()->createUrl($this->route) ?>"><?= Lang::t('Clear filter') ?></a>
            </div>
        </div>
    </div>
</div>
<?= CHtml::endForm() ?>