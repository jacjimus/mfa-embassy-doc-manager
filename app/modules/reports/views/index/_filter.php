<?= CHtml::form(Yii::app()->createUrl($this->route, $this->actionParams), 'get', ['class' => '']) ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?=Lang::t('Filter By:') ?></h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-2">
                <?= CHtml::label('Reference', "", ['class' => 'control-label']) ?>
                <br/>
                <?= CHtml::textField('reference', $reference, ['class' => 'form-control']); ?>
            </div>
            <div class="col-md-2">
                <?= CHtml::label('Document type', "", ['class' => 'control-label']) ?>
                 <?php
             Yii::import('ext.chosen.Chosen');
                    $datas = Doc::model()->findAll();
                    $sigs = array();

                    foreach ($datas as $ds)
                        $sigs[$ds->id] = $ds->name; 
                    echo Chosen::dropDownList('Doc_template', $Doc_template, $sigs, array('prompt' => 'Select type',  'class' => 'form-control',
                        ));?>
            </div>
            <div class="col-md-2">
                <?= CHtml::label('Document status', "", ['class' => 'control-label']) ?>
                 <?php
                $data = Newdoc::model()->getStatus();
                    $arr = array();

                    foreach ($data as $ds=>$v)
                        $arr[$ds] = $v; 
                    echo Chosen::dropDownList('status', $status, $arr, array('prompt' => 'Select status',  'class' => 'form-control',
                        ));?>
            </div>

           
            
			<div class="col-md-1">
                <br/>
                <button class="btn btn-primary" type="submit"><?= Lang::t('Filter') ?></button>
            </div>
            <div class="col-md-1">
                <br/>
                <a class="btn btn-default"
                   href="<?= Yii::app()->createUrl($this->route) ?>"><?= Lang::t('Clear filter') ?></a>
            </div>
        </div>
    </div>
</div>
<?= CHtml::endForm() ?>