<?php

$grid_id = 'logs-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'id' => "reports-grid",
    'titleIcon' => '<i class="fa fa-bar-chart"></i>',
    'showExportButton' => true,
    'showSearch' => false,
    'createButton' => array('visible' => false, 'modal' => false,'url'=>'importCsv'),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            array(
            'name' => 'action_date',
            'type' => 'datetime',
             'value' => '$data->action_date'
       ),
            
            array(
            'name' => 'description',
            'type' => 'raw',
             'value' => '$data->description'
                ),
            array(
            'name' => 'action',
            'type' => 'raw',
             'value' => '$data->action'
                ),
            array(
            'name' => 'user_id',
            'type' => 'raw',
             'value' => 'UsersView::model()->get($data->user_id , "name")'
                ),
       
            
           
            
        ),
    )
));
?>
