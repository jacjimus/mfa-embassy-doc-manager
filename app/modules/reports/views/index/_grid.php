<?php

$grid_id = 'employee-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'id' => "reports-grid",
    'titleIcon' => '<i class="fa fa-bar-chart"></i>',
    'showExportButton' => true,
    'showSearch' => false,
    'createButton' => array('visible' => false, 'modal' => false,'url'=>'importCsv'),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '', //'!$data->inuse?"bg-danger":""',
        'columns' => array(
            'Reference',
            
            array(
            'name' => 'CreateDate',
            'type' => 'date',
             'value' => '$data->CreateDate'
                ),
            array(
            'name' => 'Author',
            'type' => 'raw',
             'value' => 'UsersView::model()->get($data->Author , "name")'
                ),
       
            array(
            'name' => 'Approver',
            'type' => 'raw',
             'value' => '!empty($data->Approver)? UsersView::model()->get($data->Approver , "name") : UsersView::model()->get($data->Author , "name")'
       ),
            'Type',
            'CreateDate',
            array(
            'name' => 'Status',
            'type' => 'raw',
             'value' => 'CHtml::tag("span", array("class"=>$data->Status=="' . Newdoc::STATUS_SAVED . '"?"badge badge-success":"badge badge-default"), $data->Status)',
       
        ),
            array(
            'name' => 'PrintCount',
            'type' => 'raw',
             'value' => 'CHtml::tag("span", array("class"=>$data->PrintCount  > 0 ?"badge badge-success":"badge badge-default"), $data->PrintCount)',
       
        ),
            array(
            'name' => 'Doc_template',
            'type' => 'raw',
             'value' => 'Doc::model()->get($data->Doc_template , "name") '
       ),
           
            
        ),
    )
));
?>
