<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">

    <div class="col-md-12">
	 <div class="well well-lights">
	
        <div class="row">
                 <?php   $this->renderPartial('_filter', [
                    'reference'=>$reference,
                    'type'=>$type,
                    'status'=>$status,
                    'Doc_template'=>$Doc_template,
                     'search' => $search]); 
            ?>
         </div>
        <div class="row">
        <?php 
		
		$this->renderPartial('_grid', array('model' => $model, 
                    )); ?>
    </div>
    </div>
	</div>
</div>

