<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">

    <div class="col-md-12">
	 <div class="well well-lights">
	
        <div class="row">
                 <?php   $this->renderPartial('_filter_logs', [
                     'reference'=> $reference,
                     'action'=>$action,
                     'user_id' => $user_id,
                     'action' => $action,
                     'action_date' => $action_date,
                     'search' => $search]); 
            ?>
         </div>
        <div class="row">
        <?php 
		
		$this->renderPartial('_grid_logs', array('model' => $model, 
                    )); ?>
    </div>
    </div>
	</div>
</div>

