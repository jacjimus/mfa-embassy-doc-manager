<?php

/**
 * System user behavior
 */
class ReportsBehavior extends CActiveRecordBehavior {

        

        private $_oldattributes = array();
 
    public function afterSave($event)
    {
        $newattributes = $this->Owner->getAttributes();
        $attributes = $this->Owner->isNewRecord ? $newattributes : $this->getOldAttributes();
        $refno = $newattributes['Reference'];
        if (!$this->Owner->isNewRecord) {
 
            // new attributes
            
            $oldattributes = $this->getOldAttributes();
            
            // compare old and new
            foreach ($newattributes as $name => $value) {
                if (!empty($oldattributes)) {
                    $old = $oldattributes[$name];
                } else {
                    $old = '';
                }
                 if ($value != $old && !in_array($name  , ['CreateDate' , 'Author'])) {
                       
                    $log=new DocumentLogs;
                    $log->description= UsersView::model()->get(Yii::app()->user->id, 'name') 
                                            . ' edited ' . $name . ' on document Ref no. ' .$refno . ' ' 
                                             . ' from  '. $old.' to '.$value . ' on '
                                            . date('Y-m-d h:i:s') 
                                            ;
                    $log->action=       'UPDATE';
                    $log->model=        get_class($this->Owner);
                    $log->id_model=      $this->Owner->getPrimaryKey();
                    $log->field=        $name;
                    $log->action_date= new CDbExpression('NOW()');
                    $log->user_id=       Yii::app()->user->id;
                    $log->save();
                    // Create a notification if Status changes
                    if($name == 'Status'):
                          $model = Doc::model()->loadModel($oldattributes['Doc_template']);
                          if($model->approval):
                              $alert = new Notif;
                              $alert->notif_type_id = $newattributes[$name] == Newdoc::STATUS_APPROVED ? Newdoc::NOTIF_TYPE_APPROVED : Newdoc::NOTIF_TYPE_REJECTED;
                              // if newly created , Notify the person to approve else for any other status change, notify the author
                              $alert->user_id =  $newattributes['Author'];
                              $alert->item_id = $newattributes['DocID'];
                              $alert->is_read = 0;
                              $alert->is_seen = 0;
                              $alert->approve_reject = $newattributes[$name] == Newdoc::STATUS_APPROVED ? 1 : 0;
                              $alert->save();
                          endif;
                    endif;
                }
            }
        } else {
            $log=new DocumentLogs;
            $log->description=  UsersView::model()->get(Yii::app()->user->id, 'name') 
                                            . ' created document Ref no. ' .$refno . ' ' 
                                            . ' on '
                                            . date('Y-m-d h:i:s') 
                                            ;
            $log->action=       'CREATE';
            $log->model=        get_class($this->Owner);
            $log->id_model=      $this->Owner->getPrimaryKey();
            $log->field=        '';
            $log->action_date= new CDbExpression('NOW()');
            $log->user_id=       Yii::app()->user->id;
            $log->save();
            
             $model = Doc::model()->loadModel($newattributes['Doc_template']);
                          if($model->approval):
                              $alert = new Notif;
                              $alert->notif_type_id = $model->notify_type_id;
                              // if newly created , Notify the person to approve else for any other status change, notify the author
                              $alert->user_id = $model->person_to_approve;
                              $alert->item_id = $newattributes['DocID'];
                              $alert->is_read = 0;
                              $alert->is_seen = 0;
                              $alert->save();
                          endif;
   
        }
    }
 
    public function afterDelete($event)
    {
        $attributes =  $this->getOldAttributes();
        $refno = $attributes['Reference'];
        $log=new DocumentLogs;
        $log->description=  UsersView::model()->get(Yii::app()->user->id, 'name') 
                                            . ' deleted document Ref no. ' .$refno . ' on ' 
                                             . date('Y-m-d h:i:s') 
                                            ;
        $log->action=       'DELETE';
        $log->model=        get_class($this->Owner);
        $log->id_model=      $this->Owner->getPrimaryKey();
        $log->field=        '';
        $log->action_date= new CDbExpression('NOW()');
        $log->user_id=       Yii::app()->user->id;
        $log->save();
    }
 
    public function afterFind($event)
    {
        // Save old values
        $this->setOldAttributes($this->Owner->getAttributes());
    }
 
    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }
 
    public function setOldAttributes($value)
    {
        $this->_oldattributes=$value;
    }
}


