<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class ReportsModuleConstants {

    //resources constants
    const MENU_REPORTS_ALL = 'all';
    const MENU_REPORTS_LOGS = 'logs';
    const MENU_REPORTS_COMPANY = 'company';
    const MENU_REPORTS_ARCHIVE = 'archive';
    const RES_REPORTS_ALL = 'RES_REPORTS_ALL';
    const RES_REPORTS_LOGS = 'RES_REPORTS_LOGS';
   

}
