<div class="list-group">
        <a href="<?php echo Yii::app()->createUrl('users/default/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_USERS ? ' active' : '' ?>"><?php echo Lang::t('Users') ?></a>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_ROLES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/roles/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_ROLES ? ' active' : '' ?>"><?php echo Lang::t('Manage Roles') ?></a>
        <?php endif; ?>
        <?php if ($this->showLink(UsersModuleConstants::RES_USER_RESOURCES)): ?>
                <a href="<?php echo Yii::app()->createUrl('users/resources/index') ?>" class="list-group-item<?php echo $this->activeTab === UsersModuleConstants::TAB_RESOURCES ? ' active' : '' ?>"><?php echo Lang::t('Manage Resources') ?></a>
        <?php endif; ?>
        
</div>
