<?php echo CHtml::errorSummary($model, '') ?>
<div class="form-group">
    <label class="col-md-3 control-label"><?php echo Lang::t('Full name') ?><span class="required">*</span></label>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'first_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('first_name'))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'middle_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('middle_name'))); ?>
    </div>
    <div class="col-md-3">
        <?php echo CHtml::activeTextField($model, 'last_name', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => $model->getAttributeLabel('last_name'))); ?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => 'col-md-3 control-label')); ?>
    <div class="col-md-8" >
        <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control', 'maxlength' => 30, 'placeholder' => "Format: 254725830529")); ?>
    
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabelEx($model, 'gender', array('class' => 'col-md-3 control-label')); ?>
    <div class="col-md-6" style="padding-top: 4px;">
        <?php echo CHtml::activeRadioButtonList($model, 'gender', Person::genderOptions(), array('separator' => '&nbsp;&nbsp;&nbsp;&nbsp;')); ?>
    </div>
</div>




