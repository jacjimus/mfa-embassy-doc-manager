<?php
if (!$model->getIsNewRecord()) {
        $can_update = Users::model()->checkPrivilege($this, Acl::ACTION_UPDATE, false, $model->user_level) && !Users::isMyAccount($model->id);
}
?>
<?php echo CHtml::errorSummary($model, '') ?>
<?php if ($model->getIsNewRecord() || $can_update) : ?>
         <?php echo CHtml::activeHiddenField($model, 'user_level', array('value' => 'ADMIN')); ?>
               
        <div class="form-group" id="">
                <?php echo CHtml::activeLabelEx($model, 'role_id', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-8">
                        <?php echo CHtml::activeDropDownList($model, 'role_id', UserRoles::model()->getListData('id', 'name'), array('class' => 'form-control select2')); ?>
                </div>
        </div>
<?php endif; ?>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-8">
                <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128, 'type' => 'email')); ?>
        </div>
</div>
<div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'username', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-8">
                <?php echo CHtml::activeTextField($model, 'username', array('class' => 'form-control', 'maxlength' => 30)); ?>
        </div>
</div>
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'password', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-8">
                        <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'confirm', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-8">
                        <?php echo CHtml::activePasswordField($model, 'confirm', array('class' => 'form-control')); ?>
                </div>
        </div>
<?php endif ?>

                <?php echo CHtml::activeHiddenField($model, 'timezone', array('value' => 'Africa/Nairobi')); ?>
        
<?php if ($model->isNewRecord): ?>
        <div class="form-group">
                <div class="col-md-offset-4 col-md-5">
                        <label class="checkbox"><?php echo CHtml::activeCheckBox($model, 'send_email'); ?> <?php echo Lang::t('Email the login details to the user.') ?></label>
                </div>
        </div>
<?php endif; ?>
