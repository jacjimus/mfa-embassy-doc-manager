<?php

/**
 *
 * @author James Makau <jacjimus@gmail.com>
 */
class ManagerModuleController extends Controller {

        const MENU_CREATE = 'Create';
        const MENU_UPDATE = 'Update';
        const MENU_APPROVE = 'Approve';
        const MENU_MANAGE = 'Doc Manager';
        const MODULE_NAME = "Documents Manager";
        public function init()
        {
                parent::init();
        }
        

}
