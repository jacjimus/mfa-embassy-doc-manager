<?php

/**
 * Defines all constants used within the module
 *
 * @author Fred <mconyango@gmail.com>
 */
class ManagerModuleConstants {

    //resources constants
    const MOD_DOC_MANAGER = 'doc_manager';
    const RES_DOC = 'RES_DOC';
    const RES_EMPLOYEE_LIST = 'EMPLOYEE_LIST';
    const MENU_DOC_MINE = 'Doc Mine';
    const MENU_DOC_OTHERS = 'Doc Others';
    const MENU_DOC_APPROVE = 'Approve_docs';
    const RES_MANAGER = 'RES_MANAGER';
    const RES_DOC_APPROVAL = 'RES_DOC_APPROVAL';
    const RES_DOC_MINE = 'RES_DOC_MINE';
    const RES_DOC_OTHERS = 'RES_DOC_OTHERS';
    //tab constants
    const MENU_TRAINING_REPORTS_IFMIS = 'TAB_BIODATA';
    const MENU_TRAINING_REPORTS_PROFESSIONAL = 'TAB_JOB_DETAILS';
    const MENU_TRAINING_REPORTS_SHT_COURSES_EXT = 'TAB_BANK_DETAILS';
    const TAB_DEPENDANTS = 'TAB_DEPENDANTS';
    const TAB_GENERAL = 'TAB_GENERAL';
    const TAB_QUALIFICATIONS = 'TAB_QUALIFICATIONS';
    const TAB_WORK_EXPERIENCE = 'TAB_WORK_EXPERIENCE';
    const TAB_SKILLS = 'TAB_SKILLS';
    const TAB_HOUSING = 'TAB_HOUSING';
    const TAB_STATUTORY = 'TAB_STATUTORY';
    const TAB_TRAININGS = 'TAB_TRAININGS';

}
