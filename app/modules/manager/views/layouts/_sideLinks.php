
<?php
if (ModulesEnabled::model()->isModuleEnabled(ManagerModuleConstants::MOD_DOC_MANAGER)): ?>
      <?php if ($this->showLink(ManagerModuleConstants::RES_DOC)): ?>
 <li>
             <a href="#"><i class="fa fa-lg fa-fw fa-file-pdf-o"></i> <span class="menu-item-parent"><?php echo Lang::t('Manage Documents') ?></span></a>
             <ul>
                  <?php if ($this->showLink(ManagerModuleConstants::RES_DOC_MINE)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_MINE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/self') ?>"><i class="fa fa-lg fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('My Documents') ?></span></a></li>
                 <?php endif; ?>
                  <?php if ($this->showLink(ManagerModuleConstants::RES_DOC_OTHERS)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_OTHERS ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/index') ?>"><i class="fa fa-lg fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Others Documents') ?></span></a></li>
                 <?php endif; ?>
                    <?php if ($this->showLink(ManagerModuleConstants::RES_DOC_APPROVAL)): ?>
                <li class="<?php echo $this->activeMenu === ManagerModuleConstants::MENU_DOC_APPROVE ? 'active' : '' ?>"><a href="<?php echo Yii::app()->createUrl('manager/default/approve') ?>"><i class="fa fa-lg fa-arrow-right"></i> <span class="menu-item-parent"><?php echo Lang::t('Approve Documents') ?></span></a></li>
             <?php endif; ?>  
             </ul>
            </li>
            <?php endif; ?>
<?php endif; ?>