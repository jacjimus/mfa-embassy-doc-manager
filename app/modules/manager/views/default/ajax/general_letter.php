<div class="alert hidden" id="my-modal-notif"></div>

<?php echo CHtml::activeHiddenField($model, "Doc_template", array('value' => $id)); ?>

<div class="form-group">
    <?php echo CHtml::activeLabel($model, 'Content', array('class' => 'col-lg-4 control-label')); ?>
    <div class="col-lg-12">
        <?php echo CHtml::error($model, 'Content') ?>
        <?php echo CHtml::activeTextArea($model, 'Content', array('class' => 'redactor', 'rows' => 10, 'size' => 500)); ?>
    </div>  
</div>  
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-success" name="forward" type="submit" ><i class="icon-forward bigger-110"></i> <?php echo Lang::t('Save Document') ?></button>
</div>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 100,
        'convertDivs' => true,
        'cleanup' => TRUE,
        'paragraphy' => false,
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
?>