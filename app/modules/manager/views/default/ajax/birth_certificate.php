<div class="alert hidden" id="my-modal-notif"></div>
<?php echo CHtml::activeHiddenField($model, "Doc_template", array('value' => $id)); ?>
<div class="form-group">
     <div class="col-lg-2"></div>
   
    <div class="col-lg-5">
        <?php echo CHtml::activeTextField($model, 'surname', array('class' => "form-control" , 'placeholder' => 'Surname')); ?>


        <?php echo CHtml::error($model, 'surname') ?>
    </div>
    <div class="col-lg-5">
        <?php echo CHtml::activeTextField($model, 'first_name', array('class' => "form-control", 'placeholder' => 'First name')); ?>

        <?php echo CHtml::error($model, 'first_name') ?>
    </div>
</div>
 
    <?php Yii::import('ext.chosen.Chosen'); ?>
       
<div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-2">
         <?php echo CHtml::activeLabel($model, 'date_of_birth', array('class' => 'form-label', 'style' => 'font-size: 12px !important;')); ?>
    </div>

        <div class="col-md-8">
            <div class="col-md-4">
                <?php echo Chosen::activeDropDownList($model, 'birthdate_month', Person::birthDateMonthOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-4">
                <?php echo Chosen::activeDropDownList($model, 'birthdate_day', Person::birthDateDayOptions(), array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-4">
                <?php echo Chosen::activeDropDownList($model, 'birthdate_year', Person::birthDateYearOptions(), array('class' => 'form-control select2')); ?>
            </div>
        </div>
    
</div>
<div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        <?php echo CHtml::activeTextField($model, 'mothers_name', array('class' => "form-control", 'placeholder' => 'Mother\'s name')); ?>


        <?php echo CHtml::error($model, 'mothers_name') ?>
    </div>
 <div class="col-lg-5">
        <?php echo CHtml::activeTextField($model, 'fathers_name', array('class' => "form-control ", 'placeholder' => 'Father\'s name')); ?>


        <?php echo CHtml::error($model, 'fathers_name') ?>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-2"></div>
    <div class="col-lg-5">
        <?php echo Chosen::activeDropDownList($model, 'marital_status', [''=> 'Marrital status', 'Single' => 'Single', 'Married' => 'Married' , 'Divorced' => 'Divorced'], array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'marital_status') ?>
    </div>
    <div class="col-lg-5">
        <?php echo Chosen::activeDropDownList($model, 'sex', [''=> 'Gender', 'Male' => 'Male', 'Female' => 'Female'], array('class' => "form-control")); ?>


        <?php echo CHtml::error($model, 'sex') ?>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-2"></div>
    
    <div class="col-lg-5">
        <?php echo CHtml::activeTextField($model, 'place_of_birth', array('class' => "form-control" ,'placeholder' => 'Place of Birth')); ?>


        <?php echo CHtml::error($model, 'place_of_birth') ?>
    </div>
 
    <div class="col-lg-5">
        <?php 
         Yii::import('ext.nationality.Nationality'); 
        $nationals = Nationality::nationals();        
        ?>
        <?php echo Chosen::activeDropDownList($model, 'nationality', $nationals , array('prompt' => 'Nationality',  'class' => "form-control" )); ?>


        <?php echo CHtml::error($model, 'nationality') ?>
    </div>
</div>
<fieldset>
            <legend><?php echo Lang::t('Passport photo') ?></legend>
            <?php $this->renderPartial('ajax/_image_field', array('model' => $model, 'htmlOptions' => array('label_class' => 'col-md-2 control-label', 'field_class' => 'col-md-4'))) ?>
        </fieldset>


<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <button class="btn btn-success" name="forward" type="submit" ><i class="icon-forward bigger-110"></i> <?php echo Lang::t('Save Document') ?></button>
</div>
<?php
Yii::import('ext.redactor.ImperaviRedactorWidget');
$this->widget('ImperaviRedactorWidget', array(
    // the textarea selector
    'selector' => '.redactor',
    // some options, see http://imperavi.com/redactor/docs/
    'options' => array(
        'minHeight' => 50,
        'convertDivs' => true,
        'cleanup' => TRUE,
        'paragraphy' => false,
    ),
    'plugins' => array(
        'fullscreen' => array(
            'js' => array('fullscreen.js',),
        ),
    ),
));
?>