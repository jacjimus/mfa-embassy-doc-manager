<?php
$this->breadcrumbs = array(' Manage Documents' => 'approve',
    $this->pageTitle,
);
?>
<div class="widget-box transparent ">
    <p>
         <?php if(Doc::model()->getScalar('person_to_approve' , "doc_alias LIKE ':alias'",  [':alias' => $template] ) == Yii::app()->user->id): ?>
       
        <?=CHtml::link("<i class='fa fa-1x fa-check-circle-o'>&nbsp;</i>Approve Document" , "#" , ['class' => 'btn btn-success change_status', "data-ajax-url" => $this->createUrl('approve', array('id' => $id, 'status' => Newdoc::STATUS_APPROVED))])?>
         
        &nbsp;&nbsp;&nbsp;
        <?=CHtml::link("<i class='fa fa-1x fa-times-circle-o'>&nbsp;</i>Reject Document" , "#" , ['class' => 'btn btn-danger change_status' , "data-ajax-url" => $this->createUrl('approve', array('id' => $id, 'status' => Newdoc::STATUS_REJECTED))])?>
   <?php endif; ?>
        &nbsp;&nbsp;&nbsp;
        <?php if(Newdoc::model()->get($id , "Status") == Newdoc::STATUS_APPROVED): ?>
        <?=CHtml::link("<i class='fa fa-1x fa-print'>&nbsp;</i>Print Document" ,"#", ['class' => 'btn btn-default btnPrint'])?>
        <?php endif; ?>
    </p>
        <div class="widget-body widget-body-style2">
                <div class="widget-main padding-12 no-padding-left no-padding-right">
                        <div class="tab-content padding-4">
                                <?php $this->renderPartial('_view', array('file' => $file, 'id' => $id)); ?>
                        </div>
                </div>
        </div>
</div>


<?php
    $theme = Yii::app()->baseUrl;
    Yii::app()->clientScript->registerScript('allowprint', '
    $(".btnPrint").printPage({
  url: "self",
  attr: "href",
  message:"Your document is being processes"
})
',  CClientScript::POS_READY)
 ->registerScriptFile($theme.' /js/jquery-1.4.4.min.js')
 ->registerScriptFile($theme.' /js/jquery.printPage.js', CClientScript::POS_END);
   