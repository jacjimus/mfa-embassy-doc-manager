
<?php

$grid_id = 'all-docs-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Available documents'),
    'titleIcon' => '<i class="fa fa-file"></i>',
    'showExportButton' => false,
    'showSearch' => true,
    //'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true , 'label' => '<i class="fa fa-plus">&nbsp;</i>Create new document' ),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        
        'columns' => array(
         
        array(
            'name' => 'Reference',
            'type' => 'raw',
            'value' => '$data->Reference',
        ),
        
        array(
            'name' => 'CreateDate',
            'type' => 'raw',
            'value' => '$data->CreateDate',
        ),
        array(
            'name' => 'Author',
            'type' => 'raw',
            'value' => 'UsersView::model()->get($data->Author , "name")',
        ),
        
        array(
            'name' => 'Applicant_name',
            'type' => 'raw',
            //'visible' => true : false,
            'value' => '$data->Applicant_name',
        ),
        
        
        array(
            'name' => 'Status',
            'type' => 'raw',
             'value' => 'CHtml::tag("span", array("class"=>$data->Status=="' . Newdoc::STATUS_SAVED . '"?"badge badge-success":"badge badge-default"), $data->Status)',
       
        ),
        
        
        array(
                'class' => 'ButtonColumn',
                'template' => '{approve}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'download' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-download fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("download",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_APPROVED.'"?true:false',
                        'options' => array(
                            'class' => '',
                            'title' => Lang::t('Download'),
                        ),
                    ),
                    'visa' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x text-warning"></i>',
                        'url' => 'Yii::app()->controller->createUrl("visarequest",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'" && $data->Doc_template == "4"?true:false',
                        'options' => array(
                            'target' => '_blank',
                            'title' => Lang::t('View details'),
                        ),
                    ),
                    'immigration' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x text-warning"></i>',
                        'url' => 'Yii::app()->controller->createUrl("immigration",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'" && $data->Doc_template == "5"?true:false',
                        'options' => array(
                            'target' => '_blank',
                            'title' => Lang::t('View details'),
                        ),
                    ),
                    'forwardnote' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x text-warning"></i>',
                        'url' => 'Yii::app()->controller->createUrl("forwardnote",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'" && $data->Doc_template == "6"?true:false',
                        'options' => array(
                            'target' => '_blank',
                            'title' => Lang::t('View details'),
                        ),
                    ),
                    'noobjection' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x text-warning"></i>',
                        'url' => 'Yii::app()->controller->createUrl("noobjection",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'" && $data->Doc_template == "7"?true:false',
                        'options' => array(
                            'target' => '_blank',
                            'title' => Lang::t('View details'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'"?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_SAVED.'"?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                    'approve' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-check fa-2x text-warning"></i>',
                        'url' => 'Yii::app()->controller->createUrl("approve",array("id"=>$data->DocID))',
                        'visible' => '$data->Status == "'.Newdoc::STATUS_PENDING.'"?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_APPROVE),
                        ),
                    ),
                )
            )
    ),
)
)
)
;
?>

