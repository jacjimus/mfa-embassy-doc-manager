
   
 <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
           <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'my-modal-form',
                    'action'=>$model->isNewRecord ? Yii::app()->createUrl('manager/default/create') : Yii::app()->createUrl('manager/default/update?id='.$model->DocID),
                    'enableAjaxValidation' => false,
                    'htmlOptions' => array(
                        'class' => 'form-horizontal',
                    )
                ));
   if($model->isNewRecord):
       ?>
   
   <div class="form-group"  style="">
       <div class="col-lg-2"></div>
                <div class="col-lg-8">
             <?php
             Yii::import('ext.chosen.Chosen');
                    $datas = Doc::model()->findAll("doc_type_id = 1");
                    $sigs = array();

                    foreach ($datas as $ds)
                        $sigs[$ds->doc_alias] = $ds->name; 
                    echo Chosen::dropDownList('Doc_template',"[Select document category]", $sigs, array('prompt' => '[Select document category]',  'class' => 'form-control',
                        'ajax' => array(
                        'type'=>'POST',
                        'url'=>CController::createUrl('loadform'),
                        'data' => array('doc_type'=>'js:this.value'),
                        'update'=>'#form-load',
                         
                    ),

                        ));?>
                        
            <?php echo CHtml::error($model, 'Doc_template') ?>
                </div>
  </div>
 <?php endif; ?>
   <div id="form-load">
      <?php
      if(!$model->isNewRecord):
          $view_file = is_null(Doc::model()->getScalar("doc_alias", "id=$model->Doc_template")) ? 'general' : Doc::model()->getScalar("doc_alias", "id=$model->Doc_template");
     $this->renderPartial("ajax/". $view_file , array('model' => $model, 'id' => $id ) , false, true);
endif;
?> 
       <?php $this->endWidget();?>
   </div>
   <div class="clearfix"></div>
</div>
   
  


