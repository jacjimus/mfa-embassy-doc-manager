<?php
$this->breadcrumbs = array(
    $this->module_name,
    "My Documents"
);
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('manager.views.default._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="row">
            
        </div>
        <?php $this->renderPartial('_grid', array('model' => $model ,'dtp' => $dtp)); ?>
    </div>
</div>