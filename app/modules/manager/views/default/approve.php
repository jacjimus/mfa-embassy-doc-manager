<div class="row">
    
    <div class="col-md-12">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title txt-color-blueDark">
                    <i class="fa fa-fw fa-file"></i>
                    <?php echo CHtml::encode($this->pageTitle); ?>
                    
                </h1> 
            </div>
        </div>
        <?php $this->renderPartial('_approve', array('model' => $model)); ?>
    </div>
</div>