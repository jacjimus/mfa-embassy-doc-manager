<?php

/**
 * This is the model class for table "user_activity".
 *
 * The followings are the available columns in table 'user_activity':
 * @property string $id
 * @property string $user_id
 * @property string $type
 * @property string $description
 * @property string $ip_address
 * @property string $datetime
 *
 * The followings are the available model relations:
 * @property Users $user
 */
class DocumentLogs extends ActiveRecord
{

    const TYPE_CREATE = 'create';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETE = 'delete';

    /**
     * Do not record activity for this model
     * @var type
     */
    public $logAuditTrail = false;

    public $reference;
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'document_logs';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('id, description, action, action,model, id_model, user_id' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'user_id' => Lang::t('User'),
            'description' => Lang::t('Description'),
            'model' => Lang::t('Model'),
            'id_model' => Lang::t('id_model'),
            'action_date' => Lang::t('Time'),
            'field' => Lang::t('Field'),
            'action' => Lang::t('Action'),
        );
    }

   
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('action', $this->action);
        $criteria->compare('action_date', $this->action_date);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('id_model', $this->id_model);
       // $criteria->compare('Deleted', $this->Deleted);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

   
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserActivity the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Add activity
     * @param integer $user_id
     * @param string $type
     * @param string $description
     */
    public function addActivity($user_id, $action, $description)
    {
        return Yii::app()->db->createCommand()
                        ->insert($this->tableName(), array(
                            'user_id' => $user_id,
                            'action' => $action,
                            'description' => $description,
                            'ip_address' => Common::getIp(),
                            'datetime' => new CDbExpression('NOW()'),
        ));
    }

    public function getLastAccountActivity()
    {
        $data = $this->getData('datetime', '', array(), 'id desc', 1);
        if (!empty($data))
            return MyYiiUtils::formatDate($data[0]['datetime'], 'Y-m-d H:i:s');
        return false;
    }

}
