<?php

class Newdoc extends ActiveRecord implements IMyActiveSearch {
        // SMS Notification Status
      
       const STATUS_SAVED = "Saved";
       const STATUS_PENDING = "Submitted";
       const STATUS_APPROVED = "Approved";
       const STATUS_REJECTED = "Rejected";
       const STATUS_PRINTED = "Printed";
       const SEARCH_FIELD = '_search';
       const STATUS_ACTIVE = false;
       const STATUS_DELETED = true;
       const BASE_DIR = 'birth_certificate_photos';
       
       const NOTIF_TYPE_APPROVED = 'alert_doc_approved';
       const NOTIF_TYPE_REJECTED = 'alert_doc_rejected';
       
      public $Title;
      public $surname;
      public $first_name;
      public $name;
      public $sex;
      public $place_of_birth;
      public $date_of_birth;
      public $mothers_name;
      public $fathers_name;
      public $marital_status;
      public $nationality;
      public $td_no;
      public $letter_ref_no;
      public $Academic_year;
      public $Content;
      public $temp_profile_image;
      public $birthdate_month;
      public $birthdate_day;
      public $birthdate_year;
      public $status_label;
       
      
      public function behaviors() {
        $behaviors = array();
        $behaviors['ReportsBehavior'] = array(
            'class' => 'application.modules.reports.components.behaviors.ReportsBehavior',
        );
        return array_merge(parent::behaviors(), $behaviors);
    }

       /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return SettingsEmailTemplate the static model class
         */
        public static function model($className = __CLASS__)
        {
                return parent::model($className);
        }
        
         public function getFilePath() {
            return $this->getDir() . DS . $this->doc_file;
      }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return 'tbl_documents';
        }
        
        /*
         * @return boolean
         */
        
        
        public static function statusOptions() {
        return array(
            self::STATUS_ACTIVE => Lang::t(self::STATUS_ACTIVE),
            self::STATUS_PENDING => Lang::t(self::STATUS_PENDING),
            self::STATUS_APPROVED => Lang::t(self::STATUS_APPROVED),
            self::STATUS_DELETED => Lang::t(self::STATUS_DELETED),
            self::STATUS_PRINTED => Lang::t(self::STATUS_PRINTED),
            self::STATUS_REJECTED => Lang::t(self::STATUS_REJECTED),
            self::STATUS_SAVED=> Lang::t(self::STATUS_SAVED),
                
        );
    }
        
        
        /* 
         * This function assings some custome variables which are not 
         * nessesarily stored in database as the user inputs but needs some modification
         * It assings the variables values before validation is done
         * @return Array[];
         */
        
        public function beforeValidate() {
            $this->Author = Yii::app()->user->id;
            $this->CreateDate = date('Y-m-d h:i:s');
            $maxid = Yii::app()->db->createCommand()
            ->select('max(DocID) as max')
            ->from('tbl_documents')
            ->queryScalar();
            $id = $maxid + 1;
            $this->Reference = "MFA/" .  "NRB/" . date('Y') . "/" .  date('m') . "/" . $id;
            if($this->Doc_template == 8): // for birth certificates
            $this->Applicant_name = sprintf("%s&nbsp;%s", $this->surname, $this->first_name);
            if (!empty($this->birthdate_day) && !empty($this->birthdate_month) && !empty($this->birthdate_year))
            $this->date_of_birth = $this->birthdate_year . "-" . $this->birthdate_month . "-" . $this->birthdate_day;
             $this->setProfileImage();
            endif;
            return parent::beforeValidate();
        }
        
        /* Converts data from table into human understandable data
         * return Array[]
         */
        
        public function afterFind() {
            $this->CreateDate = date('d-M-Y', strtotime($this->CreateDate));
            $this->Date_departure = date('d-M-Y', strtotime($this->Date_departure));
            
            if ($this->Status == self::STATUS_SAVED)
                $this->status_label = "default";
            else if ($this->Status == self::STATUS_APPROVED)
                $this->status_label = "success";
            else if ($this->Status == self::STATUS_REJECTED)
                $this->status_label = "danger";
            else if ($this->Status == self::STATUS_PRINTED)
                $this->status_label = "primary";
           else 
                $this->status_label = "default";
           
           
            if($this->Doc_template == 8 && !empty($this->Applicant_name)):
            $name = explode("&nbsp;", $this->Applicant_name);
            $this->surname = $name[0];
            $this->first_name = $name[1];
             if (!empty($this->birthdate)) {
            $this->birthdate_day = (int) Common::formatDate($this->date_of_birth, 'd');
            $this->birthdate_month = (int) Common::formatDate($this->date_of_birth, 'm');
            $this->birthdate_year = (int) Common::formatDate($this->date_of_birth, 'Y');
        }
            endif;
            
            return parent::afterFind();
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
        {

                return array(
                    array('Embassy_name,Reference, Type, Salutation, c, Passport_no, Destination, Purpose, Date_departure, Occupation' , 'required', 'on' => self::SCENARIO_VISA_REQUEST), 
                    array('Embassy_name,Reference, Salutation, Applicant_name, Passport_no, Destination,  Academic_year, Degree_type, School_name' , 'required', 'on' => self::SCENARIO_NO_OBJECTION), 
                    array('Embassy_name,Reference, letter_ref_no, letter_date, ministry_name' , 'required', 'on' => self::SCENARIO_FORWARD_NOTE), 
                    array('Reference' , 'required', 'on' => self::SCENARIO_IMMIGRATION), 
                    array('Content, Reference' , 'required', 'on' => self::SCENARIO_GENERAL), 
                    array('nationality, passport_photo_url, surname, first_name, date_of_birth, place_of_birth, sex, marital_status, fathers_name,mothers_name' , 'required', 'on' => self::SCENARIO_BIRTH_CERTIFICATE), 
                    array('Approver, PrintCount, Doc_template', 'numerical', 'integerOnly' => true),
                    //array('Reference', 'unique', 'message' => '{attribute} has been used for another document'),
                    array('Subject', 'length', 'max' => 100),
                    array('temp_profile_image', 'length', 'max' => 200),
                    array('Title', 'length', 'max' => 50),
                    array('birthdate_day,birthdate_month,birthdate_year,temp_profile_image', 'safe'),
                    array('DocID,Reference,Embassy_name,Type,temp_profile_image,Applicant_name,Passport_no,Destination,Purpose,Date_departure,Status'  . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                    );
        }
        /*
         * @return html 
         */
       
        public function processNotifTemplate($template, $item_id) {
            //placeholders supported: {{event_name}},{{date}}
            $row = Newdoc::model()->getRow('Applicant_name,Author, Doc_template, Reference', '`DocID`=:id', array(':id' => $item_id));
           // var_dump($row);die;
            if (!empty($row)) {
                  $author = UsersView::model()->get($row['Author'], 'name');
                  return CHtml::link(CHtml::encode(Common::myStringReplace($template, array(
                              '{{Applicant_name}}' => $row['Applicant_name'],
                              '{{Author}}' => $author,
                              '{{Reference}}' => $row['Reference'],
                  ))), Yii::app()->createUrl('manager/default/view' , ['id' => $item_id , 'template' => Doc::model()->get($row['Doc_template'] , 'doc_alias')]));
            }
            return $template;
      }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
        {
              return array(
                    'DocID' => Lang::t('ID'),
                    'Subject' => Lang::t('Subject'),
                    'Title' => Lang::t('Document title'),
                    'Embassy_name' => Lang::t('Embassy name'),
                    'Type' => Lang::t('Type of Visa'),
                    'Salutation' => Lang::t('Salutation'),
                    'Applicant_name' => Lang::t('Applicant name'),
                    'Passport_no' => Lang::t('Passport number'),
                    'Destination' => Lang::t('Location of Visit'),
                    'Purpose' => Lang::t('Purpose of Travel'),
                    'Date_departure' => Lang::t('Date of Entry/Departure'),
                    'Reference' => Lang::t('Reference'),
                    'Occupation' => Lang::t('Occupation / Company & Position'),
                    'Doc_template' => Lang::t('Document category'),
                    'CreateDate' => Lang::t('Create Date'),
                    'Academic_year' => Lang::t('Academic year'),
                    'Status' => Lang::t('Status'),
                    'Author' => Lang::t('Author'),
                    'Approver' => Lang::t('Approval'),
                    'PrintCount' => Lang::t('Print count'),
                    'letter_ref_no' => Lang::t('Letter Ref No'),
                    'letter_date' => Lang::t('Letter date'),
                    'ministry_name' => Lang::t('Ministry name'),
                    'School_name' => Lang::t('School name'),
                    'Degree_type' => Lang::t('Degree type'),
                    'Content' => Lang::t('Content of the letter'),
                    'Deleted' => Lang::t('Deleted'),
                    'sex' => Lang::t('Gender'),
                    'date_of_birth' => Lang::t('Date of Birth'),
                    'place_of_birth' => Lang::t('Place of birth'),
                    'marital_status' => Lang::t('Marital Status'),
                    'mothers_name' => Lang::t('Mother\'s aame'),
                    'fathers_name' => Lang::t('Father\'s name'),
                    'passport_photo_url' => Lang::t('Passport Photo'),
                    
                );
        }
        
        
        
         /**
         * Get the dir of a user
         * @param string $id
         */
        public function getDir($id = null)
        {
                if (empty($id))
                        $id = $this->DocID;
                return Common::createDir($this->getBaseDir());
        }

        public function getBaseDir()
        {
                return PUBLIC_DIR . DS . self::BASE_DIR;
        }

        /**
         * Gets user profile image path
         * @param type $id
         * @return string
         */
        public function getProfileImagePath($id = null)
        {
                if (!empty($this->DocID))
                        $id = $this->DocID;
                $passport_image = !empty($this->DocID) ? $this->passport_photo_url : $this->get($id, 'passport_photo_url');
                if (!empty($passport_image)) {
                        $file_path = Yii::app()->baseUrl . DS.  "public" . DS . Newdoc::BASE_DIR. DS .  $passport_image;
                        //var_dump(file_exists($file_path));die;
                         if (file_exists($file_path))
                                return $file_path;
                }
                return $this->getDefaultProfileImagePath();
        }

        protected function getDefaultProfileImagePath()
        {
                return  Yii::app()->theme->baseUrl . DS . 'images' . DS . 'default-profile-image.png';
        }

        public function setProfileImage()
        {
                //using fineuploader
           // var_dump($this->temp_profile_image);die;
                if (!empty($this->temp_profile_image)) {
                    
                        $temp_file = Common::parseFilePath($this->temp_profile_image);
                        $file_name = $temp_file['name'];
                        $temp_dir = $temp_file['dir'];
                        //var_dump( $this->getDir() . DS .$this->DocID . DS . $file_name);die;
                        if (@copy($this->temp_profile_image, $this->getDir() . DS . $file_name)) {
                                if (!empty($temp_dir))
                                        Common::deleteDir($temp_dir);
                                $this->passport_photo_url = $file_name;
                                
                                $this->temp_profile_image = null;
                        }
                }
        }


        public function searchParams()
        {
                return array(
                    'DocID',
                array('Reference', self::SEARCH_FIELD, true, 'OR'),
                array('Embassy_name', self::SEARCH_FIELD, true, 'OR'),
                array('Type', self::SEARCH_FIELD, true, 'OR'),
                array('Applicant_name', self::SEARCH_FIELD, true, 'OR'),
                array('Passport_no', self::SEARCH_FIELD, true, 'OR'),
                array('Destination', self::SEARCH_FIELD, true, 'OR'),
                array('Purpose', self::SEARCH_FIELD, true, 'OR'),
                array('Destination', self::SEARCH_FIELD, true, 'OR'),
                array('Date_departure', self::SEARCH_FIELD, true, 'OR'),
                array('Status', self::SEARCH_FIELD, true, 'OR'),
                
            );
      }
        

     /**
       * Get doc count
       * @param type $doc_type_id
       * @return int
       */
      public function getDocCount($doc_type = NUll) {
            $conditions = " deleted = :del";
            $params = array(':del' => 0);
            if (!empty($doc_type)) {
                  $conditions.=' AND `Doc_template`=:doc_template';
                  $params[':doc_template'] = $doc_type;
            }
            
            return $this->getTotals($conditions, $params);
      }
      
      public function getStatus()
      {
          return 
          [self::STATUS_APPROVED => self::STATUS_APPROVED,
          self::STATUS_PENDING => self::STATUS_PENDING,
          self::STATUS_PRINTED => self::STATUS_PRINTED,
          self::STATUS_SAVED => self::STATUS_SAVED];
      }

      /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('DocID', $this->DocID);
        $criteria->compare('Status', $this->Status);
        $criteria->compare('Reference', $this->Reference);
        $criteria->compare('Doc_template', $this->Doc_template);
        $criteria->compare('Author', $this->Author);
        $criteria->compare('Deleted', $this->Deleted);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

      
}

