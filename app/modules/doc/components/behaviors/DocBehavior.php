<?php

/**
 * System user behavior
 */
class DocBehavior extends CActiveRecordBehavior {

        

        private $_oldattributes = array();
 
    public function afterSave($event)
    {
        $newattributes = $this->Owner->getAttributes();
        $attributes = $this->Owner->isNewRecord ? $newattributes : $this->getOldAttributes();
        $refno = $this->Owner == 'Newdoc' ? $newattributes['Reference'] : "";
        if (!$this->Owner->isNewRecord) {
 
            // new attributes
            
            $oldattributes = $this->getOldAttributes();
            
            // compare old and new
            foreach ($newattributes as $name => $value) {
                if (!empty($oldattributes)) {
                    $old = $oldattributes[$name];
                } else {
                    $old = '';
                }
                if ($value != $old) {
                    
                    $log=new DocumentLogs;
                    $log->description =  $this->Owner == 'Newdoc' ? Yii::app()->user->full_name 
                                            . ' edited ' . $name . ' on document Ref no. ' .$refno . ' ' 
                                             . ' from  '. $old.' to '. $value . ' on '
                                            . date('Y-m-d h:i:s') :
                                             Yii::app()->user->full_name 
                                            . ' edited ' . $newattributes['name'] . ' on '. $oldattributes[$name]
                                             . ' from  '. $old.' to '. $value . ' at '
                                            . date('Y-m-d h:i:s') ;
                    ;
                    $log->action=       'UPDATE';
                    $log->model=        get_class($this->Owner);
                    $log->id_model=      $this->Owner->getPrimaryKey();
                    $log->field=        $name;
                    $log->action_date= new CDbExpression('NOW()');
                    $log->user_id=       Yii::app()->user->id;
                    $log->save();
                }
            }
        } else {
            $log=new DocumentLogs;
            $log->description=  $this->Owner == 'Newdoc' ? Yii::app()->user->full_name 
                                            . ' created document Ref no. ' .$refno . ' ' 
                                            . ' on '
                                            . date('Y-m-d h:i:s')  :
                                             Yii::app()->user->full_name 
                                            . ' created Embassy document - ' . $attributes['name'] . ' on '
                                             
                                            . date('Y-m-d h:i:s') ;
            $log->action=       'CREATE';
            $log->model=        get_class($this->Owner);
            $log->id_model=      $this->Owner->getPrimaryKey();
            $log->field=        '';
            $log->action_date= new CDbExpression('NOW()');
            $log->user_id=       Yii::app()->user->id;
            $log->save();
        }
    }
 
    public function afterDelete($event)
    {
        $attributes =  $this->getOldAttributes();
        $refno = $this->Owner == 'Newdoc' ? $attributes['Reference']: "";
        $log=new DocumentLogs;
        $log->description=  $this->Owner == 'Newdoc' ? Yii::app()->user->full_name 
                                            . ' deleted document Ref no. ' .$refno . ' on ' 
                                             . date('Y-m-d h:i:s')   :
                                             Yii::app()->user->full_name 
                                            . ' deleted Embassy document - ' . $attributes['name'] . ' on '
                                            . date('Y-m-d h:i:s') ;
        $log->action=       'DELETE';
        $log->model=        get_class($this->Owner);
        $log->id_model=      $this->Owner->getPrimaryKey();
        $log->field=        '';
        $log->action_date= new CDbExpression('NOW()');
        $log->user_id=       Yii::app()->user->id;
        $log->save();
    }
 
    public function afterFind($event)
    {
        // Save old values
        $this->setOldAttributes($this->Owner->getAttributes());
    }
 
    public function getOldAttributes()
    {
        return $this->_oldattributes;
    }
 
    public function setOldAttributes($value)
    {
        $this->_oldattributes=$value;
    }
}


