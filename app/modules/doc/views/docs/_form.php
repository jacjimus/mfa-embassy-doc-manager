<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        
       <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'doc_type_id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'doc_type_id', [1 => 'Template document' , 2 => 'Embassy upload document'], array('prompt' => 'Select',  'class' => 'form-control' , 'id' => 'doc_type_id')); ?>
                   
                </div>
        </div>
        
        

        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeTextArea($model, 'description', array('class' => 'form-control', 'maxlength' => 255, 'rows' => 3)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'approval', array('class' => 'col-md-3 control-label' )); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'approval', array()); ?>
                </div>
        </div>
        <div class="form-group <?=!$model->isNewRecord && $model->approval ? "" : "hide"?> " id="approver-field">
                <?php echo CHtml::activeLabelEx($model, 'person_to_approve', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                    <?php Yii::import('ext.chosen.Chosen'); 
                    $datas = UsersView::model()->findAll();
                    $sigs = array();
                    
                    foreach ($datas as $ds)
                        $sigs[$ds->id] = $ds->name;
                  echo CHtml::activeDropDownList($model, 'person_to_approve', UsersView::model()->getListData('id', 'name'), array('class' => 'form-control select2')); ?>
                </div>
                <div class="col-md-12">
                    <?php echo CHtml::activeLabelEx($model, 'notify_approver', array('class' => 'col-md-3 control-label')); ?>
                    <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'notify_approver', array()); ?>
                    </div>
                    <div class="col-md-6 <?=!$model->isNewRecord && $model->notify_approver ? "" : "hide"?>" id="notify-temp">
                        <?php echo CHtml::activeDropDownList($model, 'notify_type_id', NotifTypes::model()->getListData('id' , 'name'), array('prompt' => 'Select Notification',  'class' => 'form-control select2')); ?>
                   
                    </div>
                </div>
                
        </div>
        <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'confidential', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo CHtml::activeCheckBox($model, 'confidential', array()); ?>
                </div>
        </div>
        <?php $this->renderPartial('_file_field', array('model' => $model)); ?>
</div>

<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); 

Yii::app()->clientScript->registerScript('doctype', '
    $(\'#Doc_approval\').click(function() {
     if($(this).is(":checked"))
        $(\'#approver-field\').removeClass("hide");
    else
    $(\'#approver-field\').addClass("hide");
    });
    $(\'#Doc_notify_approver\').click(function() {
     if($(this).is(":checked"))
        $(\'#notify-temp\').removeClass("hide");
    else
    $(\'#notify-temp\').addClass("hide");
    });
');