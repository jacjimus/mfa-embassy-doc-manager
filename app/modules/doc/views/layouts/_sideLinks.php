<?php if (ModulesEnabled::model()->isModuleEnabled(DocModuleConstants::MOD_COMPANY_DOCS)): ?>
      <?php if ($this->showLink(DocModuleConstants::RES_COMPANY_DOCS)): ?>
            <li class="<?php echo $this->activeMenu === DocModuleConstants::MENU_DOC ? 'active' : '' ?>">
                  <a href="<?php echo Yii::app()->createUrl('doc/docs/index') ?>"><i class="fa fa-lg fa-fw fa-folder-open-o"></i> <span class="menu-item-parent"><?php echo Lang::t('Document Category') ?></span></a>
            </li>
      <?php endif; ?>
<?php endif; ?>