/*
 * Custom scripts
 */
var MyGridView = {
    optionsDefault: {
        grid_id: null,
        showDateRange: false,
        dateRangeId: undefined
    },
    options: {},
    init: function(options) {
        'use strict';
        var $this = this;
        $this.options = $.extend({}, $this.optionsDefault, options || {});
        $this.refreshGrid();
        $('#' + $this.options.grid_id).find('.show_modal_form').attr('data-grid-id', $this.options.grid_id);
        $this.printGrid();
        $this.initDaterangePicker();
        $this.enableLinkableRow();
    },
    refreshGrid: function() {
        var selector = '.refresh-grid'
                , refresh_grid = function(e) {
                    MyUtils.GridView.updateGridView($(e).data('grid-id'));
                };
        $(selector).on('click', function(event) {
            refresh_grid(this);
            event.preventDefault();
        });
    },
    printGrid: function() {
        var selector = 'a.print-mygrid'
                , print_grid = function(e) {
                    var grid = $(e).data('grid-id');
                    MyUtils.print_div('#' + grid);
                };
        $(selector).on('click', function(e) {
            e.preventDefault();
            print_grid(this);
        });
    },
    enableLinkableRow: function() {
        var selector = '.grid-view2 tr.linkable';
        $('body').on('click', selector, function() {
            var url = $(this).data('href');
            if (!MyUtils.empty(url)) {
                MyUtils.reload(url);
            }
        });
    },
    initDaterangePicker: function() {
        var $this = this;
        if ($this.options.dateRangeId) {
            $('#' + $this.options.dateRangeId).daterangepicker({
                format: 'YYYY/MM/DD',
            });
        }

    }
}


