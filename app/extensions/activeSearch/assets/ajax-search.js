/**
 * Ajax search for cgridview search
 * @type type
 */
var ExtAjaxSearch = {
    options: {
        form_id: undefined,
        grid_id: undefined,
        grid_type: 'cgridview',
        search_trigger: 'keypress',
    },
    init: function(options) {
        'use strict';
        var $this = this
                , settings
                , searchForm;
        settings = $.extend({}, $this.options, options || {});
        searchForm = $('#' + settings.form_id);


        //key press trigger
        if (settings.search_trigger === 'keypress') {
            searchForm.find('input[type="text"]').on('keyup', function() {
                if (!MyUtils.empty($(this).data('timer')))
                    clearTimeout($.data(this, 'timer'));
                var wait = setTimeout(function() {
                    searchForm.trigger('submit');
                }, 500);
                $(this).data('timer', wait);
            });
        }

        //blur trigger
        searchForm.find('input[type="text"]').on('blur', function() {
            searchForm.trigger('submit');
        });

        if (settings.grid_type === 'cgridview') {
            searchForm.submit(function() {
                $.fn.yiiGridView.update(settings.grid_id, {
                    data: $(this).serialize()
                });
                return false;
            });
        }
        else {
            searchForm.submit(function() {
                $.fn.yiiListView.update(settings.grid_id, {
                    data: $(this).serialize()
                });
                return false;
            });
        }

    }
};

